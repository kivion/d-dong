package com.dmall.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dmall.dao.MenueDao;
import com.dmall.entity.Menue;
import com.dmall.entity.MenueCollection;
import com.dmall.pagedata.PageData;
import com.dmall.service.MenuCollectionService;
import com.dmall.service.MenuService;
import com.dmall.util.AjaxResponse;
import com.google.gson.Gson;


/**
 * Servlet implementation class MenuHomeServlet
 */
@WebServlet("/menuhome.do")
public class MenuHomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        MenuService ms=new  MenuService();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuHomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String op="";
		if(request.getParameter("op")!=null) {
			op=request.getParameter("op");
			System.out.println("op"+op);
		}
		if("homesearch".equals(op)) {
			doHomeSearch(request, response);
		}else if("menudetail".equals(op)){
			doMenuDetail(request, response);
		}else if("menucollection".equals(op)){
			doMenuCollection(request, response);
		}else if("addmenucoll".equals(op)){
			doaddMenuColl(request, response);
		}else if("addmenu".equals(op)){
			doaddMenu(request, response);
		}else if("menupersonal".equals(op)) {
			doMenuPersonal(request, response);
		}
		
}
    /**
     * 菜谱主页-前台界面模糊搜索
     * @param request 
     * @param response 
     * @throws ServletException
     * @throws IOException
     */
	protected void doHomeSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String keywords="";
		if(request.getParameter("keywords")!=null) {
			keywords=request.getParameter("keywords");
		}
		System.out.println("keywords"+keywords);
		List<Menue> list = ms.getMenulist(keywords);
		list.forEach(System.out::print);
		request.setAttribute("list",list);
		request.getRequestDispatcher("./front/Menu_All.jsp").forward(request, response);
	}
	/**
	 * 通过登入的用户编号展示个人菜谱的
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doMenuPersonal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String cid="c000000001";
		if(request.getParameter("cid")!=null) {
			cid=request.getParameter("cid");
		}
		List<Menue> list = ms.getMenuPersonbycid(cid);
		list.forEach(System.out::print);
		request.setAttribute("list",list);
		request.getRequestDispatcher("./front/Menu_PersonalAll.jsp").forward(request, response);
	}
	/**
	 * 通过菜谱编号查询菜谱详情
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doMenuDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String menuid="";
		if(request.getParameter("menuid")!=null) {
			menuid=request.getParameter("menuid");
		}
		System.out.println("menuid"+menuid);
		List<Menue> list = ms.getMenubyid(menuid);
		list.forEach(System.out::print);
		request.setAttribute("list",list);
		request.getRequestDispatcher("./front/Menu_detail.jsp").forward(request, response);
	}
	/**
	 * 通过登入的用户编号展示收藏菜谱的
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doMenuCollection(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String menukeywords="";
		String cid="";
		if(request.getParameter("menukeywords")!=null) {
			menukeywords=request.getParameter("menukeywords");
		}
		if(request.getParameter("cid")!=null) {
			cid=request.getParameter("cid");
		}
		System.out.println("menukeywords"+menukeywords);
		MenuCollectionService mcs=new MenuCollectionService();
		List<Menue> list = mcs.getMenuCollectionlist(cid, menukeywords);
		list.forEach(System.out::print);
		request.setAttribute("list",list);
		request.getRequestDispatcher("./front/Meun_collection.jsp").forward(request, response);
	}
	/**
	 * 添加收藏
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doaddMenuColl(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String menuid="";
		String cid="";
		if(request.getParameter("menuid")!=null) {
			menuid=request.getParameter("menuid");
		}
		if(request.getParameter("cid")!=null) {
			cid=request.getParameter("cid");
		}
		System.out.println("menuid"+menuid);
		MenuCollectionService mcs=new MenuCollectionService();
		MenueCollection mc=new MenueCollection(menuid,cid);
		boolean flag=mcs.addMenueCollection(mc);
		if(flag) {
			System.out.println("成功");
			
		}else {
			System.out.println("失败");
		}
		
	}
	/**
	 * 添加菜谱
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doaddMenu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cid="c000000001";
		String title="";
		String used="";
		String difficult="";
		String time="";
		String content="";
		String mid="m"+(int)((Math.random()*10000));
		if(request.getParameter("cid")!=null) {
			cid=request.getParameter("cid");
		}
		if(request.getParameter("title")!=null) {
			cid=request.getParameter("title");
		}
		if(request.getParameter("used")!=null) {
			cid=request.getParameter("used");
		}
		if(request.getParameter("difficult")!=null) {
			cid=request.getParameter("difficult");
		}
		if(request.getParameter("time")!=null) {
			cid=request.getParameter("time");
		}
		if(request.getParameter("content")!=null) {
			cid=request.getParameter("content");
		}
		Menue menu=new Menue(mid,title,used,content,null,difficult,time,cid);
		boolean flag=ms.addMenue(menu);
		if(flag) {
			System.out.println("成功");
			request.getRequestDispatcher("./front/Menu_release.jsp").forward(request, response);
		}else {
			System.out.println("失败");
			request.getRequestDispatcher("./front/Menu_release.jsp").forward(request, response);
		}
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
