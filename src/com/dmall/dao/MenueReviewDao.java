package com.dmall.dao;

import com.dmall.entity.Menue;
import com.dmall.entity.MenueReview;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

public class MenueReviewDao extends DBUtil{
	/**
	 *	分页+模糊查询菜谱评论
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<MenueReview> getMenuReview(String keyword,int pageNo,int pageSize){
		
		String sql = "select mr_sn,c_photo, c_name,m_name,mr_time,mr_content, from `mmenu_reviews` join `menu` on mr_content.m_id=menu.m_id join `customer` on `customer.c_id=mr_content.c_id` where m_name like ? or m_content like ?";
		PageData pd = exeQueryByPage(sql, Menue.class, pageNo, pageSize,"%"+keyword+"%","%"+keyword+"%","%"+keyword+"%");

		return pd;
	}
	/**
	 *	添加菜谱评论
	 * @param Menue
	 * @return MenuReview 对象
	 */
	public boolean addMenueReview(MenueReview menureview) {
		String sql = "INSERT INTO `menue_reviews` (`c_id`, `m_id`,`mr_time`,`mr_content`) VALUES (?,?,?,?)";
		boolean flag = exeUpdate(sql, menureview.getC_id(),menureview.getM_id(),menureview.getMr_content());
		return flag;
	}
	/**
	 *	按(评论者)客户id删除菜谱评论
	 * @param c_id
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean delMenueById(int c_id) {
		String sql = "delete FROM `menue_reviews` WHERE (`c_id`=?)";
		boolean flag = exeUpdate(sql, c_id);
		return flag;
	}
	/**
	 *	根据菜谱id修改菜谱评论
	 * @param menureview
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean updMenueById(MenueReview menureview) {
		String sql = "UPDATE `menue` SET `c_id`=?, `m_id`=?,`mr_time`=?,`mr_content`=? WHERE (`m_id`=?)";
		System.out.println(menureview);
		boolean flag = exeUpdate(sql, menureview.getC_id(),menureview.getM_id(),menureview.getMr_time(),menureview.getMr_content());
		return flag;
	}
	
}
