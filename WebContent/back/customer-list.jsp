<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<c:set var="basepath" scope="page"
	value="${pageContext.request.contextPath}"></c:set>
<meta charset="UTF-8">
<title>欢迎页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<script src="./lib/layui/layui.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>

</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="">首页</a> <a href="">演示</a>
			<a> <cite>导航元素</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
						<form class="layui-form layui-col-space5">
							<div class="layui-inline layui-show-xs-block">
								<input class="layui-input" autocomplete="off" placeholder="开始日"
									name="start" id="start">
							</div>
							<div class="layui-inline layui-show-xs-block">
								<input class="layui-input" autocomplete="off" placeholder="截止日"
									name="end" id="end">
							</div>
							<div class="layui-inline layui-show-xs-block">
								<input type="text" name="keywords" placeholder="请输入搜索关键字"
									autocomplete="off" class="layui-input">
							</div>
							<div class="layui-inline layui-show-xs-block">
								<button class="layui-btn" lay-submit="" lay-filter="search">
									<i class="layui-icon">&#xe615;</i>
								</button>
							</div>
						</form>
					</div>
					<div class="layui-card-body ">
						<table class="layui-table" lay-data="{url:'../customer.do',page:true,toolbar: '#toolbarDemo',id:'test'}"
							lay-filter="test">
							<thead>
								<tr>
									<th lay-data="{type:'checkbox'}"></th>
									<th lay-data="{field:'c_id', width:120, sort: true}">ID</th>
									<th lay-data="{field:'c_name', width:90, sort: true, edit: 'text'}">用户名</th>
									<th lay-data="{field:'c_sex', edit: 'text',width:55}">性别</th>
									<th lay-data="{field:'c_tell', width:120}">手机</th>
									<th lay-data="{field:'address', width:80,templet: function(d){ return d.address.a_name;}}">地址</th>
									<th lay-data="{field:'c_photo', width:60,templet:'<div><img src="E:/Program Files/apache-tomcat-9/webapps/dmall/picture/{{ d.ban_img }}"></div>'}">头像</th>
									<th lay-data="{field:'c_account', edit: 'text', Width: 100}">账号</th>
									<th lay-data="{field:'c_score', sort: true, edit: 'text',Width: 45}">积分</th>
									<th lay-data="{field:'c_state', width:80, sort: true, edit: 'text',templet:'#stateTpl'}">状态</th>
									<th lay-data="{field:'', width:100,templet: '#opTpl'}">操作</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" id="opTpl">
	
	<a href="javascript:;" title="启用"> 
		<i class="layui-icon"> &#xe601;</i>
	</a> 
	<a title="编辑" href="javascript:;"> 
		<i class="layui-icon">&#xe642;</i>
	<a title="删除" href="javascript:;" lay-event="del"> 
		<i class="layui-icon">&#xe640;</i>
	</a>
</script>
<script type="text/html" id="toolbarDemo">
        <div class = "layui-btn-container" > 
            <button class = "layui-btn layui-btn-sm" lay-event = "getCheckData" > 获取选中行数据 </button>
            <button class="layui-btn layui-btn-sm" lay-event="getCheckLength">获取选中数目</button > 
            <button class = "layui-btn layui-btn-sm" lay-event = "isAll" > 验证是否全选</button>
        </div > 
    </script>
<script type="text/html" id="stateTpl">
    {{#  if(d.c_state ==1){ }}
        	正常
    {{#  }  else  { }}
       		禁用
    {{#  } }}
</script>

<script>
    layui.use('laydate',function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });

        });
    </script>
<script type="text/javascript" id="toolbarDemo">
	layui.use(['table','form'],function(){
        var table = layui.table;
		var form = layui.form;
		$ = layui.jquery;
		//监听工具条事件，删除表格中的一行数据
		table.on('tool(test)',function(obj){
			var data = obj.data;//获取当前行的数据
			var layEvent = obj.event;//获取lay-event对应的值
			var tr = obj.tr;//获取当前行tr的DOM对象
			if(layEvent == 'del'){
				layer.confirm('真的要删除该客户吗？',function(index){
					layer.closeAll('dialog');
					$.ajax({
						url:'../customer.do?op=del',
						type:'post',
						dataType:'json',
						data:{'c_id':data.c_id},//向服务器端发送要删除的id
						success:function(res){
							if(res.code==0){
								console.log("res.msg:"+res.msg);
								table.reload('test', {
									url : '../customer.do', 
									where : data.field//设定异步数据接口的额外参数
								});						
								layer.msg(res.msg,{icon:6});
							}
						},
						error:function(res){
							console.log(res);
							console.log("res:"+res.msg);
							layer.msg('删除失败！',{icon:5})
						}
					});
				});
			}
		});
	})
</script>
<script>
    layui.use(['table','form'],function() {
            var table = layui.table;
			var form = layui.form;
			//商品搜索按钮，data是搜索结果的数据
			form.on('submit(search)',function(data){
				console.log(data.field)
				table.reload('test', {
					url : '../customer.do', 
					where : data.field//设定异步数据接口的额外参数
				});
				return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可
			});
			
			
            //监听单元格编辑
            table.on('edit(test)',
            function(obj) {
                var value = obj.value //得到修改后的值
                ,
                data = obj.data //得到所在行所有键值
                ,
                field = obj.field; //得到字段
                layer.msg('[ID: ' + data.id + '] ' + field + ' 字段更改为：' + value);
            });
        });</script>
<script>
    var _hmt = _hmt || []; (function() {
     var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();</script>

</html>