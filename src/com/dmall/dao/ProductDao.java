package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Product;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

/**
 * 商品-Dao
 * 
 * @author ccp 蔡陈鹏
 *
 */
public class ProductDao extends DBUtil {
	/**
	 * 模糊查询-关键字
	 * 
	 * @param keywords 关键字
	 * @return list 集合
	 */
	public List<Product> getProduct(String keywords) {

		String sql = "SELECT product.p_id, product.p_name, product.p_weight, product.p_stock, product.p_origin, product.p_save, product.p_detaill, product.p_pro, product.p_launch, product.p_guarantee, product.p_appraise, product.p_photo, product.ptn_id, product.p_price, product.p_sn FROM product WHERE product.p_name LIKE ? OR product.p_detaill LIKE ?";

		List<Product> list = exeQuery(sql, Product.class, "%" + keywords + "%", "%" + keywords + "%");
		return list;
	}

	/**
	 * 模糊查询 + 分页
	 * 
	 * @author ccp 蔡陈鹏
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData分页对象
	 */
	public PageData<Product> getProductBypage(String keywords, int pageNo, int pageSize) {

		String sql = "SELECT product.p_id, product.p_name, product.p_weight, product.p_stock, product.p_origin, product.p_save, product.p_detaill, product.p_pro, product.p_launch, product.p_guarantee, product.p_appraise, product.p_photo, product.ptn_id, product.p_price, product.p_sn FROM product WHERE product.p_name LIKE ? OR product.p_detaill LIKE ?";

		PageData<Product> pd = exeQueryByPage(sql, Product.class, pageNo, pageSize, "%" + keywords + "%",
				"%" + keywords + "%");
		return pd;
	}

	/**
	 * 查询商品（按商品编号）
	 * 
	 * @param p_id 商品编号
	 * @return
	 */
	public Product getProductByProductId(String p_id) {
		String sql = "SELECT * FROM product WHERE product.p_id = ?";

		List<Product> list = exeQuery(sql, Product.class, p_id);
		System.out.println(list);
		if (list != null) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 查询商品（按大分类查询）-用于主页进入时自动加载到各部分
	 * @param ptf_id 商品大分类编号
	 * @return
	 */
	public List<Product> getProductByShopListPart(String ptf_id) {
		String sql = "SELECT product.p_name,product.p_price,product_type_next.ptn_name,product_type_first.ptf_name FROM product INNER JOIN product_type_next ON product.ptn_id = product_type_next.ptn_id INNER JOIN product_type_first ON product_type_next.ptf_id = product_type_first.ptf_id WHERE product_type_first.ptf_id = ?";

		List<Product> list = exeQuery(sql, Product.class, ptf_id);
		return list;
	}

	/**
	 * 商品列表的搜索，按小类型或者商品名
	 * 
	 * @param keywords
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<Product> getProductByShopListPage(String keywords) {
//		 product.p_name,product_type_first.ptf_name,product.p_price,product.p_detaill,product.p_status
		String sql = "SELECT * FROM product ,product_type_first INNER JOIN product_type_next ON product.ptn_id = product_type_next.ptn_id AND product_type_first.ptf_id = product_type_next.ptf_id"
				+ " FROM product WHERE product.p_name LIKE ? OR product_type_next.ptn_name LIKE ?";

		List<Product> list = exeQuery(sql, Product.class, "%" + keywords + "%", "%" + keywords + "%");
		return list;
	}

	/**
	 * 增加商品
	 * 
	 * @author ccp
	 * @param product 商品对象
	 * @return flag 布尔类型，true操作成功，false操作失败
	 */
	public boolean addProduct(Product product) {
		// sql语句
		String sql = "INSERT INTO `product` (`p_id`, `p_name`, `p_weight`, `p_stock`, `p_origin`, `p_save`,`p_photo`, `p_detaill`, `p_pro`, `p_launch`, `p_guarantee`, `p_appraise`, `ptn_id`, `p_price`,`p_status`) VALUES (?,?,?,?,?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
		// 调用增加方法
		boolean flag = exeUpdate(sql, product.getP_id(), product.getP_name(), product.getP_weight(),
				product.getP_stock(), product.getP_origin(), product.getP_save(), product.getP_photo(),
				product.getP_detaill(), product.getP_pro(), product.getP_launch(), product.getP_guarantee(),
				product.getP_appraise(), product.getPtn_id(), product.getP_price(), product.getP_status());
		return flag;
	}

	/**
	 * 按商品id删除商品
	 * 
	 * @author ccp
	 * @param p_id 商品id
	 * @return flag 布尔类型，true操作成功，false操作失败
	 */
	public boolean delProductById(String p_id) {
		String sql = "delete FROM `product` WHERE (`p_id`=?)";
		boolean flag = exeUpdate(sql, p_id);
		return flag;
	}

	/**
	 * 按商品id修改商品
	 * 
	 * @author ccp
	 * @param product 商品对象
	 * @return flag true操作成功，false操作失败
	 */
	public boolean updProductById(Product product) {
		String sql = "UPDATE `product` SET `p_name`=?, `p_weight`=?, `p_stock`=?,`p_origin` = ?,`p_save`=?,`p_detaill`=?,`p_pro`=?,`p_launch`=?,`p_guarantee`=?,`p_photo`=?,`ptn_id`=?,`p_price`=? WHERE (`p_id`=?)";
		boolean flag = exeUpdate(sql, product.getP_name(), product.getP_weight(), product.getP_stock(),
				product.getP_origin(), product.getP_save(), product.getP_detaill(), product.getP_pro(),
				product.getP_launch(), product.getP_guarantee(), product.getP_photo(), product.getPtn_id(),
				product.getP_price(), product.getP_id());

		return flag;
	}

}
