package com.dmall.service;

import java.util.List;

import com.dmall.dao.ProductTypeNextDao;
import com.dmall.entity.MenueTypeNext;
import com.dmall.entity.ProductTypeNext;
import com.dmall.pagedata.PageData;

/**
 * 商品小分类-业务层
 * 
 * @author 蔡陈鹏
 */
public class ProductTypeNextService {
	ProductTypeNextDao ptfd = new ProductTypeNextDao();

	/**
	 * 模糊查询
	 * 
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData 分页对象
	 */
	public List<ProductTypeNext> getProductTypeNext(String keyword) {
		return ptfd.getProductTypeNext(keyword);
	}

	/**
	 * 分页+模糊查询
	 * 
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData 分页对象
	 */
	public PageData<ProductTypeNext> getProductTypeNext(String keyword, int pageNo, int pageSize) {

		if (pageNo < 1) {
			pageNo = 1;
		}
		return ptfd.getProductTypeNext(keyword, pageNo, pageSize);
	}
	/**
	 *	根据类型名查询类型编号
	 * @param typename
	 * @return
	 */
	public String getPtn_id(String typename) {
		if(!"".equals(typename)&&typename!=null) {
			return  ptfd.getPtn_id(typename);
		}
		return "";		
	}
	
	/**
	 * 添加商品小分类
	 * 
	 * @param ProductTypeNext 商品小分类对象
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean addProductTypeNext(ProductTypeNext productTypeNext) {

		return ptfd.addProductTypeNext(productTypeNext);
	}

	/**
	 * 按商品小分类id删除商品小分类内容
	 * 
	 * @param ptn_id 商品小分类id
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean delProductTypeNext(String ptn_id) {
		return ptfd.delProductTypeNextById(ptn_id);

	}

	/**
	 * 按商品小分类id修改商品小分类
	 * 
	 * @param ProductTypeNext 商品小分类对象
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean updProductTypeNext(ProductTypeNext producttypeNext) {
		return ptfd.updProductTypeNextById(producttypeNext);

	}
}