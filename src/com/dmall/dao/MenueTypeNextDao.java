package com.dmall.dao;

import java.util.List;

import com.dmall.entity.MenueTypeNext;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

public class MenueTypeNextDao extends DBUtil{
	/**
	 *	查询某个一级菜谱分类下的所有二级菜谱分类信息，针对后台
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return PagData分页对象
	 */
	public PageData<MenueTypeNext> getMenuTypeFrst(String keyword,int pageNo,int pageSize){
		String sql = "select mtn_name from menu_type_next where mtf_id=(select mtf_id from menu_type_first where mtf_id='mtf01');";
		PageData pd = exeQueryByPage(sql, MenueTypeNext.class, pageNo, pageSize);
		return pd;
	}
	/**
	 *	查询某个一级菜谱分类下的所有二级菜谱分类信息，针对前台
	 * @param keyword
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<MenueTypeNext> getMenuTypeFrst(){
		String sql = "select mtn_name from menu_type_next where mtf_id=(select mtf_id from menu_type_first where mtf_id='mtf01');";
		List<MenueTypeNext> list = exeQuery(sql, MenueTypeNext.class);
		return list;
	}

	/**
	 *	增加二级菜谱分类
	 * @param mtf
	 * @return
	 */
	public boolean addMenuTypeNext(MenueTypeNext mtn) {
		String sql = "insert into menu_type_next(mtn_id,mtn_name,mtf_id) VALUE(?,?,?)";
		boolean flag = exeUpdate(sql, mtn.getMtn_id(),mtn.getMtn_name(),mtn.getMtf_id());
		return flag;
	}
	/**
	 *	按二级菜谱分类的id删除
	 * @param mtf_id
	 * @return
	 */
	public boolean delMenuTypeNext(String mtn_id) {
		String sql = "delete from menu_type_next where mtf_id=?";
		boolean flag = exeUpdate(sql, mtn_id);
		return flag;
	}
	/**
	 *	根据二级菜谱分类的id修改分类学信息
	 * @param mtf_id
	 * @param mtf_name
	 * @return
	 */
	public boolean updMenuTypeNext(String mtn_id,String mtn_name) {
		String sql = "update menu_type_Next set mtf_name=? where mtf_id=?";
		boolean flag = exeUpdate(sql,mtn_name, mtn_id);
		return flag;
	}
}
