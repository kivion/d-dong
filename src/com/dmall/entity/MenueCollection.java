package com.dmall.entity;

public class MenueCollection {
	private int mc_sn;
	private String m_id;
	private String c_id;
	
	//关连
	//菜谱
	
	//用户
	
	public MenueCollection(int mc_sn, String m_id, String c_id) {
		super();
		this.mc_sn = mc_sn;
		this.m_id = m_id;
		this.c_id = c_id;
	}
	
	public MenueCollection(String m_id, String c_id) {
		super();
		this.m_id = m_id;
		this.c_id = c_id;
	}

	public MenueCollection() {
	}

	public int getMc_sn() {
		return mc_sn;
	}

	public void setMc_sn(int mc_sn) {
		this.mc_sn = mc_sn;
	}

	public String getM_id() {
		return m_id;
	}

	public void setM_id(String m_id) {
		this.m_id = m_id;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	@Override
	public String toString() {
		return "MenueCollection [mc_sn=" + mc_sn + ", m_id=" + m_id + ", c_id=" + c_id + "]";
	}
	
}
