package com.dmall.service;

import java.util.List;

import com.dmall.dao.GradeDao;
import com.dmall.entity.Grade;

/**
 * 等级的业务层
 * @author Administrator
 *
 */
public class GradeService {

	private GradeDao gd=new GradeDao();
	/**
	 * 模糊查询--
	 * @param keywords 关键字
	 * @return 对象
	 */
	public List<Grade> getGradeByKey(String keywords){
		return gd.getGradeByKeywords(keywords);
	}
	/**
	 * 通过骑手编号查询等级
	 * @param keywords 关键字
	 * @return 对象
	 */
	public List<Grade> getGradeBygid(String rid){
		return gd.getGradeByrid(rid);
	}
}
