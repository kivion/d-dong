<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 设置taglib 格式 -- >
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html class="no-js" lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>叮咚到家-你想要的生鲜好菜，说到就到</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<c:set var="basepath" scope="page"
	value="${pageContext.request.contextPath}"></c:set>

<!-- CSS 
    ========================= -->
<!--bootstrap min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/bootstrap.min.css">
<!--owl carousel min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/owl.carousel.min.css">
<!--slick min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/slick.css">
<!--magnific popup min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/magnific-popup.css">
<!--font awesome css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/font.awesome.css">
<!--ionicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/ionicons.min.css">
<!--linearicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/linearicons.css">
<!--animate css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/animate.css">
<!--jquery ui min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/jquery-ui.min.css">
<!--slinky menu css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/slinky.menu.css">
<!--plugins css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/plugins.css">
<!--layui-->
<script type="text/javascript"
	src="${basepath}/front/lib/layui/layui.js" charset="utf-8"></script>
<link rel="stylesheet" href="${basepath}/front/lib/xadmin.css">

<!-- Main Style CSS -->
<link rel="stylesheet" href="${basepath}/front/assets/css/style.css">

<!--modernizr min js here-->
<script src="${basepath}/front/assets/js/vendor/modernizr-3.7.1.min.js"></script>


</head>

<body>

	<!--header area start-->

	<header>
		<div class="main_header">
			<div class="header_top">
				<div class="container">
					<div class="row align-items-center">

						<!--导航栏-->
						<div class="col-lg-6 col-md-6">
							<div class="language_currency">
								<ul>
									<li class="language"><a href="#"> 语言 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_language">
											<li><a href="#">中文</a></li>
											<li><a href="#">英语</a></li>
										</ul></li>
									<!--<li class="currency"><a href="#"> Currency <i class="icon-right ion-ios-arrow-down"></i></a>
                                  <ul class="dropdown_currency">
                                      <li><a href="#">€ Euro</a></li>
                                      <li><a href="#">£ Pound Sterling</a></li>
                                      <li><a href="#">$ US Dollar</a></li>
                                  </ul>
                              </li>-->
								</ul>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="header_social text-right">
								<ul>
									<li><a href="#"><i class="ion-social-twitter"></i></a></li>
									<li><a href="#"><i
											class="ion-social-googleplus-outline"></i></a></li>
									<li><a href="#"><i class="ion-social-youtube-outline"></i></a>
									</li>
									<li><a href="#"><i class="ion-social-facebook"></i></a></li>
									<li><a href="#"><i
											class="ion-social-instagram-outline"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_middle">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-2">
							<div class="logo">
								<a href="index.html"><img src="assets/img/logo/logo.jpg"
									alt=""></a>
							</div>
						</div>
						<div class="col-lg-10">
							<div class="header_right_info">
								<!--导航栏左边的类别-->
								<div class="search_container">
									<form action="#">
										<div class="hover_category">
											<select class="select_option" name="select" id="categori1">
												<option selected value="1">选择类别</option>
												<option value="2">水果</option>
												<option value="3">蔬菜</option>
												<option value="4">肉禽蛋品</option>
												<option value="5">海鲜水产</option>
												<option value="6">熟食卤味</option>
												<option value="7">粮油调味</option>
												<option value="8">休闲零食</option>
												<option value="9">冰品面点</option>
												<option value="10">牛奶面包</option>
												<option value="11">酒水冲饮</option>
											</select>
										</div>
										<div class="search_box">
											<input placeholder="国产红心火龙果0.99元/个..." type="text">
											<button type="submit">
												<span class="lnr lnr-magnifier"></span>
											</button>
										</div>
									</form>
								</div>
								<div class="header_account_area">
									<div class="header_account_list register">
										<ul>
											<li><a href="login.html">注册</a></li>
											<li><span>/</span></li>
											<li><a href="login.html">登录</a></li>
										</ul>
									</div>
									<div class="header_account_list header_wishlist">
										<a href="wishlist.html"><span class="lnr lnr-heart"></span>
											<span class="item_count">5</span> </a>
									</div>
									<div class="header_account_list  mini_cart_wrapper">
										<a href="javascript:void(0)"><span class="lnr lnr-cart"></span><span
											class="item_count">2</span></a>
										<!--mini cart-->
										<div class="mini_cart">
											<div class="cart_gallery">
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Primis In Faucibus</a>
														<p>
															1 x <span> $65.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product2.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Letraset Sheets</a>
														<p>
															1 x <span> $60.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
											</div>
											<div class="mini_cart_table">
												<div class="cart_table_border">
													<div class="cart_total">
														<span>Sub total:</span> <span class="price">$125.00</span>
													</div>
													<div class="cart_total mt-10">
														<span>total:</span> <span class="price">$125.00</span>
													</div>
												</div>
											</div>
											<div class="mini_cart_footer">
												<div class="cart_button">
													<a href="cart.html"><i class="fa fa-shopping-cart"></i>
														View cart</a>
												</div>
												<div class="cart_button">
													<a href="checkout.html"><i class="fa fa-sign-in"></i>
														Checkout</a>
												</div>

											</div>
										</div>
										<!--mini cart end-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_bottom sticky-header">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-3">
							<div class="categories_menu">
								<div class="categories_title">
									<h2 class="categori_toggle">所有商品</h2>
								</div>
								<div class="categories_menu_toggle">
									<ul>
										<li class="menu_item_children"><a href="#part1">蔬菜<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu">
												<li class="menu_item_children"><a href="#">Dresses</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Sweater</a></li>
														<li><a href="">Evening</a></li>
														<li><a href="">Day</a></li>
														<li><a href="">Sports</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Handbags</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Shoulder</a></li>
														<li><a href="">Satchels</a></li>
														<li><a href="">kids</a></li>
														<li><a href="">coats</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">shoes</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Ankle Boots</a></li>
														<li><a href="">Clog sandals </a></li>
														<li><a href="">run</a></li>
														<li><a href="">Books</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Clothing</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Coats Jackets </a></li>
														<li><a href="">Raincoats</a></li>
														<li><a href="">Jackets</a></li>
														<li><a href="">T-shirts</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part1">水果<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_3">
												<li class="menu_item_children"><a href="#">Chair</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Dining room</a></li>
														<li><a href="">bedroom</a></li>
														<li><a href=""> Home & Office</a></li>
														<li><a href="">living room</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Lighting</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Ceiling Lighting</a></li>
														<li><a href="">Wall Lighting</a></li>
														<li><a href="">Outdoor Lighting</a></li>
														<li><a href="">Smart Lighting</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Sofa</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Fabric Sofas</a></li>
														<li><a href="">Leather Sofas</a></li>
														<li><a href="">Corner Sofas</a></li>
														<li><a href="">Sofa Beds</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part1">肉禽蛋品<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">Brake
														Tools</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Driveshafts</a></li>
														<li><a href="">Spools</a></li>
														<li><a href="">Diesel </a></li>
														<li><a href="">Gasoline</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Emergency
														Brake</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Dolls for Girls</a></li>
														<li><a href="">Girls' Learning Toys</a></li>
														<li><a href="">Arts and Crafts for Girls</a></li>
														<li><a href="">Video Games for Girls</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part2">海鲜水产<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">Check
														Trousers</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Building</a></li>
														<li><a href="">Electronics</a></li>
														<li><a href="">action figures </a></li>
														<li><a href="">specialty & boutique toy</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Calculators</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Dolls for Girls</a></li>
														<li><a href="">Girls' Learning Toys</a></li>
														<li><a href="">Arts and Crafts for Girls</a></li>
														<li><a href="">Video Games for Girls</a></li>
													</ul></li>
											</ul></li>
										<li><a href="#part3">熟食卤味</a></li>
										<li><a href="#part4">休闲零食</a></li>
										<li><a href="#part5">粮油调味</a></li>
										<li><a href="#">冰品面点</a></li>
										<li><a href="#">牛奶面包</a></li>
										<li><a href="#">酒水冲饮</a></li>

										<li id="cat_toggle" class="has-sub"><a href="#">>更多</a>
											<ul class="categorie_sub">
												<li><a href="#">O(∩_∩)O哈哈~</a></li>
											</ul></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<!--main menu start-->
							<div class="main_menu menu_position">
								<nav>
									<ul>
										<li><a class="active" href="index.html">主页<i
												class="fa fa-angle-down"></i></a> <!--<ul class="sub_menu">
  											<li>
  												<a href="index.html">Home shop 1</a>
  											</li>
  											<li>
  												<a href="index-2.html">Home shop 2</a>
  											</li>
  											<li>
  												<a href="index-3.html">Home shop 3</a>
  											</li>
  											<li>
  												<a href="index-4.html">Home shop 4</a>
  											</li>
  										</ul>--></li>
										<li class="mega_items"><a href="shop.html">商铺<i
												class="fa fa-angle-down"></i></a>
											<div class="mega_menu">
												<ul class="mega_menu_inner">
													<li><a href="#">Shop Layouts</a>
														<ul>
															<li><a href="shop-fullwidth.html">Full Width</a></li>
															<li><a href="shop-fullwidth-list.html">Full
																	Width list</a></li>
															<li><a href="shop-right-sidebar.html">Right
																	Sidebar </a></li>
															<li><a href="shop-right-sidebar-list.html">
																	Right Sidebar list</a></li>
															<li><a href="shop-list.html">List View</a></li>
														</ul></li>
													<li><a href="#">other Pages</a>
														<ul>
															<li><a href="cart.html">cart</a></li>
															<li><a href="wishlist.html">Wishlist</a></li>
															<li><a href="checkout.html">Checkout</a></li>
															<li><a href="my-account.html">my account</a></li>
															<li><a href="404.html">Error 404</a></li>
														</ul></li>
													<li><a href="#">Product Types</a>
														<ul>
															<li><a href="product-details.html">product
																	details</a></li>
															<li><a href="product-sidebar.html">product
																	sidebar</a></li>
															<li><a href="product-grouped.html">product
																	grouped</a></li>
															<li><a href="variable-product.html">product
																	variable</a></li>

														</ul></li>
												</ul>
											</div></li>
										<li><a href="blog.html">blog<i
												class="fa fa-angle-down"></i></a>
											<ul class="sub_menu pages">
												<li><a href="blog-details.html">blog details</a></li>
												<li><a href="blog-fullwidth.html">blog fullwidth</a></li>
												<li><a href="blog-sidebar.html">blog sidebar</a></li>
											</ul></li>
										<li><a href="#">页面<i class="fa fa-angle-down"></i></a>
											<ul class="sub_menu pages">
												<li><a href="about.html">About Us</a></li>
												<li><a href="services.html">services</a></li>
												<li><a href="faq.html">Frequently Questions</a></li>
												<li><a href="contact.html">contact</a></li>
												<li><a href="login.html">login</a></li>
												<li><a href="404.html">Error 404</a></li>
											</ul></li>
										<li><a href="contact.html">联系我们</a></li>
									</ul>
								</nav>
							</div>
							<!--main menu end-->
						</div>
						<div class="col-lg-3">
							<div class="call-support">
								<p>
									<a href="tel:(08)8399608">(08)8399 608</a> 用户反馈
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!--header area end-->

	<!--breadcrumbs area start-->
	<div class="breadcrumbs_area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="breadcrumb_content">
						<h3>个人中心</h3>
						<ul>
							<li><a href="index.html">首页</a></li>
							<li><a href="my-account.html">我的账号</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--breadcrumbs area end-->

	<!-- my account start  -->
	<section class="main_content_area">
		<div class="container">
			<div class="account_dashboard">
				<div class="row">
					<div class="col-sm-12 col-md-3 col-lg-3">
						<!-- Nav tabs -->
						<div class="dashboard_tab_button">
							<ul role="tablist" class="nav flex-column dashboard-list">
								<li><a href="#dashboard" data-toggle="tab"
									class="nav-link active">我的叮咚</a></li>
								<li><a href="#orders" data-toggle="tab" class="nav-link">订单管理</a></li>
								<li><a href="#downloads" data-toggle="tab" class="nav-link">待支付</a></li>
								<li><a href="#shouhuo" data-toggle="tab" class="nav-link">待收货</a></li>
								<li><a href="#shoucang" data-toggle="tab" class="nav-link">我的收藏</a></li>
								<li><a href="#address" data-toggle="tab" class="nav-link">地址管理</a></li>
								<li><a href="#newaddress" data-toggle="tab"
									class="nav-link">新增收货地址</a></li>
								<li><a href="#account-details" data-toggle="tab"
									class="nav-link">账号设置</a></li>
								<li><a href="login.html" class="nav-link">退出</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-12 col-md-9 col-lg-9">
						<!-- Tab panes -->
						<div class="tab-content dashboard_content">
							<div class="tab-pane fade show active" id="dashboard"
								style="float: left; vertical-align: bottom; text-align: center; margin-left: 60px;">
								<div
									style="width: 680px; padding: 10px; float: left; background-color: #fff; height: 250px;">
									<div
										style="float: left; width: 200px; height: 190px; border: 1px #ccc solid; box-shadow: 1px 1px 1px #F5F5F5; padding: 5px;">
										<div
											style="width: 100%; height: 100%; border: 1px #F2873B dashed;">
											<span
												style="font-size: 18px; color: #686868; font-weight: bold; display: block; display: block; width: 100px; margin-left: 45px; margin-top: 20px;">我的优惠券</span>
											<span
												style="font-size: 36px; color: #F88600; display: block; display: block; width: 100px; margin-left: 50px; margin-top: 30px;"><a
												href="优惠券余额.html">3</a></span>
											<button type="button"
												style="background-color: #f56a48; border-radius: 5px; color: #fff; font-size: 14px; border: 0px; width: 107px; height: 26px; margin-top: 30px;">
												<a href="领优惠券.html">领券</a>
											</button>

										</div>
									</div>

									<div
										style="float: left; width: 200px; height: 190px;; margin-left: 30px; border: 1px #ccc solid; box-shadow: 1px 1px 1px #F5F5F5; padding: 5px;">
										<div
											style="width: 100%; height: 100%; border: 1px #F2873B dashed;">
											<span
												style="font-size: 18px; color: #686868; font-weight: bold; display: block; display: block; width: 100px; margin-left: 45px; margin-bottom: 20px; margin-top: 20px;">我的资产</span>
											<button type="button"
												style="background-color: #f56a48; border-radius: 5px; color: #fff; font-size: 14px; border: 0px; width: 107px; height: 26px;">显示账户余额</button>
											<img src="img/user/矢量智能对象.png"
												style="position: absolute; margin-top: 40px; margin-left: -90px;" />
										</div>
									</div>

									<div
										style="float: left; width: 200px; height: 190px; margin-left: 30px; border: 1px #ccc solid; box-shadow: 1px 1px 1px #F5F5F5; padding: 5px;">
										<div
											style="width: 100%; height: 100%; border: 1px #F2873B dashed;">
											<span
												style="font-size: 18px; color: #686868; font-weight: bold; display: block; display: block; width: 100px; margin-left: 45px; margin-top: 20px; margin-bottom: 20px;">我的积分</span>
											<button type="button"
												style="background-color: #f56a48; border-radius: 5px; color: #fff; font-size: 14px; border: 0px; width: 107px; height: 26px;">
												<a href="我的积分.html">显示积分</a>
											</button>
											<img src="img/user/猪的图标.png"
												style="position: absolute; margin-top: 25px; margin-left: -80px;" />
										</div>
									</div>

									<div
										style="border-top: 1px #ccc solid; width: 650px; position: absolute; margin-top: 240px; float: left;"></div>
									<div
										style="position: absolute; margin-top: 270px; font-size: 14px; color: #686868; float: left;">
										<span style="margin-left: 20px;">待付款</span> <font
											style="color: #CF2D27;"><a href="">0</a></font> <span
											style="margin-left: 40px; margin-right: 40px;">|</span> <span>待发货</span>
										<font style="color: #CF2D27;"><a href="">0</a></font> <span
											style="margin-left: 40px; margin-right: 40px;">|</span> <span>待收货</span>
										<font style="color: #CF2D27;"><a href="">0</a></font> <span
											style="margin-left: 40px; margin-right: 40px;">|</span> <span>待评价</span>
										<font style="color: #CF2D27;"><a href="">0</a></font> <span
											style="margin-left: 40px; margin-right: 40px;">|</span> <span><a
											href="">退款</a></span>

									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="orders">

								<div class="table-responsive">
									<table class="layui-table"
										lay-data="{url:'${basepath}/order.do',page:true,toolbar: '#toolbarDemo',id:'test'}"
										lay-filter="test">
										<thead>
											<tr>
												<th lay-data="{type:'checkbox'}">ID</th>
												<th lay-data="{field:'o_sn', width:80, sort: true}">订单流水号</th>
												<th
													lay-data="{field:'o_id', width:120, sort: true, edit: 'text'}">订单编号</th>
												<th
													lay-data="{field:'o_creat', edit: 'text', minWidth: 150}">创建时间</th>
												<th lay-data="{field:'paystate', width:80}">支付状态</th>
												<th
													lay-data="{field:'o_paymethod', edit: 'text', width: 150}">支付方式</th>
												<th lay-data="{field:'o_money', edit: 'text', width: 150}">金额</th>
												<th lay-data="{field:'r_id', edit: 'text', width: 150}">骑手编号</th>
												<th lay-data="{field:'o_paytime', edit: 'text', width: 150}">支付时间</th>
												<th lay-data="{field:'o_end', edit: 'text', width: 150}">收货时间</th>
												<th
													lay-data="{field:'o_state',width:120, sort: true, edit: 'text'}">订单状态</th>

												<th lay-data="{field:'', width:150,templet: '#opTpl'}">操作</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="downloads">

								<div class="table-responsive">
									<table class="layui-table"
										lay-data="{url:'${basepath}/order.do',page:true,toolbar: '#toolbarDemo',id:'test'}"
										lay-filter="test">
										<thead>
											<tr>
												<th lay-data="{type:'checkbox'}">ID</th>
												<th lay-data="{field:'o_sn', width:80, sort: true}">订单流水号</th>
												<th
													lay-data="{field:'o_id', width:120, sort: true, edit: 'text'}">订单编号</th>
												<th
													lay-data="{field:'o_creat', edit: 'text', minWidth: 150}">创建时间</th>
												<th
													lay-data="{field:'paystate', width:80,templet: '#switchTpl'}">支付状态</th>

												<th lay-data="{field:'o_money', edit: 'text', width: 150}">金额</th>

											</tr>
										</thead>
									</table>

								</div>
							</div>
							<div class="tab-pane fade" id="shouhuo">

								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>订单编号</th>
												<th>日期</th>
												<th>状态</th>
												<th>金额</th>
												<th>详情</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>2021-1-7 14:10:23</td>
												<td><span class="success">待收货</span></td>
												<td>￥88.00</td>
												<td><a href="cart.html" class="view">订单详情</a></td>
											</tr>
											<tr>
												<td>2</td>
												<td>2021-1-8 14:10:23</td>
												<td>待收货</td>
												<td>￥128.00</td>
												<td><a href="cart.html" class="view">订单详情</a></td>
											</tr>
											<tr>
												<td>3</td>
												<td>2021-1-8 14:10:23</td>
												<td>待收货</td>
												<td>￥128.00</td>
												<td><a href="cart.html" class="view">订单详情</a></td>
											</tr>
										</tbody>
									</table>
									<div class="shop_toolbar t_bottom">
										<div class="pagination">
											<ul>
												<li class="current">1</li>
												<li><a href="#">2</a></li>
												<li><a href="#">3</a></li>
												<li class="next"><a href="#">next</a></li>
												<li><a href="#">>></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="shoucang">

								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>流水号</th>
												<th>商品ID</th>
												<th>菜谱ID</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td>s000003</td>
												<td><a href="">Fish88820</a></td>
												<td><span class="danger"> </span></td>

											</tr>
											<tr>
												<td>c000002</td>
												<td></td>
												<td><a href="">Fish9998</a></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

							<div class="tab-pane" id="address">
								<div class="table-responsive">
									<table class="layui-table"
										lay-data="{url:'${basepath}/ajaxaddress.do',page:true,toolbar: '#toolbarDemo',id:'test'}"
										lay-filter="test">
										<thead>
											<tr>
												<th lay-data="{type:'checkbox'}">ID</th>
												<th lay-data="{field:'a_sn', width:80, sort: true}">地址流水号</th>
												<th
													lay-data="{field:'a_id', width:120, sort: true, edit: 'text'}">地址编号</th>
												<th lay-data="{field:'c_name', edit: 'text', minWidth: 150}">收货人姓名</th>
												<th lay-data="{field:'c_tell', width:80}">手机号码</th>

												<th lay-data="{field:'a_name', edit: 'text', width: 150}">地址</th>

											</tr>
										</thead>
									</table>

								</div>
							</div>


							<div class="tab-pane" id="newaddress">
								<form class="layui-form" action="">

									<div class="layui-form-item">
										<label class="layui-form-label">收货人</label>
										<div class="layui-input-inline">
											<input type="addname" name="addname" required
												lay-verify="required" placeholder="请输入收货人姓名"
												autocomplete="off" class="layui-input">
										</div>

									</div>
									<div class="layui-form-item">
										<label class="layui-form-label">性别</label>
										<div class="layui-input-block">
											<input type="radio" name="sex" value="男" title="男"> <input
												type="radio" name="sex" value="女" title="女" checked>
										</div>
									</div>
									<div class="layui-form-item">
										<label class="layui-form-label">电话</label>
										<div class="layui-input-inline">
											<input type="addtell" name="addtell" required
												lay-verify="required" placeholder="请输入收货人电话号码"
												autocomplete="off" class="layui-input">
										</div>

									</div>
									<div class="layui-form-item">
										<label class="layui-form-label">城市</label>
										<div class="layui-input-block">
											<select name="city" lay-verify="required">
												<option value=""></option>
												<option value="0" selected="selected">福建省厦门市</option>
												<option value="1">福建省龙岩市</option>
												<option value="2">福建省福州市</option>
												<option value="3">福建省莆田市</option>
												<option value="4">福建省泉州市</option>
											</select>
										</div>
									</div>

									<div class="layui-form-item layui-form-text">
										<label class="layui-form-label">地址</label>
										<div class="layui-input-block">
											<textarea name="addcontent"
												placeholder="建议您如实填写详细收货地址，例如街道 名称，门牌号码，楼层和房间号等信息"
												class="layui-textarea"></textarea>
										</div>
									</div>

									<div class="layui-form-item">
										<label class="layui-form-label">类型</label>
										<div class="layui-input-block">
											<input type="radio" name="address" title="家" checked>
											<input type="radio" name="address" title="公司"> <input
												type="radio" name="address" title="父母家">
										</div>
									</div>
									<div class="layui-form-item">
										<div class="layui-input-block">
											<button class="layui-btn" lay-submit lay-filter="add"
												style="background-color: #00aa7f; color: #fff; width: 100px; height: 30px; border: 0px; border-radius: 5px;">
												立即提交</button>

										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane fade" id="account-details">

								<div id="ms-center" class="personal-member">
									<div class="cont">

										<div class="cont-main">
											<div class="main-wrap mt15">
												<!--<h3>
                                                    <strong>个人信息</strong>
                                                </h3>-->
												<div class="user-profile clearfix">
													<div class="user-profile-wrap">
														<h3>您的基础信息</h3>
														<div class="control-group clearfix "
															style="margin-top: 20px; margin-left: 20px;">
															<div class="controls lh26" style="width: 320px;">
																<font style="font-weight: bold;">头&nbsp;像：</font> <img
																	src="img/user/猪的图标.png" /><font
																	style="color: #2D5BC1; margin-left: 160px;"><a
																	href="#address">修改</a></font>
															</div>
															<br />
															<div class="controls lh26" style="width: 320px;">
																<font style="font-weight: bold;">会&nbsp;员&nbsp;名：</font>fffbm<font
																	style="color: #2D5BC1; float: right;"><a
																	href="手机修改1.html">修改</a></font>
															</div>
															<br />
															<div class="controls lh26" style="width: 320px;">
																<font style="font-weight: bold;">绑定手机：</font>188***210<font
																	style="color: #2D5BC1; float: right;"><a
																	href="手机修改1.html">修改手机</a></font>
															</div>
															<br />
															<div class="controls lh26" style="width: 320px;">
																<font style="font-weight: bold;">账号绑定：</font>微信/QQ<font
																	style="color: #2D5BC1; float: right;"><a
																	href="账号绑定.html">马上绑定</a></font>
															</div>
														</div>

													</div>
												</div>
												<input id="" type="hidden">
												<div class="form-list tab-switch personal-wrap-show">
													<form>
														<div class="control-group clearfix"
															style="margin-top: 20px;">
															<h3>您的安全服务</h3>
														</div>
														<div class="control-group clearfix">
															<label style="margin-left: 20px;">登录等级：</label> <font
																style="color: #F37B1D;">中</font>
															<progress value="22" max="100" style="margin-left: 10px;"></progress>
															<span style="margin-left: 20px;">加强密码设置,提升账户安全</span>
														</div>
														<div class="control-group clearfix"
															style="margin-top: 34px; width: 920px;">
															<label style="margin-left: 20px;">密码登录</label> <span
																style="margin-left: 54px;">已设置 <img
																src="img/user/ysz.png"
																style="margin-left: 5px; margin-bottom: -4px;" /></span>
															<div
																style="margin-left: 280px; margin-top: -40px; width: 500px; float: left">
																安全性高的密码可以使账户更安全,建议您定期更换密码,且设置一个包含数字<br>母,并长度超过6位以上的密码。
															</div>
															<div
																style="float: right; margin-top: -40px; margin-right: 60px; color: #007AFF;">
																<a href="修改密码1.html">修改</a>
															</div>
														</div>
														<div
															style="border-bottom: 1px dashed #ccc; width: 850px; margin-left: 20px"></div>
														<div class="control-group clearfix"
															style="margin-top: 40px; width: 920px;">
															<label style="margin-left: 20px;">支付密码</label> <span
																style="margin-left: 54px; color: #8e8e8e;">已完成</span>
															<div
																style="margin-left: 280px; margin-top: -30px; width: 500px; float: left; color: #8e8e8e; font-size: 12px;">
																在账户资金变动,修改账户信息时需要输入的密码。<br>&nbsp;
															</div>
															<div
																style="float: right; margin-top: -30px; margin-right: 60px; color: #F88600;">
																<a href="">马上设置</a>
															</div>
														</div>
														<div
															style="border-bottom: 1px dashed #ccc; width: 850px; margin-left: 20px"></div>


														<div class="control-group clearfix"
															style="margin-top: 40px; width: 920px;">
															<label style="margin-left: 20px;">手机号码</label> <span
																style="margin-left: 54px;">已绑定 <img
																src="img/user/ysz.png"
																style="margin-left: 5px; margin-bottom: -4px;" /></span>
															<div
																style="margin-left: 280px; margin-top: -30px; width: 500px; float: left">
																绑定手机后，您即可享受淘宝丰富的手机服务，如手机找回密码等。<br>&nbsp;
															</div>
															<div
																style="float: right; margin-top: -30px; margin-right: 60px; color: #007AFF;">
																<a href="手机修改1.html">修改</a>
															</div>
														</div>
														<div
															style="border-bottom: 1px dashed #ccc; width: 850px; margin-left: 20px"></div>

														<!--<div style="border-bottom: 1px dashed #ccc;width:850px;margin-left:87px"></div>-->
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
		
	</section>
	<!-- my account end   -->

	<!--footer area start-->
	<footer class="footer_widgets">
		<div class="footer_top">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-7">
						<div class="widgets_container contact_us">
							<div class="footer_logo">
								<a href="index.html"><img src="" alt=""></a>
							</div>
							<p class="footer_desc">服务、质量、效率</p>
							<p>
								<span>Address:</span> xmzretc
							</p>
							<p>
								<span>Email:</span> <a href="#">760008559@qq.com</a>
							</p>
							<p>
								<span>Call us:</span> <a href="tel:(08)23456789">(08) 23 456
									789</a>
							</p>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关于叮咚到家</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">了解叮咚</a></li>
									<li><a href="#">加入叮咚</a></li>
									<li><a href="#">投资者关系</a></li>
									<li><a href="#">团队信息</a></li>
									<li><a href="contact.html">联系我们</a></li>
									<li><a href="#">廉洁举报</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关注我们</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">新浪微博</a></li>
									<li><a href="#">官方微信</a></li>
									<li><a href="#">Q群加入</a></li>
									<li><a href="#">公益基金会</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>特色服务</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">兑换券码领取</a></li>
									<li><a href="#">礼物码</a></li>
									<li><a href="#">防伪查询</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4">
						<div class="widgets_container widget_menu">
							<h3>帮助中心</h3>
							<div class="footer_menu">
								<ul>
									<li><a href="#">账户管理</a></li>
									<li><a href="#">购物指南</a></li>
									<li><a href="#">订单操作</a></li>
									<li><a href="#">售后政策</a></li>
									<li><a href="#">自助服务</a></li>
									<li><a href="#">相关下载</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer_bottom">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-7">
						<div class="copyright_area">
							<p>
								Copyright &copy; 2020.闽ICP证110xxx号 闽ICP备10046xxx号
								闽公网安备11010802020xxx号
								<!--<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a>-->
							</p>
						</div>
					</div>
					<div class="col-lg-6 col-md-5">
						<div class="footer_payment">
							<ul>
								<li><a href="#"><img src="assets/img/icon/v-logo-1.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-2.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-3.png"
										alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--footer area end-->

	<!-- JS
============================================ -->
	<!--jquery min js-->
	<script src="${basepath}/front/assets/js/vendor/jquery-3.4.1.min.js"></script>
	<!--popper min js-->
	<script src="${basepath}/front/assets/js/popper.js"></script>
	<!--bootstrap min js-->
	<script src="${basepath}/front/assets/js/bootstrap.min.js"></script>
	<!--owl carousel min js-->
	<script src="${basepath}/front/assets/js/owl.carousel.min.js"></script>
	<!--slick min js-->
	<script src="${basepath}/front/assets/js/slick.min.js"></script>
	<!--magnific popup min js-->
	<script src="${basepath}/front/assets/js/jquery.magnific-popup.min.js"></script>
	<!--counterup min js-->
	<script src="${basepath}/front/assets/js/jquery.counterup.min.js"></script>
	<!--jquery countdown min js-->
	<script src="${basepath}/front/assets/js/jquery.countdown.js"></script>
	<!--jquery ui min js-->
	<script src="${basepath}/front/assets/js/jquery.ui.js"></script>
	<!--jquery elevatezoom min js-->
	<script src="${basepath}/front/assets/js/jquery.elevatezoom.js"></script>
	<!--isotope packaged min js-->
	<script src="${basepath}/front/assets/js/isotope.pkgd.min.js"></script>
	<!--slinky menu js-->
	<script src="${basepath}/front/assets/js/slinky.menu.js"></script>
	<!-- Plugins JS -->
	<script src="${basepath}/front/assets/js/plugins.js"></script>

	<!-- Main JS -->
	<script src="${basepath}/front/assets/js/main.js"></script>

	<script type="text/html" id="switchTpl">
        <!-- 这里的checked的状态只是演示 -->
        <input type = "checkbox" name = "usersex" value = "{{d.id}}" lay-skin = "switch" lay-text = "已支付|待支付" lay-filter = "payDemo" {{ d.paystate == 1 ? 'checked': ''}} >
    </script>

	<script>
		layui.use([ 'table', 'form' ], function() {
			var table = layui.table;
			var form = layui.form;
			//监听单元格编辑
			table.on('edit(test)',
					function(obj) {
						var value = obj.value //得到修改后的值
						, data = obj.data //得到所在行所有键值
						, field = obj.field; //得到字段
						layer.msg('[ID: ' + data.id + '] ' + field + ' 字段更改为：'
								+ value);
					});
			//监听提交（添加地址 ）
			form.on('submit(add)', function(data) {
				//console.log(data.field);

				//发异步，把数据提交给servlet,代码需要我们自己完成
				//jquery
				$.ajax({
					url : '../ajaxaddress.do?op=add',
					//data	规定要发送到服务器的数据。
					data : data.field,
					//type	规定请求的类型POST
					type : 'post',
					//success(result,status,xhr)当请求成功时运行的函数，res是在Servlet返回的数据
					success : function(res) {
						console.log(res.msg);
						layer.alert(res.msg, {
							icon : 6
						});
					}
				//error(xhr,status,error)	如果请求失败要运行的函数。
				});
				return false;
			});
			//头工具栏事件
			table.on('toolbar(test)', function(obj) {
				var checkStatus = table.checkStatus(obj.config.id);
				switch (obj.event) {
				case 'getCheckData':
					var data = checkStatus.data;
					layer.alert(JSON.stringify(data));
					break;
				case 'getCheckLength':
					var data = checkStatus.data;
					layer.msg('选中了：' + data.length + ' 个');
					break;
				case 'isAll':
					layer.msg(checkStatus.isAll ? '全选' : '未全选');
					break;
				}
				;
			});
		});
	</script>


</body>

</html>
