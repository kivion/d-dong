package com.dmall.dao;

import java.util.List;


import com.dmall.entity.Grade;
import com.dmall.util.DBUtil;
/**
 * 等级的方法--不需要分页
 * @author Administrator
 *
 */
public class GradeDao {
	
	/**
	 * 模糊查询
	 * @param keywords 关键字
	 * @return 对象
	 */
		public List<Grade> getGradeByKeywords(String keywords) {
			String sql = "SELECT grade.g_id, grade.g_name, grade.g_remark, grade.g_reward FROM grade WHERE grade.g_name like ? or grade.g_remark like ?";
			List<Grade> list = DBUtil.exeQuery(sql, Grade.class, "%" + keywords + "%", "%" + keywords + "%");
			return list;
		}
		/** 
		 * 通过骑手编号查询等级
		 * @param keywords 骑手编号
		 * @return 对象
		 */
			public List<Grade> getGradeByrid(String rid) {
				String sql = "select grade.g_id, grade.g_name, grade.g_remark, grade.g_reward  " + 
						"FROM grade INNER JOIN (select r_id,g_id from rider)as rider ON grade.g_id = rider.g_id  " + 
						"where rider.r_id=?";
				List<Grade> list = DBUtil.exeQuery(sql, Grade.class, rid);
				return list;
			}
	/**
	 * 添加等级信息
	 * @param g 等级对象
	 * @return true操作成功，false操作失败
	 */
	public boolean AddGrade(Grade g) {
		String sql = "INSERT INTO `grade` (`g_id`, `g_name`, `g_remark`, `g_reward`) VALUES (?,?,?, ?)";
		return DBUtil.exeUpdate(sql,g.getG_id(),g.getG_name(),g.getG_remark(),g.getG_reward());
	}
	
	
	

	
	

}
