package com.dmall.pagedata;

import java.util.List;

public class PageData<T> {
	//页码
	private int pageNo;
	//当前页的记录数
	private int pageSize;
	//总记录数
	private int totalCount;
	//总页数
	private int totalPage;
	//当前页的数据
	private List<T> list;
	
	public int getTotalPage() {
		totalPage = totalCount / pageSize;
		if (totalCount%pageSize!=0) {
			totalPage = totalPage+1;
		}
		return totalPage;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	@Override
	public String toString() {
		return "PageData [pageNo=" + pageNo + ", pageSize=" + pageSize + ", totalCount=" + totalCount + ", totalPage="
				+ totalPage + ", list=" + list + "]";
	}
	
	
}
