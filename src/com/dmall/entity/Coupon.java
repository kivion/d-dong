package com.dmall.entity;

public class Coupon {
	private int co_sn;
	private String co_id;
	//加了个优惠券名称  
	private String co_name;
	private String co_num;
	private String co_start;
	private String co_end;
	private String co_state;
	
	
	public Coupon(int co_sn, String co_id, String co_name, String co_num, String co_start, String co_end,
			String co_state) {
		super();
		this.co_sn = co_sn;
		this.co_id = co_id;
		this.co_name = co_name;
		this.co_num = co_num;
		this.co_start = co_start;
		this.co_end = co_end;
		this.co_state = co_state;
	}

	public Coupon() {
		// TODO Auto-generated constructor stub
	}

	public int getCo_sn() {
		return co_sn;
	}

	public void setCo_sn(int co_sn) {
		this.co_sn = co_sn;
	}

	public String getCo_id() {
		return co_id;
	}

	public void setCo_id(String co_id) {
		this.co_id = co_id;
	}
	
	public String getCo_name() {
		return co_name;
	}

	public void setCo_name(String co_name) {
		this.co_name = co_name;
	}

	public String getCo_num() {
		return co_num;
	}

	public void setCo_num(String co_num) {
		this.co_num = co_num;
	}

	public String getCo_start() {
		return co_start;
	}

	public void setCo_start(String co_start) {
		this.co_start = co_start;
	}

	public String getCo_end() {
		return co_end;
	}

	public void setCo_end(String co_end) {
		this.co_end = co_end;
	}

	public String getCo_state() {
		return co_state;
	}

	public void setCo_state(String co_state) {
		this.co_state = co_state;
	}

	@Override
	public String toString() {
		return "Coupon [co_sn=" + co_sn + ", co_id=" + co_id + ", co_name=" + co_name + ", co_num=" + co_num
				+ ", co_start=" + co_start + ", co_end=" + co_end + ", co_state=" + co_state + "]";
	}

	
	
}
