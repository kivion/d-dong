<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>
<html class="no-js" lang="en">
<!-- 加入 taglib 标记-->
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>叮咚到家 - 商品详情</title>
<p:set var="basepath" scope="page"
	value="${pageContext.request.contextPath}"></p:set>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS 
    ========================= -->
<!--bootstrap min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/bootstrap.min.css">
<!--owl carousel min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/owl.carousel.min.css">
<!--slick min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/slick.css">
<!--magnific popup min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/magnific-popup.css">
<!--font awesome css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/font.awesome.css">
<!--ionicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/ionicons.min.css">
<!--linearicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/linearicons.css">
<!--animate css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/animate.css">
<!--jquery ui min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/jquery-ui.min.css">
<!--slinky menu css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/slinky.menu.css">
<!--plugins css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/plugins.css">

<!-- Main Style CSS -->
<link rel="stylesheet" href="${basepath}/front/assets/css/style.css">
<!--layUI的css-->
<link rel="stylesheet" href="${basepath}/front/layui/css/layui.css"
	media="all">

<!--modernizr min js here-->
<script src="${basepath}/front/assets/js/vendor/modernizr-3.7.1.min.js"></script>
<style>
.zoom1 {
	width: 500px;
	height: 500px;
}

#img-1 {
	border: 0px !important;
}
</style>
</head>

<body>

	<!--头部开始-->
	<header>
		<div class="main_header">
			<div class="header_top">
				<div class="container">
					<div class="row align-items-center">

						<!--导航栏-->
						<div class="col-lg-6 col-md-6">
							<div class="language_currency">
								<ul>
									<li class="language"><a href="#"> 语言 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_language">
											<li><a href="#">中文</a></li>
											<li><a href="#">英语</a></li>
										</ul></li>
									<li class="currency"><a href="#"> 简繁体 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_currency">
											<li><a href="#">简体</a></li>
											<li><a href="#">繁体</a></li>
										</ul></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="header_social text-right">
								<ul>
									<li><a href="#"><i class="ion-social-twitter"></i></a></li>
									<li><a href="#"><i
											class="ion-social-googleplus-outline"></i></a></li>
									<li><a href="#"><i class="ion-social-youtube-outline"></i></a>
									</li>
									<li><a href="#"><i class="ion-social-facebook"></i></a></li>
									<li><a href="#"><i
											class="ion-social-instagram-outline"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_middle">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-2">
							<div class="logo">
								<a href="${basepath}/producthome.do"><img src="${basepath}/front/assets/img/logo/logo.jpg"
									alt=""></a>
							</div>
						</div>
						<div class="col-lg-10">
							<div class="header_right_info">
								<!--导航栏左边的类别-->
								<div class="search_container">
									<form action="#">
										<div class="hover_category">
											<select class="select_option" name="select" id="categori1">
												<option selected value="1">选择类别</option>
												<option value="2">水果</option>
												<option value="3">蔬菜</option>
												<option value="4">肉禽蛋品</option>
												<option value="5">海鲜水产</option>
												<option value="6">熟食卤味</option>
												<option value="7">粮油调味</option>
												<option value="8">休闲零食</option>
												<option value="9">冰品面点</option>
												<option value="10">牛奶面包</option>
												<option value="11">酒水冲饮</option>
											</select>
										</div>
										<div class="search_box">
											<input placeholder="国产红心火龙果0.99元/个..." type="text">
											<button type="submit">
												<span class="lnr lnr-magnifier"></span>
											</button>
										</div>
									</form>
								</div>
								<div class="header_account_area">
									<div class="header_account_list register">
										<ul class="layui-nav">
											<!--layUI的状态头像登录-->
											<li class="layui-nav-item" lay-unselect=""><a
												href="${basepath}/front/my-account.jsp"><img
													src="${basepath}/front/assets/img/icon/1.jpg"
													class="layui-nav-img">佚名</a>
												<dl class="layui-nav-child">
													<dd>
														<a href="javascript:;">修改信息</a>
													</dd>
													<dd>
														<a href="javascript:;">账号管理</a>
													</dd>
													<dd>
														<a href="javascript:;">退出</a>
													</dd>
												</dl></li>
											<!--<li>
													<a href="login.html">注册</a>
												</li>
												<li><span>/</span></li>
												<li>
													<a href="login.html">登录</a>
												</li>-->
										</ul>
									</div>
									<div class="header_account_list header_wishlist">
										<a href="收藏页面.html"><span class="lnr lnr-heart"></span> <span
											class="item_count">5</span> </a>
									</div>
									<div class="header_account_list  mini_cart_wrapper">
										<a href="${basepath}/front/Cart.jsp"><span class="lnr lnr-cart"></span><span
											class="item_count">2</span></a>
										<!--mini cart-->
										<!-- 
										<div class="mini_cart">
											<div class="cart_gallery">
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Primis In Faucibus</a>
														<p>
															1 x <span> $65.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product2.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Letraset Sheets</a>
														<p>
															1 x <span> $60.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
											</div>
											<div class="mini_cart_table">
												<div class="cart_table_border">
													<div class="cart_total mt-10">
														<span>总计:</span> <span class="price">$125.00</span>
													</div>
												</div>
											</div>
											<div class="mini_cart_footer">
												<div class="cart_button">
													<a href="购物车.html"><i class="fa fa-shopping-cart"></i>
														前往购物车</a>
												</div>
												<div class="cart_button">
													<a href="#"><i class="fa fa-sign-in"></i> ...</a>
												</div>

											</div>
										</div>
										 -->
										<!--mini cart end-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_bottom sticky-header">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-3">
							<div class="categories_menu">
								<div class="categories_title">
									<h2 class="categori_toggle">所有商品</h2>
								</div>
								<div class="categories_menu_toggle">
									<ul>
										<li class="menu_item_children"><a href="#part1">蔬菜<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu">
												<li class="menu_item_children"><a href="#">苗菜</a>
													<ul class="categorie_sub_menu">
														<li><a href="">叶菜</a></li>
														<li><a href="">根茎菜</a></li>
														<li><a href="">调味菜</a></li>
														<li><a href="">豆瓜果茄</a></li>
														<li><a href="">菌菇</a></li>
														<li><a href="">方便菜</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part1">水果<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_3">
												<li class="menu_item_children"><a href="#">热带水果</a>
													<ul class="categorie_sub_menu">
														<li><a href="">进口水果</a></li>
														<li><a href="">瓜类</a></li>
														<li><a href="">常见类水果</a></li>
													</ul></li>
												<!-- 	<li class="menu_item_children"><a href="#">qqqqqq</a>
													<ul class="categorie_sub_menu">
														<li><a href="">q</a></li>
														<li><a href="">q</a></li>
														<li><a href="">2</a></li>
														<li><a href="">q</a></li>
													</ul></li>
												 -->
											</ul></li>
										<li class="menu_item_children"><a href="#part1">肉禽蛋品<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">猪肉类</a>
													<ul class="categorie_sub_menu">
														<li><a href="">鲜家禽</a></li>
														<li><a href="">蛋</a></li>
														<li><a href="">牛排</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part2">海鲜水产<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">鱼</a>
													<ul class="categorie_sub_menu">
														<li><a href="">虾</a></li>
														<li><a href="">贝</a></li>
														<li><a href="">蟹</a></li>
													</ul></li>
											</ul></li>
										<li><a href="#part3">熟食卤味</a></li>
										<li><a href="#part4">休闲零食</a></li>
										<li><a href="#part5">粮油调味</a></li>
										<li><a href="#">冰品面点</a></li>
										<li><a href="#">牛奶面包</a></li>
										<li><a href="#">酒水冲饮</a></li>

										<li id="cat_toggle" class="has-sub"><a href="#">>更多</a>
											<ul class="categorie_sub">
												<li><a href="#">暂时没有更多了，小咚会继续努力的~</a></li>
											</ul></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<!--main menu start-->
							<div class="main_menu menu_position">
								<nav>
									<ul>
										<li><a class="active" href="${basepath}/front/index.jsp">主页</a></li>
										<li><a href="${basepath}/front/Menu_Home.jsp">菜谱 <!--<i class="fa fa-angle-down"></i>--></a>
										</li>
										<li><a href="关于我们.html">关于我们</a></li>
										<li><a href="联系我们.html">联系我们</a></li>
									</ul>
								</nav>
							</div>
							<!--main menu end-->
						</div>
						<div class="col-lg-3">
							<div class="call-support">
								<p>
									<a href="tel:(08)8399608">(08)8399 608</a> 用户反馈
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!--header area end-->

	<!--breadcrumbs area start-->
	<div class="breadcrumbs_area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="breadcrumb_content">
						<ul>
							<li><a href="index.html">小咚</a></li>
							<li>这是主人点的商品详情哦</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--breadcrumbs area end-->

	<!--product details start-->
	<div class="product_details mt-70 mb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="product-details-tab">
						<div id="img-1" class="zoomWrapper single-zoom">
							<a href="#"> <img class="zoom1" src=""> <!--  data-zoom-image="assets/img/product/productbig4.jpg" alt="big-1"-->

							</a>
						</div>
						<div class="single-zoom-thumb">
							<ul class="s-tab-zoom owl-carousel single-product-active"
								id="gallery_01">
								<li><a href="#" class="elevatezoom-gallery active"
									data-update="" data-image="assets/img/product/productbig4.jpg"
									data-zoom-image=""> <img src="" class="zoom2" alt="zo-th-1" />
								</a></li>
								<li><a href="#" class="elevatezoom-gallery active"
									data-update="" data-image="assets/img/product/productbig1.jpg"
									data-zoom-image=""> <img src="" class="zoom2" alt="zo-th-1" />
								</a></li>
								<li><a href="#" class="elevatezoom-gallery active"
									data-update="" data-image="assets/img/product/productbig2.jpg"
									data-zoom-image=""> <img src="" class="zoom2" alt="zo-th-1" />
								</a></li>
								<li><a href="#" class="elevatezoom-gallery active"
									data-update="" data-image="assets/img/product/productbig3.jpg"
									data-zoom-image=""> <img src="" class="zoom2" alt="zo-th-1" />
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="product_d_right">
						<form action="#">

							<h1>
								<a href="#" class="p_name">${param.p_name}</a>
							</h1>
							<div class="product_nav">
								<ul>
									<li class="prev"><a href="${basepath}/productshoplist.do"><i
											class="fa fa-angle-left"></i></a></li>
								</ul>
							</div>
							<div class=" product_ratting">
								<ul>
									<li><a href="#"><i class="icon-star"></i></a></li>
									<li><a href="#"><i class="icon-star"></i></a></li>
									<li><a href="#"><i class="icon-star"></i></a></li>
									<li><a href="#"><i class="icon-star"></i></a></li>
									<li><a href="#"><i class="icon-star"></i></a></li>
									<li class="review"><a href="#"> 去打分 </a></li>
								</ul>

							</div>
							<div class="price_box">
								<span class="current_price" id="p_price">${param.p_price}</span>
								<span class="current_price">元</span>
							</div>
							<div class="product_desc" class="p_detaill">
								<p class="p_detaill">${param.p_detaill}</p>
							</div>
							<!--<div class="product_variant color">
									<h3>Available Options</h3>
									<label>color</label>
									<ul>
										<li class="color1">
											<a href="#"></a>
										</li>
										<li class="color2">
											<a href="#"></a>
										</li>
										<li class="color3">
											<a href="#"></a>
										</li>
										<li class="color4">
											<a href="#"></a>
										</li>
									</ul>
								</div>-->
							<div class="product_variant quantity">
								<label>数量：</label> <input min="1" max="100" value="1" id="num"
									type="number">
								<button class="button" type="button" id="addcart">添加购物车</button>

							</div>
							<div class=" product_d_action">
								<button class="button" type="button" onclick="location.href=''">加入收藏</button>
							</div>
							<div class=" product_d_action">
								<a href="${basepath}/customer.do?op=login">模拟登陆</a>
							</div>
							<div class="product_meta">
								<span>标签：</span>
							</div>

						</form>
						<div class="priduct_social">
							<ul>
								<li><a class="twitter" href="#" title="支付"pointer-events:none;><i
										class="fa fa-twitter"></i> 支付宝</a></li>
								<li><a class="google-plus" href="#" title="来源"pointer-events:none;><i
										class="fa fa-google-plus"></i> QS</a></li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!--product details end-->

	<!--product info start-->
	<div class="product_d_info mb-65">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="product_d_inner">
						<div class="product_info_button">
							<ul class="nav" role="tablist">
								<li><a class="active" data-toggle="tab" href="#info"
									role="tab" aria-controls="info" aria-selected="false">具体描述</a>
								</li>
								<li><a data-toggle="tab" href="#sheet" role="tab"
									aria-controls="sheet" aria-selected="false">属性列表</a></li>
								<li><a data-toggle="tab" href="#reviews" role="tab"
									aria-controls="reviews" aria-selected="false">查看评论（）</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade show active" id="info" role="tabpanel">
								<div class="product_info_content">
									<p class="p_detaill">${param.p_detaill}</p>
								</div>
							</div>
							<div class="tab-pane fade" id="sheet" role="tabpanel">
								<div class="product_d_table">
									<form action="#">
										<table>
											<tbody>
												<tr>
													<td class="first_child">商品名</td>
													<td class="p_name">${param.p_name}</td>
												</tr>
												<tr>
													<td class="first_child">商品重量</td>
													<td class="p_weight">${param.p_weight}</td>
												</tr>
												<tr>
													<td class="first_child">发源地</td>
													<td class="p_origin">${param.p_origin}</td>
												</tr>
												<tr>
													<td class="first_child">保存方式</td>
													<td class="p_save">${param.p_save}</td>
												</tr>
												<tr>
													<td class="first_child">保质期</td>
													<td class="p_guarantee">${param.p_guarantee}</td>
												</tr>
												<tr>
													<td class="first_child">单价（元）</td>
													<td class="p_price">${param.p_price}</td>
												</tr>
											</tbody>
										</table>
									</form>
								</div>
								<div class="product_info_content">
									<p class="p_detaill">${param.p_detaill}</p>
								</div>
							</div>

							<div class="tab-pane fade" id="reviews" role="tabpanel">
								<div class="reviews_wrapper">
									<h2>共有count条记录评论</h2>
									<div class="reviews_comment_box">
										<div class="comment_thmb">
											<img src="assets/img/blog/comment2.jpg" alt="">
										</div>
										<div class="comment_text">
											<div class="reviews_meta">
												<div class="star_rating">
													<ul>
														<li><a href="#"><i class="icon-star"></i></a></li>
														<li><a href="#"><i class="icon-star"></i></a></li>
														<li><a href="#"><i class="icon-star"></i></a></li>
														<li><a href="#"><i class="icon-star"></i></a></li>
														<li><a href="#"><i class="icon-star"></i></a></li>
													</ul>
												</div>
												<p>
													<strong>admin </strong>- September 12, 2018
												</p>
												<span>roadthemes</span>
											</div>
										</div>

									</div>
									<div class="comment_title">
										<h2>Add a review</h2>
										<p>Your email address will not be published. Required
											fields are marked</p>
									</div>
									<div class="product_ratting mb-10">
										<h3>Your rating</h3>
										<ul>
											<li><a href="#"><i class="icon-star"></i></a></li>
											<li><a href="#"><i class="icon-star"></i></a></li>
											<li><a href="#"><i class="icon-star"></i></a></li>
											<li><a href="#"><i class="icon-star"></i></a></li>
											<li><a href="#"><i class="icon-star"></i></a></li>
										</ul>
									</div>
									<div class="product_review_form">
										<form action="#">
											<div class="row">
												<div class="col-12">
													<label for="review_comment">Your review </label>
													<textarea name="comment" id="review_comment"></textarea>
												</div>
												<div class="col-lg-6 col-md-6">
													<label for="author">Name</label> <input id="author"
														type="text">

												</div>
												<div class="col-lg-6 col-md-6">
													<label for="email">Email </label> <input id="email"
														type="text">
												</div>
											</div>
											<button type="submit">Submit</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--product info end-->

	<!--product area start-->
	<section class="product_area related_products">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section_title">
						<h2>Related Products</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="product_carousel product_column5 owl-carousel">
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product20.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product21.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span> <span class="label_new">New</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Quisque In Arcu</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$55.00</span> <span
											class="old_price">$235.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product15.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product14.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Cas Meque Metus</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$26.00</span> <span
											class="old_price">$362.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product17.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product16.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Aliquam Consequat</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$26.00</span> <span
											class="old_price">$362.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product14.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product15.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span> <span class="label_new">New</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Mauris Vel Tellus</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$48.00</span> <span
											class="old_price">$257.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product16.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product17.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Nunc Neque Eros</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$35.00</span> <span
											class="old_price">$245.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product18.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product19.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Proin Lectus Ipsum</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$26.00</span> <span
											class="old_price">$362.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--product area end-->

	<!--product area start-->
	<section class="product_area upsell_products">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section_title">
						<h2>Upsell Products</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="product_carousel product_column5 owl-carousel">
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product1.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product2.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Proin Lectus Ipsum</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$26.00</span> <span
											class="old_price">$362.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product9.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product4.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span> <span class="label_new">New</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Quisque In Arcu</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$55.00</span> <span
											class="old_price">$235.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product13.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product1.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span> <span class="label_new">New</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Mauris Vel Tellus</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$48.00</span> <span
											class="old_price">$257.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product12.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product2.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Nunc Neque Eros</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$35.00</span> <span
											class="old_price">$245.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product1.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product2.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span> <span class="label_new">New</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Aliquam Consequat</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$26.00</span> <span
											class="old_price">$362.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
						<article class="single_product">
							<figure>
								<div class="product_thumb">
									<a class="primary_img" href="product-details.html"><img
										src="assets/img/product/product3.jpg" alt=""></a> <a
										class="secondary_img" href="product-details.html"><img
										src="assets/img/product/product4.jpg" alt=""></a>
									<div class="label_product">
										<span class="label_sale">Sale</span>
									</div>
									<div class="action_links">
										<ul>
											<li class="add_to_cart"><a href="cart.html"
												title="Add to cart"><span class="lnr lnr-cart"></span></a></li>
											<li class="quick_button"><a href="#" data-toggle="modal"
												data-target="#modal_box" title="quick view"> <span
													class="lnr lnr-magnifier"></span></a></li>
											<li class="wishlist"><a href="wishlist.html"
												title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
											</li>
											<li class="compare"><a href="#" title="Add to Compare"><span
													class="lnr lnr-sync"></span></a></li>
										</ul>
									</div>
								</div>
								<figcaption class="product_content">
									<h4 class="product_name">
										<a href="product-details.html">Donec Non Est</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$46.00</span> <span
											class="old_price">$382.00</span>
									</div>
								</figcaption>
							</figure>
						</article>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--product area end-->

	<!--footer area start-->
	<footer class="footer_widgets">
		<div class="footer_top">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-7">
						<div class="widgets_container contact_us">
							<div class="footer_logo">
								<a href="index.html"><img src="assets/img/logo/logo.png"
									alt=""></a>
							</div>
							<p class="footer_desc">服务、质量、效率</p>
							<p>
								<span>Address:</span> xmzretc
							</p>
							<p>
								<span>Email:</span> <a href="#">760008559@qq.com</a>
							</p>
							<p>
								<span>Call us:</span> <a href="tel:(08)23456789">(08) 23 456
									789</a>
							</p>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关于叮咚到家</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">了解叮咚</a></li>
									<li><a href="#">加入叮咚</a></li>
									<li><a href="#">投资者关系</a></li>
									<li><a href="#">团队信息</a></li>
									<li><a href="contact.html">联系我们</a></li>
									<li><a href="#">廉洁举报</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关注我们</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">新浪微博</a></li>
									<li><a href="#">官方微信</a></li>
									<li><a href="#">Q群加入</a></li>
									<li><a href="#">公益基金会</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>特色服务</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">兑换券码领取</a></li>
									<li><a href="#">礼物码</a></li>
									<li><a href="#">防伪查询</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4">
						<div class="widgets_container widget_menu">
							<h3>帮助中心</h3>
							<div class="footer_menu">
								<ul>
									<li><a href="#">账户管理</a></li>
									<li><a href="#">购物指南</a></li>
									<li><a href="#">订单操作</a></li>
									<li><a href="#">售后政策</a></li>
									<li><a href="#">自助服务</a></li>
									<li><a href="#">相关下载</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer_bottom">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-7">
						<div class="copyright_area">
							<!--<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a>-->
							<p>Copyright &copy; 2020.闽ICP证110xxx号 闽ICP备10046xxx号
								闽公网安备11010802020 号</p>
						</div>
					</div>
					<div class="col-lg-6 col-md-5">
						<div class="footer_payment">
							<ul>
								<li><a href="#"><img src="assets/img/icon/v-logo-1.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-2.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-3.png"
										alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--footer area end-->

	<!-- modal area start-->
	<div class="modal fade" id="modal_box" tabindex="-1" role="dialog"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"><i class="icon-x"></i></span>
				</button>
				<div class="modal_body">
					<div class="container">
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								<div class="modal_tab">
									<div class="tab-content product-details-large">
										<div class="tab-pane fade show active" id="tab1"
											role="tabpanel">
											<div class="modal_tab_img">
												<a href="#"><img
													src="assets/img/product/productbig1.jpg" alt=""></a>
											</div>
										</div>
										<div class="tab-pane fade" id="tab2" role="tabpanel">
											<div class="modal_tab_img">
												<a href="#"><img
													src="assets/img/product/productbig2.jpg" alt=""></a>
											</div>
										</div>
										<div class="tab-pane fade" id="tab3" role="tabpanel">
											<div class="modal_tab_img">
												<a href="#"><img
													src="assets/img/product/productbig3.jpg" alt=""></a>
											</div>
										</div>
										<div class="tab-pane fade" id="tab4" role="tabpanel">
											<div class="modal_tab_img">
												<a href="#"><img
													src="assets/img/product/productbig4.jpg" alt=""></a>
											</div>
										</div>
									</div>
									<div class="modal_tab_button">
										<ul class="nav product_navactive owl-carousel" role="tablist">
											<li><a class="nav-link active" data-toggle="tab"
												href="#tab1" role="tab" aria-controls="tab1"
												aria-selected="false"><img
													src="assets/img/product/product1.jpg" alt=""></a></li>
											<li><a class="nav-link" data-toggle="tab" href="#tab2"
												role="tab" aria-controls="tab2" aria-selected="false"><img
													src="assets/img/product/product6.jpg" alt=""></a></li>
											<li><a class="nav-link button_three" data-toggle="tab"
												href="#tab3" role="tab" aria-controls="tab3"
												aria-selected="false"><img
													src="assets/img/product/product2.jpg" alt=""></a></li>
											<li><a class="nav-link" data-toggle="tab" href="#tab4"
												role="tab" aria-controls="tab4" aria-selected="false"><img
													src="assets/img/product/product7.jpg" alt=""></a></li>

										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<div class="modal_right">
									<div class="modal_title mb-10">
										<h2>Donec Ac Tempus</h2>
									</div>
									<div class="modal_price mb-10">
										<span class="new_price">$64.99</span> <span class="old_price">$78.99</span>
									</div>
									<div class="modal_description mb-15">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Mollitia iste laborum ad impedit pariatur esse optio
											tempora sint ullam autem deleniti nam in quos qui nemo ipsum
											numquam, reiciendis maiores quidem aperiam, rerum vel
											recusandae</p>
									</div>
									<div class="variants_selects">
										<div class="variants_size">
											<h2>size</h2>
											<select class="select_option">
												<option selected value="1">s</option>
												<option value="1">m</option>
												<option value="1">l</option>
												<option value="1">xl</option>
												<option value="1">xxl</option>
											</select>
										</div>
										<div class="variants_color">
											<h2>color</h2>
											<select class="select_option">
												<option selected value="1">purple</option>
												<option value="1">violet</option>
												<option value="1">black</option>
												<option value="1">pink</option>
												<option value="1">orange</option>
											</select>
										</div>
										<div class="modal_add_to_cart">
											<form action="#">
												<input min="1" max="100" step="2" value="1" type="number">
												<button type="submit">add to cart</button>
											</form>
										</div>
									</div>
									<div class="modal_social">
										<h2>Share this product</h2>
										<ul>
											<li class="facebook"><a href="#"><i
													class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a href="#"><i
													class="fa fa-twitter"></i></a></li>
											<li class="pinterest"><a href="#"><i
													class="fa fa-pinterest"></i></a></li>
											<li class="google-plus"><a href="#"><i
													class="fa fa-google-plus"></i></a></li>
											<li class="linkedin"><a href="#"><i
													class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- modal area end-->

	<!-- JS
============================================ -->
	<!--jquery min js-->
	<script src="${basepath}/front/assets/js/vendor/jquery-3.4.1.min.js"></script>
	<!--popper min js-->
	<script src="${basepath}/front/assets/js/popper.js"></script>
	<!--bootstrap min js-->
	<script src="${basepath}/front/assets/js/bootstrap.min.js"></script>
	<!--owl carousel min js-->
	<script src="${basepath}/front/assets/js/owl.carousel.min.js"></script>
	<!--slick min js-->
	<script src="${basepath}/front/assets/js/slick.min.js"></script>
	<!--magnific popup min js-->
	<script src="${basepath}/front/assets/js/jquery.magnific-popup.min.js"></script>
	<!--counterup min js-->
	<script src="${basepath}/front/assets/js/jquery.counterup.min.js"></script>
	<!--jquery countdown min js-->
	<script src="${basepath}/front/assets/js/jquery.countdown.js"></script>
	<!--jquery ui min js-->
	<script src="${basepath}/front/assets/js/jquery.ui.js"></script>
	<!--jquery elevatezoom min js-->
	<script src="${basepath}/front/assets/js/jquery.elevatezoom.js"></script>
	<!--isotope packaged min js-->
	<script src="${basepath}/front/assets/js/isotope.pkgd.min.js"></script>
	<!--slinky menu js-->
	<script src="${basepath}/front/assets/js/slinky.menu.js"></script>
	<!-- Plugins JS -->
	<script src="${basepath}/front/assets/js/plugins.js"></script>

	<!-- Main JS -->
	<script src="${basepath}/front/assets/js/main.js"></script>
	<!--layUI的js-->
	<script src="${basepath}/front/layui/layui.js" charset="utf-8"></script>
	<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
	<script>
		layui.use('element', function() {
			var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

			//监听导航点击
			element.on('nav(demo)', function(elem) {
				//console.log(elem)
				layer.msg(elem.text());
			});
		});
	</script>

	<script type="text/javascript">
		//传递接收的p_id值；param是el表达式中的一个对象
		let p_id = "${param.p_id}";
		console.log(p_id);
		//ajax请求-根据id查询product信息
		$.ajax({
			url : '${basepath}/productshopdetail.do?op=querybyid',
			type : 'get',
			data : {
				'p_id' : p_id
			},
			success : function(res) {
				console.log(res);
				//zoom1因为是模板自带，不改成p_photo为好
				$(".zoom1").attr("src", "${basepath}/picture/" + res.p_photo);
				$(".zoom2").attr("src", "${basepath}/picture/" + res.p_photo);
				$(".p_name").html(res.p_name);
				$(".p_price").html(res.p_price);
				$("#p_price").html(res.p_price);
				$(".p_detaill").html(res.p_detaill);
				//以下主要显示在商品详情
				$(".p_weight").html(res.p_weight + "克");
				$(".p_origin").html(res.p_origin);
				$(".p_save").html(res.p_save);
				$(".p_guarantee").html(res.p_guarantee + "天");
			}
		});

		$("#addcart").click(function() {
			console.log($("#num").val());
			console.log("${param.p_id}");
			$.ajax({
				url : '${basepath}/cart.do?op=add',
				type : 'post',
				data : {
					'p_id' : '${param.p_id}',
					'cart_num' : $("#num").val(),
					'p_price' : $("#p_price").val(),
					'p_name' : '${param.p_name}'
				},
				success : function(res) {
					console.log(res.msg);
				}
			})
		})
	</script>

</body>

</html>