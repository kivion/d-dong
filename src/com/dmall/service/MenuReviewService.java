package com.dmall.service;

import com.dmall.dao.MenueReviewDao;
import com.dmall.entity.MenueReview;
import com.dmall.pagedata.PageData;

public class MenuReviewService {
	MenueReviewDao mrd = new MenueReviewDao();
	/**
	 *	分页+模糊查询菜谱评论
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<MenueReview> getMenuReview(String keyword,int pageNo,int pageSize){
		
		PageData<MenueReview> pd = mrd.getMenuReview(keyword, pageNo, pageSize);
		return pd;
	}
	/**
	 *	添加菜谱评论
	 * @param Menue
	 * @return MenuReview 对象
	 */
	public boolean addMenueReview(MenueReview menureview) {
		boolean flag = mrd.addMenueReview(menureview);
		return flag;
	}
	/**
	 *	按(评论者)客户id删除菜谱评论
	 * @param c_id
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean delMenueById(int c_id) {
		boolean flag = mrd.delMenueById(c_id);
		return flag;
	}
	/**
	 *	根据菜谱id修改菜谱评论
	 * @param menureview
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean updMenueById(MenueReview menureview) {
		boolean flag = mrd.updMenueById(menureview);
		return flag;
	}
}
