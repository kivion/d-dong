package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Product;
import com.dmall.entity.ProductCollection;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;
/**
 * 商品收藏的相关方法
 * @author 王佳慧
 *
 */
public class ProductCollectionDao {
	/**
	 * 查询所有商品收藏（客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存）
	 * 
	 * @return 商品对象
	 */
	public List<Product> getProduct() {
		String sql = "SELECT customer.c_id, customer.c_name,product.p_name,product.p_price,product.p_stock " + 
				"FROM product_collection " + 
				"INNER JOIN customer ON product_collection.c_id = customer.c_id " + 
				"INNER JOIN product ON product.p_id = product_collection.p_id  ";
		List<Product> list =DBUtil.exeQuery(sql, Product.class, null);
		return list;
	}
	
	/**
	 * 查询所有商品收藏（客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存）
	 * @param pageNo 页码
	 * @param pageSize 当前页的记录数
	 * @return 分页对象
	 */
		public PageData<Product> getProByPage(int pageNo, int pageSize) {
			String sql = "SELECT customer.c_id, customer.c_name,product.p_name,product.p_price,product.p_stock " + 
					"FROM product_collection " + 
					"INNER JOIN customer ON product_collection.c_id = customer.c_id " + 
					"INNER JOIN product ON product.p_id = product_collection.p_id  ";
			PageData<Product> pd =DBUtil.exeQueryByPage(sql, Product.class, pageNo,pageSize,null);
			return pd;
		}
	
/**
 * 模糊查询--通过商品名称查询客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存
 * @param keywords 商品名称关键字
 * @return 商品对象
 */
	public List<Product> getCustomeByKeywords(String keywords) {
		String sql = "SELECT customer.c_id, customer.c_name,product.p_name,product.p_price,product.p_stock " + 
				"FROM product_collection " + 
				"INNER JOIN customer ON product_collection.c_id = customer.c_id " + 
				"INNER JOIN product ON product.p_id = product_collection.p_id  "+
				"WHERE product.p_name like ?";
		List<Product> list = DBUtil.exeQuery(sql, Product.class, "%" + keywords + "%");
		return list;
	}
/**
 * 分页查询-模糊查询--通过商品名称查询客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存
 * @param keywords 商品名称关键字
 * @param pageNo 页码
 * @param pageSize 当前页的记录数
 * @return 分页对象
 */
	public PageData<Product> getCusKeyByPage(String keywords, int pageNo, int pageSize) {
		String sql = "SELECT customer.c_id, customer.c_name,product.p_name,product.p_price,product.p_stock " + 
				"FROM product_collection " + 
				"INNER JOIN customer ON product_collection.c_id = customer.c_id " + 
				"INNER JOIN product ON product.p_id = product_collection.p_id  "+
				"WHERE product.p_name like ?";
		PageData<Product> pd =DBUtil.exeQueryByPage(sql, Product.class, pageNo,pageSize,"%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%");
		return pd;
	}
	
	/**
	 * 添加商品收藏表
	 * 
	 * @param Customer
	 * @return true操作成功，false操作失败
	 */
	public boolean AddCustomer(ProductCollection pc) {
		String sql = "INSERT INTO `product_collection` (`pc_sn`, `p_id`, `c_id`) "
				+ "VALUES (null,?.?  )";
		return DBUtil.exeUpdate(sql,pc.getPc_sn(),pc.getP_id(),pc.getC_id());
	}
	
	/**
	 * 删除信息--->通过商品编号删除
	 * 
	 * @param blog
	 * @return true操作成功，false操作失败
	 */
	public boolean DelCustomer(String pid) {
		String sql = "delete from `product_collection` where p_id=?";
		return DBUtil.exeUpdate(sql,pid);
	}

}
