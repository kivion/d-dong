package com.dmall.test;

import java.util.List;

import org.junit.Test;

import com.dmall.dao.CustomerDao;
import com.dmall.dao.GradeDao;
import com.dmall.entity.Customer;
import com.dmall.entity.Grade;

public class TestGrade {

	/**
	 * 模糊查询--通过编号、姓名、性别、电话号码查询客户
	 */
	@Test
	public void  TestgetGradeByKey() {
		GradeDao bd=new GradeDao();
		List<Grade> list=bd.getGradeByKeywords("");
		list.forEach(System.out::println);
	}
	@Test
	public void  TestgetGradeByrid() {
		GradeDao bd=new GradeDao();
		List<Grade> list=bd.getGradeByrid("r00001");
		list.forEach(System.out::println);
	}
}
