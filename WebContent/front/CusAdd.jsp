<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<title>用户注册</title>

<link rel="stylesheet" href="css/panda/style.css">

</head>

<body>

<div class="dwo">
	<div class="panda">
		<div class="ear"></div>
		<div class="face">
			<div class="eye-shade"></div>
			<div class="eye-white">
				<div class="eye-ball"></div>
			</div>
			<div class="eye-shade rgt"></div>
			<div class="eye-white rgt">
				<div class="eye-ball"></div>
			</div>
			<div class="nose"></div>
			<div class="mouth"></div>
		</div>
		<div class="body"> </div>
		<div class="foot">
			<div class="finger"></div>
		</div>
		<div class="foot rgt">
			<div class="finger"></div>
		</div>
	</div>
	<form class="form-signin" action="../Cuspanda.do?op=cusadd" method="post">
		<div class="hand"></div>
		<div class="hand rgt"></div>
		<h1>用户注册</h1>
		<div class="form-group">
			<input required="required" class="form-control" name="custell">
			<label class="form-label">手机号</label>
		</div>
		<div class="form-group">
			<input id="password" type="password" required="required" class="form-control" name="cuspwd">
			<label class="form-label" >密码</label>
			<p class="alert">注册成功</p>
			<button class="btn" type="submit">注册</button><br><br>
		    
		</div>
	</form>
	
</div>

<script src="js/panda/jquery.min.js"></script>
<script src="js/panda/script.js"></script>


</body>
</html>