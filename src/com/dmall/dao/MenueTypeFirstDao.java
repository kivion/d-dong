package com.dmall.dao;

import com.dmall.entity.MenueTypeFirst;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;
/**
 *	菜谱一级分类数据库访问dao
 * @author Administrator
 *
 */
public class MenueTypeFirstDao extends DBUtil{
	/**
	 *	查询菜谱一级分类信息
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return PagData分页对象
	 */
	public PageData<MenueTypeFirst> getMenuTypeFrst(String keyword,int pageNo,int pageSize){
		String sql = "select * from menu_type_first";
		PageData pd = exeQueryByPage(sql, MenueTypeFirst.class, pageNo, pageSize);
		return pd;
	}
	/**
	 *	增加一级菜谱分类
	 * @param mtf
	 * @return
	 */
	public boolean addMenuTypeFirst(MenueTypeFirst mtf) {
		String sql = "insert into menu_type_first(mtf_id,mtf_name) VALUE(?,?)";
		boolean flag = exeUpdate(sql, mtf.getMtf_id(),mtf.getMtf_name());
		return flag;
	}
	/**
	 *	按一级菜谱分类的id删除
	 * @param mtf_id
	 * @return
	 */
	public boolean delMenuTypeFirst(String mtf_id) {
		String sql = "delete from menu_type_first where mtf_id=?";
		boolean flag = exeUpdate(sql, mtf_id);
		return flag;
	}
	/**
	 *	根据一级菜谱分类的id修改分类学信息
	 * @param mtf_id
	 * @param mtf_name
	 * @return
	 */
	public boolean updMenuTypeFirst(String mtf_id,String mtf_name) {
		String sql = "update menu_type_first set mtf_name=? where mtf_id=?";
		boolean flag = exeUpdate(sql,mtf_name, mtf_id);
		return flag;
	}
	
}
