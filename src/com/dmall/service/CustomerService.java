package com.dmall.service;


import java.util.List;

import com.dmall.dao.CustomerDao;
import com.dmall.entity.Customer;
import com.dmall.entity.Menue;
import com.dmall.pagedata.PageData;
/**
 * 客户业务层
 * @author Administrator
 *
 */
public class CustomerService {

	private CustomerDao cd=new CustomerDao();
	/**
	 * 模糊查询--通过编号、姓名、性别、电话号码查询客户
	 * @param keywords 关键字
	 * @return 对象
	 */
	public List<Customer> getCusByKey(String keywords){
		return cd.getCustomeByKeywords(keywords); 
	}
	
	/**
	 *	分页+模糊查询
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<Customer> getCusKeyByPage(String keyword,int pageNo,int pageSize){
		
		if(pageNo<1) {
			pageNo=1;
		}
		return cd.getCusKeyByPage(keyword, pageNo, pageSize);
	}
	
	/**
	 *	添加客户个人信息
	 * @param Customer
	 * @return Customer 对象
	 */
	public boolean AddCustomer(Customer c) {
		
		return cd.AddCustomer(c);
	}
	/**
	 *	注册
	 * @param Customer
	 * @return Customer 对象
	 */
	public boolean AddCusBytell(Customer c) {
		
		return cd.AddCusBytell(c);
	}
	/**
	 *	删除信息--->通过客户编号删除
	 * @param cusid
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean delCusById(String cusid) {
		return cd.DelCustomer(cusid);
		
	}
	/**
	 *	按客户个人编号修改客户个人信息姓名，性别，电话号码
	 * @param Customer
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean updCusById(Customer c) {
		return cd.updCusById(c);
		
	}
	/**
	 *	通过手机号重置密码
	 * @param Customer
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean updCusPwd(Customer c) {
		return cd.updCusById(c);
		
	}
	/**
	 * 通过账号和密码进行登入
	 * @param useremail 邮箱
	 * @param userpwd 密码
	 * @return 用户对象
	 */
	public Customer login(String cusname,String cuspwd) {
		return cd.getcusByaccountpwd(cusname, cuspwd);
	}
	
}
