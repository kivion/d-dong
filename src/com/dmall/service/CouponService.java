package com.dmall.service;

import java.util.List;
import com.dmall.dao.CouponDao;
import com.dmall.entity.Coupon;
import com.dmall.pagedata.PageData;
/**
 * 优惠券服务层
 * @author yong
 *
 */
public class CouponService {

	//调用dao
	CouponDao cd = new CouponDao();
	
	/**
	 * 优惠券查询操作
	 * @param keywords 关键字
	 * @return list集合
	 */
	public List<Coupon> getCoupon(String keywords){
		return cd.getCouponByName(keywords);	 
	}
	
	/**
	 * 增加操作
	 * @param coupon 
	 * @return true 成功 false 失败
	 */
	public boolean addCoupon(Coupon coupon) {
		return cd.addCoupon(coupon);
	}
	
	/**
	 * 删除操作
	 * @param co_id 优惠券id
	 * @return true 成功 false 失败
	 */
	public boolean delCoupon(int co_id) {
		return cd.delCouponById(co_id);
	}
	
	/**
	 * 分页业务层
	 * @param keywords 优惠券名称关键字
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public PageData<Coupon> getCouponByPage(String keywords,int pageNo,int pageSize){
		//判断页码和页面记录数
		if (pageNo < 1) {
			pageNo=1;
		}
		if (pageSize < 0) {
			pageNo=10;
		}
		
		return cd.getCouponByPage(keywords, pageNo, pageSize);
	}
	
}
