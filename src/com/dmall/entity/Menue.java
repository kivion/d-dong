package com.dmall.entity;



public class Menue {
	private String m_id;
	private String m_name;
	private String m_used;
	private String m_content;
	private String m_file;
	private String m_difficult;
	private String m_time;
	private String c_id;
	private String m_creat;
	private String p_id;
	private int m_sn;
	private String mtn_id;
	
	//和客户关连
		private Customer customer=new Customer();

		public Customer getCustomer() {
			return customer;
		}
		public void setCustomer(Customer customer) {
			this.customer = customer;
		}
		
		public void setC_name(String c_name) {
			this.customer.setC_name (c_name);
		}
	
	
	
	public Menue() {
	}

	public Menue(String m_id, String m_name, String m_used, String m_content, String m_file, String m_difficult,
			String m_time, String c_id, String m_creat, String p_id, int m_sn, String mtn_id) {
		super();
		this.m_id = m_id;
		this.m_name = m_name;
		this.m_used = m_used;
		this.m_content = m_content;
		this.m_file = m_file;
		this.m_difficult = m_difficult;
		this.m_time = m_time;
		this.c_id = c_id;
		this.m_creat = m_creat;
		this.p_id = p_id;
		this.m_sn = m_sn;
		this.mtn_id = mtn_id;
	}


	public Menue(String m_id, String m_name, String m_used, String m_content, String m_file, String m_difficult,
			String m_time, String c_id) {
		super();
		this.m_id = m_id;
		this.m_name = m_name;
		this.m_used = m_used;
		this.m_content = m_content;
		this.m_file = m_file;
		this.m_difficult = m_difficult;
		this.m_time = m_time;
		this.c_id = c_id;
	}
	public Menue(String m_id, String m_name, String m_used, String m_content, String m_file, String m_difficult,
			String m_time, String c_id, String m_creat, String p_id, int m_sn, String mtn_id, Customer customer) {
		super();
		this.m_id = m_id;
		this.m_name = m_name;
		this.m_used = m_used;
		this.m_content = m_content;
		this.m_file = m_file;
		this.m_difficult = m_difficult;
		this.m_time = m_time;
		this.c_id = c_id;
		this.m_creat = m_creat;
		this.p_id = p_id;
		this.m_sn = m_sn;
		this.mtn_id = mtn_id;
		this.customer = customer;
	}
	
	public Menue(String m_id, String m_name, String m_used, String m_content, String m_file, String m_difficult,
			String m_time, String c_id, String m_creat) {
		super();
		this.m_id = m_id;
		this.m_name = m_name;
		this.m_used = m_used;
		this.m_content = m_content;
		this.m_file = m_file;
		this.m_difficult = m_difficult;
		this.m_time = m_time;
		this.c_id = c_id;
		this.m_creat = m_creat;
	}
	public String getM_id() {
		return m_id;
	}

	public void setM_id(String m_id) {
		this.m_id = m_id;
	}

	public String getM_name() {
		return m_name;
	}

	public void setM_name(String m_name) {
		this.m_name = m_name;
	}

	public String getM_used() {
		return m_used;
	}

	public void setM_used(String m_used) {
		this.m_used = m_used;
	}

	public String getM_content() {
		return m_content;
	}

	public void setM_content(String m_content) {
		this.m_content = m_content;
	}

	public String getM_file() {
		return m_file;
	}

	public void setM_file(String m_file) {
		this.m_file = m_file;
	}

	public String getM_difficult() {
		return m_difficult;
	}

	public void setM_difficult(String m_difficult) {
		this.m_difficult = m_difficult;
	}

	public String getM_time() {
		return m_time;
	}

	public void setM_time(String m_time) {
		this.m_time = m_time;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getM_creat() {
		return m_creat;
	}

	public void setM_creat(String m_creat) {
		this.m_creat = m_creat;
	}

	public String getP_id() {
		return p_id;
	}

	public void setP_id(String p_id) {
		this.p_id = p_id;
	}

	public int getM_sn() {
		return m_sn;
	}

	public void setM_sn(int m_sn) {
		this.m_sn = m_sn;
	}

	public String getMtn_id() {
		return mtn_id;
	}

	public void setMtn_id(String mtn_id) {
		this.mtn_id = mtn_id;
	}
	@Override
	public String toString() {
		return "Menue [m_id=" + m_id + ", m_name=" + m_name + ", m_used=" + m_used + ", m_content=" + m_content
				+ ", m_file=" + m_file + ", m_difficult=" + m_difficult + ", m_time=" + m_time + ", c_id=" + c_id
				+ ", m_creat=" + m_creat + ", p_id=" + p_id + ", m_sn=" + m_sn + ", mtn_id=" + mtn_id + ", customer="
				+ customer + "]";
	}


	
}
