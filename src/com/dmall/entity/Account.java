package com.dmall.entity;

public class Account {
	private String account;
	private String password;
	public Account(String account, String password) {
		super();
		this.account = account;
		this.password = password;
	}
	public Account() {
		// TODO Auto-generated constructor stub
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Account [account=" + account + ", password=" + password + "]";
	}
	
}
