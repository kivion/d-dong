package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Address;
import com.dmall.entity.Card;
import com.dmall.entity.Rider;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

public class CardDao extends DBUtil {
	/**
	 * 增加银行卡
	 */
	public boolean addCard(Card card) {
		// 判断card是否为空
		if (card == null) {
			return false;
		}
		// 定义sql语句
		String sql = "INSERT INTO `card` (`cardname`,`cardid`,`cardaddress`,`bank`) VALUES (?,?,?,?)";
		// 调用通用类代码
		boolean flag = exeUpdate(sql, card.getCardname(), card.getCardid(), card.getCardaddress(), card.getBank());
		return flag;
	}

	/**
	 * 删除银行卡
	 */

	public boolean delCardById(int cardid) {
		// 定义sql语句
		String sql = "delete FROM `card` WHERE (`cardid`=?)";
		// 调用DBUtil方法
		boolean flag = exeUpdate(sql, cardid);
		return flag;
	}

	/**
	 * 根据id修改银行
	 * 
	 * @param card
	 * @return true 成功 false 失败
	 */
	public boolean updCardById(Card card) {
		// 定义sql语句
		String sql = "UPDATE `card` SET `cardname`=?, `cardaddress`=?, `bank`=? WHERE (`cardid`=?)";
		// 调用DBUtil方法
		boolean flag = exeUpdate(sql, card.getCardname(), card.getCardaddress(), card.getBank());
		return flag;
	}

	/**
	 * 查询所有信息
	 * 
	 * @param Card
	 * @return flag true操作成功，false操作失败
	 */

	public List<Card> getAll() {
		String sql = "select * from card";
		List<Card> list = exeQuery(sql, Card.class);
		
		
		return list;

	}
	
	public PageData<Card> getCardByPage(String keywords, int pageNo, int pageSize) {
		// 声明sql语句
		String sql = "select * from `card` where cardname like ?";
		// 调用DBUtil方法
		PageData<Card> pd = exeQueryByPage(sql, Card.class, pageNo, pageSize, "%" + keywords + "%");

		return pd;
	}
}
