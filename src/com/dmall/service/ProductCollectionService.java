package com.dmall.service;

import java.util.List;

import com.dmall.dao.ProductCollectionDao;
import com.dmall.entity.Product;
import com.dmall.entity.ProductCollection;
import com.dmall.pagedata.PageData;

/**
 * 商品业务层-业务层
 * 
 * @author 蔡陈鹏
 */
public class ProductCollectionService {
	ProductCollectionDao pcd = new ProductCollectionDao();

	/**
	 * 查询所有商品收藏（客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存）
	 * 
	 * @return 商品对象
	 */
	public List<Product> getProduct(String keyword) {
		return pcd.getProduct();
	}

	/**
	 * 查询所有商品收藏（客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存）
	 * 
	 * @param pageNo   页码
	 * @param pageSize 当前页的记录数
	 * @return 分页对象
	 */
	public PageData<Product> getProByPage(int pageNo, int pageSize) {

		if (pageNo < 1) {
			pageNo = 1;
		}
		return pcd.getProByPage(pageNo, pageSize);
	}

	/**
	 * 模糊查询--通过商品名称查询客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存
	 * 
	 * @param keywords 商品名称关键字
	 * @return 商品对象
	 */
	public List<Product> getCustomeByKeywords(String keyword) {
		return pcd.getCustomeByKeywords(keyword);
	}

	/**
	 * 分页查询-模糊查询--通过商品名称查询客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存
	 * 
	 * @param keywords 商品名称关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的记录数
	 * @return 分页对象
	 */
	public PageData<Product> getCusKeyByPage(String keyword,int pageNo, int pageSize) {
		return pcd.getCusKeyByPage(keyword,pageNo,pageSize);
	}

	/**
	 * 添加商品
	 * 
	 * @param product 商品对象
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean addProduct(ProductCollection pc) {

		return pcd.AddCustomer(pc);
	}

	/**
	 * 按商品id删除商品内容
	 * 
	 * @param p_id
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean delProduct(String p_id) {
		return pcd.DelCustomer(p_id);

	}

}