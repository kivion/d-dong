package com.dmall.service;

import java.util.List;

import com.dmall.dao.ProductTypeFirstDao;
import com.dmall.entity.Product;
import com.dmall.entity.ProductTypeFirst;
import com.dmall.pagedata.PageData;

/**
 * 商品大分类-业务层
 * 
 * @author 蔡陈鹏
 */
public class ProductTypeFirstService {
	ProductTypeFirstDao ptfd = new ProductTypeFirstDao();

	/**
	 * 分页+模糊查询
	 * 
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData分页对象
	 */
	public PageData<ProductTypeFirst> getProductTypeFirst(String keyword, int pageNo, int pageSize) {

		if (pageNo < 1) {
			pageNo = 1;
		}
		return ptfd.getProductTypeFirst(keyword, pageNo, pageSize);
	}

	/**
	 * 添加商品大分类
	 * 
	 * @param producttypeFirst 商品大分类对象
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean addProductTypeFirst(ProductTypeFirst producttypeFirst) {

		return ptfd.addProductTypeFirst(producttypeFirst);
	}
	/**
	 * 查询商品（按大类别）
	 * @param ptf_name 大类别名字
	 * @return	Product 对象
	 */
	public List<Product> getProductByTypeFirst(String ptf_name) {

		return ptfd.getProductByTypeFirst(ptf_name);
		}

	/**
	 * 按商品大分类id删除商品大分类内容
	 * 
	 * @param ptf_id 商品大分类id
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean delProductTypeFirst(int ptf_id) {
		return ptfd.delProductTypeFirstById(ptf_id);

	}

	/**
	 * 按商品大分类id修改商品大分类
	 * 
	 * @param producttypeFirst 商品大分类对象
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean updProductTypeFirst(ProductTypeFirst producttypeFirst) {
		return ptfd.updProductTypeFirstById(producttypeFirst);

	}
}