package com.dmall.service;


import com.dmall.dao.RiderReviewDao;
import com.dmall.entity.Customer;
import com.dmall.entity.Rider;
import com.dmall.pagedata.PageData;

/**
 *	骑手业务层
 * @author Administrator
 */
public class RiderService {
	RiderReviewDao rd = new RiderReviewDao();
	/**
	 *	分页+模糊查询  
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<Rider> getRider(String keyword,int pageNo,int pageSize){
		
		if(pageNo<1) {
			pageNo=1;
		}
		return rd.getRider(keyword, pageNo, pageSize);
	}
	/**
	 *	添加骑手
	 * @param Rider
	 * @return Rider 对象
	 */
	public boolean addRider(Rider rider) {
		
		return rd.addRider(rider);
	}
	/**
	 *	按骑手id删除骑手
	 * @param r_id
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean delRiderById(String r_id) {
		return rd.delRiderById(r_id);
		
	}
	/**
	 *	按骑手id修改菜谱
	 * @param Rider
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean updRiderById(Rider rider) {
		return rd.updRiderById(rider);
		
	}
	/**
	 * 通过账号和密码进行登入
	 * @param useremail 邮箱
	 * @param userpwd 密码
	 * @return 用户对象
	 */
	public Rider login(String rid,String rtell) {
		return rd.getriderlogin(rid, rtell);
	}
}
