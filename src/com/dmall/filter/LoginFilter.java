package com.dmall.filter;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter(dispatcherTypes = {DispatcherType.REQUEST},urlPatterns = {"/back/index.jsp"})
public class LoginFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		System.out.println("filter");
		if (req.getSession().getAttribute("username")!=null && !("".equals(req.getSession().getAttribute("username"))) ) {
			System.out.println("chai ");
			System.out.println(req.getSession().getAttribute("username"));
			System.out.println("".equals(req.getSession().getAttribute("username")));
			chain.doFilter(request, response);
			
		} else {
			res.sendRedirect("/dmall/back/login.jsp");
		}
	}
}
