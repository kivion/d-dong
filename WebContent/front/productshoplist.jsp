<%@page import="com.dmall.entity.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!-- 加入 taglib 标记-->
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!doctype html>
<html class="no-js" lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>叮咚到家 - 商品列表</title>
<!--在pageScope内存储了一个basepath，值设为${pageContext.request.contextPath}，实际上就是当前工程的上下文路径-->
<p:set var="basepath" scope="page"
	value="${pageContext.request.contextPath}"></p:set>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon"
	href="front/assets/img/favicon.ico">

<!-- CSS 
    ========================= -->
<!--bootstrap min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/bootstrap.min.css">
<!--owl carousel min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/owl.carousel.min.css">
<!--slick min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/slick.css">
<!--magnific popup min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/magnific-popup.css">
<!--font awesome css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/font.awesome.css">
<!--ionicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/ionicons.min.css">
<!--linearicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/linearicons.css">
<!--animate css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/animate.css">
<!--jquery ui min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/jquery-ui.min.css">
<!--slinky menu css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/slinky.menu.css">
<!--plugins css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/plugins.css">

<!-- Main Style CSS -->
<link rel="stylesheet" href="${basepath}/front/assets/css/style.css">
<!--layUI的css-->
<link rel="stylesheet" href="${basepath}/front/layui/css/layui.css"
	media="all">
<style>
.widget_list #amount {
	width: 70px;
}
</style>
<!--modernizr min js here-->
<script src="${basepath}/front/assets/js/vendor/modernizr-3.7.1.min.js"></script>

</head>

<body>
	<!--头部开始-->
	<header>
		<div class="main_header">
			<div class="header_top">
				<div class="container">
					<div class="row align-items-center">

						<!--导航栏-->
						<div class="col-lg-6 col-md-6">
							<div class="language_currency">
								<ul>
									<li class="language"><a href="#"> 语言 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_language">
											<li><a href="#">中文</a></li>
											<li><a href="#">英语</a></li>
										</ul></li>
									<li class="currency"><a href="#"> 简繁体 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_currency">
											<li><a href="#">简体</a></li>
											<li><a href="#">繁体</a></li>
										</ul></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="header_social text-right">
								<ul>
									<li><a href="#"><i class="ion-social-twitter"></i></a></li>
									<li><a href="#"><i
											class="ion-social-googleplus-outline"></i></a></li>
									<li><a href="#"><i class="ion-social-youtube-outline"></i></a>
									</li>
									<li><a href="#"><i class="ion-social-facebook"></i></a></li>
									<li><a href="#"><i
											class="ion-social-instagram-outline"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_middle">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-2">
							<div class="logo">
								<a href="${basepath}/producthome.do"><img
									src="${basepath}/front/assets/img/logo/logo.jpg" alt=""></a>
							</div>
						</div>
						<div class="col-lg-10">
							<div class="header_right_info">
								<!--导航栏左边的类别-->
								<div class="search_container">
									<form action="#">
										<div class="hover_category">
											<select class="select_option" name="select" id="categori1">
												<option selected value="1">选择类别</option>
												<option value="2">水果</option>
												<option value="3">蔬菜</option>
												<option value="4">肉禽蛋品</option>
												<option value="5">海鲜水产</option>
												<option value="6">熟食卤味</option>
												<option value="7">粮油调味</option>
												<option value="8">休闲零食</option>
												<option value="9">冰品面点</option>
												<option value="10">牛奶面包</option>
												<option value="11">酒水冲饮</option>
											</select>
										</div>
										<div class="search_box">
											<input placeholder="国产红心火龙果0.99元/个..." type="text"
												id="keywords" name="keywords">
											<button type="submit">
												<span class="lnr lnr-magnifier"></span>
											</button>
										</div>
									</form>
								</div>
								<div class="header_account_area">
									<div class="header_account_list register">
										<ul class="layui-nav">
											<!--layUI的状态头像登录-->
											<li class="layui-nav-item" lay-unselect=""><a
												href="${basepath}/front/my-account.jsp"><img
													src="${basepath}/front/assets/img/icon/1.jpg"
													class="layui-nav-img">佚名</a>
												<dl class="layui-nav-child">
													<dd>
														<a href="javascript:;">修改信息</a>
													</dd>
													<dd>
														<a href="javascript:;">账号管理</a>
													</dd>
													<dd>
														<a href="javascript:;">退出</a>
													</dd>
												</dl></li>
											<!--<li>
													<a href="login.html">注册</a>
												</li>
												<li><span>/</span></li>
												<li>
													<a href="login.html">登录</a>
												</li>-->
										</ul>
										<!--<script>
												$('.layui-nav').css('background','#fff !important');
												$('.layui-nav .layui-nav-item a').css('color','#000 !important');
											</script>-->
									</div>
									<div class="header_account_list header_wishlist">
										<a href="收藏页面.html"><span class="lnr lnr-heart"></span> <span
											class="item_count">5</span> </a>
									</div>
									<div class="header_account_list  mini_cart_wrapper">
										<a href="${basepath}/front/Cart.jsp"><span
											class="lnr lnr-cart"></span><span class="item_count">2</span></a>
										<!--mini cart-->
										<div class="mini_cart">
											<div class="cart_gallery">
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Primis In Faucibus</a>
														<p>
															1 x <span> $65.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product2.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Letraset Sheets</a>
														<p>
															1 x <span> $60.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
											</div>
											<div class="mini_cart_table">
												<div class="cart_table_border">
													<div class="cart_total mt-10">
														<span>总计:</span> <span class="price">$125.00</span>
													</div>
												</div>
											</div>
											<div class="mini_cart_footer">
												<div class="cart_button">
													<a href="${basepath}/front/Cart.jsp"><i
														class="fa fa-shopping-cart"></i> 前往购物车</a>
												</div>
												<div class="cart_button">
													<a href="${basepath}/front/Cart.jsp"><i
														class="fa fa-sign-in"></i> ...</a>
												</div>

											</div>
										</div>
										<!--mini cart end-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_bottom sticky-header">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-3">
							<div class="categories_menu">
								<div class="categories_title">
									<h2 class="categori_toggle">所有商品</h2>
								</div>
								<div class="categories_menu_toggle">
									<ul>
										<li class="menu_item_children"><a href="#part1">蔬菜<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu">
												<li class="menu_item_children"><a href="#">苗菜</a>
													<ul class="categorie_sub_menu">
														<li><a href="">叶菜</a></li>
														<li><a href="">根茎菜</a></li>
														<li><a href="">调味菜</a></li>
														<li><a href="">豆瓜果茄</a></li>
														<li><a href="">菌菇</a></li>
														<li><a href="">方便菜</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part1">水果<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_3">
												<li class="menu_item_children"><a href="#">热带水果</a>
													<ul class="categorie_sub_menu">
														<li><a href="">进口水果</a></li>
														<li><a href="">瓜类</a></li>
														<li><a href="">常见类水果</a></li>
													</ul></li>
												<!-- 	<li class="menu_item_children"><a href="#">qqqqqq</a>
													<ul class="categorie_sub_menu">
														<li><a href="">q</a></li>
														<li><a href="">q</a></li>
														<li><a href="">2</a></li>
														<li><a href="">q</a></li>
													</ul></li>
												 -->
											</ul></li>
										<li class="menu_item_children"><a href="#part1">肉禽蛋品<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">猪肉类</a>
													<ul class="categorie_sub_menu">
														<li><a href="">鲜家禽</a></li>
														<li><a href="">蛋</a></li>
														<li><a href="">牛排</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part2">海鲜水产<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">鱼</a>
													<ul class="categorie_sub_menu">
														<li><a href="">虾</a></li>
														<li><a href="">贝</a></li>
														<li><a href="">蟹</a></li>
													</ul></li>
											</ul></li>
										<li><a href="#part3">熟食卤味</a></li>
										<li><a href="#part4">休闲零食</a></li>
										<li><a href="#part5">粮油调味</a></li>
										<li><a href="#">冰品面点</a></li>
										<li><a href="#">牛奶面包</a></li>
										<li><a href="#">酒水冲饮</a></li>

										<li id="cat_toggle" class="has-sub"><a href="#">>更多</a>
											<ul class="categorie_sub">
												<li><a href="#">暂时没有更多了，小咚会继续努力的~</a></li>
											</ul></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<!--main menu start-->
							<div class="main_menu menu_position">
								<nav>
									<ul>
										<li><a class="active" href="${basepath}/front/index.jsp">主页</a></li>
										<li><a href="${basepath}/front/Menu_Home.jsp">菜谱 <!--<i class="fa fa-angle-down"></i>--></a>
										</li>
										<li><a href="关于我们.html">关于我们</a></li>
										<li><a href="联系我们.html">联系我们</a></li>
									</ul>
								</nav>
							</div>
							<!--main menu end-->
						</div>
						<div class="col-lg-3">
							<div class="call-support">
								<p>
									<a href="tel:(08)8399608">(08)8399 608</a> 用户反馈
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!--header area end-->

	<!--breadcrumbs area start-->
	<div class="breadcrumbs_area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="breadcrumb_content">
						<h3>商品中心</h3>
						<ul>
							<li><a href="index.html">小咚</a></li>
							<li>这是更详细的商品哟主人</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--breadcrumbs area end-->

	<!--shop  area start-->
	<div class="shop_area shop_reverse mt-70 mb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-12">
					<!--sidebar widget start-->
					<aside class="sidebar_widget">
						<div class="widget_inner">
							<div class="widget_list widget_categories">
								<h3>热销商品类型</h3>
								<ul>
									<li class="widget_sub_categories sub_categories1"><a
										href="javascript:void(0)">热带水果</a>
										<ul class="widget_dropdown_categories dropdown_categories1">
											<li><a href="#">西红柿</a></li>
											<li><a href="#">芭蕉</a></li>
											<li><a href="#">芭乐</a></li>
										</ul></li>
									<li class="widget_sub_categories sub_categories2"><a
										href="javascript:void(0)">卤味熟食</a>
										<ul class="widget_dropdown_categories dropdown_categories2">
											<li><a href="#">卤肉</a></li>
											<li><a href="#">鸭脖</a></li>
											<li><a href="#">熟面饼</a></li>
										</ul></li>
									<li class="widget_sub_categories sub_categories3"><a
										href="javascript:void(0)">蔬菜</a>
										<ul class="widget_dropdown_categories dropdown_categories3">
											<li><a href="#">窝瓜</a></li>
											<li><a href="#">豌豆</a></li>
											<li><a href="#">白菜</a></li>
										</ul></li>
								</ul>
							</div>
							<div class="widget_list widget_filter">
								<h3>价格区间</h3>
								<form action="#">
									<div id="slider-range"></div>
									<!--<button type="submit">Filter</button>-->
									<input type="text" name="text" id="amount" />

								</form>

							</div>
							<div class="widget_list widget_color">
								<h3>Select By Color</h3>
								<ul>
									<li><a href="#">Black <span>(6)</span></a></li>
									<li><a href="#"> Blue <span>(8)</span></a></li>
									<li><a href="#">Brown <span>(10)</span></a></li>
									<li><a href="#"> Green <span>(6)</span></a></li>
									<li><a href="#">Pink <span>(4)</span></a></li>

								</ul>
							</div>
							<div class="widget_list widget_color">
								<h3>Select By SIze</h3>
								<ul>
									<li><a href="#">S <span>(6)</span></a></li>
									<li><a href="#"> M <span>(8)</span></a></li>
									<li><a href="#">L <span>(10)</span></a></li>
									<li><a href="#"> XL <span>(6)</span></a></li>
									<li><a href="#">XLL <span>(4)</span></a></li>

								</ul>
							</div>
							<div class="widget_list widget_manu">
								<h3>Manufacturer</h3>
								<ul>
									<li><a href="#">Brake Parts <span>(6)</span></a></li>
									<li><a href="#">Accessories <span>(10)</span></a></li>
									<li><a href="#">Engine Parts <span>(4)</span></a></li>
									<li><a href="#">hermes <span>(10)</span></a></li>
									<li><a href="#">louis vuitton <span>(8)</span></a></li>

								</ul>
							</div>
							<div class="widget_list tags_widget">
								<h3>Product tags</h3>
								<div class="tag_cloud">
									<a href="#">Men</a> <a href="#">Women</a> <a href="#">Watches</a>
									<a href="#">Bags</a> <a href="#">Dress</a> <a href="#">Belt</a>
									<a href="#">Accessories</a> <a href="#">Shoes</a>
								</div>
							</div>
							<div class="widget_list banner_widget">
								<div class="banner_thumb">
									<a href="#"><img src="assets/img/bg/banner17.jpg" alt=""></a>
								</div>
							</div>
						</div>
					</aside>
					<!--sidebar widget end-->
				</div>
				<div class="col-lg-9 col-md-12">
					<!--shop wrapper start-->
					<!--shop toolbar start-->
					<div class="shop_toolbar_wrapper">
						<div class="shop_toolbar_btn">

							<button data-role="grid_3" type="button" class=" btn-grid-3"
								data-toggle="tooltip" title="3"></button>

							<button data-role="grid_4" type="button" class=" btn-grid-4"
								data-toggle="tooltip" title="4"></button>

							<button data-role="grid_list" type="button"
								class="active btn-list" data-toggle="tooltip" title="List"></button>
						</div>
						<div class=" niceselect_option">
							<form class="select_option" action="#">
								<select name="orderby" id="short">
									<option selected value="1" onchange="change()" id="selecttype">按类别&nbsp;&nbsp;&nbsp;&nbsp;</option>
									<option value="2">水果</option>
									<option value="3">蔬菜</option>
									<option value="4">肉禽蛋品</option>
									<option value="5">海鲜水产</option>
									<option value="6">熟食卤味</option>
									<option value="7">休闲零食</option>
									<option value="8">粮油调味</option>
									<option value="9">冰品面点</option>
									<option value="10">牛奶面包</option>
									<option value="11">酒水冲饮</option>
								</select>
							</form>
						</div>

						<div class="page_amount">
							<p>共 ${fn:length(list)} 条记录</p>
						</div>
					</div>
					<!--shop toolbar end-->
					<div class="row shop_wrapper grid_list">
						<p:if test="${list!=null}">
							<p:forEach items="${list}" var="product">
								<div class="col-12 ">
									<div class="single_product">
										<div class="product_thumb">
											<a class="primary_img"
												href="${basepath}/front/productdetails.jsp?p_id=${product.p_id}"><img
												src="${basepath}/picture/${product.p_photo}" alt=""></a> <a
												class="secondary_img"
												href="${basepath}/front/productdetails.jsp?p_id=${product.p_id}">
												<!-- <img src="assets/img/product/productbig7.jpg" alt=""> -->
											</a>
											<div class="label_product">
												<span class="label_sale">${product.p_status==0? "已上架":"未上架"}</span>
											</div>
											<div class="action_links">
												<ul>
													<li class="add_to_cart"><a href="#" title="加入购物车"><span
															class="lnr lnr-cart"></span></a></li>
													<li class="quick_button"><a href="#"
														data-toggle="modal" data-target="#modal_box"
														title="快速预览（无效）"> <span class="lnr lnr-magnifier"></span>
													</a></li>
													<li class="wishlist"><a href="#" title="加入收藏"><span
															class="lnr lnr-heart"></span></a></li>
													<!--<li class="compare">
													<a href="#" title="进行比较"><span class="lnr lnr-sync"></span></a>
												</li>-->
												</ul>
											</div>
										</div>
										<!-- <div class="product_content grid_content">
									<h4 class="product_name">
										<a href="product-details.html">Proin Lectus Ipsum</a>
									</h4>
									<p>
										<a href="#">Fruits</a>
									</p>
									<div class="price_box">
										<span class="current_price">$44.00</span> <span
											class="old_price">$332.00</span>
									</div>
								</div> -->
										<div class="product_content list_content">
											<h4 class="product_name">
												<a
													href="${basepath}/front/productdetails.jsp?p_id=${product.p_id}">${product.p_name}</a>
											</h4>
											<p>
												<a href="#"></a>
											</p>
											<div class="price_box">
												<span class="current_price">${product.p_price}元</span>
											</div>
											<div class="product_desc">
												<p>${product.p_detaill}</p>
											</div>
											<div class="action_links list_action_right">
												<ul>
													<li class="add_to_cart"><a href="#" title="购物车+1">加入购物车</a></li>
													<li class="quick_button"><a href="#"
														data-toggle="modal" data-target="#modal_box" title="快速预览">
															<span class="lnr lnr-magnifier"></span>
													</a></li>
													<li class="wishlist"><a href="#" title="收藏+1"><span
															class="lnr lnr-heart"></span></a></li>
													<!-- <li class="compare"><a href="#" title="进行比较"><span
													class="lnr lnr-sync"></span></a></li> -->
												</ul>
											</div>
										</div>
									</div>
								</div>
							</p:forEach>
						</p:if>
					</div>

					<div class="shop_toolbar t_bottom">
						<div class="pagination">
							<ul>
								<li class="current">更多</li>
								<li><a href="#">>></a></li>
							</ul>
						</div>
					</div>
					<!--shop toolbar end-->
					<!--shop wrapper end-->
				</div>
			</div>
		</div>
	</div>
	<!--shop  area end-->

	<!--footer area start-->
	<footer class="footer_widgets">
		<div class="footer_top">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-7">
						<div class="widgets_container contact_us">
							<div class="footer_logo">
								<a href="index.html"><img src="assets/img/logo/logo.png"
									alt=""></a>
							</div>
							<p class="footer_desc">服务、质量、效率</p>
							<p>
								<span>Address:</span> xmzretc
							</p>
							<p>
								<span>Email:</span> <a href="#">760008559@qq.com</a>
							</p>
							<p>
								<span>Call us:</span> <a href="tel:(08)23456789">(08) 23 456
									789</a>
							</p>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关于叮咚到家</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">了解叮咚</a></li>
									<li><a href="#">加入叮咚</a></li>
									<li><a href="#">投资者关系</a></li>
									<li><a href="#">团队信息</a></li>
									<li><a href="contact.html">联系我们</a></li>
									<li><a href="#">廉洁举报</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关注我们</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">新浪微博</a></li>
									<li><a href="#">官方微信</a></li>
									<li><a href="#">Q群加入</a></li>
									<li><a href="#">公益基金会</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>特色服务</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">兑换券码领取</a></li>
									<li><a href="#">礼物码</a></li>
									<li><a href="#">防伪查询</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4">
						<div class="widgets_container widget_menu">
							<h3>帮助中心</h3>
							<div class="footer_menu">
								<ul>
									<li><a href="#">账户管理</a></li>
									<li><a href="#">购物指南</a></li>
									<li><a href="#">订单操作</a></li>
									<li><a href="#">售后政策</a></li>
									<li><a href="#">自助服务</a></li>
									<li><a href="#">相关下载</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer_bottom">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-7">
						<div class="copyright_area">
							<!--<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a>-->
							<p>Copyright &copy; 2020.闽ICP证110xxx号 闽ICP备10046xxx号
								闽公网安备11010802020 号</p>
						</div>
					</div>
					<div class="col-lg-6 col-md-5">
						<div class="footer_payment">
							<ul>
								<li><a href="#"><img src="assets/img/icon/v-logo-1.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-2.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-3.png"
										alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--footer area end-->

	<!-- JS
============================================ -->
	<!--jquery min js-->
	<script src="${basepath}/front/assets/js/vendor/jquery-3.4.1.min.js"></script>
	<!--popper min js-->
	<script src="${basepath}/front/assets/js/popper.js"></script>
	<!--bootstrap min js-->
	<script src="${basepath}/front/assets/js/bootstrap.min.js"></script>
	<!--owl carousel min js-->
	<script src="${basepath}/front/assets/js/owl.carousel.min.js"></script>
	<!--slick min js-->
	<script src="${basepath}/front/assets/js/slick.min.js"></script>
	<!--magnific popup min js-->
	<script src="${basepath}/front/assets/js/jquery.magnific-popup.min.js"></script>
	<!--counterup min js-->
	<script src="${basepath}/front/assets/js/jquery.counterup.min.js"></script>
	<!--jquery countdown min js-->
	<script src="${basepath}/front/assets/js/jquery.countdown.js"></script>
	<!--jquery ui min js-->
	<script src="${basepath}/front/assets/js/jquery.ui.js"></script>
	<!--jquery elevatezoom min js-->
	<script src="${basepath}/front/assets/js/jquery.elevatezoom.js"></script>
	<!--isotope packaged min js-->
	<script src="${basepath}/front/assets/js/isotope.pkgd.min.js"></script>
	<!--slinky menu js-->
	<script src="${basepath}/front/assets/js/slinky.menu.js"></script>
	<!-- Plugins JS -->
	<script src="${basepath}/front/assets/js/plugins.js"></script>

	<!-- Main JS -->
	<script src="${basepath}/front/assets/js/main.js"></script>
	<!--layUI的js-->
	<script src="${basepath}/front/layui/layui.js" charset="utf-8"></script>
	<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
	<script>
		layui.use('element', function() {
			var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

			//监听导航点击
			element.on('nav(demo)', function(elem) {
				//console.log(elem)
				layer.msg(elem.text());
			});
		});
		function change() {
			$.ajax({
				url : '${basepath}/productshoplist.do',
				type : 'post',
				data: {
					"pft_name":ptf_name
				},
				success : function(res) {
					console.log(res.msg);
				}}
			}
		})
	</script>
</body>

</html>