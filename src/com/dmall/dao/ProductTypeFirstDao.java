package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Product;
import com.dmall.entity.ProductTypeFirst;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

/**
 * 商品大分类-Dao
 * 
 * @author 蔡陈鹏
 */
public class ProductTypeFirstDao extends DBUtil {
	/**
	 * 模糊查询-关键字 
	 * 
	 * @author ccp 蔡陈鹏
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData分页对象
	 */
	public List<ProductTypeFirst> getProductTypeFirst(String keywords) {

		String sql = "SELECT product_type_first.ptf_id,product_type_first.ptf_name FROM product_type_first WHERE product.ptf_name LIKE ?";

		List<ProductTypeFirst> list = exeQuery(sql, ProductTypeFirst.class, "%" + keywords + "%");
		return list;
	}
	/**
	 * 模糊查询-大分类名字
	 * 
	 * @author ccp 蔡陈鹏
	 * @param ptf_name  大分类名字
	 * @return Product 商品对象
	 */
	public List<Product> getProductByTypeFirst(String ptf_name) {

		String sql = "SELECT product.p_id,product.p_name,product.p_stock,product.p_photo,product.p_price,product_type_next.ptn_name,product_type_next.ptn_id FROM product_type_next INNER JOIN product_type_first ON product_type_first.ptf_id = product_type_next.ptf_id INNER JOIN product ON product_type_next.ptn_id = product.ptn_id WHERE product_type_first.ptf_name = ?";

		List<Product> list = exeQuery(sql, ProductTypeFirst.class, ptf_name);
		return list;
	}

	/**
	 * 模糊查询 + 分页
	 * 
	 * @author ccp 蔡陈鹏
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData分页对象
	 */
	public PageData<ProductTypeFirst> getProductTypeFirst(String keywords, int pageNo, int pageSize) {

		String sql = "SELECT product_type_first.ptf_id,product_type_first.ptf_name FROM product_type_first WHERE product.ptf_name LIKE ?";

		PageData<ProductTypeFirst> pd = exeQueryByPage(sql, ProductTypeFirst.class, pageNo, pageSize,
				"%" + keywords + "%");
		return pd;
	}

	/**
	 * 增加大分类
	 * 
	 * @author ccp
	 * @param producttypeFirst 商品的大分类对象
	 * @return flag 布尔类型，true操作成功，false操作失败
	 */
	public boolean addProductTypeFirst(ProductTypeFirst producttypeFirst) {
		// sql语句
		String sql = "INSERT INTO `product_type_first` (`ptf_id`, `ptf_name`, `ptf_sn`) VALUES (?,?,null)";
		// 调用增加方法
		boolean flag = exeUpdate(sql, producttypeFirst.getPtf_id(), producttypeFirst.getPtf_name());
		return flag;
	}

	/**
	 * 按商品大分类的编号id删除商品大分类
	 * 
	 * @author ccp
	 * @param ptf_id 商品大分类编号
	 * @return flag 布尔类型，true操作成功，false操作失败
	 */
	public boolean delProductTypeFirstById(int ptf_id) {
		String sql = "delete FROM `product_type_first` WHERE (`ptf_id`=?)";
		boolean flag = exeUpdate(sql, ptf_id);
		return flag;
	}

	/**
	 * 按商品大分类的编号id修改商品大分类名称
	 * 
	 * @author ccp
	 * @param productreview 商品大分类对象
	 * @return flag true操作成功，false操作失败
	 */
	public boolean updProductTypeFirstById(ProductTypeFirst producttypeFirst) {
		String sql = "UPDATE `product_type_first` SET  `ptf_name`=? WHERE (`ptf_id`=?)";
		boolean flag = exeUpdate(sql, producttypeFirst.getPtf_name());
		return flag;
	}
}
