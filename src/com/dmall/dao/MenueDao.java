package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Customer;
import com.dmall.entity.Menue;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

public class MenueDao extends DBUtil{

	/**
	 *	分页+模糊查询
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<Menue> getMenu(String keyword,int pageNo,int pageSize){
		
		String sql = "select * from `menu` where m_name like ? or m_used like ? or m_content like ?";
		PageData pd = exeQueryByPage(sql, Menue.class, pageNo, pageSize,"%"+keyword+"%","%"+keyword+"%","%"+keyword+"%");

		return pd;
	}
	/**
	 * 通过关键字模糊查询
	 * @param keywords
	 * @return
	 */
	public List<Menue> getMenuByKeywords(String keywords) {
		String sql = "SELECT menu.m_id, menu.m_name, menu.m_used, menu.m_content, menu.m_file, menu.m_difficult, menu.m_time, menu.m_creat, customer.c_name  " + 
				"FROM menu INNER JOIN customer ON menu.c_id = customer.c_id  " + 
				"WHERE menu.m_name LIKE ? OR menu.m_used LIKE ? OR menu.m_content LIKE ? ";
		List<Menue> list = DBUtil.exeQuery(sql, Menue.class, "%"+keywords+"%","%"+keywords+"%","%"+keywords+"%");
		return list;
	}
	/**
	 * 通过用户变化查询编写的菜谱。按时间降序
	 * @param keywords
	 * @return
	 */
	public List<Menue> getMenuPersonal(String cid) {
		String sql = "SELECT menu.m_id, menu.m_name, menu.m_used, menu.m_content, menu.m_file, menu.m_difficult, menu.m_time, menu.m_creat, customer.c_name  " + 
				"FROM menu INNER JOIN customer ON menu.c_id = customer.c_id  " + 
				"WHERE   customer.c_id=? order BY menu.m_creat desc";
		List<Menue> list = DBUtil.exeQuery(sql, Menue.class, cid);
		return list;
	}
	/**
	 *  查询--按照菜谱编号来查询
	 * @param m_id
	 * @return
	 */
	public List<Menue> getMenuBymid(String mid) {
		String sql = "SELECT menu.m_id, menu.m_name, menu.m_used, menu.m_content, menu.m_file, menu.m_difficult, menu.m_time, menu.m_creat, customer.c_name  " + 
				"FROM menu INNER JOIN customer ON menu.c_id = customer.c_id  " + 
				"WHERE menu.m_id =?";
		List<Menue> list = DBUtil.exeQuery(sql, Menue.class,mid );
		return list;
	}
	/**
	 *	添加菜谱
	 * @param Menue
	 * @return Menu 对象
	 */
	public boolean addMenue(Menue menu) {
		String sql = "INSERT INTO `menu` (`m_id`, `m_name`,`m_used`,`m_content`,`m_file`,`m_difficult`,`m_time`,`c_id`,`m_creat`) VALUES (?,?,?,?,?,?,?,?,now())";
		boolean flag = exeUpdate(sql, menu.getM_id(),menu.getM_name(),menu.getM_used(),menu.getM_content(),menu.getM_file(),menu.getM_difficult(),menu.getM_time(),menu.getC_id());
		return flag;
	}
	/**
	 *	按菜谱id删除菜谱内容
	 * @param m_id
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean delMenueById(int m_id) {
		String sql = "delete FROM `menue` WHERE (`m_id`=?)";
		boolean flag = exeUpdate(sql, m_id);
		return flag;
	}
	/**
	 *	按菜谱id修改菜谱
	 * @param Menue
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean updMenueById(Menue menu) {
		String sql = "UPDATE `menue` SET `m_name`=?, `m_used`=?, `m_content`=?,`m_file = ?`,`m_difficult`=?,`m_time=?` WHERE (`m_id`=?)";
		System.out.println(menu);
		boolean flag = exeUpdate(sql, menu.getM_name(),menu.getM_used(),menu.getM_content(),menu.getM_file(),menu.getM_difficult(),menu.getM_time());
		return flag;
	}
	
}
