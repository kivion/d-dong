package com.dmall.service;

import java.util.List;

import com.dmall.dao.ProductReviewDao;
import com.dmall.entity.ProductReview;
import com.dmall.pagedata.PageData;

/**
 * 商品评论-业务层
 * 
 * @author 蔡陈鹏
 */
public class ProductReviewService {
	ProductReviewDao prd = new ProductReviewDao();

	/**
	 * 模糊查询
	 * 
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData分页对象
	 */
	public List<ProductReview> getProduct(String keyword) {
		return prd.getProductReview(keyword);
	}

	/**
	 * 分页+模糊查询
	 * 
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData分页对象
	 */
	public PageData<ProductReview> getProductReview(String keyword, int pageNo, int pageSize) {

		if (pageNo < 1) {
			pageNo = 1;
		}
		return prd.getProductReview(keyword, pageNo, pageSize);
	}

	/**
	 * 添加商品评论
	 * 
	 * @param productReview 商品评论对象
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean addProductReview(ProductReview productReview) {

		return prd.addProductReview(productReview);
	}

	/**
	 * 按商品评论id删除商品评论内容
	 * 
	 * @param pr_id
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean delProductReview(int pr_id) {
		return prd.delProductReviewById(pr_id);

	}

	/**
	 * 按商品评论id修改商品评论
	 * 
	 * @param Product
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean updProductReview(ProductReview productReview) {
		return prd.updProductReviewById(productReview);

	}
}