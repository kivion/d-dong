package com.dmall.dao;

import java.util.List;
import com.dmall.entity.Order;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

/**
 * 订单dao
 * 
 * @author yong
 *
 */
public class OrderDao extends DBUtil { 

	/**
	 * 订单增加的方法
	 * 
	 * @param order 订单对象
	 * @return true 成功 false 失败
	 */
	public boolean addPayOrder(Order order) {
		// 判断order是否为空
		if (order == null) {
			return false;
		}
		// 定义sql语句
		String sql = "INSERT INTO `order` (`o_id`, `c_id`,`o_creat`,`cart_id`,`paystate`,`o_paymethod`,`o_money`,`r_id`,`o_end`,`o_paytime`,`o_state`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		// 调用通用类代码
		boolean flag = exeUpdate(sql, order.getO_id(), order.getC_id(), order.getO_creat(), order.getCart_id(),
				order.getPaystate(), order.getO_paymethod(), order.getR_id(), order.getO_end(), order.getO_state());
		return flag;
	}

	/**
	 * 未支付订单增加方法
	 * @param order 订单对象
	 * @return true 成功 false 失败
	 */
	public boolean addOrder(Order order) {
		// 判断order是否为空
		if (order == null) {
			return false;
		}
		// 定义sql语句
		String sql = "INSERT INTO `order` (`o_id`, `c_id`,`o_creat`,`cart_id`,`paystate`,`o_money`,`o_state`) VALUES (?,?,?,?,?,?,?)";
		// 调用通用类代码
		boolean flag = exeUpdate(sql, order.getO_id(), order.getC_id(), order.getO_creat(), order.getCart_id(),
				order.getPaystate(),order.getO_money(),order.getO_state());
		return flag;
	}
	
	/**
	 * 根据订单编号删除订单
	 * 
	 * @param o_id 订单编号
	 * @return true 成功 false 失败
	 */
	public boolean delOrderById(String o_id) {
		// 定义sql语句
		String sql = "delete FROM `order` WHERE (`o_id`=?)";
		// 调用DBUtil方法
		boolean flag = exeUpdate(sql, o_id); 
		return flag;
	}

	/**
	 * 根据订单编号模糊检索订单
	 * 
	 * @param keywords 订单编号关键字
	 * @return
	 */
	public List<Order> getOrderByName(String keywords) {
		// 声明sql语句
		String sql = "select * from `order` where o_id like ? ";
		List<Order> list = exeQuery(sql, Order.class, "%" + keywords + "%");

		return list;
	}

	/**
	 * 订单分页+根据订单编号模糊查询
	 * 
	 * @param keywords 检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<Order> getOrderByPage(String keywords,String begindate,String enddate, int pageNo, int pageSize) {
		PageData<Order> pd = null;
		// 声明sql语句
		String sql = "select * from `order` where o_id like ? ";
		if (!"".equals(begindate) && !"".equals(enddate)) {
			sql = sql+" and (o_paytime >=? and o_paytime<=?)";
			pd = exeQueryByPage(sql, Order.class, pageNo, pageSize,"%"+keywords+"%",begindate,enddate);
		} else {
			pd = exeQueryByPage(sql, Order.class, pageNo, pageSize,"%"+keywords+"%");

		}
		return pd;
	}

	/**
	 * 根据订单编号修改支付状态，仅限待支付修改为已取消
	 * @param order 订单对象
	 * @return true 成功 false 失败
	 */
	public boolean updOrderById(Order order) {
		// 定义sql语句
		String sql = "UPDATE `order` SET `paystate`=? WHERE (`o_id`=?)";
		// 调用DBUtil方法
		boolean flag = exeUpdate(sql,order.getPaystate(),order.getO_id());
		return flag;
	}
}
