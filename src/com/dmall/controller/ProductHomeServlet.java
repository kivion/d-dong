package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dmall.entity.Product;
import com.dmall.service.ProductService;
import com.dmall.util.AjaxResponse;
import com.google.gson.Gson;

/**
 * 主页页面-控制层-前台
 * 
 * @author ccp
 *
 */
@WebServlet("/producthome.do")
public class ProductHomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ProductService ps = new ProductService();

	public ProductHomeServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 设置请求编码的格式
		// request.setCharacterEncoding("utf-8");

		String op = "";
		// 判断防止空指针
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if ("doQuery".equals(op)) {
			doQuery(request, response);
		} else {
			// 查询水果类
			// 查询蔬菜类
			// 查询肉禽蛋品类
			doPtf01Query(request, response);
			doPtf02Query(request, response);
			doPtf03Query(request, response);
			doPtf04Query(request, response);
			doPtf05Query(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	protected void doQuery(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 接收表单提交的数据，获胜name="keywords"的值
		System.out.println("keywords :");

		List<Product> list1 = ps.getProductByShopListPart("ptf01");

		// 可以存放在request session中
		request.setAttribute("ptf_name", "ptf01");

		// 将list集合对象存在request请求范围内，其中双引号list（key）
		request.setAttribute("list1", list1);

		list1.forEach(System.out::println);

		// 从servlet跳转到jsp页面
		request.getRequestDispatcher("../front/index.jsp").forward(request, response);

	}

	protected void doPtf01Query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();
		String ptf_id = "ptf01";
		// 类别名，传递的查询
		List<Product> list1 = ps.getProductByShopListPart(ptf_id);

		// 单个对象直接转
		AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", list1);
		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);

		out.print(str);
		out.close();
	}

	protected void doPtf02Query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();
		String ptf_id = "ptf02";

		// 类别名，传递的查询
		List<Product> list2 = ps.getProductByShopListPart(ptf_id);

		// 单个对象直接转
		AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", list2);
		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);

		out.print(str);
		out.close();
	}

	protected void doPtf03Query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();
		String ptf_id = "ptf03";

		// 类别名，传递的查询
		List<Product> list3 = ps.getProductByShopListPart(ptf_id);

		// 单个对象直接转
		AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", list3);
		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);

		out.print(str);
		out.close();
	}

	protected void doPtf04Query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();
		String ptf_id = "ptf04";

		// 类别名，传递的查询
		List<Product> list4 = ps.getProductByShopListPart(ptf_id);

		// 单个对象直接转
		AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", list4);
		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);

		out.print(str);
		out.close();
	}

	protected void doPtf05Query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();
		String ptf_id = "ptf05";

		// 类别名，传递的查询
		List<Product> list5 = ps.getProductByShopListPart(ptf_id);

		// 单个对象直接转
		AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", list5);
		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);

		out.print(str);
		out.close();
	}
	protected void doPtf06Query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();
		String ptf_id = "ptf05";

		// 类别名，传递的查询
		List<Product> list6 = ps.getProductByShopListPart(ptf_id);

		// 单个对象直接转
		AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", list6);
		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);

		out.print(str);
		out.close();
	}
	protected void doPtf07Query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
 
		PrintWriter out = response.getWriter();
		String ptf_id = "ptf05";
 
		// 类别名，传递的查询
		List<Product> list7 = ps.getProductByShopListPart(ptf_id);

		// 如果是单个对象直接转
		AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", list7);
		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);

		// 从servlet跳转到jsp页面
		request.getRequestDispatcher("./front/producthome.jsp").forward(request, response);
		out.print(str);
		out.close();
	}
}
