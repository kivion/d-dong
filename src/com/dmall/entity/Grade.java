package com.dmall.entity;

public class Grade {
	private int g_sn;
	private String g_id;
	private String g_name;
	private String g_remark;
	private String g_reward;
	
	public Grade() {
		// TODO Auto-generated constructor stub
	}

	public Grade(int g_sn, String g_id, String g_name, String g_remark, String g_reward) {
		super();
		this.g_sn = g_sn;
		this.g_id = g_id;
		this.g_name = g_name;
		this.g_remark = g_remark;
		this.g_reward = g_reward;
	}
	public Grade( String g_id, String g_name, String g_remark, String g_reward) {
		super();
		this.g_id = g_id;
		this.g_name = g_name;
		this.g_remark = g_remark;
		this.g_reward = g_reward;
	}
	public int getG_sn() {
		return g_sn;
	}

	public void setG_sn(int g_sn) {
		this.g_sn = g_sn;
	}

	public String getG_id() {
		return g_id;
	}

	public void setG_id(String g_id) {
		this.g_id = g_id;
	}

	public String getG_name() {
		return g_name;
	}

	public void setG_name(String g_name) {
		this.g_name = g_name;
	}

	public String getG_remark() {
		return g_remark;
	}

	public void setG_remark(String g_remark) {
		this.g_remark = g_remark;
	}

	public String getG_reward() {
		return g_reward;
	}

	public void setG_reward(String g_reward) {
		this.g_reward = g_reward;
	}

	@Override
	public String toString() {
		return "Grade [g_sn=" + g_sn + ", g_id=" + g_id + ", g_name=" + g_name + ", g_remark=" + g_remark
				+ ", g_reward=" + g_reward + "]";
	}
	
}
