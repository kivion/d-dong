package com.dmall.dao;

import java.util.List;

import com.dmall.entity.MenueTypeNext;
import com.dmall.entity.ProductTypeNext;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

/**
 * 商品小分类-Dao
 * 
 * @author 蔡陈鹏
 */
public class ProductTypeNextDao extends DBUtil {
	/**
	 * 模糊查询-关键字
	 * 
	 * @author ccp 蔡陈鹏
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData分页对象
	 */
	public List<ProductTypeNext> getProductTypeNext(String keywords) {

		String sql = "SELECT product_type_next.ptn_id,product_type_next.ptn_name,product_type_next.ptf_id FROM product_type_next WHERE ptn_name LIKE ?";

		List<ProductTypeNext> list = exeQuery(sql, ProductTypeNext.class, "%" + keywords + "%");
		return list;
	}

	/**
	 * 模糊查询 + 分页
	 * 
	 * @author ccp 蔡陈鹏
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData分页对象
	 */
	public PageData<ProductTypeNext> getProductTypeNext(String keywords, int pageNo, int pageSize) {

		String sql = "SELECT product_type_next.ptn_id,product_type_next.ptn_name,product_type_next.ptf_id FROM product_type_next WHERE ptn_name LIKE ?";

		PageData<ProductTypeNext> pd = exeQueryByPage(sql, ProductTypeNext.class, pageNo, pageSize,
				"%" + keywords + "%");
		return pd;
	}
	/**
	 *	根据类型名查询类型编号
	 * @param typename
	 * @return
	 */
	public String getPtn_id(String typename) {
		String sql = "select * from product_type_next where ptn_name = ?";
		List<ProductTypeNext> list = exeQuery(sql, ProductTypeNext.class,typename);
		System.out.println(list.get(0));
		if(list.size()!=0) {
			ProductTypeNext ptn = list.get(0);
			return ptn.getPtn_id();
		}
		return "";
				
	}
	/**
	 * 增加小分类
	 * 
	 * @author ccp
	 * @param producttypeFirst 商品的小分类对象
	 * @return flag 布尔类型，true操作成功，false操作失败
	 */
	public boolean addProductTypeNext(ProductTypeNext producttypeNext) {
		// sql语句
		String sql = "INSERT INTO `product_type_next` (`ptn_id`, `ptn_name`, `ptn_sn`) VALUES (?,?,null)";
		// 调用增加方法
		boolean flag = exeUpdate(sql, producttypeNext.getPtn_id(), producttypeNext.getPtn_name());
		return flag;
	}

	/**
	 * 按商品小分类的编号id删除商品小分类
	 * 
	 * @author ccp
	 * @param ptf_id 商品小分类编号
	 * @return flag 布尔类型，true操作成功，false操作失败
	 */
	public boolean delProductTypeNextById(String ptn_id) {
		String sql = "delete FROM `product_type_next` WHERE (`ptn_id`=?)";
		boolean flag = exeUpdate(sql, ptn_id);
		return flag;
	}

	/**
	 * 按商品小分类的编号id修改商品小分类名称
	 * 
	 * @author ccp
	 * @param productreview 商品小分类对象
	 * @return flag true操作成功，false操作失败
	 */
	public boolean updProductTypeNextById(ProductTypeNext producttypeNext) {
		String sql = "UPDATE `product_type_next` SET  `ptn_name`=?,`ptf_id`=? WHERE (`ptn_id`=?)";
		boolean flag = exeUpdate(sql, producttypeNext.getPtn_name(), producttypeNext.getPtf_id(),
				producttypeNext.getPtn_id());
		return flag;
	}
}
