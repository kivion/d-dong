package com.dmall.entity;
/**
 * 
 * @author Administrator
 *
 */
public class MenueTypeFirst {
	private String mtf_id;
	private String mtf_name;
	public MenueTypeFirst() {
	}
	public MenueTypeFirst(int mtf_sn, String mtf_id, String mtf_name) {
		super();
		this.mtf_id = mtf_id;
		this.mtf_name = mtf_name;
	}

	public String getMtf_id() {
		return mtf_id;
	}
	public void setMtf_id(String mtf_id) {
		this.mtf_id = mtf_id;
	}
	public String getMtf_name() {
		return mtf_name;
	}
	public void setMtf_name(String mtf_name) {
		this.mtf_name = mtf_name;
	}
	@Override
	public String toString() {
		return "MenueTypeFirst ["+ mtf_id + ", mtf_name=" + mtf_name + "]";
	}
	
}
