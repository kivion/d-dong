package com.dmall.entity;

public class Customer {
	private String c_id;
	private String c_name;
	private String c_sex;
	private String c_tell;
	private String a_id;
	private String c_photo;
	private double c_balance;
	private String c_account;
	private String c_pwd;
	private int c_score;
	private int c_state;
	private String co_id;
	private String c_sn;
	private Address address = new Address();
	
	public void setAddress(Address address) {
		this.address = address;
	}
	public void setA_name(String a_name) {
		this.address.setA_name(a_name);
	}
	
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}
	public String getC_name() {
		return c_name;
	}
	public void setC_name(String c_name) {
		this.c_name = c_name;
	}
	public String getC_sex() {
		return c_sex;
	}
	public void setC_sex(String c_sex) {
		this.c_sex = c_sex;
	}
	public String getC_tell() {
		return c_tell;
	}
	public void setC_tell(String c_tell) {
		this.c_tell = c_tell;
	}
	public String getA_id() {
		return a_id;
	}
	public void setA_id(String a_id) {
		this.a_id = a_id;
	}
	public String getC_photo() {
		return c_photo;
	}
	public void setC_photo(String c_photo) {
		this.c_photo = c_photo;
	}
	public double getC_balance() {
		return c_balance;
	}
	public void setC_balance(double c_balance) {
		this.c_balance = c_balance;
	}
	public String getC_account() {
		return c_account;
	}
	public void setC_account(String c_account) {
		this.c_account = c_account;
	}
	public String getC_pwd() {
		return c_pwd;
	}
	public void setC_pwd(String c_pwd) {
		this.c_pwd = c_pwd;
	}
	public int getC_score() {
		return c_score;
	}
	public void setC_score(int c_score) {
		this.c_score = c_score;
	}
	public int getC_state() {
		return c_state;
	}
	public void setC_state(int c_state) {
		this.c_state = c_state;
	}
	public String getCo_id() {
		return co_id;
	}
	public void setCo_id(String co_id) {
		this.co_id = co_id;
	}
	public String getC_sn() {
		return c_sn;
	}
	public void setC_sn(String c_sn) {
		this.c_sn = c_sn;
	}
	public Customer() {
		// TODO Auto-generated constructor stub
	}
	public Customer(String c_id, String c_name, String c_sex, String c_tell, String a_id, String c_photo,
			double c_balance, String c_account, String c_pwd, int c_score, int c_state, String co_id, String c_sn) {
		super();
		this.c_id = c_id;
		this.c_name = c_name;
		this.c_sex = c_sex;
		this.c_tell = c_tell;
		this.a_id = a_id;
		this.c_photo = c_photo;
		this.c_balance = c_balance;
		this.c_account = c_account;
		this.c_pwd = c_pwd;
		this.c_score = c_score;
		this.c_state = c_state;
		this.co_id = co_id;
		this.c_sn = c_sn;
	}
	
	public Customer(String c_id, String c_name, String c_sex, String c_tell, String a_id, double c_balance,
			String c_account, String c_pwd, int c_score, int c_state, String co_id) {
		super();
		this.c_id = c_id;
		this.c_name = c_name;
		this.c_sex = c_sex;
		this.c_tell = c_tell;
		this.a_id = a_id;
		this.c_balance = c_balance;
		this.c_account = c_account;
		this.c_pwd = c_pwd;
		this.c_score = c_score;
		this.c_state = c_state;
		this.co_id = co_id;
	}
	
	public Customer(String c_id, String c_name, String c_sex, String c_tell) {
		super();
		this.c_id = c_id;
		this.c_name = c_name;
		this.c_sex = c_sex;
		this.c_tell = c_tell;
	}
	
	public Customer(String c_tell, String c_pwd) {
		super();
		this.c_tell = c_tell;
		this.c_pwd = c_pwd;
	}
	
  
	public Customer(String c_id, String c_tell, String c_account, String c_pwd, int c_state) {
		super();
		this.c_id = c_id;
		this.c_tell = c_tell;
		this.c_account = c_account;
		this.c_pwd = c_pwd;
		this.c_state = c_state;
	}
	/**
	 * 存session用
	 * 
	 * @author ccp 蔡陈鹏
	 * @param c_id
	 * @param c_account
	 * @param c_pwd
	 */
	public Customer(String c_id, String c_account, String c_pwd) {
		super();
		this.c_id = c_id;
		this.c_account = c_account;
		this.c_pwd = c_pwd;
	}
	@Override
	public String toString() {
		return "Customer [c_id=" + c_id + ", c_name=" + c_name + ", c_sex=" + c_sex + ", c_tell=" + c_tell + ", a_id="
				+ a_id + ", c_photo=" + c_photo + ", c_balance=" + c_balance + ", c_account=" + c_account + ", c_pwd="
				+ c_pwd + ", c_score=" + c_score + ", c_state=" + c_state + ", co_id=" + co_id + ", c_sn=" + c_sn + "]";
	}
	
	
}

