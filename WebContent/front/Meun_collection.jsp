<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    	   <!-- 瀵煎叆浜嗕竴浜涘繀瑕佺殑鍖� -->
<%@page import="com.dmall.service.MenuService"%>
<%@page import="com.dmall.entity.Menue"%>
<%@page import="java.util.List"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
    <!-- taglib -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	

    
<!doctype html>
<html class="no-js" lang="en">

	<head>
	
<c:set var="basepath" scope="page"
	value="${pageContext.request.contextPath}"></c:set>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>菜谱收藏</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

		<!-- CSS 
    ========================= -->
		<!--bootstrap min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/bootstrap.min.css">
		<!--owl carousel min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/owl.carousel.min.css">
		<!--slick min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/slick.css">
		<!--magnific popup min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/magnific-popup.css">
		<!--font awesome css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/font.awesome.css">
		<!--ionicons css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/ionicons.min.css">
		<!--linearicons css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/linearicons.css">
		<!--animate css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/animate.css">
		<!--jquery ui min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/jquery-ui.min.css">
		<!--slinky menu css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/slinky.menu.css">
		<!--plugins css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/plugins.css">

		<!-- Main Style CSS -->
		<link rel="stylesheet" href="${basepath}/front/assets/css/style.css">
		<!--layUI鐨刢ss-->
		<link rel="stylesheet" href="${basepath}/front/layui/css/layui.css" media="all">

		<!--modernizr min js here-->
		<script src="${basepath}/front/assets/js/vendor/modernizr-3.7.1.min.js"></script>

	</head>

	<body>

		<!--澶撮儴寮�濮�-->
		<header>
			<div class="main_header">
				<div class="header_top">
					<div class="container">
						<div class="row align-items-center">

							<!--导航栏-->
							<div class="col-lg-6 col-md-6">
								<div class="language_currency">
									<ul>
										<li class="language">
											<a href="#"> 语言 <i class="icon-right ion-ios-arrow-down"></i></a>
											<ul class="dropdown_language">
												<li>
													<a href="#">中文</a>
												</li>
												<li>
													<a href="#">英语</a>
												</li>
											</ul>
										</li>
										<li class="currency">
											<a href="#"> 简繁体 <i class="icon-right ion-ios-arrow-down"></i></a>
											<ul class="dropdown_currency">
												<li>
													<a href="#">简体</a>
												</li>
												<li>
													<a href="#">繁体</a>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="header_social text-right">
									<ul>
										<li>
											<a href="#"><i class="ion-social-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i class="ion-social-googleplus-outline"></i></a>
										</li>
										<li>
											<a href="#"><i class="ion-social-youtube-outline"></i></a>
										</li>
										<li>
											<a href="#"><i class="ion-social-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i class="ion-social-instagram-outline"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_middle">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-lg-2">
								<div class="logo">
									<a href="index.html"><img src="assets/img/logo/logo.jpg" alt=""></a>
								</div>
							</div>
							<div class="col-lg-10">
								<div class="header_right_info">
									<!--导航栏左边的类别-->
									<div class="search_container">
										<form action="#">
											<div class="hover_category">
												<select class="select_option" name="select" id="categori2">
													<option selected value="1">热门</option>
													<option value="2">土豆</option>
													<option value="3">白菜</option>
													<option value="4">茄子</option>
													<option value="5">鸡胸肉</option>
													<option value="6">鸡腿</option>
													<option value="7">炒饭</option>
													<option value="8">面条</option>
													<option value="9">红烧肉</option>
													<option value="10">蛋糕</option>
													<option value="11">热饮</option>
													<option value="12">粥</option>
													<option value="13">减肥餐</option>
													<option value="14">汤</option>
													<option value="15">排骨</option>
													<option value="16">牛肉</option>
												</select>
											</div>
											<div class="search_box">
												<input placeholder="搜索食材、菜谱  (๑´•﹃•`๑)" type="text">
												<button type="submit"><span class="lnr lnr-magnifier"></span></button>
											</div>
										</form>
									</div>
									<div class="header_account_area">
										<div class="header_account_list register">
											<ul class="layui-nav">
												<!--layUI的状态头像登录-->
												<li class="layui-nav-item" lay-unselect="">
													<a href="javascript:;"><img src="assets/img/icon/1.jpg" class="layui-nav-img">佚名</a>
													<dl class="layui-nav-child">
														<dd>
															<a href="javascript:;">修改信息</a>
														</dd>
														<dd>
															<a href="javascript:;">账号管理</a>
														</dd>
														<dd>
															<a href="javascript:;">退出</a>
														</dd>
													</dl>
												</li>
												<!--<li>
													<a href="login.html">娉ㄥ唽</a>
												</li>
												<li><span>/</span></li>
												<li>
													<a href="login.html">鐧诲綍</a>
												</li>-->
											</ul>
										</div>
										<div class="header_account_list header_wishlist">
											<a href="收藏页面.html"><span class="lnr lnr-heart"></span> <span class="item_count">5</span> </a>
										</div>
										<div class="header_account_list  mini_cart_wrapper">
											<a href="购物车.html"><span class="lnr lnr-cart"></span><span class="item_count">2</span></a>
											<!--mini cart-->
											<div class="mini_cart">
												<div class="cart_gallery">
													<div class="cart_item">
														<div class="cart_img">
															<a href="#"><img src="assets/img/s-product/product.jpg" alt=""></a>
														</div>
														<div class="cart_info">
															<a href="#">Primis In Faucibus</a>
															<p>1 x <span> $65.00 </span></p>
														</div>
														<div class="cart_remove">
															<a href="#"><i class="icon-x"></i></a>
														</div>
													</div>
													<div class="cart_item">
														<div class="cart_img">
															<a href="#"><img src="assets/img/s-product/product2.jpg" alt=""></a>
														</div>
														<div class="cart_info">
															<a href="#">Letraset Sheets</a>
															<p>1 x <span> $60.00 </span></p>
														</div>
														<div class="cart_remove">
															<a href="#"><i class="icon-x"></i></a>
														</div>
													</div>
												</div>
												<div class="mini_cart_table">
													<div class="cart_table_border">
														<div class="cart_total mt-10">
															<span>总计:</span>
															<span class="price">$125.00</span>
														</div>
													</div>
												</div>
												<div class="mini_cart_footer">
													<div class="cart_button">
														<a href="购物车.html"><i class="fa fa-shopping-cart"></i> 前往购物车</a>
													</div>
													<div class="cart_button">
														<a href="#"><i class="fa fa-sign-in"></i> ...</a>
													</div>

												</div>
											</div>
											<!--mini cart end-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_bottom sticky-header">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-lg-3">
								<div class="categories_menu">
									<div class="categories_title">
										<h2 class="categori_toggle">全部分类</h2>
									</div>
									<div class="categories_menu_toggle">
										<ul>
											<li class="menu_item_children">
												<a href="#">菜式<i class="fa fa-angle-right"></i></a>
												<ul class="categories_mega_menu">
													<li class="menu_item_children">
														<a href="#">快手菜</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">家常菜</a>
															</li>
															<li>
																<a href="">下素菜</a>
															</li>
														</ul>
													</li>
													<li class="menu_item_children">
														<a href="#">宝宝辅食</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">烹饪基础</a>
															</li>
															<li>
																<a href="">异国料理</a>
															</li>
														</ul>
													</li>
													<li class="menu_item_children">
														<a href="#">素菜</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">沙拉</a>
															</li>
															<li>
																<a href="">汤 </a>
															</li>
														</ul>
													</li>
													<li class="menu_item_children">
														<a href="#">减肥餐</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">孕妇餐</a>
															</li>
														</ul>
													</li>
												</ul>
											</li>
											<li class="menu_item_children">
												<a href="#">场景 <i class="fa fa-angle-right"></i></a>
												<ul class="categories_mega_menu column_3">
													<li class="menu_item_children">
														<a href="#">早餐</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">便当</a>
															</li>
														</ul>
													</li>
													<li class="menu_item_children">
														<a href="#">午餐</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">宿舍食谱</a>
															</li>
														</ul>
													</li>
													<li class="menu_item_children">
														<a href="#">晚餐</a>

													</li>
												</ul>
											</li>
											<li class="menu_item_children">
												<a href="#"> 主食<i class="fa fa-angle-right"></i></a>
												<ul class="categories_mega_menu column_2">
													<ul class="categorie_sub_menu">
														<li>
															<a href="">炒面</a>
														</li>
														<li>
															<a href="">炒饭</a>
														</li>
														<li>
															<a href="">粥</a>
														</li>
														<li>
															<a href="">饼</a>
														</li>
													</ul>
												</ul>
											</li>
											<li class="menu_item_children">
												<a href="#"> 烹饪方式 <i class="fa fa-angle-right"></i></a>
												<ul class="categories_mega_menu column_2">
													<ul class="categorie_sub_menu">
														<li>
															<a href="">烤箱</a>
														</li>
														<li>
															<a href="">电饭锅</a>
														</li>
														<li>
															<a href="">平底锅</a>
														</li>
														<li>
															<a href="">微波炉</a>
														</li>
													</ul>
												</ul>
											</li>
											<li class="menu_item_children">
												<a href="#">甜品饮品 <i class="fa fa-angle-right"></i></a>
												<ul class="categories_mega_menu column_3">
													<li class="menu_item_children">
														<a href="#">蛋糕</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">面包</a>
															</li>
															<li>
																<a href="">饮品</a>
															</li>
														</ul>
													</li>
													<li class="menu_item_children">
														<a href="#">抹茶</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">免烤甜品</a>
															</li>
															<li>
																<a href="">冰淇淋</a>
															</li>
														</ul>
													</li>

												</ul>
											</li>
											<li class="menu_item_children">
												<a href="#">食材<i class="fa fa-angle-right"></i></a>
												<ul class="categories_mega_menu column_3">
													<li class="menu_item_children">
														<a href="#">猪肉</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">鸡肉</a>
															</li>
															<li>
																<a href="">牛肉</a>
															</li>
														</ul>
													</li>
													<li class="menu_item_children">
														<a href="#">羊肉</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">鸭肉</a>
															</li>
															<li>
																<a href="">海鲜水产</a>
															</li>
														</ul>
													</li>
													<li class="menu_item_children">
														<a href="#">蔬菜</a>
														<ul class="categorie_sub_menu">
															<li>
																<a href="">方便速食</a>
															</li>
														</ul>
													</li>
												</ul>
											</li>
											<li class="menu_item_children">
												<a href="#"> 风味 <i class="fa fa-angle-right"></i></a>
												<ul class="categories_mega_menu column_2">
													<ul class="categorie_sub_menu">
														<li>
															<a href="">香辣</a>
														</li>
														<li>
															<a href="">糖醋</a>
														</li>
														<li>
															<a href="">清淡</a>
														</li>
														<li>
															<a href="">烧烤</a>
														</li>
													</ul>
												</ul>
											</li>
											<li id="cat_toggle" class="has-sub">
												<a href="#">敬请期待更多 ε≡?(?>?
													<)? </a>
														<ul class="categorie_sub">
															<li>
																<a href="#">Hide Categories</a>
															</li>
														</ul>

											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<!--main menu start-->
								<div class="main_menu menu_position">
									<nav>
										<ul>
											<li>
												<a href="index.html">主页</a>
											</li>
											<li>
												<a href="菜谱—主页.html">菜谱主页</a>
											</li>
											<li>
												<a href="菜谱—收藏.html">收藏</a>
											</li>
											<li>
												<a href="菜谱—发布.html">发布</a>
											</li>
											<li>
												<a href="菜谱—个人笔记展示.html">个人笔记</a>
											</li>
										</ul>
									</nav>
								</div>
								<!--main menu end-->
							</div>
							<div class="col-lg-3">
								<div class="call-support">
									<p>
										<a href="tel:(08)23456789">(08) 23 456 789</a> Customer Support</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!--header area end-->

		<!--breadcrumbs area start-->
		<div class="breadcrumbs_area">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="breadcrumb_content">
							<h3>吃货的世界没有诗和远方，唯有美食不可辜负</h3>
							<ul>
								<li>没有什么是一顿大餐不能解决的，如果有，那就来两顿</li>
								<br>
								<li>o(´^｀)o </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--breadcrumbs area end-->

		<!--shop  area start-->
		<div class="shop_area shop_fullwidth mt-70 mb-70">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<!--shop wrapper start-->
						<!--shop toolbar start-->
						<div class="shop_toolbar_wrapper">
							<div class="shop_toolbar_btn">

								<button data-role="grid_3" type="button" class=" btn-grid-3" data-toggle="tooltip" title="3"></button>

								<button data-role="grid_4" type="button" class="active btn-grid-4" data-toggle="tooltip" title="4"></button>

								<button data-role="grid_list" type="button" class="btn-list" data-toggle="tooltip" title="List"></button>
							</div>

							<div class="input-group">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="我的收藏...">
									<span class="input-group-btn">
                                        <button class="btn btn-success" type="button"  id="go">Go!</button>
                                      </span>
								</div>
							</div>

						</div>
						<!--shop toolbar end-->
						<div class="row shop_wrapper">
						
						<c:forEach items="${list}" var="mc">
							<div class="col-lg-3 col-md-4 col-sm-6 col-12 ">
								<div class="single_product">
									<div class="product_thumb">
										<img src="assets/img/menu-home/first-products/1-6_鍓湰.jpg" alt="">
									</div>
									<div class="product_content grid_content">
										<h4 class="product_name">
										<a href="./menuhome.do?op=menudetail&menuid=${mc.m_id}">${mc.m_name}</a></h4>
										<p>作者：${mc.customer.getC_name()}</p><br>
										<div class="price_box">
											<span>时间：${mc.m_time}</span>
											<span>难度：${mc.m_difficult}</span>
										</div>
									</div>
								</div>
							</div>
							</c:forEach>
							
							<!-- 濡傛灉鍚庨潰杩樻兂瑕佺户缁鍔犳敹钘忕殑鍟嗗搧锛屽湪杩欏悗闈㈠姞--杩欐槸閽堝html椤甸潰鎻掑叆鏁版嵁-->
						</div>

						
						<!--shop toolbar end-->
						<!--shop wrapper end-->
					</div>
				</div>
			</div>
		</div>
		<!--shop  area end-->

			<!--footer area start-->
		<footer class="footer_widgets">
			<div class="footer_top">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-12 col-sm-7">
							<div class="widgets_container contact_us">
								<div class="footer_logo">
									<a href="index.html"><img src="assets/img/logo/logo.png" alt=""></a>
								</div>
								<p class="footer_desc">服务、质量、效率</p>
								<p><span>Address:</span> xmzretc </p>
								<p><span>Email:</span>
									<a href="#">760008559@qq.com</a>
								</p>
								<p><span>Call us:</span>
									<a href="tel:(08)23456789">(08) 23 456 789</a>
								</p>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-5">
							<div class="widgets_container widget_menu">
								<h3>关于叮咚到家</h3>
								<div class="footer_menu">

									<ul>
										<li>
											<a href="about.html">了解叮咚</a>
										</li>
										<li>
											<a href="#">加入叮咚</a>
										</li>
										<li>
											<a href="#">投资者关系</a>
										</li>
										<li>
											<a href="#">团队信息</a>
										</li>
										<li>
											<a href="contact.html">联系我们</a>
										</li>
										<li>
											<a href="#">廉洁举报</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-5">
							<div class="widgets_container widget_menu">
								<h3>关注我们</h3>
								<div class="footer_menu">

									<ul>
										<li>
											<a href="about.html">新浪微博</a>
										</li>
										<li>
											<a href="#">官方微信</a>
										</li>
										<li>
											<a href="#">Q群加入</a>
										</li>
										<li>
											<a href="#">公益基金会</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-5">
							<div class="widgets_container widget_menu">
								<h3>特色服务</h3>
								<div class="footer_menu">

									<ul>
										<li>
											<a href="about.html">兑换券码领取</a>
										</li>
										<li>
											<a href="#">礼物码</a>
										</li>
										<li>
											<a href="#">防伪查询</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-4">
							<div class="widgets_container widget_menu">
								<h3>帮助中心</h3>
								<div class="footer_menu">
									<ul>
										<li>
											<a href="#">账户管理</a>
										</li>
										<li>
											<a href="#">购物指南</a>
										</li>
										<li>
											<a href="#">订单操作</a>
										</li>
										<li>
											<a href="#">售后政策</a>
										</li>
										<li>
											<a href="#">自助服务</a>
										</li>
										<li>
											<a href="#">相关下载</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="footer_bottom">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-6 col-md-7">
							<div class="copyright_area">
								<!--<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a>-->
								<p>Copyright &copy; 2020.闽ICP证110xxx号 闽ICP备10046xxx号 闽公网安备11010802020 号
								</p>
							</div>
						</div>
						<div class="col-lg-6 col-md-5">
							<div class="footer_payment">
								<ul>
									<li>
										<a href="#"><img src="assets/img/icon/v-logo-1.png" alt=""></a>
									</li>
									<li>
										<a href="#"><img src="assets/img/icon/v-logo-2.png" alt=""></a>
									</li>
									<li>
										<a href="#"><img src="assets/img/icon/v-logo-3.png" alt=""></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--footer area end-->

		<!-- modal area start-->
		<div class="modal fade" id="modal_box" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="icon-x"></i></span>
                </button>
					<div class="modal_body">
						<div class="container">
							<div class="row">
								<div class="col-lg-5 col-md-5 col-sm-12">
									<div class="modal_tab">
										<div class="tab-content product-details-large">
											<div class="tab-pane fade show active" id="tab1" role="tabpanel">
												<div class="modal_tab_img">
													<a href="#"><img src="assets/img/product/productbig1.jpg" alt=""></a>
												</div>
											</div>
											<div class="tab-pane fade" id="tab2" role="tabpanel">
												<div class="modal_tab_img">
													<a href="#"><img src="assets/img/product/productbig2.jpg" alt=""></a>
												</div>
											</div>
											<div class="tab-pane fade" id="tab3" role="tabpanel">
												<div class="modal_tab_img">
													<a href="#"><img src="assets/img/product/productbig3.jpg" alt=""></a>
												</div>
											</div>
											<div class="tab-pane fade" id="tab4" role="tabpanel">
												<div class="modal_tab_img">
													<a href="#"><img src="assets/img/product/productbig4.jpg" alt=""></a>
												</div>
											</div>
										</div>
										<div class="modal_tab_button">
											<ul class="nav product_navactive owl-carousel" role="tablist">
												<li>
													<a class="nav-link active" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false"><img src="assets/img/product/product1.jpg" alt=""></a>
												</li>
												<li>
													<a class="nav-link" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false"><img src="assets/img/product/product6.jpg" alt=""></a>
												</li>
												<li>
													<a class="nav-link button_three" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false"><img src="assets/img/product/product2.jpg" alt=""></a>
												</li>
												<li>
													<a class="nav-link" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false"><img src="assets/img/product/product7.jpg" alt=""></a>
												</li>

											</ul>
										</div>
									</div>
								</div>
								<div class="col-lg-7 col-md-7 col-sm-12">
									<div class="modal_right">
										<div class="modal_title mb-10">
											<h2>Donec Ac Tempus</h2>
										</div>
										<div class="modal_price mb-10">
											<span class="new_price">$64.99</span>
											<span class="old_price">$78.99</span>
										</div>
										<div class="modal_description mb-15">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui nemo ipsum numquam, reiciendis maiores quidem aperiam, rerum vel recusandae </p>
										</div>
										<div class="variants_selects">
											<div class="variants_size">
												<h2>size</h2>
												<select class="select_option">
													<option selected value="1">s</option>
													<option value="1">m</option>
													<option value="1">l</option>
													<option value="1">xl</option>
													<option value="1">xxl</option>
												</select>
											</div>
											<div class="variants_color">
												<h2>color</h2>
												<select class="select_option">
													<option selected value="1">purple</option>
													<option value="1">violet</option>
													<option value="1">black</option>
													<option value="1">pink</option>
													<option value="1">orange</option>
												</select>
											</div>
											<div class="modal_add_to_cart">
												<form action="#">
													<input min="1" max="100" step="2" value="1" type="number">
													<button type="submit">add to cart</button>
												</form>
											</div>
										</div>
										<div class="modal_social">
											<h2>Share this product</h2>
											<ul>
												<li class="facebook">
													<a href="#"><i class="fa fa-facebook"></i></a>
												</li>
												<li class="twitter">
													<a href="#"><i class="fa fa-twitter"></i></a>
												</li>
												<li class="pinterest">
													<a href="#"><i class="fa fa-pinterest"></i></a>
												</li>
												<li class="google-plus">
													<a href="#"><i class="fa fa-google-plus"></i></a>
												</li>
												<li class="linkedin">
													<a href="#"><i class="fa fa-linkedin"></i></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- modal area end-->

		<!-- JS
============================================ -->
		<!--jquery min js-->
		<script src="${basepath}/front/assets/js/vendor/jquery-3.4.1.min.js"></script>
		<!--popper min js-->
		<script src="${basepath}/front/assets/js/popper.js"></script>
		<!--bootstrap min js-->
		<script src="${basepath}/front/assets/js/bootstrap.min.js"></script>
		<!--owl carousel min js-->
		<script src="${basepath}/front/assets/js/owl.carousel.min.js"></script>
		<!--slick min js-->
		<script src="${basepath}/front/assets/js/slick.min.js"></script>
		<!--magnific popup min js-->
		<script src="${basepath}/front/assets/js/jquery.magnific-popup.min.js"></script>
		<!--counterup min js-->
		<script src="${basepath}/front/assets/js/jquery.counterup.min.js"></script>
		<!--jquery countdown min js-->
		<script src="${basepath}/front/assets/js/jquery.countdown.js"></script>
		<!--jquery ui min js-->
		<script src="${basepath}/front/assets/js/jquery.ui.js"></script>
		<!--jquery elevatezoom min js-->
		<script src="${basepath}/front/assets/js/jquery.elevatezoom.js"></script>
		<!--isotope packaged min js-->
		<script src="${basepath}/front/assets/js/isotope.pkgd.min.js"></script>
		<!--slinky menu js-->
		<script src="${basepath}/front/assets/js/slinky.menu.js"></script>
		<!-- Plugins JS -->
		<script src="${basepath}/front/assets/js/plugins.js"></script>

		<!-- Main JS -->
		<script src="${basepath}/front/assets/js/main.js"></script>
		<!--layUI鐨刯s-->
		<script src="${basepath}/front/layui/layui.js" charset="utf-8"></script>
		<!-- 娉ㄦ剰锛氬鏋滀綘鐩存帴澶嶅埗鎵�鏈変唬鐮佸埌鏈湴锛屼笂杩癹s璺緞闇�瑕佹敼鎴愪綘鏈湴鐨� -->
		<script>
			layui.use('element', function() {
				var element = layui.element; //瀵艰埅鐨刪over鏁堟灉銆佷簩绾ц彍鍗曠瓑鍔熻兘锛岄渶瑕佷緷璧杄lement妯″潡

				//鐩戝惉瀵艰埅鐐瑰嚮
				element.on('nav(demo)', function(elem) {
					//console.log(elem)
					layer.msg(elem.text());
				});
			});
		</script>
		
		<script type="text/javascript">
		//浼犻�掓帴鏀剁殑p_id鍊硷紱param鏄痚l琛ㄨ揪寮忎腑鐨勪竴涓璞�
		//let c_id = "${param.c_id}";
		//console.log(p_id);
		//ajax璇锋眰-鏍规嵁id鏌ヨproduct淇℃伅
		

	</script>
		<script type="text/javascript">
			$(function(){
				$("#go").click(function(){
					location.href="${basepath}/menuhome.do?op=menucollection&cid=c000000001";
					})
				})
		
		</script>
		
	</body>

</html>