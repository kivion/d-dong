package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Customer;
import com.dmall.entity.Menue;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;


/**
 * 编写客户的相关方法
 * @author 王佳慧
 *
 */
public class CustomerDao {
	
		/**
		 * 查询所有客户个人信息
		 * 
		 * @return 客户对象
		 */
		public List<Customer> getCustomer() {
			String sql = "SELECT customer.c_id, customer.c_name, customer.c_sex, customer.c_tell, customer.a_id, customer.c_photo, customer.c_balance, customer.c_account, customer.c_pwd, customer.c_score, customer.c_state FROM customer";
			List<Customer> list =DBUtil.exeQuery(sql, Customer.class, null);
			return list;
		}
		
		/**
		 * 分页查询-查询所有客户个人信息
		 * @param pageNo 页码
		 * @param pageSize 当前页的记录数
		 * @return 分页对象
		 */
			public PageData<Customer> getCusByPage(int pageNo, int pageSize) {
				String sql = "SELECT customer.c_id, customer.c_name, customer.c_sex, customer.c_tell, customer.a_id, customer.c_photo, customer.c_balance, customer.c_account, customer.c_pwd, customer.c_score, customer.c_state FROM customer";
				PageData<Customer> pd =DBUtil.exeQueryByPage(sql, Customer.class, pageNo,pageSize,null);
				return pd;
			}
		
	/**
	 * 模糊查询--通过编号、姓名、性别、电话号码查询客户
	 * @param keywords 编号、姓名、性别、电话号码关键字
	 * @return 客户对象
	 */
		public List<Customer> getCustomeByKeywords(String keywords) {
			String sql = "SELECT customer.c_id, customer.c_name, customer.c_sex, customer.c_tell, customer.a_id, customer.c_photo, customer.c_balance, customer.c_account, customer.c_pwd, customer.c_score, customer.c_state   " + 
					"FROM customer WHERE customer.c_id like ? or  customer.c_name like ? or customer.c_sex like ? or customer.c_tell like ?";
			List<Customer> list = DBUtil.exeQuery(sql, Customer.class, "%" + keywords + "%", "%" + keywords + "%","%" + keywords + "%","%" + keywords + "%");
			return list;
		}
		/**
		 * 分页查询-模糊查询--通过编号、姓名、性别、电话号码查询客户
		 * @param keywords 编号、姓名、性别、电话号码关键字
		 * @param pageNo 页码
		 * @param pageSize 当前页的记录数
		 * @return 分页对象
		 */
			public PageData<Customer> getCusKeyByPage(String keywords, int pageNo, int pageSize) {
				String sql = "SELECT customer.c_id, customer.c_name, customer.c_sex, customer.c_tell, address.a_name, customer.c_photo, customer.c_balance, customer.c_account, customer.c_pwd, customer.c_score, customer.c_state   " + 
						"FROM customer join address on customer.a_id=address.a_id WHERE customer.c_id like ? or  customer.c_name like ? or customer.c_sex like ? or customer.c_tell like ?";
				PageData<Customer> pd =DBUtil.exeQueryByPage(sql, Customer.class, pageNo,pageSize,"%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%");
				pd.getList().forEach(System.out::println);
				return pd;
			}
		
		/**
		 * 添加客户个人信息
		 * @param c 客户对象
		 * @return true操作成功，false操作失败
		 */
		public boolean AddCustomer(Customer c) {
			String sql = "INSERT INTO `customer` (`c_id`, `c_name`, `c_sex`, `c_tell`, `a_id`, `c_balance`, `c_account`, `c_pwd`, `c_score`, `c_state`, `co_id`) "
					+ "VALUES (?, ? ,?  ,?  ,?  , ? ,?  , ? , ? , ?,null)";
			return DBUtil.exeUpdate(sql, c.getC_id(),c.getC_name(),c.getC_sex(),c.getC_tell(),c.getA_id(),c.getC_balance(),c.getC_account(),c.getC_pwd(),c.getC_score(),c.getC_state());
		}
		
		/**
		 * 删除信息--->通过客户编号删除
		 * 
		 * @param 客户编号
		 * @return true操作成功，false操作失败
		 */
		public boolean DelCustomer(String cusid) {
			String sql = "delete from `customer` where c_id=?";
			return DBUtil.exeUpdate(sql,cusid);
		}
		
		/**
		 *	按客户个人编号修改客户个人信息姓名，性别，电话号码
		 * @param 
		 * @return flag  true操作成功，false操作失败
		 */
		public boolean updCusById(Customer c) {
			String sql = "UPDATE `customer` SET `c_name`=?, `c_sex`=?, `c_tell`=?  WHERE (`c_id`=?)";
			System.out.println(c);
			boolean flag = DBUtil.exeUpdate(sql,c.getC_name(),c.getC_sex(),c.getC_tell(),c.getC_id());
			return flag;
		}
		/**
		 *	通过手机号重置密码
		 * @param 
		 * @return flag  true操作成功，false操作失败
		 */
		public boolean updCusPwdBytell(Customer c) {
			String sql = "UPDATE `customer` SET `c_pwd`=?  WHERE (`c_tell`=?)";
			System.out.println(c);
			boolean flag = DBUtil.exeUpdate(sql,c.getC_pwd(),c.getC_tell());
			return flag;
		}
		/**
		 * 注册客户
		 * @param c 客户对象
		 * @return true操作成功，false操作失败
		 */
		public boolean AddCusBytell(Customer c) {
			String cuspwd=com.dmall.util.MD5Util.getEncodeByMd5(c.getC_pwd());
			String sql = "INSERT INTO `customer` (`c_id`, `c_tell`,  `c_account`, `c_pwd`,`c_state`) "
					+ "VALUES (?, ? ,?  ,?,? )";
			return DBUtil.exeUpdate(sql, c.getC_id(), c.getC_tell() ,c.getC_account(),cuspwd,c.getC_state());
		}
		/**
		 * 通过账号和密码进行查找用户
		 * @param useremail 用户邮箱
		 * @param userpwd 密码
		 * @return 用户对象
		 */
		public Customer getcusByaccountpwd(String cusname,String cuspwd) {
			cuspwd=com.dmall.util.MD5Util.getEncodeByMd5(cuspwd);
			String sql="SELECT * FROM `customer` where c_account=? and c_pwd= ? ";
			List<Customer> list=DBUtil.exeQuery(sql, Customer.class, cusname,cuspwd);
			if(list.isEmpty()) {
				return null;
			}
			return list.get(0);
		}
}
