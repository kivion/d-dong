package com.dmall.service;
import java.util.List;
import com.dmall.dao.CardDao;
import com.dmall.entity.Card;
import com.dmall.entity.Cart;
import com.dmall.pagedata.PageData;

public class CardService {
	// 调用dao
		CardDao cd = new CardDao();

		/**
		 * 查询业务
		 * 
		 * @param keywords 商品名称关键字
		 * @return list集合
		 */
		public List<Card> getCard(String keywords) {
			return cd.getAll();
		}
		
		/**
		 *	删除操作
		 * @param cardid 地址流水号
		 * @return
		 */
		public boolean delCard(int cardid) {
			return cd.delCardById(cardid);
		}
		
		/**
		 * 修改操作
		 * @param card
		 * @return
		 */
		public boolean updateCard(Card card) {
			return cd.updCardById(card);
		}
		
		
		/**
		 * Card业务层
		 * @param keywords 检索关键字
		 * @param pageNo 页码
		 * @param pageSize 当前页码数
		 * @return 
		 */
		public PageData<Card> getCardByPage(String keywords, int pageNo, int pageSize) {
			// 判断页码和页面记录数
			if (pageNo < 1) {
				pageNo = 1;
			}
			if (pageSize < 0) {
				pageNo = 10;
			}

			return cd.getCardByPage(keywords, pageNo, pageSize);
		}
		

}
