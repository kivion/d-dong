package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Product;
import com.dmall.entity.ProductReview;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;
/**
 * 商品评论-Dao
 * 
 * @author 蔡陈鹏
 */
public class ProductReviewDao extends DBUtil {
	/**
	 * 模糊查询-关键字 
	 * 
	 * @param keywords 关键字
	 * @return list 集合
	 */
	public List<ProductReview> getProductReview(String keywords) {

		String sql = "SELECT product.p_id, product.p_name, product.p_weight, product.p_stock, product.p_origin, product.p_save, product.p_detaill, product.p_pro, product.p_launch, product.p_guarantee, product.p_appraise, product.p_photo, product.ptn_id, product.p_price, product.p_sn FROM product WHERE product.p_name LIKE ? OR product.p_detaill LIKE ?";

		List<ProductReview> list = exeQuery(sql, Product.class, "%" + keywords + "%", "%" + keywords + "%");
		return list;
	}

	/**
	 * 模糊查询 - 分页
	 * 
	 * @author ccp 蔡陈鹏
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData分页对象
	 */
	public PageData<ProductReview> getProductReview(String keywords, int pageNo, int pageSize) {

		String sql = "SELECT product_reviews.pr_content, product_reviews.pr_time,customer.c_name,product.p_name FROM product_reviews INNER JOIN product ON product.p_id = product_reviews.p_id INNER JOIN customer ON product_reviews.c_id = customer.c_id";

		PageData<ProductReview> pd = exeQueryByPage(sql, Product.class, pageNo, pageSize, "%" + keywords + "%",
				"%" + keywords + "%");
		return pd;
	}

	/**
	 * 增加商品评论
	 * 
	 * @author ccp
	 * @param product 商品对象
	 * @return flag 布尔类型，true操作成功，false操作失败
	 */
	public boolean addProductReview(ProductReview productreview) {
		// sql语句
		String sql = "INSERT INTO `product_reviews` (`pr_sn`, `p_id`, `pr_content`, `c_id`, `pr_time`) VALUES (null,?, ?, ?,?)";
		// 调用增加方法
		boolean flag = exeUpdate(sql, productreview.getP_id(), productreview.getPr_conten(), productreview.getC_id(),
				productreview.getPr_time());
		return flag;
	}

	/**
	 * 按商品评论id删除商品评论
	 * 
	 * @author ccp
	 * @param pr_id 商品评论编号
	 * @return flag 布尔类型，true操作成功，false操作失败
	 */
	public boolean delProductReviewById(int pr_id) {
		String sql = "delete FROM `product_reviews` WHERE (`pr_id`=?)";
		boolean flag = exeUpdate(sql, pr_id);
		return flag;
	}

	/**
	 * 按商品评论id修改商品评论的评论内容和评论时间
	 * 
	 * @author ccp
	 * @param productreview 商品评论对象
	 * @return flag true操作成功，false操作失败
	 */
	public boolean updProductReviewById(ProductReview productreview) {
		String sql = "UPDATE `product_reviews` SET `pr_content`=?, `pr_time`=? WHERE (`pr_id`=?)";
		boolean flag = exeUpdate(sql, productreview.getPr_conten(), productreview.getPr_time());
		return flag;
	}
}
