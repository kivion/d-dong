package com.dmall.test;

import java.util.List;

import org.junit.Test;

import com.dmall.dao.CustomerDao;
import com.dmall.entity.Customer;
import com.dmall.pagedata.PageData;
import com.dmall.util.MD5Util;


/**
 * 测试：客户方法
 * @author 王佳慧
 *
 */
public class TestCustomer {
/**
 * 测试：查询所有客户个人信息
 * 目前出现空指针异常，未改，不知道怎么改
 */
	@Test
	public void TestgetCustomer() {
		CustomerDao cd=new CustomerDao();
		List<Customer> list=cd.getCustomer();
		list.forEach(System.out::println);
	}
	/**
	 * 对分页查询进行测试--查询所有客户个人信息
	 * 目前出现空指针异常，未改，不知道怎么改
	 */
	@Test
	public void TestBlogByPage() {
		CustomerDao cd=new CustomerDao();
		PageData<Customer> pd=cd.getCusByPage(2, 5);
		System.out.println("pageNo: "+pd.getPageNo());
		System.out.println("pagesize: "+pd.getPageSize());
		System.out.println("list: "+pd.getList());
		System.out.println("totalcount: "+pd.getTotalCount());
		System.out.println("totalpage: "+pd.getTotalPage());
	}
	
	/**
	 * 模糊查询--通过编号、姓名、性别、电话号码查询客户
	 */
	@Test
	public void  TestgetCusByKey() {
		CustomerDao bd=new CustomerDao();
		List<Customer> list=bd.getCustomeByKeywords("小");
		list.forEach(System.out::println);
	}
	
	/**
	 * 对分页查询进行测试--通过编号、姓名、性别、电话号码查询客户
	 */
	@Test
	public void TestgetCusKeyByPage() {
		CustomerDao cd=new CustomerDao();
		PageData<Customer> pd=cd.getCusKeyByPage("", 2,5);
		System.out.println("pageNo: "+pd.getPageNo());
		System.out.println("pagesize: "+pd.getPageSize());
		System.out.println("list: "+pd.getList());
		System.out.println("totalcount: "+pd.getTotalCount());
		System.out.println("totalpage: "+pd.getTotalPage());
	}
	
	/**
	 * 对添加客户个人信息进行测试
	 */
//	@Test
//	public void TestAddCustomer() {
//		CustomerDao bd=new CustomerDao();
//		Customer c = new Customer("c0000000015", "赞赞","女", "15163200002", "a00001", 0.0, "15163200002", "e10adc3949ba59abbe56e057f20f883e", 0, 1, null);
//		boolean flag = bd.AddCustomer(c);
//		System.out.println(flag);
//	}
	/**
	 * 对注册进行测试
	 */
//	@Test
//	public void TestAddCusBytell() {
//		CustomerDao bd=new CustomerDao();
//		Customer c = new Customer("c0000111111","15160203881","15160203881","123456",1);
//		boolean flag = bd.AddCusBytell(c);
//		System.out.println(flag);
//	}
	/**
	 * 删除客户个人信息进行测试
	 */
	@Test
	public void TestDelCustomer() {
		CustomerDao bd=new CustomerDao();
		boolean flag = bd.DelCustomer("c0000000014");
		System.out.println(flag);
	}
	
	/**
	 * 按客户个人编号修改客户个人信息姓名，性别，电话号码进行测试
	 */
	@Test
	public void TestupdCusById() {
		CustomerDao bd=new CustomerDao();
		Customer c=new Customer("c0000000014","zz","女","15160203221");
		boolean flag =bd.updCusById(c);
		System.out.println(flag);
	}
	
	/**
	 * 按通过手机号重置密码进行测试
	 */
	@Test
	public void TestupdCusPwdBytell() {
		CustomerDao bd=new CustomerDao();
		Customer c=new Customer("15160203221","887986bacc454b0bec7e0722f932b156");
		boolean flag =bd.updCusPwdBytell(c);
		System.out.println(flag);
	}
	
	
	/**
	 * 对用户密码进行加密的测试
	 */
	@Test
	public void TestMD5() {
		String str=MD5Util.getEncodeByMd5("abxddd");
		System.out.println(str);
		
	}
	
}
