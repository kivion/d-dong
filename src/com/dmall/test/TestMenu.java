package com.dmall.test;

import java.util.List;

import org.junit.Test;

import com.dmall.dao.CustomerDao;
import com.dmall.dao.MenueDao;
import com.dmall.entity.Customer;
import com.dmall.entity.Menue;
import com.dmall.pagedata.PageData;
import com.dmall.service.MenuService;
import com.dmall.util.AjaxResponse;
import com.google.gson.Gson;

public class TestMenu {
	MenuService ms = new MenuService();
	@Test
	public void testGetMenu() {
		String keyword = "健康";
		int pageNo = 1;
		int pageSize = 5;
		PageData<Menue> pd = ms.getMenu(keyword, pageNo, pageSize);
		pd.getList().forEach(System.out::println);
	} 
	@Test
	public void testAddMenu() {
		Menue menu = new Menue("m01011", "蜜桃", "水蜜桃2个；纯净水1瓶；茶包1包", "第一步：；第二步：第三步：","", "零厨艺",
				"20-30分钟", "c0000000001");
		boolean flag = ms.addMenue(menu);
		System.out.println(flag);
	}
	@Test
	public void testAjaxMenu() {
		String keyword = "健康";
		int pageNo = 1;
		int pageSize = 5;
		PageData<Menue> pd = ms.getMenu(keyword, pageNo, pageSize);
		pd.getList().forEach(System.out::println);
		
		AjaxResponse<Menue> ar = new AjaxResponse<Menue>(0,"success",pd.getTotalCount(),pd.getList());
		//将菜谱菜谱信息转换成json格式在控制台输出
		Gson gson = new Gson();
		String str = gson.toJson(ar);
		System.out.println("srt："+str);
	}
	@Test
	public void  TestgetMenuByKey() {
		List<Menue> list=ms.getMenulist("");
		list.forEach(System.out::println);
	}
	@Test
	public void  TestgetMenuBymid() {
		List<Menue> list=ms.getMenulist("m00002");
		list.forEach(System.out::println);
	}
}
