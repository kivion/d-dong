package com.dmall.entity;

public class MenueTypeNext {
	private int mtn_sn;
	private String mtn_id;
	private String mtn_name;
	private String mtf_id;
	
	public MenueTypeNext() {
		// TODO Auto-generated constructor stub
	}

	public MenueTypeNext(int mtn_sn, String mtn_id, String mtn_name, String mtf_id) {
		super();
		this.mtn_sn = mtn_sn;
		this.mtn_id = mtn_id;
		this.mtn_name = mtn_name;
		this.mtf_id = mtf_id;
	}

	public int getMtn_sn() {
		return mtn_sn;
	}

	public void setMtn_sn(int mtn_sn) {
		this.mtn_sn = mtn_sn;
	}

	public String getMtn_id() {
		return mtn_id;
	}

	public void setMtn_id(String mtn_id) {
		this.mtn_id = mtn_id;
	}

	public String getMtn_name() {
		return mtn_name;
	}

	public void setMtn_name(String mtn_name) {
		this.mtn_name = mtn_name;
	}

	public String getMtf_id() {
		return mtf_id;
	}

	public void setMtf_id(String mtf_id) {
		this.mtf_id = mtf_id;
	}

	@Override
	public String toString() {
		return "MenueTypeNext [mtn_sn=" + mtn_sn + ", mtn_id=" + mtn_id + ", mtn_name=" + mtn_name + ", mtf_id="
				+ mtf_id + "]";
	}
	
}
