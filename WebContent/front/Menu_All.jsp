<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	
	   <!-- 导入了一些必要的包 -->
<%@page import="com.dmall.service.MenuService"%>
<%@page import="com.dmall.entity.Menue"%>
<%@page import="java.util.List"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
    <!-- taglib -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	



<!doctype html>
<html class="no-js" lang="en">

<head>

<c:set var="basepath" scope="page"
	value="${pageContext.request.contextPath}"></c:set>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>菜谱—所有笔记浏览</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon"
	href="front/assets/img/favicon.ico">

<!-- CSS 
    ========================= -->
<!--bootstrap min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/bootstrap.min.css">
<!--owl carousel min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/owl.carousel.min.css">
<!--slick min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/slick.css">
<!--magnific popup min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/magnific-popup.css">
<!--font awesome css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/font.awesome.css">
<!--ionicons css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/ionicons.min.css">
<!--linearicons css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/linearicons.css">
<!--animate css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/animate.css">
<!--jquery ui min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/jquery-ui.min.css">
<!--slinky menu css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/slinky.menu.css">
<!--plugins css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/plugins.css">

<!-- Main Style CSS -->
<link rel="stylesheet" href="${basepath}/front/assets/css/style.css">
<!--layUI的css-->
<link rel="stylesheet" href="${basepath}/front/layui/css/layui.css" media="all">

<!--modernizr min js here-->
<script src="${basepath}/front/assets/js/vendor/modernizr-3.7.1.min.js"></script>

</head>

<body>
	<!--头部开始-->
	<header>
		<div class="main_header">
			<div class="header_top">
				<div class="container">
					<div class="row align-items-center">

						<!--导航栏-->
						<div class="col-lg-6 col-md-6">
							<div class="language_currency">
								<ul>
									<li class="language"><a href="#"> 语言 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_language">
											<li><a href="#">中文</a></li>
											<li><a href="#">英语</a></li>
										</ul></li>
									<li class="currency"><a href="#"> 简繁体 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_currency">
											<li><a href="#">简体</a></li>
											<li><a href="#">繁体</a></li>
										</ul></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="header_social text-right">
								<ul>
									<li><a href="#"><i class="ion-social-twitter"></i></a></li>
									<li><a href="#"><i
											class="ion-social-googleplus-outline"></i></a></li>
									<li><a href="#"><i class="ion-social-youtube-outline"></i></a>
									</li>
									<li><a href="#"><i class="ion-social-facebook"></i></a></li>
									<li><a href="#"><i
											class="ion-social-instagram-outline"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_middle">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-2">
							<div class="logo">
								<a href="index.html"><img src="assets/img/logo/logo.jpg"
									alt=""></a>
							</div>
						</div>
						<div class="col-lg-10">
							<div class="header_right_info">
								<!--导航栏左边的类别-->
								<div class="search_container">
									<form action="../menuhome.do" method="post">
										<div class="hover_category">
											<select class="select_option" name="select" id="categori2">
												<option selected value="1">热门</option>
												<option value="2">土豆</option>
												<option value="3">白菜</option>
												<option value="4">茄子</option>
												<option value="5">鸡胸肉</option>
												<option value="6">鸡腿</option>
												<option value="7">炒饭</option>
												<option value="8">面条</option>
												<option value="9">红烧肉</option>
												<option value="10">蛋糕</option>
												<option value="11">热饮</option>
												<option value="12">粥</option>
												<option value="13">减肥餐</option>
												<option value="14">汤</option>
												<option value="15">排骨</option>
												<option value="16">牛肉</option>
											</select>
										</div>
										<div class="search_box">
											<input placeholder="搜索食材、菜谱  (๑´•﹃•`๑)" type="text" name="keywords">
											<button type="submit">
												<span class="lnr lnr-magnifier"></span>
											</button>
										</div>
									</form>
								</div>
								<div class="header_account_area">
									<div class="header_account_list register">
										<ul class="layui-nav">
											<!--layUI的状态头像登录-->
											<li class="layui-nav-item" lay-unselect=""><a
												href="javascript:;"><img src="assets/img/icon/1.jpg"
													class="layui-nav-img">佚名</a>
												<dl class="layui-nav-child">
													<dd>
														<a href="javascript:;">修改信息</a>
													</dd>
													<dd>
														<a href="javascript:;">账号管理</a>
													</dd>
													<dd>
														<a href="javascript:;">退出</a>
													</dd>
												</dl></li>
											<!--<li>
													<a href="login.html">注册</a>
												</li>
												<li><span>/</span></li>
												<li>
													<a href="login.html">登录</a>
												</li>-->
										</ul>
									</div>
									<div class="header_account_list header_wishlist">
										<a href="收藏页面.html"><span class="lnr lnr-heart"></span> <span
											class="item_count">5</span> </a>
									</div>
									<div class="header_account_list  mini_cart_wrapper">
										<a href="购物车.html"><span class="lnr lnr-cart"></span><span
											class="item_count">2</span></a>
										<!--mini cart-->
										<div class="mini_cart">
											<div class="cart_gallery">
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Primis In Faucibus</a>
														<p>
															1 x <span> $65.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product2.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Letraset Sheets</a>
														<p>
															1 x <span> $60.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
											</div>
											<div class="mini_cart_table">
												<div class="cart_table_border">
													<div class="cart_total mt-10">
														<span>总计:</span> <span class="price">$125.00</span>
													</div>
												</div>
											</div>
											<div class="mini_cart_footer">
												<div class="cart_button">
													<a href="购物车.html"><i class="fa fa-shopping-cart"></i>
														前往购物车</a>
												</div>
												<div class="cart_button">
													<a href="#"><i class="fa fa-sign-in"></i> ...</a>
												</div>

											</div>
										</div>
										<!--mini cart end-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_bottom sticky-header">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-3">
							<div class="categories_menu">
								<div class="categories_title">
									<h2 class="categori_toggle">全部分类</h2>
								</div>
								<div class="categories_menu_toggle">
									<ul>
										<li class="menu_item_children"><a href="#">菜式<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu">
												<li class="menu_item_children"><a href="#">快手菜</a>
													<ul class="categorie_sub_menu">
														<li><a href="">家常菜</a></li>
														<li><a href="">下素菜</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">宝宝辅食</a>
													<ul class="categorie_sub_menu">
														<li><a href="">烹饪基础</a></li>
														<li><a href="">异国料理</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">素菜</a>
													<ul class="categorie_sub_menu">
														<li><a href="">沙拉</a></li>
														<li><a href="">汤 </a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">减肥餐</a>
													<ul class="categorie_sub_menu">
														<li><a href="">孕妇餐</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#">场景 <i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_3">
												<li class="menu_item_children"><a href="#">早餐</a>
													<ul class="categorie_sub_menu">
														<li><a href="">便当</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">午餐</a>
													<ul class="categorie_sub_menu">
														<li><a href="">宿舍食谱</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">晚餐</a></li>
											</ul></li>
										<li class="menu_item_children"><a href="#"> 主食<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<ul class="categorie_sub_menu">
													<li><a href="">炒面</a></li>
													<li><a href="">炒饭</a></li>
													<li><a href="">粥</a></li>
													<li><a href="">饼</a></li>
												</ul>
											</ul></li>
										<li class="menu_item_children"><a href="#"> 烹饪方式 <i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<ul class="categorie_sub_menu">
													<li><a href="">烤箱</a></li>
													<li><a href="">电饭锅</a></li>
													<li><a href="">平底锅</a></li>
													<li><a href="">微波炉</a></li>
												</ul>
											</ul></li>
										<li class="menu_item_children"><a href="#">甜品饮品 <i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_3">
												<li class="menu_item_children"><a href="#">蛋糕</a>
													<ul class="categorie_sub_menu">
														<li><a href="">面包</a></li>
														<li><a href="">饮品</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">抹茶</a>
													<ul class="categorie_sub_menu">
														<li><a href="">免烤甜品</a></li>
														<li><a href="">冰淇淋</a></li>
													</ul></li>

											</ul></li>
										<li class="menu_item_children"><a href="#">食材<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_3">
												<li class="menu_item_children"><a href="#">猪肉</a>
													<ul class="categorie_sub_menu">
														<li><a href="">鸡肉</a></li>
														<li><a href="">牛肉</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">羊肉</a>
													<ul class="categorie_sub_menu">
														<li><a href="">鸭肉</a></li>
														<li><a href="">海鲜水产</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">蔬菜</a>
													<ul class="categorie_sub_menu">
														<li><a href="">方便速食</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#"> 风味 <i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<ul class="categorie_sub_menu">
													<li><a href="">香辣</a></li>
													<li><a href="">糖醋</a></li>
													<li><a href="">清淡</a></li>
													<li><a href="">烧烤</a></li>
												</ul>
											</ul></li>
										<li id="cat_toggle" class="has-sub"><a href="#">敬请期待更多
												ε≡?(?>? <)? </a>
											<ul class="categorie_sub">
												<li><a href="#">Hide Categories</a></li>
											</ul></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<!--main menu start-->
							<div class="main_menu menu_position">
								<nav>
									<ul>
										<li><a href="index.html">主页</a></li>
										<li><a href="菜谱—主页.html">菜谱主页</a></li>
										<li><a href="菜谱—收藏.html">收藏</a></li>
										<li><a href="菜谱—发布.html">发布</a></li>
										<li><a href="菜谱—个人笔记展示.html">个人笔记</a></li>
									</ul>
								</nav>
							</div>
							<!--main menu end-->
						</div>
						<div class="col-lg-3">
							<div class="call-support">
								<p>
									<a href="tel:(08)23456789">(08) 23 456 789</a> Customer Support
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!--header area end-->

	<!--breadcrumbs area start-->
	<div class="breadcrumbs_area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="breadcrumb_content">
						<h3>吃货的世界没有诗和远方，唯有美食不可辜负</h3>
						<ul>
							<li>没有什么是一顿大餐不能解决的，如果有，那就来两顿</li>
							<br>
							<li>o(´^｀)o</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--breadcrumbs area end-->

	<!--blog area start-->
	<div class="blog_page_section blog_fullwidth mt-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-12">
					<div class="blog_wrapper">
						<article class="single_blog">
							<figure>
								<div class="blog_thumb">
									<img src="${basepath}/front/assets/img/menu-personal/personalblog/video_capture_gif.gif"
										alt=""></a>
								</div>
								<figcaption class="blog_content">
								<h3 >置顶</h3>
								<br>
									<h4 class="post_title">
										夏天的味道~蜜桃乌龙  
									</h4>
									<div class="articles_date">
										<p>
											2021-01-11 18:09:02.0 | 纠结ing</a>
										</p>
									</div>
									<p class="post_desc">步骤一：茶包用350ml热水冲泡2分钟后....</p>
									<footer class="btn_more">
										<a href="菜谱—笔记详情页.html">查看详情</a>
									</footer>
								</figcaption>
							</figure>
						</article>
						<c:forEach items="${list}" var="m">
						<article class="single_blog">
								
							<figure>
							
								<div class="blog_thumb">
									<img src="front/assets/img/menu-personal/personalblog/personal2.jpg"
										alt="">
								</div>
								<figcaption class="blog_content">
								
									<h4 class="post_title">
									${m.m_name} 
									</h4>
									<div class="articles_date">
										<p>
											${m.m_creat} | ${m.customer.getC_name()}
										</p>
									
									</div>
										
									<p class="post_desc">步骤一：....</p>
									<footer class="btn_more">
										<!--  <input type="button" class="btndetail btn btn-success btn-lg" value=" 查看详情  ">  -->
										
									<a href="./menuhome.do?op=menudetail&menuid=${m.m_id}"> 查看详情</a>
									</footer>
								
								</figcaption>
							</figure>
						
						</article>
</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--blog area end-->
		
	<!--blog pagination area start-->

	<!--blog pagination area end-->

	<!--footer area start-->
	<footer class="footer_widgets">
		<div class="footer_top">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-7">
						<div class="widgets_container contact_us">
							<div class="footer_logo">
								<a href="index.html"><img src="assets/img/logo/logo.png"
									alt=""></a>
							</div>
							<p class="footer_desc">服务、质量、效率</p>
							<p>
								<span>Address:</span> xmzretc
							</p>
							<p>
								<span>Email:</span> <a href="#">760008559@qq.com</a>
							</p>
							<p>
								<span>Call us:</span> <a href="tel:(08)23456789">(08) 23 456
									789</a>
							</p>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关于叮咚到家</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">了解叮咚</a></li>
									<li><a href="#">加入叮咚</a></li>
									<li><a href="#">投资者关系</a></li>
									<li><a href="#">团队信息</a></li>
									<li><a href="contact.html">联系我们</a></li>
									<li><a href="#">廉洁举报</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关注我们</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">新浪微博</a></li>
									<li><a href="#">官方微信</a></li>
									<li><a href="#">Q群加入</a></li>
									<li><a href="#">公益基金会</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>特色服务</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">兑换券码领取</a></li>
									<li><a href="#">礼物码</a></li>
									<li><a href="#">防伪查询</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4">
						<div class="widgets_container widget_menu">
							<h3>帮助中心</h3>
							<div class="footer_menu">
								<ul>
									<li><a href="#">账户管理</a></li>
									<li><a href="#">购物指南</a></li>
									<li><a href="#">订单操作</a></li>
									<li><a href="#">售后政策</a></li>
									<li><a href="#">自助服务</a></li>
									<li><a href="#">相关下载</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer_bottom">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-7">
						<div class="copyright_area">
							<!--<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a>-->
							<p>Copyright &copy; 2020.闽ICP证110xxx号 闽ICP备10046xxx号
								闽公网安备11010802020 号</p>
						</div>
					</div>
					<div class="col-lg-6 col-md-5">
						<div class="footer_payment">
							<ul>
								<li><a href="#"><img src="${basepath}/front/assets/img/icon/v-logo-1.png"
										alt=""></a></li>
								<li><a href="#"><img src="${basepath}/front/assets/img/icon/v-logo-2.png"
										alt=""></a></li>
								<li><a href="#"><img src="${basepath}/front/assets/img/icon/v-logo-3.png"
										alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--footer area end-->

	<!-- JS
============================================ -->
	<!--jquery min js-->
	<script src="${basepath}/front/assets/js/vendor/jquery-3.4.1.min.js"></script>
	<!--popper min js-->
	<script src="${basepath}/front/assets/js/popper.js"></script>
	<!--bootstrap min js-->
	<script src="${basepath}/front/assets/js/bootstrap.min.js"></script>
	<!--owl carousel min js-->
	<script src="${basepath}/front/assets/js/owl.carousel.min.js"></script>
	<!--slick min js-->
	<script src="${basepath}/front/assets/js/slick.min.js"></script>
	<!--magnific popup min js-->
	<script src="${basepath}/front/assets/js/jquery.magnific-popup.min.js"></script>
	<!--counterup min js-->
	<script src="${basepath}/front/assets/js/jquery.counterup.min.js"></script>
	<!--jquery countdown min js-->
	<script src="${basepath}/front/assets/js/jquery.countdown.js"></script>
	<!--jquery ui min js-->
	<script src="${basepath}/front/assets/js/jquery.ui.js"></script>
	<!--jquery elevatezoom min js-->
	<script src="${basepath}/front/assets/js/jquery.elevatezoom.js"></script>
	<!--isotope packaged min js-->
	<script src="${basepath}/front/assets/js/isotope.pkgd.min.js"></script>
	<!--slinky menu js-->
	<script src="${basepath}/front/assets/js/slinky.menu.js"></script>
	<!-- Plugins JS -->
	<script src="${basepath}/front/assets/js/plugins.js"></script>

	<!-- Main JS -->
	<script src="${basepath}/front/assets/js/main.js"></script>
	<!--layUI的js-->
	<script src="${basepath}/front/layui/layui.js" charset="utf-8"></script>
	<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
	<script>
		layui.use('element', function() {
			var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

			//监听导航点击
			element.on('nav(demo)', function(elem) {
				//console.log(elem)
				layer.msg(elem.text());
			});
		});
	</script>
</body>

</html>