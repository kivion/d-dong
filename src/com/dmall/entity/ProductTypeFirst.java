package com.dmall.entity;

public class ProductTypeFirst {
	private String ptf_id;
	private String ptf_name;
	private int ptf_sn;

	public ProductTypeFirst(String ptf_id, String ptf_name, int ptf_sn) {
		super();
		this.ptf_id = ptf_id;
		this.ptf_name = ptf_name;
		this.ptf_sn = ptf_sn;
	}

	public ProductTypeFirst() {
	}

	public String getPtf_id() {
		return ptf_id;
	}

	public void setPtf_id(String ptf_id) {
		this.ptf_id = ptf_id;
	}

	public String getPtf_name() {
		return ptf_name;
	}

	public void setPtf_name(String ptf_name) {
		this.ptf_name = ptf_name;
	}

	public int getPtf_sn() {
		return ptf_sn;
	}

	public void setPtf_sn(int ptf_sn) {
		this.ptf_sn = ptf_sn;
	}

	@Override
	public String toString() {
		return "ProductTypeFirst [ptf_id=" + ptf_id + ", ptf_name=" + ptf_name + ", ptf_sn=" + ptf_sn + "]";
	}

}
