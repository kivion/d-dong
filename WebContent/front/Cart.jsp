<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 加入 taglib 标记-->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html class="no-js" lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>叮咚到家 - 购物车</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:set var="basepath" scope="page"
	value="${pageContext.request.contextPath}"></c:set>
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon"
	href="assets/img/favicon.ico">

<!-- CSS 
    ========================= -->
<!--bootstrap min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/bootstrap.min.css">
<!--owl carousel min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/owl.carousel.min.css">
<!--slick min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/slick.css">
<!--magnific popup min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/magnific-popup.css">
<!--font awesome css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/font.awesome.css">
<!--ionicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/ionicons.min.css">
<!--linearicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/linearicons.css">
<!--animate css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/animate.css">
<!--jquery ui min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/jquery-ui.min.css">
<!--slinky menu css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/slinky.menu.css">
<!--plugins css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/plugins.css">

<!-- Main Style CSS -->
<link rel="stylesheet" href="${basepath}/front/assets/css/style.css">

<!--layUI的css-->
<link rel="stylesheet" href="${basepath}/back/lib/layui/css/layui.css"
	media="all">
<!--layui-->
<script type="text/javascript" src="${basepath}/back/lib/layui/layui.js"
	charset="utf-8"></script>

<!--modernizr min js here-->
<script src="${basepath}/front/assets/js/vendor/modernizr-3.7.1.min.js"></script>

</head>

<body>
	<!--头部开始-->
	<header>
		<div class="main_header">
			<div class="header_top">
				<div class="container">
					<div class="row align-items-center">

						<!--导航栏-->
						<div class="col-lg-6 col-md-6">
							<div class="language_currency">
								<ul>
									<li class="language"><a href="#"> 语言 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_language">
											<li><a href="#">中文</a></li>
											<li><a href="#">英语</a></li>
										</ul></li>
									<li class="currency"><a href="#"> 简繁体 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_currency">
											<li><a href="#">简体</a></li>
											<li><a href="#">繁体</a></li>
										</ul></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="header_social text-right">
								<ul>
									<li><a href="#"><i class="ion-social-twitter"></i></a></li>
									<li><a href="#"><i
											class="ion-social-googleplus-outline"></i></a></li>
									<li><a href="#"><i class="ion-social-youtube-outline"></i></a>
									</li>
									<li><a href="#"><i class="ion-social-facebook"></i></a></li>
									<li><a href="#"><i
											class="ion-social-instagram-outline"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_middle">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-2">
							<div class="logo">
								<a href="${basepath}/producthome.do"><img
									src="${basepath}/front/assets/img/logo/logo.jpg" alt=""></a>
							</div>
						</div>
						<div class="col-lg-10">
							<div class="header_right_info">
								<!--导航栏左边的类别-->
								<div class="search_container">
									<form action="#">
										<div class="hover_category">
											<select class="select_option" name="select" id="categori1">
												<option selected value="1">选择类别</option>
												<option value="2">水果</option>
												<option value="3">蔬菜</option>
												<option value="4">肉禽蛋品</option>
												<option value="5">海鲜水产</option>
												<option value="6">熟食卤味</option>
												<option value="7">粮油调味</option>
												<option value="8">休闲零食</option>
												<option value="9">冰品面点</option>
												<option value="10">牛奶面包</option>
												<option value="11">酒水冲饮</option>
											</select>
										</div>
										<div class="search_box">
											<input placeholder="国产红心火龙果0.99元/个..." type="text">
											<button type="submit">
												<span class="lnr lnr-magnifier"></span>
											</button>
										</div>
									</form>
								</div>
								<div class="header_account_area">
									<div class="header_account_list register">
										<ul class="layui-nav">
											<!--layUI的状态头像登录-->
											<li class="layui-nav-item" lay-unselect=""><a
												href="${basepath}/front/my-account.jsp"><img
													src="${basepath}/front/assets/img/icon/1.jpg"
													class="layui-nav-img">佚名</a>
												<dl class="layui-nav-child">
													<dd>
														<a href="javascript:;">修改信息</a>
													</dd>
													<dd>
														<a href="javascript:;">账号管理</a>
													</dd>
													<dd>
														<a href="javascript:;">退出</a>
													</dd>
												</dl></li>
											<!--<li>
													<a href="login.html">注册</a>
												</li>
												<li><span>/</span></li>
												<li>
													<a href="login.html">登录</a>
												</li>-->
										</ul>
										<!--<script>
												$('.layui-nav').css('background','#fff !important');
												$('.layui-nav .layui-nav-item a').css('color','#000 !important');
											</script>-->
									</div>
									<div class="header_account_list header_wishlist">
										<a href="收藏页面.html"><span class="lnr lnr-heart"></span> <span
											class="item_count">5</span> </a>
									</div>
									<div class="header_account_list  mini_cart_wrapper">
										<a href="${basepath}/front/Cart.jsp"><span class="lnr lnr-cart"></span><span
											class="item_count">2</span></a>
										<!--mini cart-->
										<div class="mini_cart">
											<div class="cart_gallery">
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Primis In Faucibus</a>
														<p>
															1 x <span> $65.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="assets/img/s-product/product2.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Letraset Sheets</a>
														<p>
															1 x <span> $60.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
											</div>
											<div class="mini_cart_table">
												<div class="cart_table_border">
													<div class="cart_total mt-10">
														<span>总计:</span> <span class="price">$125.00</span>
													</div>
												</div>
											</div>
											<div class="mini_cart_footer">
												<div class="cart_button">
													<a href="${basepath}/front/Cart.jsp"><i class="fa fa-shopping-cart"></i>
														前往购物车</a>
												</div>
												<div class="cart_button">
													<a href="#"><i class="fa fa-sign-in"></i> ...</a>
												</div>

											</div>
										</div>
										<!--mini cart end-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_bottom sticky-header">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-3">
							<div class="categories_menu">
								<div class="categories_title">
									<h2 class="categori_toggle">所有商品</h2>
								</div>
								<div class="categories_menu_toggle">
									<ul>
										<li class="menu_item_children"><a href="#part1">蔬菜<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu">
												<li class="menu_item_children"><a href="#">Dresses</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Sweater</a></li>
														<li><a href="">Evening</a></li>
														<li><a href="">Day</a></li>
														<li><a href="">Sports</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Handbags</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Shoulder</a></li>
														<li><a href="">Satchels</a></li>
														<li><a href="">kids</a></li>
														<li><a href="">coats</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">shoes</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Ankle Boots</a></li>
														<li><a href="">Clog sandals </a></li>
														<li><a href="">run</a></li>
														<li><a href="">Books</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Clothing</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Coats Jackets </a></li>
														<li><a href="">Raincoats</a></li>
														<li><a href="">Jackets</a></li>
														<li><a href="">T-shirts</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part1">水果<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_3">
												<li class="menu_item_children"><a href="#">Chair</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Dining room</a></li>
														<li><a href="">bedroom</a></li>
														<li><a href=""> Home & Office</a></li>
														<li><a href="">living room</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Lighting</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Ceiling Lighting</a></li>
														<li><a href="">Wall Lighting</a></li>
														<li><a href="">Outdoor Lighting</a></li>
														<li><a href="">Smart Lighting</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Sofa</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Fabric Sofas</a></li>
														<li><a href="">Leather Sofas</a></li>
														<li><a href="">Corner Sofas</a></li>
														<li><a href="">Sofa Beds</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part1">肉禽蛋品<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">Brake
														Tools</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Driveshafts</a></li>
														<li><a href="">Spools</a></li>
														<li><a href="">Diesel </a></li>
														<li><a href="">Gasoline</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Emergency
														Brake</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Dolls for Girls</a></li>
														<li><a href="">Girls' Learning Toys</a></li>
														<li><a href="">Arts and Crafts for Girls</a></li>
														<li><a href="">Video Games for Girls</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part2">海鲜水产<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">Check
														Trousers</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Building</a></li>
														<li><a href="">Electronics</a></li>
														<li><a href="">action figures </a></li>
														<li><a href="">specialty & boutique toy</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Calculators</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Dolls for Girls</a></li>
														<li><a href="">Girls' Learning Toys</a></li>
														<li><a href="">Arts and Crafts for Girls</a></li>
														<li><a href="">Video Games for Girls</a></li>
													</ul></li>
											</ul></li>
										<li><a href="#part3">熟食卤味</a></li>
										<li><a href="#part4">休闲零食</a></li>
										<li><a href="#part5">粮油调味</a></li>
										<li><a href="#">冰品面点</a></li>
										<li><a href="#">牛奶面包</a></li>
										<li><a href="#">酒水冲饮</a></li>

										<li id="cat_toggle" class="has-sub"><a href="#">>更多</a>
											<ul class="categorie_sub">
												<li><a href="#">O(∩_∩)O哈哈~</a></li>
											</ul></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<!--main menu start-->
							<div class="main_menu menu_position">
								<nav>
									<ul>
										<li><a class="active" href="${basepath}/front/index.jsp">主页</a></li>
										<li><a href="菜谱—主页.html">菜谱 <!--<i class="fa fa-angle-down"></i>--></a>
										</li>
										<li><a href="关于我们.html">关于我们</a></li>
										<li><a href="联系我们.html">联系我们</a></li>
									</ul>
								</nav>
							</div>
							<!--main menu end-->
						</div>
						<div class="col-lg-3">
							<div class="call-support">
								<p>
									<a href="tel:(08)8399608">(08)8399 608</a> 用户反馈
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!--header area end-->

	<!--购物车开始-->
	<!--breadcrumbs area start-->
	<div class="breadcrumbs_area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="breadcrumb_content">
						<h3>购物车</h3>
						<ul>
							<li><a href="#">小咚</a></li>
							<li>主人，我都整理好了</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--breadcrumbs area end-->

	<!--shopping cart area start -->
	<div class="shopping_cart_area mt-70">
		<div class="container">
			<form action="#">
				<div class="row">
					<div class="col-12">
						<div class="table_desc">
							<div class="cart_page table-responsive">
								<form class="layui-form" action="" action="">

									<div class="table-responsive">
										<table class="layui-table"
											lay-data="{url:'../cart.do',page:true,toolbar: '#toolbarDemo',id:'test'}"
											lay-filter="test">
											<thead>
												<tr>
													<th lay-data="{type:'checkbox'}">全选</th>

													<th lay-data="{field:'', minWidth:120}">图片</th>
													<th
														lay-data="{field:'p_name', edit: 'text', minWidth: 150}">商品</th>

													<th
														lay-data="{field:'p_price', edit: 'text', minWidth: 150}">单价</th>
													<th
														lay-data="{field:'cart_num', edit: 'text', minWidth: 150}">数量</th>

													<th lay-data="{field:'c_total',minWidth:120}">总价</th>

													<th lay-data="{field:'', minWidth:150,templet: '#opTpl' }">操作</th>
												</tr>
											</thead>
										</table>
									</div>
									<div class="layui-form-item">

										<button class="layui-btn" lay-submit lay-filter="sub" style=""
											type="submit" />
										去结算
									</div>
								</form>
							</div>

						</div>
					</div>
				</div>
				<!--coupon code area start-->
				<!--<div class="coupon_area">
						<div class="row">
							<div class="col-lg-6 col-md-6">
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="coupon_code right">
									<h3>订单详细</h3>
									<div class="coupon_inner">
										<div class="cart_subtotal">
											<p>总计</p>
											<p class="cart_amount">£215.00</p>
										</div>
										<div class="cart_subtotal ">
											<p>优惠总减</p>
											<p class="cart_amount">£255.00</p>
										</div>
										<a href="#">查看减额明细</a>

										<div class="cart_subtotal">
											<p>最终价格</p>
											<p class="cart_amount">£215.00</p>
										</div>
										<div class="checkout_btn">
											<a href="#">支付</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>-->
				<!--coupon code area end-->
			</form>
		</div>
	</div>
	<!--shopping cart area end -->

	<div class="brand_area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="brand_container owl-carousel ">
						<!--<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand1.jpg" alt=""></a>
							</div>
							<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand2.jpg" alt=""></a>
							</div>
							<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand3.jpg" alt=""></a>
							</div>
							<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand4.jpg" alt=""></a>
							</div>
							<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand2.jpg" alt=""></a>
							</div>-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--brand area end-->
	<!--brand area end-->

	<!--footer area start-->
	<footer class="footer_widgets">
		<div class="footer_top">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-7">
						<div class="widgets_container contact_us">
							<div class="footer_logo">
								<a href="index.html"><img src="" alt=""></a>
							</div>
							<p class="footer_desc">服务、质量、效率</p>
							<p>
								<span>Address:</span> xmzretc
							</p>
							<p>
								<span>Email:</span> <a href="#">760008559@qq.com</a>
							</p>
							<p>
								<span>Call us:</span> <a href="tel:(08)23456789">(08) 23 456
									789</a>
							</p>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关于叮咚到家</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">了解叮咚</a></li>
									<li><a href="#">加入叮咚</a></li>
									<li><a href="#">投资者关系</a></li>
									<li><a href="#">团队信息</a></li>
									<li><a href="contact.html">联系我们</a></li>
									<li><a href="#">廉洁举报</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关注我们</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">新浪微博</a></li>
									<li><a href="#">官方微信</a></li>
									<li><a href="#">Q群加入</a></li>
									<li><a href="#">公益基金会</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>特色服务</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">兑换券码领取</a></li>
									<li><a href="#">礼物码</a></li>
									<li><a href="#">防伪查询</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4">
						<div class="widgets_container widget_menu">
							<h3>帮助中心</h3>
							<div class="footer_menu">
								<ul>
									<li><a href="#">账户管理</a></li>
									<li><a href="#">购物指南</a></li>
									<li><a href="#">订单操作</a></li>
									<li><a href="#">售后政策</a></li>
									<li><a href="#">自助服务</a></li>
									<li><a href="#">相关下载</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer_bottom">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-7">
						<div class="copyright_area">
							<!--<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a>-->
							<p>Copyright &copy; 2020.闽ICP证110xxx号 闽ICP备10046xxx号
								闽公网安备11010802020 号</p>
						</div>
					</div>
					<div class="col-lg-6 col-md-5">
						<div class="footer_payment">
							<ul>
								<li><a href="#"><img src="assets/img/icon/v-logo-1.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-2.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-3.png"
										alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--footer area end-->

	<!-- modal area start-->
	<div class="modal fade" id="modal_box" tabindex="-1" role="dialog"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"><i class="icon-x"></i></span>
				</button>
				<div class="modal_body">
					<div class="container">
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								<div class="modal_tab">
									<div class="tab-content product-details-large">
										<div class="tab-pane fade show active" id="tab1"
											role="tabpanel">
											<div class="modal_tab_img">
												<a href="#"><img
													src="assets/img/product/productbig1.jpg" alt=""></a>
											</div>
										</div>
										<div class="tab-pane fade" id="tab2" role="tabpanel">
											<div class="modal_tab_img">
												<a href="#"><img
													src="assets/img/product/productbig2.jpg" alt=""></a>
											</div>
										</div>
										<div class="tab-pane fade" id="tab3" role="tabpanel">
											<div class="modal_tab_img">
												<a href="#"><img
													src="assets/img/product/productbig3.jpg" alt=""></a>
											</div>
										</div>
										<div class="tab-pane fade" id="tab4" role="tabpanel">
											<div class="modal_tab_img">
												<a href="#"><img
													src="assets/img/product/productbig4.jpg" alt=""></a>
											</div>
										</div>
									</div>
									<div class="modal_tab_button">
										<ul class="nav product_navactive owl-carousel" role="tablist">
											<li><a class="nav-link active" data-toggle="tab"
												href="#tab1" role="tab" aria-controls="tab1"
												aria-selected="false"><img
													src="assets/img/product/product1.jpg" alt=""></a></li>
											<li><a class="nav-link" data-toggle="tab" href="#tab2"
												role="tab" aria-controls="tab2" aria-selected="false"><img
													src="assets/img/product/product6.jpg" alt=""></a></li>
											<li><a class="nav-link button_three" data-toggle="tab"
												href="#tab3" role="tab" aria-controls="tab3"
												aria-selected="false"><img
													src="assets/img/product/product2.jpg" alt=""></a></li>
											<li><a class="nav-link" data-toggle="tab" href="#tab4"
												role="tab" aria-controls="tab4" aria-selected="false"><img
													src="assets/img/product/product7.jpg" alt=""></a></li>

										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<div class="modal_right">
									<div class="modal_title mb-10">
										<h2>Donec Ac Tempus</h2>
									</div>
									<div class="modal_price mb-10">
										<span class="new_price">$64.99</span> <span class="old_price">$78.99</span>
									</div>
									<div class="modal_description mb-15">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Mollitia iste laborum ad impedit pariatur esse optio
											tempora sint ullam autem deleniti nam in quos qui nemo ipsum
											numquam, reiciendis maiores quidem aperiam, rerum vel
											recusandae</p>
									</div>
									<div class="variants_selects">
										<div class="variants_size">
											<h2>size</h2>
											<select class="select_option">
												<option selected value="1">s</option>
												<option value="1">m</option>
												<option value="1">l</option>
												<option value="1">xl</option>
												<option value="1">xxl</option>
											</select>
										</div>
										<div class="variants_color">
											<h2>color</h2>
											<select class="select_option">
												<option selected value="1">purple</option>
												<option value="1">violet</option>
												<option value="1">black</option>
												<option value="1">pink</option>
												<option value="1">orange</option>
											</select>
										</div>
										<div class="modal_add_to_cart">
											<form action="#">
												<input min="1" max="100" step="2" value="1" type="number">
												<button type="submit">add to cart</button>
											</form>
										</div>
									</div>
									<div class="modal_social">
										<h2>Share this product</h2>
										<ul>
											<li class="facebook"><a href="#"><i
													class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a href="#"><i
													class="fa fa-twitter"></i></a></li>
											<li class="pinterest"><a href="#"><i
													class="fa fa-pinterest"></i></a></li>
											<li class="google-plus"><a href="#"><i
													class="fa fa-google-plus"></i></a></li>
											<li class="linkedin"><a href="#"><i
													class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- modal area end-->

	<!-- JS
============================================ -->
	<!--jquery min js-->
	<script src="${basepath}/front/assets/js/vendor/jquery-3.4.1.min.js"></script>
	<!--popper min js-->
	<script src="${basepath}/front/assets/js/popper.js"></script>
	<!--bootstrap min js-->
	<script src="${basepath}/front/assets/js/bootstrap.min.js"></script>
	<!--owl carousel min js-->
	<script src="${basepath}/front/assets/js/owl.carousel.min.js"></script>
	<!--slick min js-->
	<script src="${basepath}/front/assets/js/slick.min.js"></script>
	<!--magnific popup min js-->
	<script src="${basepath}/front/assets/js/jquery.magnific-popup.min.js"></script>
	<!--counterup min js-->
	<script src="${basepath}/front/assets/js/jquery.counterup.min.js"></script>
	<!--jquery countdown min js-->
	<script src="${basepath}/front/assets/js/jquery.countdown.js"></script>
	<!--jquery ui min js-->
	<script src="${basepath}/front/assets/js/jquery.ui.js"></script>
	<!--jquery elevatezoom min js-->
	<script src="${basepath}/front/assets/js/jquery.elevatezoom.js"></script>
	<!--isotope packaged min js-->
	<script src="${basepath}/front/assets/js/isotope.pkgd.min.js"></script>
	<!--slinky menu js-->
	<script src="${basepath}/front/assets/js/slinky.menu.js"></script>
	<!-- Plugins JS -->
	<script src="${basepath}/front/assets/js/plugins.js"></script>

	<!-- Main JS -->
	<script src="${basepath}/front/assets/js/main.js"></script>
	<!--layUI的js-->

	<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
	<script>
		layui.use([ 'table', 'form' ], function() {
			var table = layui.table;
			var form = layui.form;
			//监听单元格编辑
			table.on('edit(test)',
					function(obj) {
						var value = obj.value //得到修改后的值
						, data = obj.data //得到所在行所有键值
						, field = obj.field; //得到字段
						layer.msg('[ID: ' + data.id + '] ' + field + ' 字段更改为：'
								+ value);
					});
			//监听提交（添加地址 ）
			form.on('submit(sub)', function(data) {

				//发异步，把数据提交给servlet,代码需要我们自己完成
				//jquery
				$.ajax({
					url : '',
					//data	规定要发送到服务器的数据。
					data : data.field,
					//type	规定请求的类型POST
					type : 'post',
					//success(result,status,xhr)当请求成功时运行的函数，res是在Servlet返回的数据
					success : function(res) {
						console.log(res.msg);
						layer.alert(res.msg, {
							icon : 6
						});
					}
				//error(xhr,status,error)	如果请求失败要运行的函数。
				});
				return false;
			});
			//头工具栏事件
			table.on('toolbar(test)', function(obj) {
				var checkStatus = table.checkStatus(obj.config.id);
				switch (obj.event) {
				case 'getCheckData':
					var data = checkStatus.data;
					layer.alert(JSON.stringify(data));
					break;
				case 'getCheckLength':
					var data = checkStatus.data;
					layer.msg('选中了：' + data.length + ' 个');
					break;
				case 'isAll':
					layer.msg(checkStatus.isAll ? '全选' : '未全选');
					break;
				}
				;
			});
		});
	</script>
	<script>
		layui.use('element', function() {
			var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

			//监听导航点击
			element.on('nav(demo)', function(elem) {
				//console.log(elem)
				layer.msg(elem.text());
			});
		});
	</script>
</body>

</html>