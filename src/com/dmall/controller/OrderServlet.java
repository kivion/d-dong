package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dmall.entity.Menue;
import com.dmall.entity.Order;
import com.dmall.pagedata.PageData;
import com.dmall.service.MenuService;
import com.dmall.service.OrderService;
import com.dmall.util.AjaxResponse;
import com.google.gson.Gson;


@WebServlet("/order.do")
public class OrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private OrderService os = new OrderService();

    public OrderServlet() {
    	
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		doQuery(request, response);
		
	}
	protected void doQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String keywords = "";
		if (request.getParameter("keywords") != null) {
			keywords = request.getParameter("keywords");
		}
		int pageNo = 1;
		int pageSize = 10;
		if (request.getParameter("page") != null) {
			pageNo = Integer.parseInt(request.getParameter("page"));
		}

		if (request.getParameter("limit") != null) {
			pageSize = Integer.parseInt(request.getParameter("limit"));
		}
		String begindate = "";
		String enddate = "";
		//判断用户是否选择了查询所需的起始时间
		if(request.getParameter("start")!=null && request.getParameter("end")!=null) {
			begindate = request.getParameter("start");
			enddate = request.getParameter("end");
		}
		System.out.println("keywords:" + keywords+" "+"bigendate:"+begindate+" "+"enddate:"+enddate);
		
		// keywords传递的查询的关键字
		PageData<Order> pd = os.getOrderByPage(keywords,begindate,enddate, pageNo, pageSize);
		
		AjaxResponse<Order> ar = new AjaxResponse<Order>(0, "success", pd.getTotalCount(), pd.getList());

		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);
		
		out.print(str);
		out.close();
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
