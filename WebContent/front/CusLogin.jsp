<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<title>panda login</title>

<link rel="stylesheet" href="css/panda/style.css">

</head>

<body>
<%
   String cusname="";
   String cuspwd="";
   Cookie[] cookies=request.getCookies();
   for(Cookie ck:cookies){
	  if(ck.getName().equals("cusname")){
		  cusname=ck.getValue();
	}
	if(ck.getName().equals("cuspwd")){
		cuspwd=ck.getValue();
	}
}
%>
<div class="dwo">
	<div class="panda">
		<div class="ear"></div>
		<div class="face">
			<div class="eye-shade"></div>
			<div class="eye-white">
				<div class="eye-ball"></div>
			</div>
			<div class="eye-shade rgt"></div>
			<div class="eye-white rgt">
				<div class="eye-ball"></div>
			</div>
			<div class="nose"></div>
			<div class="mouth"></div>
		</div>
		<div class="body"> </div>
		<div class="foot">
			<div class="finger"></div>
		</div>
		<div class="foot rgt">
			<div class="finger"></div>
		</div>
	</div>
	<form class="form-signin" action="../Cuspanda.do?op=login" method="post">
		<div class="hand"></div>
		<div class="hand rgt"></div>
		<h1>用户登录</h1>
		<div class="form-group">
			<input required="required" class="form-control" name="cusname" value="<%=cusname %>">
			<label class="form-label">用户名</label>
		</div>
		<div class="form-group">
			<input id="password" type="password" required="required" class="form-control" name="cuspwd" value="<%=cuspwd %>">
			<label class="form-label">密码</label>
			<p class="alert">用户名或密码错误 ...</p>
			<button class="btn" type="submit">登 录</button><br><br>
			 <input type="hidden" name="rem" value="remember-me"> 
		     <a href="CusForget.jsp" style="text-decoration:none;"><p style="display: inline; color:#40A944 ;">忘记密码</p></a>
			 <p style="display: inline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
			<a href="CusAdd.jsp" style="text-decoration:none;"><p style="display: inline; color:#40A944 ;">注册</p></a>
			
		</div>
	</form>
	
</div>

<script src="js/panda/jquery.min.js"></script>
<script src="js/panda/script.js"></script>


</body>
</html>