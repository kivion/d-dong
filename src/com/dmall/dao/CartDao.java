package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Cart;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

/**
 * 购物车dao
 * 
 * @author yong
 *
 */
public class CartDao extends DBUtil {

	/**
	 * 新增购物车方法
	 * 
	 * @param cart 购物车对象
	 * @return true 成功 false 失败
	 */
	public boolean addCart(Cart cart) {
		// 判断cart是否为空
		if (cart == null) {
			return false;
		}
		// 定义sql语句
		String sql = "INSERT INTO `cart` (`c_id`, `cart_id`,`p_id`,`p_name`,`cart_num`,`c_total`) VALUES (?,?,?,?)";
		// 调用通用类代码
		boolean flag = exeUpdate(sql, cart.getC_id(), cart.getCart_id(), cart.getP_id(),cart.getP_name(),cart.getCart_num(),cart.getC_total());
		return flag;
	}

	/**
	 * 根据购物车id删除购物车方法
	 * 
	 * @param cart_id 购物车编号
	 * @return true 成功 false 失败
	 */
	public boolean delCartById(int cart_id) {
		// 定义sql语句
		String sql = "delete FROM `cart` WHERE (`cart_id`=?)";
		// 调用DBUtil方法
		boolean flag = exeUpdate(sql, cart_id);
		return flag;
	}

	/**
	 * 购物车分页+根据商品名称模糊查询
	 * 
	 * @param keywords 检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<Cart> getCartByPage(String keywords, int pageNo, int pageSize) {
		// 声明sql语句
		// String sql = "select * from `cart` where p_id like ? ";
		String sql = "SELECT cart.cart_sn,cart.c_id,cart.cart_id,cart.cart_num,cart.c_total,cart.p_id,cart.p_name,product.p_photo from cart inner join product on cart.p_id = product.p_id  where cart.p_name like ?";
		// 调用DBUtil方法
		PageData<Cart> pd = exeQueryByPage(sql, Cart.class, pageNo, pageSize, "%" + keywords + "%");

		return pd;
	}

	/**
	 * 根据商品名称检索购物车列表
	 * 
	 * @param keywords 商品名称关键字
	 * @return list集合
	 */
	public List<Cart> getCartByName(String keywords) {
		// 声明sql语句
		String sql = "SElECT * FROM cart WHERE p_name like ? ";
		List<Cart> list = exeQuery(sql, Cart.class, "%" + keywords + "%");

		return list;
	}

	/**
	 * 根据用户id和商品id查询购物车订单
	 * 
	 * @param c_id
	 * @param p_id
	 * @return
	 */
	public Cart getCartByCidAndPid(String c_id, String p_id) {
		// 声明sql语句
		String sql = "SElECT * FROM cart WHERE c_id=? and p_id=?";
		List<Cart> list = exeQuery(sql, Cart.class, c_id, p_id);
		if (list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * 根据购物车流水号修改购物车商品数量和金额
	 * @param cart
	 * @return
	 */
	public boolean updCart(Cart cart) {
		// 定义sql语句
		String sql = "UPDATE `cart` SET `cart_num`=`cart_num`+?, `c_total`=`c_total`+? WHERE (`cart_sn`=?)";
		// 调用DBUtil方法
		boolean flag = exeUpdate(sql,cart.getCart_num(),cart.getC_total(),cart.getCart_sn());
		return flag;
	}
}
