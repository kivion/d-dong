package com.dmall.entity;

public class MenueReview {
	private int mr_sn;
	private String c_id;
	private String m_id;
	private String mr_time;
	private String mr_content;
	
	public MenueReview() {
	}

	public MenueReview(int mr_sn, String c_id, String m_id, String mr_time, String mr_content) {
		super();
		this.mr_sn = mr_sn;
		this.c_id = c_id;
		this.m_id = m_id;
		this.mr_time = mr_time;
		this.mr_content = mr_content;
	}

	public int getMr_sn() {
		return mr_sn;
	}

	public void setMr_sn(int mr_sn) {
		this.mr_sn = mr_sn;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getM_id() {
		return m_id;
	}

	public void setM_id(String m_id) {
		this.m_id = m_id;
	}

	public String getMr_time() {
		return mr_time;
	}

	public void setMr_time(String mr_time) {
		this.mr_time = mr_time;
	}

	public String getMr_content() {
		return mr_content;
	}

	public void setMr_content(String mr_content) {
		this.mr_content = mr_content;
	}

	@Override
	public String toString() {
		return "MenueReview [mr_sn=" + mr_sn + ", c_id=" + c_id + ", m_id=" + m_id + ", mr_time=" + mr_time
				+ ", mr_content=" + mr_content + "]";
	}
	
}
