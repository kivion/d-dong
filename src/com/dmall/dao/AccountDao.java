package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Account;
import com.dmall.util.DBUtil;

public class AccountDao extends DBUtil{
	
	/**
	 *	根据用户名及密码检索，验证用户登录
	 * @param username
	 * @param password
	 * @return
	 */
	public Account getAcount(String account,String password) {
		String sql = "select * from account where account=? and password=?";
		List<Account> list = exeQuery(sql, Account.class, account,password);
		if(list.size()!=0) {
			return list.get(0);
		}else {
			return null;
		}
	}
}
