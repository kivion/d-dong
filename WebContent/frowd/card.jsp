<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>银行卡信息</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="./css/font.css">
        <link rel="stylesheet" href="./css/xadmin.css">
        <script src="./lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="./js/xadmin.js"></script>
 
    </head>
    <body>
        <div class="x-nav">
          <button  class="layui-btn" lay-filter="add" lay-submit="">
            增加
            </button>
         </div>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                              <thead>
                                <tr>
                                  <th>持卡人</th>
                                  <th>银行卡号</th>
                                  <th>开户城市</th>
								  <th>开户行</th>
                                
                              </thead>
                              <tbody>
                                <tr>
                                  <td>张三</td>
                                  <td>6212261401212184569</td>
                                  <td>厦门</td>
                                  <td>厦门支行</td>
                                </tr>
                              </tbody>
							
                            </table>
                        </div>
                    </div>
				
                </div>
				
            </div>
        </div> 
    </body>
</html>
