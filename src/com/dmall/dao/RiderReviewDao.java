package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Customer;
import com.dmall.entity.Rider;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

public class RiderReviewDao extends DBUtil {
	/**
	 * 分页+模糊查询
	 * 
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<Rider> getRider(String keyword, int pageNo, int pageSize) {

		String sql = "select * from `rider` where r_name like ? or r_nickname like ?";
		PageData pd = exeQueryByPage(sql, Rider.class, pageNo, pageSize, "%" + keyword + "%", "%" + keyword + "%");

		return pd;
	}

	/**
	 * 添加骑手
	 * 
	 * @param Rider
	 * @return Rider 对象
	 */
	public boolean addRider(Rider rider) {
		String sql = "INSERT INTO rider (r_id, r_nickname, r_name, r_sex, r_age, r_healthy, r_address, r_idcard, r_tell, r_state, r_carnum, g_id, r_order, r_totalorders, r_salary, r_entry, r_leave) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		boolean flag = exeUpdate(sql, rider.getR_id(), rider.getR_nickname(), rider.getR_name(), rider.getR_sex(),
				rider.getR_age(), rider.getR_healthy(), rider.getR_address(), rider.getR_idcard(), rider.getR_tell(),
				rider.getR_state(), rider.getR_carnum(), rider.getG_id(), rider.getR_order(), rider.getR_totalorders(),
				rider.getR_salary(), rider.getR_entry(), rider.getR_leave());
		return flag;
	}

	/**
	 * 按骑手id删除骑手
	 * 
	 * @param r_id
	 * @return flag true操作成功，false操作失败
	 */
	public boolean delRiderById(String r_id) {
		String sql = "delete FROM rider WHERE r_id=?";
		boolean flag = exeUpdate(sql, r_id);
		return flag;
	}

	/**
	 * 按骑手id修改骑手信息
	 * 
	 * @param Rider
	 * @return flag true操作成功，false操作失败
	 */
	public boolean updRiderById(Rider rider) {
		String sql = "UPDATE rider SET r_name=?, r_nickname=?, r_age=?,r_sex= ?,r_address=?,r_tell=?,r_state=?,r_carnum=?,r_order=?,r_totalorders=?,r_salary=? WHERE r_id=?";
		System.out.println(rider);
		boolean flag = exeUpdate(sql, rider.getR_name(), rider.getR_nickname(), rider.getR_age(), rider.getR_sex(),
				rider.getR_address(), rider.getR_tell(), rider.getR_state(), rider.getR_carnum(), rider.getR_order(),
				rider.getR_totalorders(), rider.getR_salary());
		return flag;
	}

	/**
	 * 查询所有信息 
	 * 
	 * @param Rider
	 * @return flag true操作成功，false操作失败
	 */

	public List<Rider> getAll() {
		String sql = "select * from rider";
		List<Rider> list = exeQuery(sql, Rider.class, null);
		for (int i = 0; i < list.size(); i++) {
			Rider rider = list.get(i);

		}
		return list;
	}
	
	/**
	 * 通过身份证和电话号码登录
	 * @param useremail 用户邮箱
	 * @param userpwd 密码
	 * @return 用户对象
	 */
	public Rider getriderlogin(String riderid,String ridertell) {
		String sql="SELECT * FROM `rider` where r_idcard=? and r_tell= ? ";
		List<Rider> list=DBUtil.exeQuery(sql, Rider.class, riderid,ridertell);
		if(list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
}
