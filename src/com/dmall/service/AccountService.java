package com.dmall.service;

import java.util.List;

import com.dmall.dao.AccountDao;
import com.dmall.entity.Account;

public class AccountService {
	private AccountDao ad = new AccountDao();
	/**
	 *	根据用户名及密码检索，验证用户登录
	 * @param username
	 * @param password
	 * @return
	 */
	public Account getAcount(String account,String password) {
		if((account!=null && !"".equals(account)) && (password!=null && !"".equals(password)) ) {
			return ad.getAcount(account, password);
		}else {
			return null;
		}
	}
}
