package com.dmall.test;

import org.junit.Test;

import com.dmall.dao.OrderDao;
import com.dmall.entity.Order;
import com.dmall.pagedata.PageData;

public class TestOrder {
	OrderDao od = new OrderDao();
	@Test
	public void testorder() {
		String keywords = "o000000000000002";
		String begindate = "2020-02-02";
		String enddate = "2021-01-10";
		int pageNo = 1;
		int pageSize = 5;
		PageData<Order> pd =  od.getOrderByPage(keywords,begindate,enddate, pageNo, pageSize);
		pd.getList().forEach(System.out::println);
	}
}
