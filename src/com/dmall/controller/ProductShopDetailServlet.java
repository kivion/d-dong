package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dmall.entity.Product;
import com.dmall.pagedata.PageData;
import com.dmall.service.ProductService;
import com.dmall.util.AjaxResponse;
import com.google.gson.Gson;

/**
 * 商品具体页面-控制层
 * 
 * @author ccp 蔡陈鹏
 *
 */
@WebServlet("/productshopdetail.do")
public class ProductShopDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ProductService ps = new ProductService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProductShopDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if ("querybyptfid".equals(op)) {
			doQueryByPtfId(request, response);
		} else {
			doQueryById(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	/**
	 * 商品列表->传id给商品详情
	 * 
	 * @author ccp
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doQueryById(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String p_id = "";
		if (request.getParameter("p_id") != null) {
			p_id = request.getParameter("p_id");
		}
		System.out.println("doQueryById" + p_id);
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();

//		// keywords传递的查询的关键字
		Product product = ps.getProductByProductId(p_id);

		// 单个对象直接转
//		AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", pd.getTotalCount(), pd.getList());
		Gson gson = new Gson();
		String str = gson.toJson(product);
		
		System.out.println("str: " + str);

		out.print(str);
		out.close();
	}

	protected void doQueryByPtfId(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String ptf_id = "";
		if (request.getParameter("ptf_id") != null) {
			ptf_id = request.getParameter("ptf_id");
		}
		System.out.println("doQueryById" + ptf_id);
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();

//				// keywords传递的查询的关键字
		List<Product> product = ps.getProductByShopListPart(ptf_id);

		// 单个对象直接转
//				AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", pd.getTotalCount(), pd.getList());
		Gson gson = new Gson();
		String str = gson.toJson(product);

		System.out.println("str: " + str);

		out.print(str);
		out.close();
	}
}
