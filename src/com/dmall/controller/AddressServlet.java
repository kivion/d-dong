package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dmall.entity.Address;
import com.dmall.pagedata.PageData;
import com.dmall.service.AddressService;
import com.dmall.util.AjaxResponse;
import com.dmall.util.UploadResponse;
import com.google.gson.Gson;

/**
 * Servlet implementation class AjaxAddressServlet
 */
@WebServlet("/ajaxaddress.do")
public class AddressServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//调用Service
	private AddressService as = new AddressService(); 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddressServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("application/json;charset=utf-8");

		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if ("add".equals(op)) {
			// 增加操作
			doAdd(request, response);
		} else {
			// 查询
			doQuery(request, response);
		}
		
	}
	
	
	protected void doAdd(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// MIME,返回类型就是json => application/json
		response.setContentType("application/json;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String city = "";
		System.out.println(request.getParameter("city") == null);
		if (request.getParameter("city") != null) {
			city = request.getParameter("city");
		}
		System.out.println("addchn: " + city);
		
		String addcontent = "";
		if (request.getParameter("addcontent") != null) {
			addcontent = request.getParameter("addcontent");
		}
		System.out.println("addcontent: " + addcontent);
		//拼接地址
		String a_name=city+addcontent;
		
		String addname = "";
		if (request.getParameter("addname") != null) {
			addname = request.getParameter("addname");
		}
		
		String addtell = "";
		if (request.getParameter("addtell") != null) {
			addtell = request.getParameter("addtell");
		}
		
		Address address =new Address("a00012", addname, addtell, a_name);
		
		System.out.println("address: " + address);

		boolean flag = as.addAddress(address);

		UploadResponse ur = new UploadResponse();
		ur.setCode(0);
		if (flag) {
			ur.setMsg("操作成功");
		} else {
			ur.setMsg("操作失败");
		}
		Gson gson = new Gson();
		String str = gson.toJson(ur);

		System.out.println("str: " + str);

		out.print(str);

		out.close();

	}

	/**
	 * 查询操作
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String keywords = "";
		if (request.getParameter("keywords") != null) {
			keywords = request.getParameter("keywords");
		}
		
		int pageNo = 1;
		int pageSize = 10;
		if (request.getParameter("page") != null) {
			pageNo = Integer.parseInt(request.getParameter("page"));
		}

		if (request.getParameter("limit") != null) {
			pageSize = Integer.parseInt(request.getParameter("limit"));
		}
		
		
		System.out.println("keywords :" + keywords);

		// keywords就是传递的查询的关键字
		
		PageData<Address> pd = as.getAddressByPage(keywords, pageNo, pageSize);
		
		// pd.getTotalCount(), 总记录数
		
		//pd.getList()  页面的数据
		AjaxResponse ar = new AjaxResponse(0, "success", pd.getTotalCount(), pd.getList());

		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);
		
		out.print(str);
		out.close();
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
