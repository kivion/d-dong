package com.dmall.entity;

public class Rider {
	private String r_id;
	private String r_nickname;
	private String r_name;
	private String r_photo;
	private String r_sex;
	private int r_age;
	private int r_healthy;
	private String r_address;
	private String r_idcard;
	private String r_tell;
	private int r_state;
	private String r_carnum;
	private String g_id;
	private int r_order;
	private int r_totalorders;
	private double r_salary;
	private String r_entry;
	private String r_leave;
	private int r_sn;
	public Rider(String r_id, String r_nickname, String r_name, String r_photo, String r_sex, int r_age, int r_healthy,
			String r_address, String r_idcard, String r_tell, int r_state, String r_carnum, String g_id, int r_order,
			int r_totalorders, double r_salary, String r_entry, String r_leave, int r_sn) {
		super();
		this.r_id = r_id;
		this.r_nickname = r_nickname;
		this.r_name = r_name;
		this.r_photo = r_photo;
		this.r_sex = r_sex;
		this.r_age = r_age;
		this.r_healthy = r_healthy;
		this.r_address = r_address;
		this.r_idcard = r_idcard;
		this.r_tell = r_tell;
		this.r_state = r_state;
		this.r_carnum = r_carnum;
		this.g_id = g_id;
		this.r_order = r_order;
		this.r_totalorders = r_totalorders;
		this.r_salary = r_salary;
		this.r_entry = r_entry;
		this.r_leave = r_leave;
		this.r_sn = r_sn;
	}
	
	public Rider() {
		// TODO Auto-generated constructor stub
	}

	public String getR_id() {
		return r_id;
	}

	public void setR_id(String r_id) {
		this.r_id = r_id;
	}

	public String getR_nickname() {
		return r_nickname;
	}

	public void setR_nickname(String r_nickname) {
		this.r_nickname = r_nickname;
	}

	public String getR_name() {
		return r_name;
	}

	public void setR_name(String r_name) {
		this.r_name = r_name;
	}

	public String getR_photo() {
		return r_photo;
	}

	public void setR_photo(String r_photo) {
		this.r_photo = r_photo;
	}

	public String getR_sex() {
		return r_sex;
	}

	public void setR_sex(String r_sex) {
		this.r_sex = r_sex;
	}

	public int getR_age() {
		return r_age;
	}

	public void setR_age(int r_age) {
		this.r_age = r_age;
	}

	public int getR_healthy() {
		return r_healthy;
	}

	public void setR_healthy(int r_healthy) {
		this.r_healthy = r_healthy;
	}

	public String getR_address() {
		return r_address;
	}

	public void setR_address(String r_address) {
		this.r_address = r_address;
	}

	public String getR_idcard() {
		return r_idcard;
	}

	public void setR_idcard(String r_idcard) {
		this.r_idcard = r_idcard;
	}

	public String getR_tell() {
		return r_tell;
	}

	public void setR_tell(String r_tell) {
		this.r_tell = r_tell;
	}

	public int getR_state() {
		return r_state;
	}

	public void setR_state(int r_state) {
		this.r_state = r_state;
	}

	public String getR_carnum() {
		return r_carnum;
	}

	public void setR_carnum(String r_carnum) {
		this.r_carnum = r_carnum;
	}

	public String getG_id() {
		return g_id;
	}

	public void setG_id(String g_id) {
		this.g_id = g_id;
	}

	public int getR_order() {
		return r_order;
	}

	public void setR_order(int r_order) {
		this.r_order = r_order;
	}

	public int getR_totalorders() {
		return r_totalorders;
	}

	public void setR_totalorders(int r_totalorders) {
		this.r_totalorders = r_totalorders;
	}

	public double getR_salary() {
		return r_salary;
	}

	public void setR_salary(double r_salary) {
		this.r_salary = r_salary;
	}

	public String getR_entry() {
		return r_entry;
	}

	public void setR_entry(String r_entry) {
		this.r_entry = r_entry;
	}

	public String getR_leave() {
		return r_leave;
	}

	public void setR_leave(String r_leave) {
		this.r_leave = r_leave;
	}

	public int getR_sn() {
		return r_sn;
	}

	public void setR_sn(int r_sn) {
		this.r_sn = r_sn;
	}

	@Override
	public String toString() {
		return "Rider [r_id=" + r_id + ", r_nickname=" + r_nickname + ", r_name=" + r_name + ", r_photo=" + r_photo
				+ ", r_sex=" + r_sex + ", r_age=" + r_age + ", r_healthy=" + r_healthy + ", r_address=" + r_address
				+ ", r_idcard=" + r_idcard + ", r_tell=" + r_tell + ", r_state=" + r_state + ", r_carnum=" + r_carnum
				+ ", g_id=" + g_id + ", r_order=" + r_order + ", r_totalorders=" + r_totalorders + ", r_salary="
				+ r_salary + ", r_entry=" + r_entry + ", r_leave=" + r_leave + ", r_sn=" + r_sn + "]";
	}
	
}
