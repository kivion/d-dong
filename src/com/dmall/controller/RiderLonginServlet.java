package com.dmall.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dmall.entity.Customer;
import com.dmall.entity.Menue;
import com.dmall.entity.MenueCollection;
import com.dmall.entity.Rider;
import com.dmall.service.CustomerService;
import com.dmall.service.MenuCollectionService;
import com.dmall.service.RiderService;

/**
 * Servlet implementation class SerConBlog
 */
@WebServlet("/riderlogin.do")
public class RiderLonginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	RiderService rs = new RiderService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RiderLonginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		
		if("login".equals(op)) {
			doLogin(request,response);
		}else {
			//doCusForget(request,response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

/**
 * 登入
 * @param request
 * @param response
 * @throws ServletException
 * @throws IOException
 */
	protected void doLogin(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String riderid=request.getParameter("riderid");
		String ridertell=request.getParameter("ridertell");
		String rem=request.getParameter("rem");
		Rider rider=rs.login(riderid, ridertell);
		if(rider==null) {
			response.sendRedirect("login.jsp");
		}else {
			HttpSession session=request.getSession();
			session.setAttribute("rider", rider);
			if(rem!=null) {
				Cookie cookie1=new Cookie("cusname",rider.getR_idcard());
			    Cookie cookie2=new Cookie("cuspwd",rider.getR_tell());
			cookie1.setMaxAge(60*60*24*7);
			cookie2.setMaxAge(60*60*24*7);
			response.addCookie(cookie1);
			response.addCookie(cookie2);
			}
			request.getRequestDispatcher("./frowd/index.jsp").forward(request, response);
		}
	}
	
}
