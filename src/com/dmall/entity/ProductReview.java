package com.dmall.entity;

public class ProductReview {
	private int pr_sn;
	private String p_id;
	private String pr_id;
	private String pr_conten;
	private String pr_time;

	/**
	 * 联表新增
	 */
	private String c_id;

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	private Customer customer = new Customer();

	public Customer getCustomer() {
		return customer;
	}

	private Product product = new Product();

	public Product getProduct() {
		return product;
	}

	/**
	 * 联表结束
	 */

	public ProductReview() {
		// TODO Auto-generated constructor stub
	}

	public ProductReview(int pr_sn, String p_id, String pr_id, String pr_conten, String pr_time) {
		super();
		this.pr_sn = pr_sn;
		this.p_id = p_id;
		this.pr_id = pr_id;
		this.pr_conten = pr_conten;
		this.pr_time = pr_time;
	}

	public String getPr_id() {
		return pr_id;
	}

	public void setPr_id(String pr_id) {
		this.pr_id = pr_id;
	}

	public int getPr_sn() {
		return pr_sn;
	}

	public void setPr_sn(int pr_sn) {
		this.pr_sn = pr_sn;
	}

	public String getP_id() {
		return p_id;
	}

	public void setP_id(String p_id) {
		this.p_id = p_id;
	}

	public String getPr_conten() {
		return pr_conten;
	}

	public void setPr_conten(String pr_conten) {
		this.pr_conten = pr_conten;
	}

	public String getPr_time() {
		return pr_time;
	}

	public void setPr_time(String pr_time) {
		this.pr_time = pr_time;
	}

}
