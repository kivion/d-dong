package com.dmall.entity;

public class Product {
	private String p_id;
	private String p_name;
	private int p_weight;
	private String p_stock;
	private String p_origin;
	private String p_save;
	private String p_detaill;
	private String p_pro;
	private String p_launch;
	private String p_guarantee;
	private String p_appraise;
	private String p_photo;
	private String p_bigphoto;
	private String ptn_id;
	private double p_price;
	private int p_sn;
	private int p_status;

	public Product() {
		// TODO Auto-generated constructor stub
	}

	public Product(String p_id, String p_name, int p_weight, String p_stock, String p_origin, String p_save,
			String p_detaill, String p_pro, String p_launch, String p_guarantee, String p_appraise, String p_photo,
			String ptn_id, double p_price, int p_sn, int p_status) {
		super();
		this.p_id = p_id;
		this.p_name = p_name;
		this.p_weight = p_weight;
		this.p_stock = p_stock;
		this.p_origin = p_origin;
		this.p_save = p_save;
		this.p_detaill = p_detaill;
		this.p_pro = p_pro;
		this.p_launch = p_launch;
		this.p_guarantee = p_guarantee;
		this.p_appraise = p_appraise;
		this.p_photo = p_photo;
		this.ptn_id = ptn_id;
		this.p_price = p_price;
		this.p_sn = p_sn;
		this.p_status = p_status;
	}

	/**
	 * 修改用的有参构造，暂用
	 * 
	 * @param p_name
	 * @param p_weight
	 * @param p_stock
	 * @param p_origin
	 * @param p_save
	 * @param p_detaill
	 * @param p_pro
	 * @param p_launch
	 * @param p_guarantee
	 * @param p_photo
	 * @param ptn_id
	 * @param p_price
	 * @param p_status
	 */
	public Product(String p_id, String p_name, int p_weight, String p_stock, String p_origin, String p_save,
			String p_detaill, String p_pro, String p_guarantee, String p_photo, String ptn_id,
			double p_price) {
		super();
		this.p_id = p_id;
		this.p_name = p_name;
		this.p_weight = p_weight;
		this.p_stock = p_stock;
		this.p_origin = p_origin;
		this.p_save = p_save;
		this.p_detaill = p_detaill;
		this.p_pro = p_pro;
		this.p_guarantee = p_guarantee;
		this.p_photo = p_photo;
		this.ptn_id = ptn_id;
		this.p_price = p_price;
	}

	/**
	 * 联表新增
	 */
	private ProductTypeFirst producttypefirst = new ProductTypeFirst();

	public ProductTypeFirst getProductTypeFirst() {
		return producttypefirst;
	}

	/**
	 * 联表结束
	 */

	public int getP_status() {
		return p_status;
	}

	public void setP_status(int p_status) {
		this.p_status = p_status;
	}

	public String getP_id() {
		return p_id;
	}

	public void setP_id(String p_id) {
		this.p_id = p_id;
	}

	public String getP_name() {
		return p_name;
	}

	public void setP_name(String p_aname) {
		this.p_name = p_aname;
	}

	public int getP_weight() {
		return p_weight;
	}

	public void setP_weight(int p_weight) {
		this.p_weight = p_weight;
	}

	public String getP_stock() {
		return p_stock;
	}

	public void setP_stock(String p_stock) {
		this.p_stock = p_stock;
	}

	public String getP_origin() {
		return p_origin;
	}

	public void setP_origin(String p_origin) {
		this.p_origin = p_origin;
	}

	public String getP_save() {
		return p_save;
	}

	public void setP_save(String p_save) {
		this.p_save = p_save;
	}

	public String getP_detaill() {
		return p_detaill;
	}

	public void setP_detaill(String p_detaill) {
		this.p_detaill = p_detaill;
	}

	public String getP_pro() {
		return p_pro;
	}

	public void setP_pro(String p_pro) {
		this.p_pro = p_pro;
	}

	public String getP_launch() {
		return p_launch;
	}

	public void setP_launch(String p_launch) {
		this.p_launch = p_launch;
	}

	public String getP_guarantee() {
		return p_guarantee;
	}

	public void setP_guarantee(String p_guarantee) {
		this.p_guarantee = p_guarantee;
	}

	public String getP_appraise() {
		return p_appraise;
	}

	public void setP_appraise(String p_appraise) {
		this.p_appraise = p_appraise;
	}

	public String getP_photo() {
		return p_photo;
	}

	public void setP_photo(String p_photo) {
		this.p_photo = p_photo;
	}

	public String getPtn_id() {
		return ptn_id;
	}

	public void setPtn_id(String ptn_id) {
		this.ptn_id = ptn_id;
	}

	public double getP_price() {
		return p_price;
	}

	public void setP_price(double p_price) {
		this.p_price = p_price;
	}

	public int getP_sn() {
		return p_sn;
	}

	public void setP_sn(int p_sn) {
		this.p_sn = p_sn;
	}

	public String getP_bigphoto() {
		return p_bigphoto;
	}

	public void setP_bigphoto(String p_bigphoto) {
		this.p_bigphoto = p_bigphoto;
	}

	@Override
	public String toString() {
		return "Product [p_id=" + p_id + ", p_name=" + p_name + ", p_weight=" + p_weight + ", p_stock=" + p_stock
				+ ", p_origin=" + p_origin + ", p_save=" + p_save + ", p_detaill=" + p_detaill + ", p_pro=" + p_pro
				+ ", p_launch=" + p_launch + ", p_guarantee=" + p_guarantee + ", p_appraise=" + p_appraise
				+ ", p_photo=" + p_photo + ", p_bigphoto=" + p_bigphoto + ", ptn_id=" + ptn_id + ", p_price=" + p_price
				+ ", p_sn=" + p_sn + ", p_status=" + p_status + ", producttypefirst=" + producttypefirst + "]";
	}

}
