package com.dmall.entity;
/**
 * 添加了2 个字段（收货人姓名和联系电话）
 * @author yong
 *
 */
public class Address {
	private int a_sn;
	private String a_id;
	private String c_name;//收货人姓名
	private String c_tell;//联系电话
	private String a_name;//收货地址
	
	public Address() {
		// TODO Auto-generated constructor stub
	}


	public Address(int a_sn, String a_id, String c_name, String c_tell, String a_name) {
		super();
		this.a_sn = a_sn;
		this.a_id = a_id;
		this.c_name = c_name;
		this.c_tell = c_tell;
		this.a_name = a_name;
	}



	public int getA_sn() {
		return a_sn;
	}

	public void setA_sn(int a_sn) {
		this.a_sn = a_sn;
	}

	public String getA_id() {
		return a_id;
	}

	public void setA_id(String a_id) {
		this.a_id = a_id;
	}

	public String getC_name() {
		return c_name;
	}

	public void setC_name(String c_name) {
		this.c_name = c_name;
	}

	public String getC_tell() {
		return c_tell;
	}

	public void setC_tell(String c_tell) {
		this.c_tell = c_tell;
	}
	
	public String getA_name() {
		return a_name;
	}

	public void setA_name(String a_name) {
		this.a_name = a_name;
	}

	
	
	public Address(String a_id, String c_name, String c_tell, String a_name) {
		super();
		this.a_id = a_id;
		this.c_name = c_name;
		this.c_tell = c_tell;
		this.a_name = a_name;
	}


	@Override
	public String toString() {
		return "Address [a_sn=" + a_sn + ", a_id=" + a_id + ", c_name=" + c_name + ", c_tell=" + c_tell + ", a_name="
				+ a_name + "]";
	}

	
}
