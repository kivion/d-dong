package com.dmall.dao;

import java.util.List;

import com.dmall.entity.Menue;
import com.dmall.entity.MenueCollection;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

public class MenueCollectionDao extends DBUtil{
	/**
	 *	分页+模糊查询菜谱收藏
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<MenueCollection> getMenuCollection(String keyword,int pageNo,int pageSize){
		
		String sql = "select `c_photo`,`c_name` `m_name`,`m_used`,`m_content`,`m_file`,`m_difficult`,`m_time`,`m_creat` from `menu_collection` where m_name like ? or m_used like ? or m_content like ? join mene_collection on menu_collection.m_id=menu.m_id join customer on menu_collection.c_id=customer.c_id";
		PageData pd = exeQueryByPage(sql, Menue.class, pageNo, pageSize,"%"+keyword+"%","%"+keyword+"%","%"+keyword+"%");

		return pd;
	}
	/**
	 * 通过菜谱关键字和登入的用户的编号模糊查询
	 * @param keywords
	 * @return
	 */
	public List<Menue> getMenuByKeywordscid( String cid,String keywords) {
		String sql = "SELECT menus.m_name,menus.m_file,menus.c_name,menus.m_time,menus.m_difficult " + 
				"FROM menu_collection INNER JOIN " + 
				"(SELECT menu.m_id, menu.m_name, menu.m_used, menu.m_content, menu.m_file, menu.m_difficult, menu.m_time, menu.m_creat, customer.c_name   " + 
				"FROM menu INNER JOIN customer ON menu.c_id = customer.c_id)as menus   " + 
				"on menu_collection.m_id=menus.m_id " + 
				"WHERE menu_collection.c_id=? AND menus.m_name like ? ";
		List<Menue> list = DBUtil.exeQuery(sql, Menue.class,cid ,"%"+keywords+"%");
		return list;
	}
	/**
	 *	添加菜谱收藏
	 * @param Menue
	 * @return Menu 对象
	 */
	public boolean addMenueCollection(MenueCollection menucollection) {
		String sql = "INSERT INTO `menu_collection` (`m_id`, `c_id`) VALUES (?,?)";
		boolean flag = exeUpdate(sql, menucollection.getM_id(),menucollection.getC_id());
		return flag;
	}
	/**
	 *	按菜谱id删除菜谱收藏
	 * @param m_id
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean delMenuCollectionById(int m_id) {
		String sql = "delete FROM `menue` WHERE (`m_id`=?)";
		boolean flag = exeUpdate(sql, m_id);
		return flag;
	}
}
