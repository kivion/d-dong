<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>欢迎页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<script src="./lib/layui/layui.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>

</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="">首页</a> <a href="">演示</a>
			<a> <cite>导航元素</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
						<form class="layui-form layui-col-space5">
							<div class="layui-inline layui-show-xs-block">
								<input class="layui-input" autocomplete="off" placeholder="开始日"
									name="start" id="start">
							</div>
							<div class="layui-inline layui-show-xs-block">
								<input class="layui-input" autocomplete="off" placeholder="截止日"
									name="end" id="end">
							</div>
							<div class="layui-inline layui-show-xs-block">
								<input type="text" name="keywords" placeholder="请输入搜索关键字"
									autocomplete="off" class="layui-input">
							</div>
							<div class="layui-inline layui-show-xs-block">
								<button class="layui-btn" lay-submit="" lay-filter="search">
									<i class="layui-icon">&#xe615;</i>
								</button>
							</div>
						</form>
					</div>
					<div class="layui-card-body ">
						<table class="layui-table"
							lay-data="{url:'../ajaxmenu.do',page:true,toolbar: '#toolbarDemo',id:'test'}"
							lay-filter="test">
							<thead>
								<tr>
									<th lay-data="{type:'checkbox'}">ID</th>
									<th lay-data="{field:'m_id', width:80, sort: true}">ID</th>
									<th
										lay-data="{field:'m_name', width:120, sort: true, edit: 'text'}">菜谱名</th>
									<th lay-data="{field:'m_used', edit: 'text', minWidth: 150}">用料</th>
									<th lay-data="{field:'m_content', width:180}">步骤</th>
									<th lay-data="{field:'m_file', edit: 'text', minWidth: 60}">文件</th>
									<th lay-data="{field:'m_difficult', sort: true, edit: 'text'}">难度</th>
									<th
										lay-data="{field:'m_time', width:100, sort: true, edit: 'text'}">耗时</th>
									<th
										lay-data="{field:'m_creat', width:100, sort: true, edit: 'text'}">发布时间</th>
									<th lay-data="{field:'', width:150,templet: '#opTpl'}">操作</th>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" id="opTpl">
	<a onclick="member_stop(this,'10001')" href="javascript:;"
		title="启用"> <i class="layui-icon"> &#xe601;</i>
	</a> 
	<a title="编辑" onclick="xadmin.open('编辑','member-edit.html',600,400)"
		href="javascript:;"> <i class="layui-icon">&#xe642;</i>
	</a> 
	<a onclick="xadmin.open('修改密码','member-password.html',600,400)"
	title="修改密码" href="javascript:;"> <i class="layui-icon">&#xe631;</i>
	</a> 
	<a title="删除" onclick="member_del(this,'要删除的id')"
		href="javascript:;"> <i class="layui-icon">&#xe640;</i>
	</a>
	</script>
<script type="text/html" id="toolbarDemo">
        <div class = "layui-btn-container" > 
            <button class = "layui-btn layui-btn-sm" lay-event = "getCheckData" > 获取选中行数据 </button>
            <button class="layui-btn layui-btn-sm" lay-event="getCheckLength">获取选中数目</button > 
            <button class = "layui-btn layui-btn-sm" lay-event = "isAll" > 验证是否全选</button>
        </div > 
    </script>
<script type="text/html" id="switchTpl">
        <!-- 这里的checked的状态只是演示 -->
        <input type = "checkbox" name = "sex" value = "{{d.id}}" lay-skin = "switch"lay-text = "女|男" lay-filter = "sexDemo" {{ d.id == 10003 ? 'checked': ''}} >
    </script>
<script>
    layui.use('laydate',function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });

        });
    </script>
<script>
    layui.use(['table','form'],function() {
            var table = layui.table;
			var form = layui.form;
			
			//菜谱搜索按钮
			form.on('submit(search)',function(data){
				console.log(data.field)
				table.reload('test', {
					url : '../ajaxmenu.do',
					where : data.field//设定异步数据接口的额外参数
				});
				return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可
			});
			
			
            //监听单元格编辑
            table.on('edit(test)',
            function(obj) {
                var value = obj.value //得到修改后的值
                ,
                data = obj.data //得到所在行所有键值
                ,
                field = obj.field; //得到字段
                layer.msg('[ID: ' + data.id + '] ' + field + ' 字段更改为：' + value);
            });

            //头工具栏事件
            table.on('toolbar(test)',
            function(obj) {
                var checkStatus = table.checkStatus(obj.config.id);
                switch (obj.event) {
                case 'getCheckData':
                    var data = checkStatus.data;
                    layer.alert(JSON.stringify(data));
                    break;
                case 'getCheckLength':
                    var data = checkStatus.data;
                    layer.msg('选中了：' + data.length + ' 个');
                    break;
                case 'isAll':
                    layer.msg(checkStatus.isAll ? '全选': '未全选');
                    break;
                };
            });
        });</script>
<script>var _hmt = _hmt || []; (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();</script>

</html>