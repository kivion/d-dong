package com.dmall.entity;

public class Cart {
	private int cart_sn;
	private String cart_id;
	private String p_id;
	private int cart_num;
	//增加商品名称
	private String p_name;
	public String getP_name() {
		return p_name;
	}
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
	private String c_id;
	//增加购物车金额
	private double c_total;
	public double getC_total() {
		return c_total;
	}
	public void setC_total(double c_total) {
		this.c_total = c_total;
	}
	public Cart() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Cart(int cart_sn, String cart_id, String p_id, int cart_num, String p_name, String c_id, double c_total) {
		super();
		this.cart_sn = cart_sn;
		this.cart_id = cart_id;
		this.p_id = p_id;
		this.cart_num = cart_num;
		this.p_name = p_name;
		this.c_id = c_id;
		this.c_total = c_total;
	}
	public int getCart_sn() {
		return cart_sn;
	}
	public void setCart_sn(int cart_sn) {
		this.cart_sn = cart_sn;
	}
	public String getCart_id() {
		return cart_id;
	}
	public void setCart_id(String cart_id) {
		this.cart_id = cart_id;
	}
	public String getP_id() {
		return p_id;
	}
	public void setP_id(String p_id) {
		this.p_id = p_id;
	}
	public int getCart_num() {
		return cart_num;
	}
	public void setCart_num(int cart_num) {
		this.cart_num = cart_num;
	}
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}
	@Override
	public String toString() {
		return "Cart [cart_sn=" + cart_sn + ", cart_id=" + cart_id + ", p_id=" + p_id + ", cart_num=" + cart_num
				+ ", p_name=" + p_name + ", c_id=" + c_id + ", c_total=" + c_total + "]";
	}
	
	
	
}
