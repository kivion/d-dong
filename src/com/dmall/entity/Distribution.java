package com.dmall.entity;

public class Distribution {
	
	private int d_sn;
	private String o_id;;
	private String d_state;
	private String d_end;
	public Distribution(int d_sn, String o_id, String d_state, String d_end) {
		super();
		this.d_sn = d_sn;
		this.o_id = o_id;
		this.d_state = d_state;
		this.d_end = d_end;
	}
	public Distribution() {
		// TODO Auto-generated constructor stub
	}
	public int getD_sn() {
		return d_sn;
	}
	public void setD_sn(int d_sn) {
		this.d_sn = d_sn;
	}
	public String getO_id() {
		return o_id;
	}
	public void setO_id(String o_id) {
		this.o_id = o_id;
	}
	public String getD_state() {
		return d_state;
	}
	public void setD_state(String d_state) {
		this.d_state = d_state;
	}
	public String getD_end() {
		return d_end;
	}
	public void setD_end(String d_end) {
		this.d_end = d_end;
	}
	@Override
	public String toString() {
		return "Distribution [d_sn=" + d_sn + ", o_id=" + o_id + ", d_state=" + d_state + ", d_end=" + d_end + "]";
	}
	
	
}
