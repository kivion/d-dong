package com.dmall.service;

import java.util.List;

import com.dmall.dao.ProductDao;
import com.dmall.entity.Product;
import com.dmall.pagedata.PageData;

/**
 * 商品业务层-业务层 
 * 
 * @author 蔡陈鹏
 */
public class ProductService {
	ProductDao pd = new ProductDao();

	/**
	 * 模糊查询
	 * 
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData 分页对象
	 */
	public List<Product> getProduct(String keyword) {
		return pd.getProduct(keyword);
	}

	/**
	 * 分页+模糊查询
	 * 
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData 分页对象
	 */
	public PageData<Product> getProduct(String keyword, int pageNo, int pageSize) {

		if (pageNo < 1) {
			pageNo = 1;
		}
		return pd.getProductBypage(keyword, pageNo, pageSize);
	}

	/**
	 * 添加商品
	 * 
	 * @param product 商品对象
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean addProduct(Product product) {

		return pd.addProduct(product);
	}

	/**
	 * 按商品id删除商品内容
	 * 
	 * @param p_id
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean delProduct(String p_id) {
		return pd.delProductById(p_id);

	}

	/**
	 * 按商品id修改商品
	 * 
	 * @param Product 商品对象
	 * @return boolean 布尔类型，true操作成功，false操作失败
	 */
	public boolean updProduct(Product product) {
		return pd.updProductById(product);
	}
	/**
	 * 模糊查询-按商品名或者小类别名
	 * 
	 * @param keyword  检索关键字
	 * @param pageNo   页码
	 * @param pageSize 当前页的条数
	 * @return PageData 分页对象
	 */
	public List<Product> getProductShopListPage(String keywords) {
		return pd.getProductByShopListPage(keywords);
	}
	
	/**
	 * 查询商品（按商品编号）
	 * @param p_id 商品编号
	 * @return
	 */
	public Product getProductByProductId(String p_id) {
		return pd.getProductByProductId(p_id);
	}
	
	/**
	 * 查询商品（按商品大分类编号）-用于主页进入时自动加载到各部分
	 * @param ptf_id 商品大分类编号
	 * @return
	 */
	public List<Product> getProductByShopListPart(String ptf_id) {
		return pd.getProductByShopListPart(ptf_id);
	}
}