package com.dmall.test;

import java.util.List;

import org.junit.Test;


import com.dmall.dao.ProductCollectionDao;
import com.dmall.entity.Product;
import com.dmall.entity.ProductCollection;
import com.dmall.pagedata.PageData;


/**
 * 对 商品收藏的方法进行测试
 * @author 王佳慧
 *
 */
public class TestProColl {
/**
 * 测试：查询所有商品收藏（客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存）
 * 目前出现空指针异常，未改，不知道怎么改
 */
	@Test
	public void TestgetProColl() {
		ProductCollectionDao pcd=new ProductCollectionDao();
		List<Product> list=pcd.getProduct();
		list.forEach(System.out::println);
	}
	/**
	 * 对分页查询进行测试-查询所有商品收藏（客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存）
	 * 目前出现空指针异常，未改，不知道怎么改
	 */
	@Test
	public void TestBlogByPage() {
		ProductCollectionDao cd=new ProductCollectionDao();
		PageData<Product> pd=cd.getProByPage(2, 5);
		System.out.println("pageNo: "+pd.getPageNo());
		System.out.println("pagesize: "+pd.getPageSize());
		System.out.println("list: "+pd.getList());
		System.out.println("totalcount: "+pd.getTotalCount());
		System.out.println("totalpage: "+pd.getTotalPage());
	}
	
	/**
	 * 测试--模糊查询--通过商品名称查询客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存
	 */
	@Test
	public void  TestgetProCollByKeywords() {
		ProductCollectionDao bd=new ProductCollectionDao();
		List<Product> list=bd.getCustomeByKeywords("西");
		list.forEach(System.out::println);
	}
	
	/**
	 * 对分页查询进行测试--模糊查询--通过商品名称查询客户编号、客户姓名、收藏的商品名称、收藏的商品价格，库存
	 */
	@Test
	public void TestgetCusProCollKeyByPage(){
		ProductCollectionDao cd=new ProductCollectionDao();
		PageData<Product> pd=cd.getCusKeyByPage("西", 2,5);
		System.out.println("pageNo: "+pd.getPageNo());
		System.out.println("pagesize: "+pd.getPageSize());
		System.out.println("list: "+pd.getList());
		System.out.println("totalcount: "+pd.getTotalCount());
		System.out.println("totalpage: "+pd.getTotalPage());
	}
	
	/**
	 * 对添加商品收藏表进行测试
	 */
	@Test
	public void TestAddCustomer() {
		ProductCollectionDao bd=new ProductCollectionDao();
		ProductCollection pc = new ProductCollection("p00001","c0000000002");
		boolean flag = bd.AddCustomer(pc);
		System.out.println(flag);
	}
	
	/**
	 * 删除信息--->通过商品编号删除进行测试
	 */
	@Test
	public void TestDelCustomer() {
		ProductCollectionDao bd=new ProductCollectionDao();
		boolean flag = bd.DelCustomer("p00001");
		System.out.println(flag);
	}
	
}
