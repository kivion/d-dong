package com.dmall.test;

import java.util.List;

import org.junit.Test;

import com.dmall.dao.ProductDao;
import com.dmall.entity.Product;

/**
 * 商品Dao方法的测试
 * 
 * @author ccp
 *
 */
public class TestProduct {
	ProductDao pd = new ProductDao();

	@Test
	public void searchProducttest() {
		List<Product> list = pd.getProduct("");
		list.forEach(System.out::println);
	}

	@Test
	public void addProducttest() {
		Product product = new Product("1", "2", 100, "200", "xiamen", "常温保存", "1", "2021-01-04 16:04:01",
				"2021-01-04 16:04:01", "20", "pr1", "12", "3", 3, 11, 1);
		boolean flag = pd.addProduct(product);
		System.out.println(flag);
	}

	@Test
	public void updProducttest() {
		Product product = new Product("23333", "3", 300, "300", "3xiamen", "3常温保存", "1", "2021-01-04 16:04:01",
				"2021-01-04 16:04:01", "20", "pr1", "12", "3", 3, 11, 1);
		boolean flag = pd.updProductById(product);
		System.out.println(flag);
	}
	
//	@Test
//	public void updProducttest1() {
//		boolean flag = pd.updProductById("23333","23333");
//		System.out.println(flag);
//	}

//	@Test
//	public void delProducttest() {
//		boolean flag = pd.delProductById(1);
//		System.out.println(flag);
//	}
	@Test
	public void getProductByProductIdTest() {
		Product product = pd.getProductByProductId("p00036");
		System.out.println(product);
	}
}
