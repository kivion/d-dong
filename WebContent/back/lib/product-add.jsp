<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>欢迎页面-添加商品</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>
<script type="text/javascript" src="./js/jquery.mini.js"></script>
<script src="./lib/layui/layui.js" charset="utf-8"></script>
<script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">
				<div class="layui-form-item">
					<label for="productid" class="layui-form-label"> <span
						class="x-red">*</span>商品编号
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productid" name="productid" required=""
							placeholder="p000+no" lay-verify="required" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="productname" class="layui-form-label"> <span
						class="x-red">*</span>商品名称
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productname" name="productname" required=""
							lay-verify="required" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item" style="display: inline">
					<label for="productprice" class="layui-form-label"> <span
						class="x-red">*</span>商品价格
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productprice" name="productprice"
							required="" lay-verify="required" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="productcount" class="layui-form-label"> <span
						class="x-red">*</span>数量
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productcount" name="productcount"
							required="" lay-verify="required" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="weight" class="layui-form-label"> <span
						class="x-red">*</span>重量
					</label>
					<div class="layui-input-inline">
						<input type="text" id="weight" name="weight" required=""
							lay-verify="required" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="address" class="layui-form-label "> <span
						class="x-red">*</span>产地
					</label>
					<div class="layui-form-item x-city" style="margin-left:150!important" id="start">
						<div class="layui-input-inline">
							<select name="province" lay-filter="province">
								<option value="">请选择省</option>
							</select>
						</div>
						<div class="layui-input-inline">
							<select name="city" lay-filter="city">
								<option value="">请选择市</option>
							</select>
						</div>
						<div class="layui-input-inline">
							<select name="area" lay-filter="area">
								<option value="">请选择县/区</option>
							</select>
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<label for="type" class="layui-form-label"> <span
						class="x-red">*</span>类别
					</label>
					<div class="layui-input-inline">
						<select name="type" id="select_type">
							<option>--选择类别--</option>
						</select>
						<!-- <input type="text" id="type" name="type" required="" placeholder="ptn+no"
							lay-verify="required" autocomplete="off" class="layui-input"> -->
					</div>
				</div>
				<div class="layui-form-item">
					<label for="savemethod" class="layui-form-label"> <span
						class="x-red">*</span>存储方式
					</label>
					<div class="layui-input-inline">
						<input type="text" id="savemethod" name="savemethod" required=""
							lay-verify="required" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="createtime" class="layui-form-label"> <span
						class="x-red">*</span>生产日期
					</label>
					<div class="layui-input-inline">
						<input type="text" id="createtime" name="createtime"
							id="createtime" required="" lay-verify="required"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="quality" class="layui-form-label"> <span
						class="x-red">*</span>保质期
					</label>
					<div class="layui-input-inline">
						<input type="text" id="quality" name="quality" required=""
							lay-verify="required" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="L_email" class="layui-form-label"> <span
						class="x-red">*</span>商品图片
					</label>
					<div class="layui-input-inline">
						<button type="button" class="layui-btn" id="picture">
							<i class="layui-icon">&#xe67c;</i>上传图片
						</button>
						<!-- 隐藏域，存储是图片的名字 -->
						<input type="hidden" name="cover" id="cover">
						<div class="layui-form-mid layui-word-aux">
							<span class="x-red">*</span>
						</div>
					</div>

					<div class="layui-form-item layui-form-text">
						<label for="productinfo" class="layui-form-label">描述</label>
						<div class="layui-input-block">
							<textarea placeholder="请输入内容" id="productinfo" name="productinfo"
								class="layui-textarea"></textarea>
						</div>
					</div>
					<div class="layui-form-item">
						<label for="L_repass" class="layui-form-label"></label>
						<button class="layui-btn" lay-filter="add" lay-submit="">增加</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script>
		layui.use([ 'upload', 'layer', 'laydate', 'form' ], function() {
			var upload = layui.upload;
			var layer = layui.layer;
			var laydate = layui.laydate;
			$ = layui.jquery;
			var form = layui.form;
			//执行实例
			var uploadInst = upload.render({
				elem : '#picture' //绑定元素（图片上传 ）
				,
				url : '../upc.do' //上传接口,就是servlet处理文件上传请求的地址
				,
				done : function(res) {

					//给id=cover的隐藏域赋值
					$("#cover").val(res.data);
					if (res.code == 0) {
						layer.msg('上传成功');
					}
				},
				error : function() {
					//请求异常回调
				}
			});
			//商品类型数据请求
			$(function() {
				$.ajax({
					/* timeout:3000, */
					async : false,
					type : 'post',
					url : '../type.do',
					dataType : 'json',
					success : function(data) {

						//console.log(data);
						$.each(data.data, function(n, value) {
							$("#select_type").append(
									"<option>" + value.ptn_name + "</option>");
						});
						//下拉框ajax动态加载数据时，由于是懒加载，在加载完后必须加上form.render()
						form.render();
					}
				});
			});
		});
	</script>
	<script type="text/javascript" src="./js/xcity.js"></script>
	<script>
		layui.use(['form', 'code'], function() {
			form = layui.form;

			$('#start').xcity();

			});
	</script>
	<script>
		layui.use([ 'form', 'layer', 'laydate' ], function() {
			$ = layui.jquery;
			var form = layui.form, layer = layui.layer;
			var laydate = layui.laydate;
			//自定义验证规则
			form.verify({
				nikename : function(value) {
					if (value.length < 5) {
						return '昵称至少得5个字符啊';
					}
				},
				pass : [ /(.+){6,12}$/, '密码必须6到12位' ],
				repass : function(value) {
					if ($('#L_pass').val() != $('#L_repass').val()) {
						return '两次密码不一致';
					}
				}
			});
			//执行一个laydate实例
			laydate.render({
				elem : '#createtime' //指定元素(生产日期)
			});

			//监听提交（添加商品 ）
			form.on('submit(add)', function(data) {
				//console.log(data.field);

				//发异步，把数据提交给servlet,代码需要我们自己完成
				//jquery
				$.ajax({
					url : '../product.do?op=add',
					//data	规定要发送到服务器的数据，data.field指的是所添加商品的字段信息
					data : data.field,
					//type	规定请求的类型POST
					type : 'post',
					//success(res)当请求成功时运行的函数，res是在Servlet返回的数据
					success : function(res) {
						layer.msg(res.msg, {
							icon : 6
						});
					},
					error:function(res){
						layer.msg(res.msg, {
							icon:5
						});
					}
				});
				return false;
			});
			
		});
	</script>
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>	

</body>

</html>