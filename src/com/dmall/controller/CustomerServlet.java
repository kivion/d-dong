package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dmall.entity.Customer;
import com.dmall.pagedata.PageData;
import com.dmall.service.CustomerService;
import com.dmall.util.AjaxResponse;
import com.google.gson.Gson;

/**
 * Servlet implementation class CustomerServlet
 */
@WebServlet("/customer.do")
public class CustomerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private CustomerService cs = new CustomerService();

	public CustomerServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if ("add".equals(op)) {
			doAdd(request, response);
		} else if ("login".equals(op)) {
			doLogin(request, response);
		} else if("del".equals(op)){
			doDel(request,response);
		}else{
			doQuery(request, response);
		}
	}

	protected void doQuery(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String keywords = "";
		if (request.getParameter("keywords") != null) {
			keywords = request.getParameter("keywords");
		}
		int pageNo = 1;
		int pageSize = 10;
		if (request.getParameter("page") != null) {
			pageNo = Integer.parseInt(request.getParameter("page"));
		}

		if (request.getParameter("limit") != null) {
			pageSize = Integer.parseInt(request.getParameter("limit"));
		}

		System.out.println("keywords:" + keywords);

		PageData<Customer> pd = cs.getCusKeyByPage(keywords, pageNo, pageSize);
		AjaxResponse<Customer> ar = new AjaxResponse<Customer>(0, "success", pd.getTotalCount(), pd.getList());

		Gson gson = new Gson();
		String str = gson.toJson(ar);

		out.print(str);
		out.close();
	}

	protected void doAdd(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		// 顾客添加
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * 模拟登陆-测试用
	 * 
	 * @author ccp 蔡陈鹏
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doLogin(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (true) {

			// 接收请求的参数
//			String useridinput = request.getParameter("useridinput");
//			String pwdinput = request.getParameter("pwdinput");
			String c_id = "c0000000001";
			String c_account = "13600000123";
			String c_pwd = "123456";

//			String remflag = request.getParameter("remflag");
			String remflag = "成功";
			Customer customer = new Customer(c_id, c_account, c_pwd);
			// 验证
//			List<Customer> list = us.validUser(customer);
			if (1 == 0) {
				// 登录失败
				request.getSession().setAttribute("message", "用户名或者密码错误");
				response.sendRedirect("index.jsp");
			} else {
				if (remflag != "失败") {
					// 登录成功
					// 先空错误提示信息，以防重复
					request.getSession().setAttribute("message", "");
					// 1.将用户信息存储在servlet->Httpsession jsp->session
					HttpSession session = request.getSession();
					// 把用户放在session对象中
					session.setAttribute("customer", customer);

					// 2.存储数据在客户端的cookie中
//					Cookie cookie1 = new Cookie("c_account", "" + user.getUserid());
//					Cookie cookie2 = new Cookie("c_pwd", user.getUserpwd());
					Cookie cookie1 = new Cookie("c_id", c_id);
					Cookie cookie2 = new Cookie("c_account", c_account);
					Cookie cookie3 = new Cookie("c_pwd", c_pwd);

					// 3.设置生存时间—— 60*60*24 一天
					cookie1.setMaxAge(60 * 60 * 24 * 1);
					cookie2.setMaxAge(60 * 60 * 24 * 1);
					cookie3.setMaxAge(60 * 60 * 24 * 1);

					// 4.存储到客户端
					response.addCookie(cookie1);
					response.addCookie(cookie2);
					response.addCookie(cookie3);

				}
				request.getRequestDispatcher("./front/productdetails.jsp").forward(request, response);
			}
		}
	}

	protected void doDel(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// request.setCharacterEncoding("utf-8");
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		String c_id = "";
		if (request.getParameter("c_id") != null) {
			c_id = request.getParameter("c_id");
			System.out.println("c_id:" + c_id);
		}
		boolean flag = false;
		flag = cs.delCusById(c_id);
		AjaxResponse<Customer> ar = new AjaxResponse<Customer>();
		if (flag) {
			ar.setCode(0);
			ar.setMsg("删除成功！");
			System.out.println("ar.getCode:" + ar.getCode());
		} else {
			ar.setCode(0);
			ar.setMsg("删除失败！");
		}
		Gson gson = new Gson();
		String str = gson.toJson(ar);
		System.out.println(str);
		out.print(str);
		out.close();
	}

}
