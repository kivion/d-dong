package com.dmall.service;

import java.util.List;

import com.dmall.dao.MenueDao;
import com.dmall.entity.Menue;
import com.dmall.pagedata.PageData;

/**
 *	菜谱业务层
 * @author Administrator
 */
public class MenuService {
	MenueDao menudao = new MenueDao();
	/**
	 *	分页+模糊查询
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<Menue> getMenu(String keyword,int pageNo,int pageSize){
		
		if(pageNo<1) {
			pageNo=1;
		}
		return menudao.getMenu(keyword, pageNo, pageSize);
	}
	/**
	 * 模糊查询
	 * @param keywords 关键字
	 * @return
	 */
    public List<Menue> getMenulist(String keywords){
		return menudao.getMenuByKeywords(keywords);
	}
    /**
	 * 模糊查询
	 * @param keywords 关键字
	 * @return
	 */
    public List<Menue> getMenubyid(String mid){
		return menudao.getMenuBymid(mid);
	}
    /**
	 * 降序展示菜谱个人
	 * @param keywords 关键字
	 * @return
	 */
    public List<Menue> getMenuPersonbycid(String cid){
		return menudao.getMenuPersonal(cid);
	}
	/**
	 *	添加菜谱
	 * @param Menue
	 * @return Menu 对象
	 */
	public boolean addMenue(Menue menu) {
		
		return menudao.addMenue(menu);
	}
	/**
	 *	按菜谱id删除菜谱内容
	 * @param m_id
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean delMenueById(int m_id) {
		return menudao.delMenueById(m_id);
		
	}
	/**
	 *	按菜谱id修改菜谱
	 * @param Menue
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean updMenueById(Menue menu) {
		return menudao.updMenueById(menu);
		
	}
}
