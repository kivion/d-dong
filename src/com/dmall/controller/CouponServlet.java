package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dmall.entity.Coupon;
import com.dmall.pagedata.PageData;
import com.dmall.service.CartService;
import com.dmall.service.CouponService;
import com.dmall.util.AjaxResponse;
import com.dmall.util.UploadResponse;
import com.google.gson.Gson;

/**
 * Servlet implementation class AjaxAddressServlet
 */
@WebServlet("/coupon.do")
public class CouponServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//调用Service
	private CouponService cs = new CouponService(); 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CouponServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if ("add".equals(op)) {
			// 增加操作
			doAdd(request, response);
		} else {
			// 查询
			doQuery(request, response);
		}
		
	}
	
	
	protected void doAdd(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// MIME,返回类型就是json => application/json
		response.setContentType("application/json;charset=utf-8");

		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		//后期加入登录判断
		String co_id = "";
		if (request.getParameter("co_id") != null) {
			co_id = request.getParameter("co_id");
		}
		
		String co_name = "";
		if (request.getParameter("co_name") != null) {
			co_name = request.getParameter("co_name");
		}
		
		String co_start = "";
		if (request.getParameter("co_start") != null) {
			co_start = request.getParameter("co_start");
		}
		
		String co_end = "";
		if (request.getParameter("co_end") != null) {
			co_end = request.getParameter("co_end");
		}
		
		String co_num = "";
		if (request.getParameter("co_num") != null) {
			co_num = request.getParameter("co_num");
		}
		
		String co_state = "";
		if (request.getParameter("co_state") != null) {
			co_state = request.getParameter("co_state");
		}
		
		int co_sn = Integer.parseInt(request.getParameter("co_sn"));

		
		//优惠券对象
		Coupon coupon = new Coupon(co_sn, co_id, co_name, co_num, co_start, co_end, co_state);
		
		System.out.println("coupon: " + coupon);

		boolean flag = cs.addCoupon(coupon);
	
		UploadResponse ur = new UploadResponse();
		ur.setCode(0);
		if (flag) {
			ur.setMsg("操作成功");
		} else {
			ur.setMsg("操作失败");
		}
		Gson gson = new Gson();
		String str = gson.toJson(ur);

		System.out.println("str: " + str);

		out.print(str);

		out.close();

	}

	/**
	 * 查询操作
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String keywords = "";
		if (request.getParameter("keywords") != null) {
			keywords = request.getParameter("keywords");
		}
		
		int pageNo = 1;
		int pageSize = 10;
		if (request.getParameter("page") != null) {
			pageNo = Integer.parseInt(request.getParameter("page"));
		}

		if (request.getParameter("limit") != null) {
			pageSize = Integer.parseInt(request.getParameter("limit"));
		}
		
		
		System.out.println("keywords :" + keywords);

		// keywords就是传递的查询的关键字
		
		PageData<Coupon> pd = cs.getCouponByPage(keywords, pageNo, pageSize);
		
		// pd.getTotalCount(), 总记录数
		
		//pd.getList()  页面的数据
		AjaxResponse<Coupon> ar = new AjaxResponse<Coupon>(0, "success", pd.getTotalCount(), pd.getList());

		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);
		
		out.print(str);
		out.append(str);
		
		out.close();
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
