package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.dmall.entity.Grade;
import com.dmall.service.GradeService;
import com.dmall.util.AjaxResponse;
import com.google.gson.Gson;

/**
 * Servlet implementation class AjaxGoodsUpload
 */
@WebServlet("/grade.do")
@MultipartConfig
public class GradeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
GradeService gs=new GradeService();
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GradeServlet() {
		super();
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 最先做的事情，设置请求编码的格式
		request.setCharacterEncoding("utf-8");
		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if ("searchbyrid".equals(op)) {
			// 增加操作
			doQuerybtrid(request, response);
		} else {
			// 查询
			doQuery(request, response);
		}

	}

	/**
	 * 查询所有等级
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doQuery(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//MIME,返回类型就是json => application/json
		response.setContentType("application/json;charset=utf-8");
		
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String keywords = "";
		if (request.getParameter("keywords") != null) {
			keywords = request.getParameter("keywords");
		}
		
		System.out.println("keywords :" + keywords);

		// keywords就是传递的查询的关键字
		List<Grade> list = gs.getGradeByKey(keywords);
		
	
		//pd.getList()  页面的数据
		AjaxResponse<Grade> ar = new AjaxResponse<Grade>(0, "success",0,list);

		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);
		
		out.print(str);
		//out.append(str);
		
		out.close();
	}
	/**
	 * 查询所有等级 
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doQuerybtrid(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//MIME,返回类型就是json => application/json
		response.setContentType("application/json;charset=utf-8");
		
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String keywords = "";
		if (request.getParameter("keywords") != null) {
			keywords = request.getParameter("keywords");
		}
		
		System.out.println("keywords :" + keywords);

		// keywords就是传递的查询的关键字
		List<Grade> list = gs.getGradeBygid(keywords);
		
	
		//pd.getList()  页面的数据
		AjaxResponse<Grade> ar = new AjaxResponse<Grade>(0, "success",0,list);

		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);
		
		out.print(str);
		//out.append(str);
		
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	
		doGet(request, response);
	}

}
