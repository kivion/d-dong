package com.dmall.entity;

public class ProductCollection {
	private int pc_sn;
	private String p_id;
	private String c_id;
	
	public ProductCollection() {
		// TODO Auto-generated constructor stub
	}
	public ProductCollection(int pc_sn, String p_id, String c_id) {
		super();
		this.pc_sn = pc_sn;
		this.p_id = p_id;
		this.c_id = c_id;
	}
	
	public ProductCollection(String p_id, String c_id) {
		super();
		this.p_id = p_id;
		this.c_id = c_id;
	}
	public int getPc_sn() {
		return pc_sn;
	}
	public void setPc_sn(int pc_sn) {
		this.pc_sn = pc_sn;
	}
	public String getP_id() {
		return p_id;
	}
	public void setP_id(String p_id) {
		this.p_id = p_id;
	}
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}
	@Override
	public String toString() {
		return "Customer [pc_sn=" + pc_sn + ", p_id=" + p_id + ", c_id=" + c_id + "]";
	}
	
}
