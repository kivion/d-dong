package com.dmall.entity;

public class Order {
	private int o_sn;
	private String o_id;
	private String c_id;
	private String o_creat;
	private String cart_id;
	private String paystate;
	private String o_paymethod;
	private String o_money;
	private String r_id;
	private String o_end;
	private String o_paytime;
	private String o_state;
	
	public Order(int o_sn, String o_id, String c_id, String o_creat, String cart_id, String paystate,
			String o_paymethod, String o_money, String r_id, String o_end, String o_paytime, String o_state) {
		super();
		this.o_sn = o_sn; 
		this.o_id = o_id;
		this.c_id = c_id;
		this.o_creat = o_creat;
		this.cart_id = cart_id;
		this.paystate = paystate;
		this.o_paymethod = o_paymethod;
		this.o_money = o_money;
		this.r_id = r_id;
		this.o_end = o_end;
		this.o_paytime = o_paytime;
		this.o_state = o_state;
	}

	public Order(String o_id, String paystate) {
		super();
		this.o_id = o_id;
		this.paystate = paystate;
	}


	//未支付订单构造
	public Order(int o_sn, String o_id, String c_id, String o_creat, String cart_id, String paystate, String o_money,
			String o_state) {
		super();
		this.o_sn = o_sn;
		this.o_id = o_id;
		this.c_id = c_id;
		this.o_creat = o_creat;
		this.cart_id = cart_id;
		this.paystate = paystate;
		this.o_money = o_money;
		this.o_state = o_state;
	}


	public Order() {
		// TODO Auto-generated constructor stub
	}

	public int getO_sn() {
		return o_sn;
	}

	public void setO_sn(int o_sn) {
		this.o_sn = o_sn;
	}

	public String getO_id() {
		return o_id;
	}

	public void setO_id(String o_id) {
		this.o_id = o_id;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getO_creat() {
		return o_creat;
	}

	public void setO_creat(String o_creat) {
		this.o_creat = o_creat;
	}

	public String getCart_id() {
		return cart_id;
	}

	public void setCart_id(String cart_id) {
		this.cart_id = cart_id;
	}

	public String getPaystate() {
		return paystate;
	}

	public void setPaystate(String paystate) {
		this.paystate = paystate;
	}

	public String getO_paymethod() {
		return o_paymethod;
	}

	public void setO_paymethod(String o_paymethod) {
		this.o_paymethod = o_paymethod;
	}
	
	public String getO_money() {
		return o_money;
	}


	public void setO_money(String o_money) {
		this.o_money = o_money;
	}

	public String getR_id() {
		return r_id;
	}

	public void setR_id(String r_id) {
		this.r_id = r_id;
	}

	public String getO_end() {
		return o_end;
	}

	public void setO_end(String o_end) {
		this.o_end = o_end;
	}

	public String getO_paytime() {
		return o_paytime;
	}

	public void setO_paytime(String o_paytime) {
		this.o_paytime = o_paytime;
	}

	public String getO_state() {
		return o_state;
	}

	public void setO_state(String o_state) {
		this.o_state = o_state;
	}

	@Override
	public String toString() {
		return "Order [o_sn=" + o_sn + ", o_id=" + o_id + ", c_id=" + c_id + ", o_creat=" + o_creat + ", cart_id="
				+ cart_id + ", paystate=" + paystate + ", o_paymethod=" + o_paymethod + ", r_id=" + r_id + ", o_end="
				+ o_end + ", o_paytime=" + o_paytime + ", o_state=" + o_state + "]";
	}
	
}
