package com.dmall.entity;

public class ProductTypeNext {
	private String ptn_id;
	private String ptn_name;
	private int ptn_sn;
	private String ptf_id;
	
	/**
	 * 联表新增
	 */
	private ProductTypeFirst producttypefirst = new ProductTypeFirst();

	public ProductTypeFirst getProductTypeFirst() {
		return producttypefirst;
	}

	private Product product = new Product();

	public Product getProduct() {
		return product;
	}

	/**
	 * 联表结束
	 */
	
	public ProductTypeNext() {
	}

	public ProductTypeNext(String ptn_id, String ptn_name, int ptn_sn, String ptf_id) {
		super();
		this.ptn_id = ptn_id;
		this.ptn_name = ptn_name;
		this.ptn_sn = ptn_sn;
		this.ptf_id = ptf_id;
	}

	public String getPtn_id() {
		return ptn_id;
	}

	public void setPtn_id(String ptn_id) {
		this.ptn_id = ptn_id;
	}

	public String getPtn_name() {
		return ptn_name;
	}

	public void setPtn_name(String ptn_name) {
		this.ptn_name = ptn_name;
	}

	public int getPtn_sn() {
		return ptn_sn;
	}

	public void setPtn_sn(int ptn_sn) {
		this.ptn_sn = ptn_sn;
	}

	public String getPtf_id() {
		return ptf_id;
	}

	public void setPtf_id(String ptf_id) {
		this.ptf_id = ptf_id;
	}

	@Override
	public String toString() {
		return "ProductTypeNext [ptn_id=" + ptn_id + ", ptn_name=" + ptn_name + ", ptn_sn=" + ptn_sn + ", ptf_id="
				+ ptf_id + "]";
	}
	
	
}
