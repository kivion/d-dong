package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dmall.entity.Customer;
import com.dmall.entity.Menue;
import com.dmall.entity.Product;
import com.dmall.pagedata.PageData;
import com.dmall.service.MenuService;
import com.dmall.service.ProductService;
import com.dmall.service.ProductTypeNextService;
import com.dmall.util.AjaxResponse;
import com.dmall.util.UploadResponse;
import com.google.gson.Gson;

@WebServlet("/product.do")
public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ProductService ps = new ProductService();
	private ProductTypeNextService ptns = new ProductTypeNextService();

	public ProductServlet() {

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if ("add".equals(op)) {
			doAdd(request, response);
		} else if ("del".equals(op)) {
			doDel(request, response);
		} else {
			doQuery(request, response);
		}
	}

	protected void doQuery(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String keywords = "";
		if (request.getParameter("keywords") != null) {
			keywords = request.getParameter("keywords");
		}
		int pageNo = 1;
		int pageSize = 10;
		if (request.getParameter("page") != null) {
			pageNo = Integer.parseInt(request.getParameter("page"));
		}

		if (request.getParameter("limit") != null) {
			pageSize = Integer.parseInt(request.getParameter("limit"));
		}
		// keywords传递的查询的关键字
		PageData<Product> pd = ps.getProduct(keywords, pageNo, pageSize);

		AjaxResponse<Product> ar = new AjaxResponse<Product>(0, "success", pd.getTotalCount(), pd.getList());

		Gson gson = new Gson();
		String str = gson.toJson(ar);
		System.out.println(str);
		out.print(str);
		out.close();
	}

	protected void doAdd(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		// 接收从jsp的商品添加表单提交的数据
		String cover = "";
		if (request.getParameter("cover") != null) {
			cover = request.getParameter("cover");
		}
		String productid = "";
		if (request.getParameter("productid") != null) {
			productid = request.getParameter("productid");
		}
		String productname = "";
		if (request.getParameter("productname") != null) {
			productname = request.getParameter("productname");
		}
		double productprice = 0;
		if (request.getParameter("productprice") != null) {
			productprice = Double.parseDouble(request.getParameter("productprice"));
		}
		String productcount = "";
		if (request.getParameter("productcount") != null) {
			productcount = request.getParameter("productcount");
		}
		int weight = 0;
		if (request.getParameter("weight") != null) {
			weight = Integer.parseInt(request.getParameter("weight"));
		}
		/* 省市区 */
		String province = "";
		if (request.getParameter("province") != null) {
			province = request.getParameter("province");
			System.out.println(province);
		}
		String city = "";
		if (request.getParameter("city") != null) {
			city = request.getParameter("city");
			System.out.println(city);
		}
		String area = "";
		if (request.getParameter("area") != null) {
			area = request.getParameter("area");
			System.out.println(area);
		}
		String address = province + city + area;
		System.out.println("address:" + address);
		/* 省市区 */

		String typename = "";
		if (request.getParameter("type") != null) {
			typename = request.getParameter("type");
		}
		String savemethod = "";
		if (request.getParameter("savemethod") != null) {
			savemethod = request.getParameter("savemethod");
		}
		String createtime = "";
		if (request.getParameter("createtime") != null) {
			createtime = request.getParameter("createtime");
		}
		int state = 1;
		String quality = "";
		if (request.getParameter("quality") != null) {
			quality = request.getParameter("quality");
		}
		String productinfo = "";
		if (request.getParameter("productinfo") != null) {
			productinfo = request.getParameter("productinfo");
		}
		String typeid = ptns.getPtn_id(typename);
		System.out.println(typeid);
		Product product = new Product(productid, productname, weight, productcount, address, savemethod, productinfo,
				createtime, quality, cover, typeid, productprice);
		boolean flag = ps.addProduct(product);
		AjaxResponse ar = new AjaxResponse();
		Gson gson = new Gson();
		if (flag) {
			ar.setCode(1);
			ar.setMsg("添加成功！");
		} else {
			ar.setCode(0);
			ar.setMsg("添加失败！");
		}

		String str = gson.toJson(ar);
		out.print(str);
		out.close();
	}

	// 删除
	protected void doDel(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		String p_id = "";
		if (request.getParameter("p_id") != null) {
			p_id = request.getParameter("p_id");
			System.out.println("p_id:" + p_id);
		}
		boolean flag = false;
		flag = ps.delProduct(p_id);
		AjaxResponse ar = new AjaxResponse();
		if (flag) {
			ar.setCode(0);
			ar.setMsg("删除成功！");
			System.out.println("ar.getCode:" + ar.getCode());
		} else {
			ar.setCode(0);
			ar.setMsg("删除失败！");
		}
		Gson gson = new Gson();
		String str = gson.toJson(ar);
		System.out.println(str);
		out.print(str);
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("dopost");
		doGet(request, response);
	}

}
