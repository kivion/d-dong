<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!-- 加入 taglib 标记-->
<%@ taglib prefix="i" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>

<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>叮咚到家 - 你想要的生鲜好菜，说到就到</title>
<!--在pageScope内存储了一个basepath，值设为${pageContext.request.contextPath}，实际上就是当前工程的上下文路径-->
<i:set var="basepath" scope="page"
	value="${pageContext.request.contextPath}"></i:set>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon"
	href="${basepath}/front/assets/img/favicon.ico">

<!-- CSS 
    ========================= -->
<!--bootstrap min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/bootstrap.min.css">
<!--owl carousel min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/owl.carousel.min.css">
<!--slick min css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/slick.css">
<!--magnific popup min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/magnific-popup.css">
<!--font awesome css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/font.awesome.css">
<!--ionicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/ionicons.min.css">
<!--linearicons css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/linearicons.css">
<!--animate css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/animate.css">
<!--jquery ui min css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/jquery-ui.min.css">
<!--slinky menu css-->
<link rel="stylesheet"
	href="${basepath}/front/assets/css/slinky.menu.css">
<!--plugins css-->
<link rel="stylesheet" href="${basepath}/front/assets/css/plugins.css">

<!-- Main Style CSS -->
<link rel="stylesheet" href="${basepath}/front/assets/css/style.css">
<!--layUI的css-->
<link rel="stylesheet" href="${basepath}/front/layui/css/layui.css"
	media="all">

<!--modernizr min js here-->
<script src="${basepath}/front/assets/js/vendor/modernizr-3.7.1.min.js"></script>
</head>

<body>
	<!--头部开始-->
	<header>
		<div class="main_header">
			<div class="header_top">
				<div class="container">
					<div class="row align-items-center">

						<!--导航栏-->
						<div class="col-lg-6 col-md-6">
							<div class="language_currency">
								<ul>
									<li class="language"><a href="#"> 语言 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_language">
											<li><a href="#">中文</a></li>
											<li><a href="#">英语</a></li>
										</ul></li>
									<li class="currency"><a href="#"> 简繁体 <i
											class="icon-right ion-ios-arrow-down"></i></a>
										<ul class="dropdown_currency">
											<li><a href="#">简体</a></li>
											<li><a href="#">繁体</a></li>
										</ul></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="header_social text-right">
								<ul>
									<li><a href="#"><i class="ion-social-twitter"></i></a></li>
									<li><a href="#"><i
											class="ion-social-googleplus-outline"></i></a></li>
									<li><a href="#"><i class="ion-social-youtube-outline"></i></a>
									</li>
									<li><a href="#"><i class="ion-social-facebook"></i></a></li>
									<li><a href="#"><i
											class="ion-social-instagram-outline"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_middle">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-2">
							<div class="logo">
								<a href="index.html"><img src="${basepath}/front/assets/img/logo/logo.jpg"
									alt=""></a>
							</div>
						</div>
						<div class="col-lg-10">
							<div class="header_right_info">
								<!--导航栏左边的类别-->
								<div class="search_container">
									<form action="${basepath}/productshoplist.do?keywords="
										method="get"">
										<div class="hover_category">
											<select class="select_option" name="select" id="categori1">
												<option selected value="1">选择类别</option>
												<option value="2">水果</option>
												<option value="3">蔬菜</option>
												<option value="4">肉禽蛋品</option>
												<option value="5">海鲜水产</option>
												<option value="6">熟食卤味</option>
												<option value="7">粮油调味</option>
												<option value="8">休闲零食</option>
												<option value="9">冰品面点</option>
												<option value="10">牛奶面包</option>
												<option value="11">酒水冲饮</option>
											</select>
										</div>
										<div class="search_box">
											<input placeholder="国产红心火龙果0.99元/个..." type="text">
											<button type="submit">
												<span class="lnr lnr-magnifier"></span>
											</button>
										</div>
									</form>
								</div>
								<div class="header_account_area">
									<div class="header_account_list register">
										<ul class="layui-nav">
											<!--layUI的状态头像登录-->
											<li class="layui-nav-item" lay-unselect=""><a
												href="${basepath}/front/my-account.jsp"><img
													src="${basepath}/front/assets/img/icon/1.jpg"
													class="layui-nav-img">佚名</a>
												<dl class="layui-nav-child">
													<dd>
														<a href="javascript:;">修改信息</a>
													</dd>
													<dd>
														<a href="javascript:;">账号管理</a>
													</dd>
													<dd>
														<a href="javascript:;">退出</a>
													</dd>
												</dl></li>
											<!--<li>
													<a href="login.html">注册</a>
												</li>
												<li><span>/</span></li>
												<li>
													<a href="login.html">登录</a>
												</li>-->
										</ul>
									</div>
									<div class="header_account_list header_wishlist">
										<a href="收藏页面.html"><span class="lnr lnr-heart"></span> <span
											class="item_count">5</span> </a>
									</div>
									<div class="header_account_list  mini_cart_wrapper">
										<a href="${basepath}/front/Cart.jsp"><span class="lnr lnr-cart"></span><span
											class="item_count">2</span></a>
										<!--mini cart-->
										<div class="mini_cart">
											<div class="cart_gallery">
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="${basepath}/front/assets/img/s-product/product.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Primis In Faucibus</a>
														<p>
															1 x <span> $65.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img
															src="${basepath}/front/assets/img/s-product/product2.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Letraset Sheets</a>
														<p>
															1 x <span> $60.00 </span>
														</p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
											</div>
											<div class="mini_cart_table">
												<div class="cart_table_border">
													<div class="cart_total mt-10">
														<span>总计:</span> <span class="price">$125.00</span>
													</div>
												</div>
											</div>
											<div class="mini_cart_footer">
												<div class="cart_button">
													<a href="${basepath}/front/Cart.jsp"><i class="fa fa-shopping-cart"></i>
														前往购物车</a>
												</div>
												<div class="cart_button">
													<a href="${basepath}/front/Cart.jsp"><i class="fa fa-sign-in"></i> ...</a>
												</div>

											</div>
										</div>
										<!--mini cart end-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header_bottom sticky-header">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-3">
							<div class="categories_menu">
								<div class="categories_title">
									<h2 class="categori_toggle">所有商品</h2>
								</div>
								<div class="categories_menu_toggle">
									<ul>
										<li class="menu_item_children"><a href="#part1">蔬菜<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu">
												<li class="menu_item_children"><a href="#">Dresses</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Sweater</a></li>
														<li><a href="">Evening</a></li>
														<li><a href="">Day</a></li>
														<li><a href="">Sports</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Handbags</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Shoulder</a></li>
														<li><a href="">Satchels</a></li>
														<li><a href="">kids</a></li>
														<li><a href="">coats</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">shoes</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Ankle Boots</a></li>
														<li><a href="">Clog sandals </a></li>
														<li><a href="">run</a></li>
														<li><a href="">Books</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Clothing</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Coats Jackets </a></li>
														<li><a href="">Raincoats</a></li>
														<li><a href="">Jackets</a></li>
														<li><a href="">T-shirts</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part1">水果<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_3">
												<li class="menu_item_children"><a href="#">Chair</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Dining room</a></li>
														<li><a href="">bedroom</a></li>
														<li><a href=""> Home & Office</a></li>
														<li><a href="">living room</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Lighting</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Ceiling Lighting</a></li>
														<li><a href="">Wall Lighting</a></li>
														<li><a href="">Outdoor Lighting</a></li>
														<li><a href="">Smart Lighting</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Sofa</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Fabric Sofas</a></li>
														<li><a href="">Leather Sofas</a></li>
														<li><a href="">Corner Sofas</a></li>
														<li><a href="">Sofa Beds</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part1">肉禽蛋品<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">Brake
														Tools</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Driveshafts</a></li>
														<li><a href="">Spools</a></li>
														<li><a href="">Diesel </a></li>
														<li><a href="">Gasoline</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Emergency
														Brake</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Dolls for Girls</a></li>
														<li><a href="">Girls' Learning Toys</a></li>
														<li><a href="">Arts and Crafts for Girls</a></li>
														<li><a href="">Video Games for Girls</a></li>
													</ul></li>
											</ul></li>
										<li class="menu_item_children"><a href="#part2">海鲜水产<i
												class="fa fa-angle-right"></i></a>
											<ul class="categories_mega_menu column_2">
												<li class="menu_item_children"><a href="#">Check
														Trousers</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Building</a></li>
														<li><a href="">Electronics</a></li>
														<li><a href="">action figures </a></li>
														<li><a href="">specialty & boutique toy</a></li>
													</ul></li>
												<li class="menu_item_children"><a href="#">Calculators</a>
													<ul class="categorie_sub_menu">
														<li><a href="">Dolls for Girls</a></li>
														<li><a href="">Girls' Learning Toys</a></li>
														<li><a href="">Arts and Crafts for Girls</a></li>
														<li><a href="">Video Games for Girls</a></li>
													</ul></li>
											</ul></li>
										<li><a href="#part3">熟食卤味</a></li>
										<li><a href="#part4">休闲零食</a></li>
										<li><a href="#part5">粮油调味</a></li>
										<li><a href="#">冰品面点</a></li>
										<li><a href="#">牛奶面包</a></li>
										<li><a href="#">酒水冲饮</a></li>

										<li id="cat_toggle" class="has-sub"><a href="#">>更多</a>
											<ul class="categorie_sub">
												<li><a href="#">O(∩_∩)O哈哈~</a></li>
											</ul></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<!--main menu start-->
							<div class="main_menu menu_position">
								<nav>
									<ul>
										<li><a class="active" href="${basepath}/front/index.jsp">主页</a></li>
										<li><a href="${basepath}/front/Menu_Home.jsp">菜谱 <!--<i class="fa fa-angle-down"></i>--></a>
										</li>
										<li><a href="关于我们.html">关于我们</a></li>
										<li><a href="联系我们.html">联系我们</a></li>
									</ul>
								</nav>
							</div>
							<!--main menu end-->
						</div>
						<div class="col-lg-3">
							<div class="call-support">
								<p>
									<a href="tel:(08)8399608">(08)8399 608</a> 用户反馈
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!--header area end-->

	<!--slider area start-->
	<section class="slider_section">
		<div class="slider_area owl-carousel">
			<div class="single_slider d-flex align-items-center"
				data-bgimg="assets/img/slider/slider1.jpg">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="slider_content">
								<h1>蔬菜大热销</h1>
								<h2>竟然只要9.9？</h2>
								<p>今日满10减1，满11加1</p>
								<a href="shop.html">前往</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="single_slider d-flex align-items-center"
				data-bgimg="assets/img/slider/slider2.jpg">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="slider_content">
								<h1>蔬菜大热销</h1>
								<h2>竟然只要9.9？</h2>
								<p>今日满10减1，满11加1</p>
								<a href="shop.html">前往</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="single_slider d-flex align-items-center"
				data-bgimg="assets/img/slider/slider3.jpg">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="slider_content">
								<h1>蔬菜大热销</h1>
								<h2>竟然只要9.9？</h2>
								<p>今日满10减1，满11加1</p>
								<a href="shop.html">前往</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--slider area end-->

	<!--shipping area start-->
	<div class="shipping_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="single_shipping">
						<div class="shipping_icone">
							<img src="assets/img/about/shipping1.jpg" alt="">
						</div>
						<div class="shipping_content">
							<h3>仓储配送模式</h3>
							<p>更快的配送方式</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single_shipping col_2">
						<div class="shipping_icone">
							<img src="assets/img/about/shipping2.jpg" alt="">
						</div>
						<div class="shipping_content">
							<h3>业务范围广</h3>
							<p>包含水果蔬菜、肉禽蛋奶、海鲜水产、粮油调味、酒水饮料、休闲食品、日用品等</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single_shipping col_3">
						<div class="shipping_icone">
							<img src="assets/img/about/shipping3.jpg" alt="">
						</div>
						<div class="shipping_content">
							<h3>急速配送</h3>
							<p>30分钟内快速送达</p>

						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single_shipping col_4">
						<div class="shipping_icone">
							<img src="assets/img/about/shipping4.jpg" alt="">
						</div>
						<div class="shipping_content">
							<h3>质量保障</h3>
							<p>高标准的食品安全管控体系</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--shipping area end-->

	<!--product area start-->
	<div class="product_area  mb-64">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="product_header">
						<div class="section_title" id="part1">
							<p>你想要的生鲜好菜，说到就到！</p>
							<h2>今日推荐</h2>
						</div>
						<div class="product_tab_btn">
							<ul class="nav" role="tablist">
								<li><a class="active" data-toggle="tab" href="#plant1"
									role="tab" aria-controls="plant1" aria-selected="true"> 蔬菜
								</a></li>
								<li><a data-toggle="tab" href="#plant2" role="tab"
									aria-controls="plant2" aria-selected="false"> 水果 </a></li>
								<li><a data-toggle="tab" href="#plant3" role="tab"
									aria-controls="plant3" aria-selected="false"> 肉禽蛋品 </a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="product_container">
				<div class="row">
					<div class="col-12">
						<div class="tab-content">
							<div class="tab-pane fade show active" id="plant1"
								role="tabpanel">
								<div class="product_carousel product_column5 owl-carousel">
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product1.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product2.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Aliquam Consequat</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product2.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product14.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Donec Non Est</a>
													</h4>
													<p>
														<a href="#">水果</a>
													</p>
													<div class="price_box">
														<span class="current_price">$46.00</span> <span
															class="old_price">$382.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product5.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product6.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>

													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Etiam Gravida</a>
													</h4>
													<p>
														<a href="#">水果</a>
													</p>
													<div class="price_box">
														<span class="current_price">$56.00</span> <span
															class="old_price">$322.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product7.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product8.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Fusce Aliquam</a>
													</h4>
													<p>
														<a href="#">水果</a>
													</p>
													<div class="price_box">
														<span class="current_price">$66.00</span> <span
															class="old_price">$312.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product9.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product10.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Letraset Sheets</a>
													</h4>
													<p>
														<a href="#">水果</a>
													</p>
													<div class="price_box">
														<span class="current_price">$38.00</span> <span
															class="old_price">$262.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product11.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product12.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Lorem Ipsum Lec</a>
													</h4>
													<p>
														<a href="#">水果</a>
													</p>
													<div class="price_box">
														<span class="current_price">$36.00</span> <span
															class="old_price">$145.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product13.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product1.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Mauris Vel Tellus</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$48.00</span> <span
															class="old_price">$257.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product12.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product2.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Nunc Neque Eros</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$35.00</span> <span
															class="old_price">$245.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product11.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product3.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Proin Lectus Ipsum</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product9.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product4.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Quisque In Arcu</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$55.00</span> <span
															class="old_price">$235.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product8.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product5.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Cas Meque Metus</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product7.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product6.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Aliquam Consequat</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="plant2" role="tabpanel">
								<div class="product_carousel product_column5 owl-carousel">
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product13.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product1.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Mauris Vel Tellus</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$48.00</span> <span
															class="old_price">$257.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product12.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product2.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Nunc Neque Eros</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$35.00</span> <span
															class="old_price">$245.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product11.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product3.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Proin Lectus Ipsum</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product9.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product4.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Quisque In Arcu</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$55.00</span> <span
															class="old_price">$235.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product8.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product5.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Cas Meque Metus</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product7.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product6.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Aliquam Consequat</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product1.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product2.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Aliquam Consequat</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product3.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product4.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Donec Non Est</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$46.00</span> <span
															class="old_price">$382.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product5.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product6.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>

													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Etiam Gravida</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$56.00</span> <span
															class="old_price">$322.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product7.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product8.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Fusce Aliquam</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$66.00</span> <span
															class="old_price">$312.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product9.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product10.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Letraset Sheets</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$38.00</span> <span
															class="old_price">$262.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product11.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product12.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Lorem Ipsum Lec</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$36.00</span> <span
															class="old_price">$145.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>

								</div>
							</div>
							<div class="tab-pane fade" id="plant3" role="tabpanel">
								<div class="product_carousel product_column5 owl-carousel">
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product11.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product3.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Proin Lectus Ipsum</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product9.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product4.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Quisque In Arcu</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$55.00</span> <span
															class="old_price">$235.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product13.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product1.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Mauris Vel Tellus</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$48.00</span> <span
															class="old_price">$257.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product12.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product2.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Nunc Neque Eros</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$35.00</span> <span
															class="old_price">$245.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product1.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product2.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Aliquam Consequat</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product3.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product4.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Donec Non Est</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$46.00</span> <span
															class="old_price">$382.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product5.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product6.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>

													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Etiam Gravida</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$56.00</span> <span
															class="old_price">$322.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product7.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product8.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Fusce Aliquam</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$66.00</span> <span
															class="old_price">$312.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product9.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product10.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Letraset Sheets</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$38.00</span> <span
															class="old_price">$262.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product11.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product12.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span> <span
															class="label_new">New</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Lorem Ipsum Lec</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$36.00</span> <span
															class="old_price">$145.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
									<div class="product_items">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product8.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product5.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Cas Meque Metus</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img" href="product-details.html"><img
														src="assets/img/product/product7.jpg" alt=""></a> <a
														class="secondary_img" href="product-details.html"><img
														src="assets/img/product/product6.jpg" alt=""></a>
													<div class="label_product">
														<span class="label_sale">Sale</span>
													</div>
													<div class="action_links">
														<ul>
															<li class="add_to_cart"><a href="购物车.html"
																title="Add to cart"><span class="lnr lnr-cart"></span></a>
															</li>
															<li class="quick_button"><a href="#"
																data-toggle="modal" data-target="#modal_box"
																title="quick view"> <span class="lnr lnr-magnifier"></span></a>
															</li>
															<li class="wishlist"><a href="收藏页面.html"
																title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
															<li class="compare"><a href="#"
																title="Add to Compare"><span class="lnr lnr-sync"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name">
														<a href="product-details.html">Aliquam Consequat</a>
													</h4>
													<p>
														<a href="#">Fruits</a>
													</p>
													<div class="price_box">
														<span class="current_price">$26.00</span> <span
															class="old_price">$362.00</span>
													</div>
												</figcaption>
											</figure>
										</article>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--product area end-->

	<!--banner area start-->
	<div class="banner_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="single_banner">
						<div class="banner_thumb">
							<a href="shop.html"><img src="assets/img/bg/banner1.jpg"
								alt=""></a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="single_banner">
						<div class="banner_thumb">
							<a href="shop.html"><img src="assets/img/bg/banner2.jpg"
								alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--banner area end-->

	<!--product area start-->
	<div class="product_area product_deals mb-65">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="section_title">
						<p>价格低廉，还不心动吗</p>
						<h2>限时热销</h2>
					</div>
				</div>
			</div>
			<div class="product_container">
				<div class="row">
					<div class="col-12">
						<div class="product_carousel product_column5 owl-carousel">
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product14.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product15.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span> <span class="label_new">New</span>
										</div>
										<div class="product_timing">
											<div data-countdown="2021/12/15"></div>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Mauris Vel Tellus</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$48.00</span> <span
												class="old_price">$257.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product16.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product17.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span>
										</div>
										<div class="product_timing">
											<div data-countdown="2021/12/15"></div>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Nunc Neque Eros</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$35.00</span> <span
												class="old_price">$245.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product18.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product19.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span>
										</div>
										<div class="product_timing">
											<div data-countdown="2021/12/15"></div>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Proin Lectus Ipsum</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$26.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product20.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product21.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span> <span class="label_new">New</span>
										</div>
										<div class="product_timing">
											<div data-countdown="2021/12/15"></div>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Quisque In Arcu</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$55.00</span> <span
												class="old_price">$235.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product15.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product14.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span>
										</div>
										<div class="product_timing">
											<div data-countdown="2021/12/15"></div>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Cas Meque Metus</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$26.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product17.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product16.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span>
										</div>
										<div class="product_timing">
											<div data-countdown="2021/12/15"></div>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="加入购物车"><span class="lnr lnr-cart"></span></a></li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Aliquam Consequat</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$26.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--product area end-->

	<!--banner fullwidth area satrt-->
	<div class="banner_fullwidth">
		<div class="container">
			<div class="row">
				<div class="col-12" id="part4">
					<div class="banner_full_content">
						<p>一起狂欢</p>
						<h2>
							休闲零食<span>所有零食都在这！</span>
						</h2>
						<a href="shop.html">前往</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--banner fullwidth area end-->

	<!--product banner area satrt-->
	<div class="product_banner_area mb-65">
		<div class="container">
			<div class="row">
				<div class="col-12" id="part2">
					<div class="section_title">
						<p>高端的食材，往往只需要采用最朴素的烹饪方式</p>
						<h2>海鲜水产</h2>
					</div>
				</div>
			</div>
			<div class="product_banner_container">
				<div class="row">
					<div class="col-lg-4 col-md-5">
						<div class="banner_thumb">
							<a href="shop.html"><img src="assets/img/bg/banner4.jpg"
								alt=""></a>
						</div>
					</div>
					<div class="col-lg-8 col-md-7">
						<div class="small_product_area product_column2 owl-carousel">
							<div class="product_items">
								<article class="single_product">
									<figure>
										<div class="product_thumb">
											<a class="primary_img" href="product-details.html"><img
												src="assets/img/product/product1.jpg" alt=""></a> <a
												class="secondary_img" href="product-details.html"><img
												src="assets/img/product/product2.jpg" alt=""></a>
										</div>
										<figcaption class="product_content">
											<h4 class="product_name">
												<a href="product-details.html">Aliquam Consequat</a>
											</h4>
											<p>
												<a href="#">Fruits</a>
											</p>
											<div class="action_links">
												<ul>
													<li class="add_to_cart"><a href="购物车.html"
														title="Add to cart"><span class="lnr lnr-cart"></span></a>
													</li>
													<li class="quick_button"><a href="#"
														data-toggle="modal" data-target="#modal_box"
														title="quick view"> <span class="lnr lnr-magnifier"></span></a>
													</li>
													<li class="wishlist"><a href="收藏页面.html"
														title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
													</li>
													<li class="compare"><a href="#" title="Add to Compare"><span
															class="lnr lnr-sync"></span></a></li>
												</ul>
											</div>
											<div class="price_box">
												<span class="current_price">$26.00</span> <span
													class="old_price">$362.00</span>
											</div>
										</figcaption>
									</figure>
								</article>
								<article class="single_product">
									<figure>
										<div class="product_thumb">
											<a class="primary_img" href="product-details.html"><img
												src="assets/img/product/product3.jpg" alt=""></a> <a
												class="secondary_img" href="product-details.html"><img
												src="assets/img/product/product4.jpg" alt=""></a>
										</div>
										<figcaption class="product_content">
											<h4 class="product_name">
												<a href="product-details.html">Donec Non Est</a>
											</h4>
											<p>
												<a href="#">Fruits</a>
											</p>
											<div class="action_links">
												<ul>
													<li class="add_to_cart"><a href="购物车.html"
														title="Add to cart"><span class="lnr lnr-cart"></span></a>
													</li>
													<li class="quick_button"><a href="#"
														data-toggle="modal" data-target="#modal_box"
														title="quick view"> <span class="lnr lnr-magnifier"></span></a>
													</li>
													<li class="wishlist"><a href="收藏页面.html"
														title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
													</li>
													<li class="compare"><a href="#" title="Add to Compare"><span
															class="lnr lnr-sync"></span></a></li>
												</ul>
											</div>
											<div class="price_box">
												<span class="current_price">$46.00</span> <span
													class="old_price">$382.00</span>
											</div>
										</figcaption>
									</figure>
								</article>
								<article class="single_product">
									<figure>
										<div class="product_thumb">
											<a class="primary_img" href="product-details.html"><img
												src="assets/img/product/product5.jpg" alt=""></a> <a
												class="secondary_img" href="product-details.html"><img
												src="assets/img/product/product6.jpg" alt=""></a>
										</div>
										<figcaption class="product_content">
											<h4 class="product_name">
												<a href="product-details.html">Mauris Vel Tellus</a>
											</h4>
											<p>
												<a href="#">Fruits</a>
											</p>
											<div class="action_links">
												<ul>
													<li class="add_to_cart"><a href="购物车.html"
														title="Add to cart"><span class="lnr lnr-cart"></span></a>
													</li>
													<li class="quick_button"><a href="#"
														data-toggle="modal" data-target="#modal_box"
														title="quick view"> <span class="lnr lnr-magnifier"></span></a>
													</li>
													<li class="wishlist"><a href="收藏页面.html"
														title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
													</li>
													<li class="compare"><a href="#" title="Add to Compare"><span
															class="lnr lnr-sync"></span></a></li>
												</ul>
											</div>
											<div class="price_box">
												<span class="current_price">$56.00</span> <span
													class="old_price">$362.00</span>
											</div>
										</figcaption>
									</figure>
								</article>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--product banner area end-->

	<!--product area start-->
	<div class="product_area mb-65">
		<div class="container">
			<div class="row">
				<div class="col-12" id="part3">
					<div class="section_title">
						<p>忙累了，直接吃卤味吧</p>
						<h2>熟食卤味</h2>
					</div>
				</div>
			</div>
			<div class="product_container">
				<div class="row">
					<div class="col-12">
						<div class="product_carousel product_column5 owl-carousel">
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product20.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product21.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span> <span class="label_new">New</span>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Quisque In Arcu</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$55.00</span> <span
												class="old_price">$235.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product15.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product14.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Cas Meque Metus</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$26.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product17.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product16.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Aliquam Consequat</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$26.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product14.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product15.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span> <span class="label_new">New</span>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Mauris Vel Tellus</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$48.00</span> <span
												class="old_price">$257.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product16.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product17.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Nunc Neque Eros</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$35.00</span> <span
												class="old_price">$245.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product18.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product19.jpg" alt=""></a>
										<div class="label_product">
											<span class="label_sale">Sale</span>
										</div>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Proin Lectus Ipsum</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="price_box">
											<span class="current_price">$26.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--product area end-->

	<!--blog area start-->
	<section class="blog_section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section_title">
						<p>你可能需要多学一道菜</p>
						<h2>每日小厨房</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="blog_carousel blog_column3 owl-carousel">
					<div class="col-lg-3">
						<article class="single_blog">
							<figure>
								<div class="blog_thumb">
									<a href="菜谱—笔记详情页.html"><img
										src="assets/img/blog/blog1.jpg" alt=""></a>
								</div>
								<figcaption class="blog_content">
									<div class="articles_date">
										<p>
											18/01/2019 | <a href="#">烤鸭为什么要烤？</a>
										</p>
									</div>
									<h4 class="post_title">
										<a href="菜谱—笔记详情页.html">因为烤鸭没熟，所以要烤</a>
									</h4>
									<footer class="blog_footer">
										<a href="菜谱—笔记详情页.html">查看详细</a>
									</footer>
								</figcaption>
							</figure>
						</article>
					</div>
					<div class="col-lg-3">
						<article class="single_blog">
							<figure>
								<div class="blog_thumb">
									<a href="菜谱—笔记详情页.html"><img
										src="assets/img/blog/blog2.jpg" alt=""></a>
								</div>
								<figcaption class="blog_content">
									<div class="articles_date">
										<p>
											18/01/2019 | <a href="#">烤鸭为什么要烤？</a>
										</p>
									</div>
									<h4 class="post_title">
										<a href="菜谱—笔记详情页.html">因为烤鸭没熟，所以要烤</a>
									</h4>
									<footer class="blog_footer">
										<a href="菜谱—笔记详情页.html">查看详细</a>
									</footer>
								</figcaption>
							</figure>
						</article>
					</div>
					<div class="col-lg-3">
						<article class="single_blog">
							<figure>
								<div class="blog_thumb">
									<a href="菜谱—笔记详情页.html"><img
										src="assets/img/blog/blog3.jpg" alt=""></a>
								</div>
								<figcaption class="blog_content">
									<div class="articles_date">
										<p>
											18/01/2019 | <a href="#">烤鸭为什么要烤？</a>
										</p>
									</div>
									<h4 class="post_title">
										<a href="菜谱—笔记详情页.html">因为烤鸭没熟，所以要烤</a>
									</h4>
									<footer class="blog_footer">
										<a href="菜谱—笔记详情页.html">查看详细</a>
									</footer>
								</figcaption>
							</figure>
						</article>
					</div>
					<div class="col-lg-3">
						<article class="single_blog">
							<figure>
								<div class="blog_thumb">
									<a href="菜谱—笔记详情页.html"><img
										src="assets/img/blog/blog2.jpg" alt=""></a>
								</div>
								<figcaption class="blog_content">
									<div class="articles_date">
										<p>
											18/01/2019 | <a href="#">烤鸭为什么要烤？</a>
										</p>
									</div>
									<h4 class="post_title">
										<a href="菜谱—笔记详情页.html">因为烤鸭没熟，所以要烤</a>
									</h4>
									<footer class="blog_footer">
										<a href="菜谱—笔记详情页.html">查看详细</a>
									</footer>
								</figcaption>
							</figure>
						</article>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--blog area end-->

	<!--custom product area start-->
	<div class="custom_product_area">
		<div class="container">
			<div class="row">
				<div class="col-12" id="part5">
					<div class="section_title">
						<p>生活不过材米油盐</p>
						<h2>粮油调味</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div
						class="small_product_area product_carousel product_column3 owl-carousel">
						<div class="product_items">
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product1.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product2.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Aliquam Consequat</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$26.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product3.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product4.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Donec Non Est</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$46.00</span> <span
												class="old_price">$382.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product5.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product6.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Mauris Vel Tellus</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$56.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="product_items">
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product7.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product8.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Quisque In Arcu</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$20.00</span> <span
												class="old_price">$352.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product9.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product10.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Cas Meque Metus</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$72.00</span> <span
												class="old_price">$352.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product11.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product12.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Proin Lectus Ipsum</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$36.00</span> <span
												class="old_price">$282.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="product_items">
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product13.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product1.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Mauris Vel Tellus</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$45.00</span> <span
												class="old_price">$162.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product10.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product3.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Donec Non Est</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$46.00</span> <span
												class="old_price">$382.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product8.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product5.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Donec Non Est</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$46.00</span> <span
												class="old_price">$382.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="product_items">
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product1.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product2.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Aliquam Consequat</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$26.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product11.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product10.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Donec Non Est</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$46.00</span> <span
												class="old_price">$382.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
							<article class="single_product">
								<figure>
									<div class="product_thumb">
										<a class="primary_img" href="product-details.html"><img
											src="assets/img/product/product9.jpg" alt=""></a> <a
											class="secondary_img" href="product-details.html"><img
											src="assets/img/product/product8.jpg" alt=""></a>
									</div>
									<figcaption class="product_content">
										<h4 class="product_name">
											<a href="product-details.html">Mauris Vel Tellus</a>
										</h4>
										<p>
											<a href="#">Fruits</a>
										</p>
										<div class="action_links">
											<ul>
												<li class="add_to_cart"><a href="购物车.html"
													title="Add to cart"><span class="lnr lnr-cart"></span></a>
												</li>
												<li class="quick_button"><a href="#"
													data-toggle="modal" data-target="#modal_box"
													title="quick view"> <span class="lnr lnr-magnifier"></span></a>
												</li>
												<li class="wishlist"><a href="收藏页面.html"
													title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
												</li>
												<li class="compare"><a href="#" title="Add to Compare"><span
														class="lnr lnr-sync"></span></a></li>
											</ul>
										</div>
										<div class="price_box">
											<span class="current_price">$56.00</span> <span
												class="old_price">$362.00</span>
										</div>
									</figcaption>
								</figure>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--custom product area end-->

	<!--brand area start-->
	<div class="brand_area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="brand_container owl-carousel ">
						<!--<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand1.jpg" alt=""></a>
							</div>
							<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand2.jpg" alt=""></a>
							</div>
							<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand3.jpg" alt=""></a>
							</div>
							<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand4.jpg" alt=""></a>
							</div>
							<div class="single_brand">
								<a href="#"><img src="assets/img/brand/brand2.jpg" alt=""></a>
							</div>-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--brand area end-->

	<!--footer area start-->
	<footer class="footer_widgets">
		<div class="footer_top">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-7">
						<div class="widgets_container contact_us">
							<div class="footer_logo">
								<a href="index.html"><img src="assets/img/logo/logo.png"
									alt=""></a>
							</div>
							<p class="footer_desc">服务、质量、效率</p>
							<p>
								<span>Address:</span> xmzretc
							</p>
							<p>
								<span>Email:</span> <a href="#">760008559@qq.com</a>
							</p>
							<p>
								<span>Call us:</span> <a href="tel:(08)23456789">(08) 23 456
									789</a>
							</p>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关于叮咚到家</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">了解叮咚</a></li>
									<li><a href="#">加入叮咚</a></li>
									<li><a href="#">投资者关系</a></li>
									<li><a href="#">团队信息</a></li>
									<li><a href="contact.html">联系我们</a></li>
									<li><a href="#">廉洁举报</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>关注我们</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">新浪微博</a></li>
									<li><a href="#">官方微信</a></li>
									<li><a href="#">Q群加入</a></li>
									<li><a href="#">公益基金会</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-5">
						<div class="widgets_container widget_menu">
							<h3>特色服务</h3>
							<div class="footer_menu">

								<ul>
									<li><a href="about.html">兑换券码领取</a></li>
									<li><a href="#">礼物码</a></li>
									<li><a href="#">防伪查询</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-4">
						<div class="widgets_container widget_menu">
							<h3>帮助中心</h3>
							<div class="footer_menu">
								<ul>
									<li><a href="#">账户管理</a></li>
									<li><a href="#">购物指南</a></li>
									<li><a href="#">订单操作</a></li>
									<li><a href="#">售后政策</a></li>
									<li><a href="#">自助服务</a></li>
									<li><a href="#">相关下载</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer_bottom">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-md-7">
						<div class="copyright_area">
							<!--<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a>-->
							<p>Copyright &copy; 2020.闽ICP证110xxx号 闽ICP备10046xxx号
								闽公网安备11010802020 号</p>
						</div>
					</div>
					<div class="col-lg-6 col-md-5">
						<div class="footer_payment">
							<ul>
								<li><a href="#"><img src="assets/img/icon/v-logo-1.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-2.png"
										alt=""></a></li>
								<li><a href="#"><img src="assets/img/icon/v-logo-3.png"
										alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--footer area end-->

	<!-- JS
============================================ -->
	<!--jquery min js-->
	<script src="${basepath}/front/assets/js/vendor/jquery-3.4.1.min.js"></script>
	<!--popper min js-->
	<script src="${basepath}/front/assets/js/popper.js"></script>
	<!--bootstrap min js-->
	<script src="${basepath}/front/assets/js/bootstrap.min.js"></script>
	<!--owl carousel min js-->
	<script src="${basepath}/front/assets/js/owl.carousel.min.js"></script>
	<!--slick min js-->
	<script src="${basepath}/front/assets/js/slick.min.js"></script>
	<!--magnific popup min js-->
	<script src="${basepath}/front/assets/js/jquery.magnific-popup.min.js"></script>
	<!--counterup min js-->
	<script src="${basepath}/front/assets/js/jquery.counterup.min.js"></script>
	<!--jquery countdown min js-->
	<script src="${basepath}/front/assets/js/jquery.countdown.js"></script>
	<!--jquery ui min js-->
	<script src="${basepath}/front/assets/js/jquery.ui.js"></script>
	<!--jquery elevatezoom min js-->
	<script src="${basepath}/front/assets/js/jquery.elevatezoom.js"></script>
	<!--isotope packaged min js-->
	<script src="${basepath}/front/assets/js/isotope.pkgd.min.js"></script>
	<!--slinky menu js-->
	<script src="${basepath}/front/assets/js/slinky.menu.js"></script>
	<!-- Plugins JS -->
	<script src="${basepath}/front/assets/js/plugins.js"></script>

	<!-- Main JS -->
	<script src="${basepath}/front/assets/js/main.js"></script>
	<!--layUI的js-->
	<script src="${basepath}/front/layui/layui.js" charset="utf-8"></script>
	<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
	<script>
		layui.use('element', function() {
			var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

			//监听导航点击
			element.on('nav(demo)', function(elem) {
				//console.log(elem)
				layer.msg(elem.text());
			});
		});
	</script>
	<script>
		//传递接收的p_id值；param是el表达式中的一个对象
		let p_id = "${param.p_id}";
		//ajax请求-根据id查询product信息
		$.ajax({
			url : '${basepath}/productshopdetail.do?op=querybyid',
			type : 'get',
			data : {
				'ptf_id' : "ptf01"
			},
			success : function(res) {
				console.log(res);
				//zoom1因为是模板自带，不改成p_photo为好
				//$(".zoom1").attr("src", "${basepath}/picture/" + res.p_photo);
				//$(".zoom2").attr("src", "${basepath}/picture/" + res.p_photo);
				$(".p_name").html(res.p_name);
				$(".p_price").html(res.p_price);
				$("#p_price").html(res.p_price);
				$(".p_detaill").html(res.p_detaill);
				//以下主要显示在商品详情
				$(".p_weight").html(res.p_weight + "克");
				$(".p_origin").html(res.p_origin);
				$(".p_save").html(res.p_save);
				$(".p_guarantee").html(res.p_guarantee + "天");
			}
		});
	</script>

</body>

</html>