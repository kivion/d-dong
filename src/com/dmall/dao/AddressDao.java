package com.dmall.dao;

import java.util.List;
import com.dmall.entity.Address;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;

/**
 * 地址dao
 * @author yong
 *
 */
public class AddressDao extends DBUtil {

	/**
	 * 新增地址方法
	 * 
	 * @param address 地址对象
	 * @return true 成功 false 失败
	 */
	public boolean addAddress(Address address) {
		// 判断address是否为空
		if (address == null) { 
			return false;
		}
		// 定义sql语句
		String sql = "INSERT INTO `address` (`a_id`,`c_name`,`c_tell`,`a_name`) VALUES (?,?,?,?)";
		// 调用通用类代码
		boolean flag = exeUpdate(sql, address.getA_id(), address.getC_name(),address.getC_tell(),address.getA_name());
		return flag;
	}

	/**
	 * 根据地址id删除地址方法
	 * 
	 * @param a_sn 地址流水号
	 * @return true 成功 false 失败
	 */
	public boolean delAddressById(int a_sn) {
		// 定义sql语句
		String sql = "delete FROM `address` WHERE (`a_sn`=?)";
		// 调用DBUtil方法
		boolean flag = exeUpdate(sql, a_sn);
		return flag;
	}

	/**
	 * 地址分页+根据地址名字模糊查询
	 * 
	 * @param keywords 关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<Address> getAddressByPage(String keywords, int pageNo, int pageSize) {
		// 声明sql语句
		String sql = "select * from `address` where a_name like ? or c_name like ? ";
		// 调用DBUtil方法
		PageData<Address> pd = exeQueryByPage(sql, Address.class, pageNo, pageSize, "%" + keywords + "%","%" + keywords + "%");

		return pd;
	}
	
	/**
	 * 根据地址id修改地址内容
	 * @param address 地址对象
	 * @return true 成功 false 失败
	 */
	public boolean updAddressById(Address address) {
		//定义sql语句
		String sql = "UPDATE `address` SET `c_name`=?, `c_tell`=?, `a_name`=? WHERE (`a_id`=?)";
		//调用DBUtil方法
		boolean flag = exeUpdate(sql,address.getC_name(),address.getC_tell(),address.getA_name(),address.getA_id());
		return flag;
	}
	
	/**
	 * 根据地址或者姓名关键字检索
	 * @param keywords 地址关键字
	 * @return List集合
	 */
	public List<Address> getAddressByName(String keywords) {
		// 声明sql语句
		String sql = "SELECT * from address where a_name like ? or c_name like ?";
		List<Address> list = exeQuery(sql, Address.class, "%" + keywords + "%","%" + keywords + "%");

		return list;
	}
}
