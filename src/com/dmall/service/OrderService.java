package com.dmall.service;

import java.util.List;

import com.dmall.dao.OrderDao;
import com.dmall.entity.Order;
import com.dmall.pagedata.PageData;
/**
 * 订单service
 * @author yong
 *
 */
public class OrderService {

	// 调用dao
	OrderDao od = new OrderDao();

	/**
	 * 订单查询操作
	 * 
	 * @param keywords 订单编号关键字
	 * @return
	 */
	public List<Order> getOrder(String keywords) {
		return od.getOrderByName(keywords);
	}

	/**
	 * 订单未支付增加操作
	 * 
	 * @param order 订单对象
	 * @return true 成功 false 失败
	 */
	public boolean addOrder(Order order) {
		return od.addOrder(order);
	}
	
	/**
	 * 支付订单（全字段）增加
	 * @param order
	 * @return
	 */
	public boolean addPayOrder(Order order) {
		return od.addPayOrder(order);
	}

	/**
	 * 订单编号删除订单操作
	 * 
	 * @param o_id 订单编号
	 * @return true 成功 false 失败
	 */
	public boolean delOrder(String o_id) {
		return od.delOrderById(o_id);
	}
	
	/**
	 * 订单修改操作
	 * @param order
	 * @return
	 */
	public boolean updOrder(Order order) {
		return od.updOrderById(order);
	}
	
	/**
	 * 订单操作业务层
	 * @param keywords 订单编号关键字
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public PageData<Order> getOrderByPage(String keywords,String begindate,String enddate, int pageNo, int pageSize) {
		// 判断页码和页面记录数
		if (pageNo < 1) {
			pageNo = 1;
		}
		if (pageSize < 0) {
			pageNo = 10;
		}

		return od.getOrderByPage(keywords,begindate,enddate, pageNo, pageSize);
	}
}
