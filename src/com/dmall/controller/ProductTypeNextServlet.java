package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dmall.dao.ProductTypeNextDao;
import com.dmall.entity.Menue;
import com.dmall.entity.Product;
import com.dmall.entity.ProductTypeNext;
import com.dmall.pagedata.PageData;
import com.dmall.service.MenuService;
import com.dmall.service.ProductService;
import com.dmall.service.ProductTypeNextService;
import com.dmall.util.AjaxResponse;
import com.dmall.util.UploadResponse;
import com.google.gson.Gson;


@WebServlet("/type.do")
public class ProductTypeNextServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ProductTypeNextService ptns = new ProductTypeNextService();
    public ProductTypeNextServlet() {
    	
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		String op = "";
		if(request.getParameter("op")!=null) {
			op = request.getParameter("op");
		}
		if("add".equals(op)) {
			doAdd(request, response);
		}else {
			doQuery(request, response);
		}
	}
	protected void doQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String keywords = "";
		if (request.getParameter("keywords") != null) {
			keywords = request.getParameter("keywords");
		}
		System.out.println("keywords:" + keywords);

		// keywords传递的查询的关键字
		List<ProductTypeNext> list = ptns.getProductTypeNext(keywords);
		
		AjaxResponse<ProductTypeNext> ar = new AjaxResponse<ProductTypeNext>(0, "success", list);

		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);
		
		out.print(str);
		out.close();
	}
	protected void doAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		//商品类型添加

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
