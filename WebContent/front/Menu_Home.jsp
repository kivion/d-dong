<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
        	   <!-- 导入了一些必要的包 -->
<%@page import="com.dmall.service.MenuService"%>
<%@page import="com.dmall.entity.Menue"%>
<%@page import="java.util.List"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
    <!-- taglib -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
    
<!doctype html>
<html class="no-js" lang="en">

	<head>
	<c:set var="basepath" scope="page"
	value="${pageContext.request.contextPath}"></c:set>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>菜谱主页</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

		<!-- CSS 
    ========================= -->
		<!--bootstrap min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/bootstrap.min.css">
		<!--owl carousel min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/owl.carousel.min.css">
		<!--slick min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/slick.css">
		<!--magnific popup min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/magnific-popup.css">
		<!--font awesome css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/font.awesome.css">
		<!--ionicons css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/ionicons.min.css">
		<!--linearicons css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/linearicons.css">
		<!--animate css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/animate.css">
		<!--jquery ui min css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/jquery-ui.min.css">
		<!--slinky menu css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/slinky.menu.css">
		<!--plugins css-->
		<link rel="stylesheet" href="${basepath}/front/assets/css/plugins.css">

		<!-- Main Style CSS -->
		<link rel="stylesheet" href="${basepath}/front/assets/css/style.css">
		<!--layUI的css-->
		<link rel="stylesheet" href="${basepath}/front/layui/css/layui.css" media="all">

		<!--modernizr min js here-->
		<script src="${basepath}/front/assets/js/vendor/modernizr-3.7.1.min.js"></script>
	</head>

	<body>

		<header>
			<div class="main_header">
				<div class="header_top">
					<div class="container">
						<div class="row align-items-center">

							<!--导航栏-->
							<div class="col-lg-6 col-md-6">
								<div class="language_currency">
									<ul>
										<li class="language">
											<a href="#"> 语言 <i class="icon-right ion-ios-arrow-down"></i></a>
											<ul class="dropdown_language">
												<li>
													<a href="#">中文</a>
												</li>
												<li>
													<a href="#">英语</a>
												</li>
											</ul>
										</li>
										<li class="currency">
											<a href="#"> 简繁体 <i class="icon-right ion-ios-arrow-down"></i></a>
											<ul class="dropdown_currency">
												<li>
													<a href="#">简体</a>
												</li>
												<li>
													<a href="#">繁体</a>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="header_social text-right">
									<ul>
										<li>
											<a href="#"><i class="ion-social-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i class="ion-social-googleplus-outline"></i></a>
										</li>
										<li>
											<a href="#"><i class="ion-social-youtube-outline"></i></a>
										</li>
										<li>
											<a href="#"><i class="ion-social-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i class="ion-social-instagram-outline"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_middle header_middle2">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-lg-4">
								<div class="search_container">
									<form action="${pageContext.request.contextPath}/menuhome.do?op=homesearch"  method="post">
										<div class="search_box">
											<input placeholder="食材、菜谱  (๑´•﹃•`๑) " type="text" name="keywords">
											<button type="submit"><span class="lnr lnr-magnifier"></span></button>
										<input type="hidden" name="op" value="homesearch">
										</div>
									</form>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="logo">
									<a href="index.html"><img src="assets/img/logo/logo2.png" alt=""></a>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="header_account_area">
									<div class="header_account_list register">
										<ul class="layui-nav">
											<!--layUI的状态头像登录-->
											<li class="layui-nav-item" lay-unselect="">
												<a href="my-account.html" ><img src="assets/img/icon/1.jpg" class="layui-nav-img">佚名</a>
												<dl class="layui-nav-child">
													<dd>
														<a href="javascript:;">修改信息</a>
													</dd>
													<dd>
														<a href="javascript:;">账号管理</a>
													</dd>
													<dd>
														<a href="javascript:;">退出</a>
													</dd>
												</dl>
											</li>
											<!--<li>
													<a href="login.html">注册</a>
												</li>
												<li><span>/</span></li>
												<li>
													<a href="login.html">登录</a>
												</li>-->
										</ul>
									</div>
									<style type="text/css">
										.header_account_area ul {
											background-color: #80B82D;
										}
										
										.header_account_list.register ul li a .header_account_area ul a {
											color: #000000;
										}
										
										.header_middle2 .header_account_list.register ul li a {
											color: #000 !important;
										}
										
										.header_middle2 .header_account_list.register ul li a:hover {
											color: #222222;
										}
										
										.layui-nav .layui-nav-itemed>a .layui-nav-more {
											border-color: #fff !important;
										}
									</style>
									<div class="header_account_list header_wishlist">
										<a href="收藏页面.html"><span class="lnr lnr-heart"></span> <span class="item_count">5</span> </a>
									</div>
									<div class="header_account_list  mini_cart_wrapper">
										<a href="购物车.html"><span class="lnr lnr-cart"></span><span class="item_count">2</span></a>
										<!--mini cart-->
										<div class="mini_cart">
											<div class="cart_gallery">
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img src="assets/img/s-product/product.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Primis In Faucibus</a>
														<p>1 x <span> $65.00 </span></p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
												<div class="cart_item">
													<div class="cart_img">
														<a href="#"><img src="assets/img/s-product/product2.jpg" alt=""></a>
													</div>
													<div class="cart_info">
														<a href="#">Letraset Sheets</a>
														<p>1 x <span> $60.00 </span></p>
													</div>
													<div class="cart_remove">
														<a href="#"><i class="icon-x"></i></a>
													</div>
												</div>
											</div>
											<div class="mini_cart_table">
												<div class="cart_table_border">
													<div class="cart_total mt-10">
														<span>总计:</span>
														<span class="price">$125.00</span>
													</div>
												</div>
											</div>
											<div class="mini_cart_footer">
												<div class="cart_button">
													<a href="购物车.html"><i class="fa fa-shopping-cart"></i> 前往购物车</a>
												</div>
												<div class="cart_button">
													<a href="#"><i class="fa fa-sign-in"></i> ...</a>
												</div>

											</div>
										</div>
										<!--mini cart end-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_bottom sticky-header">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-lg-8 offset-lg-2">
								<!--main menu start-->
								<div class="main_menu  menu_two color_two menu_position">
									<nav>
										<ul>
											<li>
												<a href="index.jsp">主页</a>
											</li>
											<li>
												<a href="Menu_Home.jsp">菜谱主页</a>
											</li>
											<li>
												<a href="Meun_collection.jsp">收藏</a>
											</li>
											<li>
												<a href="Menu_release.jsp">发布</a>
											</li>
											<li>
												<a href="Menu_PersonalAll.jsp">个人笔记</a>
											</li>
										</ul>
									</nav>
								</div>
								<!--main menu end-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!--header area end-->

		<!--slider area start-->
		<section class="slider_section color_two mb-70">
			<div class="slider_area owl-carousel">
				<div class="single_slider d-flex align-items-center" data-bgimg="assets/img/slider/slider4.jpg">
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<div class="slider_content">
									<h1>唯有美食不可辜负</h1>
									<h3>吃货的世界没有诗和远方 & o(´^｀)o </h3>
									<p>
										没有什么是一顿大餐不能解决的，如果有，那就来两顿
									</p>
									<a href="../menuhome.do?op=homesearch">─━ _ ─━✧</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="single_slider d-flex align-items-center" data-bgimg="assets/img/slider/slider5.jpg">
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<div class="slider_content">
									<h1>洋葱 </h1>
									<h3>沙拉、汉堡、三明治里、罗宋汤...</h3>
									<h3>(￣～￣)嚼！</h3>
									<p>
										洋葱含有前列腺素A，能降低外周血管阻力，降低血黏度，可用于降低血压、提神醒脑、缓解压力、预防感冒。 此外，洋葱还能清除体内氧自由基，增强新陈代谢能力，抗衰老，预防骨质疏松，是适合中老年人的保健食物。
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="single_slider d-flex align-items-center" data-bgimg="assets/img/slider/slider6.jpg">
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<div class="slider_content">
									<h1>绿色蔬菜</h1>
									<h3>安全、优质、营养 & o(￣▽￣)ｄ good</h3>
									<p>
										绿色蔬菜中.十字花科植物、根茎类蔬菜、海藻类植物有一定的抗癌效果。
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--slider area end-->

		<!--banner area start-->
		<!--第二部分开始 -->
		<div class="banner_area banner_gallery2">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4">
						<div class="single_banner">
							<div class="banner_thumb">
								<a href="#part1"><img src="assets/img/bg/banner5.jpg" alt=""></a>
							</div>
						</div>
					</div>
					<div class="col-lg-5 col-md-5">
						<div class="single_banner">
							<div class="banner_thumb">
								<a href="#part2"><img src="assets/img/bg/banner6.jpg" alt=""></a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="banner2_sidebar">
							<div class="banner_thumb mb-30">
								<a href="#part3"><img src="assets/img/bg/banner7.jpg" alt=""></a>
							</div>
							<div class="banner_thumb">
								<a href="#part4"><img src="assets/img/bg/banner8.jpg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--banner area end-->

		<!--product area start-->
		<div class="product_area color_two  mb-60">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="product_header">
							<div class="section_title">
								<h2>家常菜小能手</h2>
							</div>
							<div class="product_tab_btn">
								<ul class="nav" role="tablist">
									<li>
										<a class="active" data-toggle="tab" href="#plant1" role="tab" aria-controls="plant1" aria-selected="true">
											清淡
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#plant2" role="tab" aria-controls="plant2" aria-selected="false">
											糖醋
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#plant3" role="tab" aria-controls="plant3" aria-selected="false">
											香辣
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="product_container">
					<div class="row">
						<div class="col-12">
							<div class="tab-content">
								<div class="tab-pane fade show active" id="plant1" role="tabpanel">
									<div class="product_carousel product_column5 owl-carousel">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-1.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-2_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="../menuhome.do?op=addmenucoll&menuid=m00021&cid=c000000001" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="../menuhome.do?op=menudetail&menuid=m00021">清淡鲜美~三鲜汤</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-3_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-4_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="# " title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">美味也能很简单~万能素菜汁</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-5_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-6_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="# " title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">蒸蛋有技巧~嫩滑水蒸蛋</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-7_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-8_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="# " title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">餐桌上的经典~番茄炒蛋</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-9_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-10_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="# " title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">柔滑自然鲜~肉末豆腐羹</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-11_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-12_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">美味不等待！超快热汤面</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
									</div>
								</div>
								<div class="tab-pane fade" id="plant2" role="tabpanel">
									<div class="product_carousel product_column5 owl-carousel">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-13_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-14_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">酸甜酥脆~糖醋鸡胸肉</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-15_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-16_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">比肉还好吃~糖醋“排骨”</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-17_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-18_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">酸甜香糯停不了口~糖醋土豆</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-19_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-20_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">韩综出镜率超高！韩式糖醋肉</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-21_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-22_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">必须刷爆朋友圈！吮指糖醋开背虾</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-23_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-24_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">酸甜脆！好开胃~糖渍樱桃萝卜</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
									</div>
								</div>
								<div class="tab-pane fade" id="plant3" role="tabpanel">
									<div class="product_carousel product_column5 owl-carousel">
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-25_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-26_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">又香又辣特别嫩~香辣小牛柳</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-27_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-28_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">香而不辣~蒜香辣子鸡</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-29_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-30_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">堪比辣条~香辣豆皮</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-31_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-32_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">湖北风味~香辣卤藕</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-33_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-34_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">红油满满够过瘾~香辣米线</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
										<article class="single_product">
											<figure>
												<div class="product_thumb">
													<a class="primary_img"><img src="assets/img/menu-home/first-products/1-35_副本.jpg" alt=""></a>
													<a class="secondary_img"><img src="assets/img/menu-home/first-products/1-36_副本.jpg" alt=""></a>

													<div class="action_links">
														<ul>
															<li class="wishlist">
																<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
															</li>
														</ul>
													</div>
												</div>
												<figcaption class="product_content">
													<h4 class="product_name"><a href="菜谱—笔记详情页.html">不用等位在家吃~香辣烤鱼</a></h4>
													<p>作者：摩托姐姐</p><br>
													<p>时间：约20-30分钟</p><br>
													<p>难度：零厨艺</p>
												</figcaption>
											</figure>
										</article>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--product area end-->

		<!--product area start-->
		<div class="product_area color_two mb-65">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section_title" id="part4">
							<h2>来自海洋的馈赠</h2>
							<p>温馨提示：疫情期间还是少吃海鲜为好 ᕦ(･ㅂ･)ᕤ</p>
						</div>
					</div>
				</div>
				<div class="product_container">
					<div class="row">
						<div class="col-12">
							<div class="product_carousel product_column5 owl-carousel">
								<article class="single_product">
									<figure>
										<div class="product_thumb">
											<a class="primary_img"><img src="assets/img/menu-home/tow-products/21_副本.jpg" alt=""></a>
											<a class="secondary_img"><img src="assets/img/menu-home/tow-products/22_副本.jpg" alt=""></a>

											<div class="action_links">
												<ul>
													<li class="wishlist">
														<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
													</li>
												</ul>
											</div>
										</div>
										<figcaption class="product_content">
											<h4 class="product_name"><a href="菜谱—笔记详情页.html">空口吃掉一大盘！金沙鱿鱼圈</a></h4>
											<p>作者：耶啵</p><br>
											<p>时间：约20-30分钟</p><br>
											<p>难度：零厨艺</p>
										</figcaption>
									</figure>
								</article>
								<article class="single_product">
									<figure>
										<div class="product_thumb">
											<a class="primary_img"><img src="assets/img/menu-home/tow-products/23_副本.jpg" alt=""></a>
											<a class="secondary_img"><img src="assets/img/menu-home/tow-products/24_副本.jpg" alt=""></a>

											<div class="action_links">
												<ul>
													<li class="wishlist">
														<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
													</li>
												</ul>
											</div>
										</div>
										<figcaption class="product_content">
											<h4 class="product_name"><a href="菜谱—笔记详情页.html">茶香盐焗虾</a></h4>
											<p>作者：耶啵</p><br>
											<p>时间：约20-30分钟</p><br>
											<p>难度：零厨艺</p>
										</figcaption>
									</figure>
								</article>
								<article class="single_product">
									<figure>
										<div class="product_thumb">
											<a class="primary_img"><img src="assets/img/menu-home/tow-products/25_副本.jpg" alt=""></a>
											<a class="secondary_img"><img src="assets/img/menu-home/tow-products/26_副本.jpg" alt=""></a>

											<div class="action_links">
												<ul>
													<li class="wishlist">
														<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
													</li>
												</ul>
											</div>
										</div>
										<figcaption class="product_content">
											<h4 class="product_name"><a href="菜谱—笔记详情页.html">平底锅就可以做！盐烤海鲜</a></h4>
											<p>作者：耶啵</p><br>
											<p>时间：约20-30分钟</p><br>
											<p>难度：零厨艺</p>
										</figcaption>
									</figure>
								</article>
								<article class="single_product">
									<figure>
										<div class="product_thumb">
											<a class="primary_img"><img src="assets/img/menu-home/tow-products/27_副本.png" alt=""></a>
											<a class="secondary_img"><img src="assets/img/menu-home/tow-products/28_副本.jpg" alt=""></a>

											<div class="action_links">
												<ul>
													<li class="wishlist">
														<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
													</li>
												</ul>
											</div>
										</div>
										<figcaption class="product_content">
											<h4 class="product_name"><a href="菜谱—笔记详情页.html">香辣炒花甲</a></h4>
											<p>作者：耶啵</p><br>
											<p>时间：约20-30分钟</p><br>
											<p>难度：零厨艺</p>
										</figcaption>
									</figure>
								</article>
								<article class="single_product">
									<figure>
										<div class="product_thumb">
											<a class="primary_img"><img src="assets/img/menu-home/tow-products/29_副本.jpg" alt=""></a>
											<a class="secondary_img"><img src="assets/img/menu-home/tow-products/210_副本.jpg" alt=""></a>

											<div class="action_links">
												<ul>
													<li class="wishlist">
														<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
													</li>
												</ul>
											</div>
										</div>
										<figcaption class="product_content">
											<h4 class="product_name"><a href="菜谱—笔记详情页.html">下酒又解馋~酱烧田螺</a></h4>
											<p>作者：耶啵</p><br>
											<p>时间：约20-30分钟</p><br>
											<p>难度：零厨艺</p>
										</figcaption>
									</figure>
								</article>
								<article class="single_product">
									<figure>
										<div class="product_thumb">
											<a class="primary_img"><img src="assets/img/menu-home/tow-products/211_副本.jpg" alt=""></a>
											<a class="secondary_img"><img src="assets/img/menu-home/tow-products/212_副本.jpg" alt=""></a>

											<div class="action_links">
												<ul>
													<li class="wishlist">
														<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
													</li>
												</ul>
											</div>
										</div>
										<figcaption class="product_content">
											<h4 class="product_name"><a href="菜谱—笔记详情页.html">海鲜日本豆腐烧</a></h4>
											<p>作者：耶啵</p><br>
											<p>时间：约20-30分钟</p><br>
											<p>难度：零厨艺</p>
										</figcaption>
									</figure>
								</article>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--product area end-->

		<!--banner fullwidth area satrt-->
		<div class="banner_fullwidth color_two">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="banner_full_content">
							<p>厨房常见调味料</p>
							<h2>大蒜 <span>不仅可作调味料，而且可入药，是著名的食药两用植物</span></h2>
							<a href="../menuhome.do?op=menudetail&menuid=m00022">了解更多</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--banner fullwidth area end-->

		<!--product banner area satrt-->
		<div class="product_banner_area color_two mb-65">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section_title" id="part2">
							<p>新手不翻车 </p>
							<h2>绿色蔬菜，健康生活</h2>
						</div>
					</div>
				</div>
				<div class="product_banner_container">
					<div class="row">
						<div class="col-lg-4 col-md-5">
							<div class="banner_thumb">
								<a href="菜谱—所有笔记浏览.html"><img src="assets/img/bg/banner4.jpg" alt=""></a>
							</div>
						</div>
						<div class="col-lg-8 col-md-7">
							<div class="small_product_area product_column2 owl-carousel">
								<div class="product_items">
									<article class="single_product">
										<figure>
											<div class="product_thumb">
												<a class="primary_img"><img src="assets/img/menu-home/three-products/31_副本.png" alt=""></a>
												<a class="secondary_img"><img src="assets/img/menu-home/three-products/32_副本.jpg" alt=""></a>
											</div>
											<figcaption class="product_content">
												<h4 class="product_name"><a href="菜谱—笔记详情页.html">轻食也诱人~橄榄油蔬菜沙拉</a></h4>
												<p>
													<a href="#">作者：小飞侠</a>
												</p>
												<div class="action_links">
													<ul>
														<li class="wishlist">
															<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
														</li>
													</ul>
												</div>
												<div class="price_box">
													<span>时间：约20-30分钟</span><br>
													<span>难度：零厨艺</span>
												</div>
											</figcaption>
										</figure>
									</article>
									<article class="single_product">
										<figure>
											<div class="product_thumb">
												<a class="primary_img"><img src="assets/img/menu-home/three-products/33_副本.jpg" alt=""></a>
												<a class="secondary_img"><img src="assets/img/menu-home/three-products/34_副本.jpg" alt=""></a>
											</div>
											<figcaption class="product_content">
												<h4 class="product_name"><a href="菜谱—笔记详情页.html">营养好搭配~西蓝花虾仁土豆泥沙拉</a></h4>
												<p>
													<a href="#">作者：小飞侠</a>
												</p>
												<div class="action_links">
													<ul>
														<li class="wishlist">
															<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
														</li>
													</ul>
												</div>
												<div class="price_box">
													<span>时间：约20-30分钟</span><br>
													<span>难度：零厨艺</span>
												</div>
											</figcaption>
										</figure>
									</article>
									<article class="single_product">
										<figure>
											<div class="product_thumb">
												<a class="primary_img"><img src="assets/img/menu-home/three-products/35_副本.jpg" alt=""></a>
												<a class="secondary_img"><img src="assets/img/menu-home/three-products/36_副本.jpg" alt=""></a>
											</div>
											<figcaption class="product_content">
												<h4 class="product_name"><a href="菜谱—笔记详情页.html">新手也能做~芝士土豆泥</a></h4>
												<p>
													<a href="#">作者：小飞侠</a>
												</p>
												<div class="action_links">
													<ul>
														<li class="wishlist">
															<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
														</li>
													</ul>
												</div>
												<div class="price_box">
													<span>时间：约20-30分钟</span><br>
													<span>难度：零厨艺</span>
												</div>
											</figcaption>
										</figure>
									</article>
								</div>
								<div class="product_items">
									<article class="single_product">
										<figure>
											<div class="product_thumb">
												<a class="primary_img"><img src="assets/img/menu-home/three-products/37_副本.jpg" alt=""></a>
												<a class="secondary_img"><img src="assets/img/menu-home/three-products/38_副本.jpg" alt=""></a>
											</div>
											<figcaption class="product_content">
												<h4 class="product_name"><a href="菜谱—笔记详情页.html">经典好味道！鸡蛋沙拉三明治</a></h4>
												<p>
													<a href="#">作者：小飞侠</a>
												</p>
												<div class="action_links">
													<ul>
														<li class="wishlist">
															<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
														</li>
													</ul>
												</div>
												<div class="price_box">
													<span>时间：约20-30分钟</span><br>
													<span>难度：零厨艺</span>
												</div>
											</figcaption>
										</figure>
									</article>
									<article class="single_product">
										<figure>
											<div class="product_thumb">
												<a class="primary_img"><img src="assets/img/menu-home/three-products/39_副本.jpg" alt=""></a>
												<a class="secondary_img"><img src="assets/img/menu-home/three-products/310_副本.jpg" alt=""></a>
											</div>
											<figcaption class="product_content">
												<h4 class="product_name"><a href="菜谱—笔记详情页.html">健身减脂必备！减脂鸡肉蔬菜沙拉</a></h4>
												<p>
													<a href="#">作者：小飞侠</a>
												</p>
												<div class="action_links">
													<ul>
														<li class="wishlist">
															<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
														</li>
													</ul>
												</div>
												<div class="price_box">
													<span>时间：约20-30分钟</span><br>
													<span>难度：零厨艺</span>
												</div>
											</figcaption>
										</figure>
									</article>
									<article class="single_product">
										<figure>
											<div class="product_thumb">
												<a class="primary_img"><img src="assets/img/menu-home/three-products/311_副本.jpg" alt=""></a>
												<a class="secondary_img"><img src="assets/img/menu-home/three-products/312_副本.jpg" alt=""></a>
											</div>
											<figcaption class="product_content">
												<h4 class="product_name"><a href="菜谱—笔记详情页.html">颜值与美味并存！鸡胸玉米翡翠卷</a></h4>
												<p>
													<a href="#">作者：小飞侠</a>
												</p>
												<div class="action_links">
													<ul>
														<li class="wishlist">
															<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
														</li>
													</ul>
												</div>
												<div class="price_box">
													<span>时间：约20-30分钟</span><br>
													<span>难度：零厨艺</span>
												</div>
											</figcaption>
										</figure>
									</article>
								</div>
								<div class="product_items">
									<article class="single_product">
										<figure>
											<div class="product_thumb">
												<a class="primary_img"><img src="assets/img/menu-home/three-products/313_副本.jpg" alt=""></a>
												<a class="secondary_img"><img src="assets/img/menu-home/three-products/314_副本.jpg" alt=""></a>
											</div>
											<figcaption class="product_content">
												<h4 class="product_name"><a href="菜谱—笔记详情页.html">经典家常~减脂姜汁菠菜</a></h4>
												<p>
													<a href="#">作者：小飞侠</a>
												</p>
												<div class="action_links">
													<ul>
														<li class="wishlist">
															<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
														</li>
													</ul>
												</div>
												<div class="price_box">
													<span>时间：约20-30分钟</span><br>
													<span>难度：零厨艺</span>
												</div>
											</figcaption>
										</figure>
									</article>
									<article class="single_product">
										<figure>
											<div class="product_thumb">
												<a class="primary_img"><img src="assets/img/menu-home/three-products/315_副本.jpg" alt=""></a>
												<a class="secondary_img"><img src="assets/img/menu-home/three-products/316_副本.jpg" alt=""></a>
											</div>
											<figcaption class="product_content">
												<h4 class="product_name"><a href="菜谱—笔记详情页.html">考伯沙拉</a></h4>
												<p>
													<a href="#">作者：小飞侠</a>
												</p>
												<div class="action_links">
													<ul>
														<li class="wishlist">
															<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
														</li>
													</ul>
												</div>
												<div class="price_box">
													<span>时间：约20-30分钟</span><br>
													<span>难度：零厨艺</span>
												</div>
											</figcaption>
										</figure>
									</article>
									<article class="single_product">
										<figure>
											<div class="product_thumb">
												<a class="primary_img"><img src="assets/img/menu-home/three-products/317_副本.jpg" alt=""></a>
												<a class="secondary_img"><img src="assets/img/menu-home/three-products/318_副本.jpg" alt=""></a>
											</div>
											<figcaption class="product_content">
												<h4 class="product_name"><a href="菜谱—笔记详情页.html">KFC菜丝沙拉</a></h4>
												<p>
													<a href="#">作者：小飞侠</a>
												</p>
												<div class="action_links">
													<ul>
														<li class="wishlist">
															<a href="#" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a>
														</li>
													</ul>
												</div>
												<div class="price_box">
													<span>时间：约20-30分钟</span><br>
													<span>难度：零厨艺</span>
												</div>
											</figcaption>
										</figure>
									</article>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--product banner area end-->

		<!--blog area start-->
		<section class="blog_section blog_section2 color_two">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section_title" id="part3">
							<p>甜品至上</p>
							<h2>人生苦短，先吃甜品</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="blog_carousel blog_column3 owl-carousel">
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/1_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">赞赞</a>
											</p>
										</div>
										<h4 class="post_title">可可爱爱~荔枝桃桃酸奶慕斯</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/2_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">赞赞</a>
											</p>
										</div>
										<h4 class="post_title">  超高颜值~抹茶草莓千层</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/3_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">赞赞</a>
											</p>
										</div>
										<h4 class="post_title"> 巧克力慕斯杯</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/4_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">赞赞</a>
											</p>
										</div>
										<h4 class="post_title">  芒果千层</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--blog area end-->

		<!--banner area start-->
		<div class="banner_area">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="single_banner">
							<div class="banner_thumb">
								<a href="shop.html"><img src="assets/img/bg/" alt=""></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--banner area end-->

		<!--blog area start-->
		<section class="blog_section blog_section2 color_two">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section_title" id="part1">
							<p>新年饮品特集</p>
							<h2>奶茶续命指南</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="blog_carousel blog_column3 owl-carousel">
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/5_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">耶啵</a>
											</p>
										</div>
										<h4 class="post_title"> Q弹香浓~珍珠奶茶</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/6_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">耶啵</a>
											</p>
										</div>
										<h4 class="post_title"> 秋冬热饮~棉花糖奶茶</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/7_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">耶啵</a>
											</p>
										</div>
										<h4 class="post_title">  暖心治愈~姜汁撞奶</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/8_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">耶啵</a>
											</p>
										</div>
										<h4 class="post_title"> 简单好喝~焦糖奶茶</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/9_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">耶啵</a>
											</p>
										</div>
										<h4 class="post_title">  10分钟打卡~汤圆版脏脏奶茶</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/10_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">耶啵</a>
											</p>
										</div>
										<h4 class="post_title">  柔软顺滑~豆乳奶茶</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/11_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">耶啵</a>
											</p>
										</div>
										<h4 class="post_title">  超好看~抹茶拿铁</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/12_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">耶啵</a>
											</p>
										</div>
										<h4 class="post_title">  回忆夏天的味道~蜜桃乌龙</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
						<div class="col-lg-3">
							<article class="single_blog">
								<figure>
									<div class="blog_thumb">
										<img src="assets/img/menu-home/four-products/13_副本.jpg" alt=""></a>
									</div>
									<figcaption class="blog_content">
										<div class="articles_date">
											<p>18/01/2019 |
												<a href="#">耶啵</a>
											</p>
										</div>
										<h4 class="post_title">  史上最简~梦幻星空水</a></h4>
										<footer class="blog_footer">
											<a href="菜谱—笔记详情页.html">详情</a>
										</footer>
									</figcaption>
								</figure>
							</article>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--blog area end-->

		<!--footer area start-->
		<footer class="footer_widgets">
			<div class="footer_top">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-12 col-sm-7">
							<div class="widgets_container contact_us">
								<div class="footer_logo">
									<a href="index.html"><img src="assets/img/logo/logo.png" alt=""></a>
								</div>
								<p class="footer_desc">服务、质量、效率</p>
								<p><span>Address:</span> xmzretc </p>
								<p><span>Email:</span>
									<a href="#">760008559@qq.com</a>
								</p>
								<p><span>Call us:</span>
									<a href="tel:(08)23456789">(08) 23 456 789</a>
								</p>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-5">
							<div class="widgets_container widget_menu">
								<h3>关于叮咚到家</h3>
								<div class="footer_menu">

									<ul>
										<li>
											<a href="about.html">了解叮咚</a>
										</li>
										<li>
											<a href="#">加入叮咚</a>
										</li>
										<li>
											<a href="#">投资者关系</a>
										</li>
										<li>
											<a href="#">团队信息</a>
										</li>
										<li>
											<a href="contact.html">联系我们</a>
										</li>
										<li>
											<a href="#">廉洁举报</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-5">
							<div class="widgets_container widget_menu">
								<h3>关注我们</h3>
								<div class="footer_menu">

									<ul>
										<li>
											<a href="about.html">新浪微博</a>
										</li>
										<li>
											<a href="#">官方微信</a>
										</li>
										<li>
											<a href="#">Q群加入</a>
										</li>
										<li>
											<a href="#">公益基金会</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-5">
							<div class="widgets_container widget_menu">
								<h3>特色服务</h3>
								<div class="footer_menu">

									<ul>
										<li>
											<a href="about.html">兑换券码领取</a>
										</li>
										<li>
											<a href="#">礼物码</a>
										</li>
										<li>
											<a href="#">防伪查询</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-4">
							<div class="widgets_container widget_menu">
								<h3>帮助中心</h3>
								<div class="footer_menu">
									<ul>
										<li>
											<a href="#">账户管理</a>
										</li>
										<li>
											<a href="#">购物指南</a>
										</li>
										<li>
											<a href="#">订单操作</a>
										</li>
										<li>
											<a href="#">售后政策</a>
										</li>
										<li>
											<a href="#">自助服务</a>
										</li>
										<li>
											<a href="#">相关下载</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="footer_bottom">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-6 col-md-7">
							<div class="copyright_area">
								<!--<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a>-->
								<p>Copyright &copy; 2020.闽ICP证110xxx号 闽ICP备10046xxx号 闽公网安备11010802020 号
								</p>
							</div>
						</div>
						<div class="col-lg-6 col-md-5">
							<div class="footer_payment">
								<ul>
									<li>
										<a href="#"><img src="assets/img/icon/v-logo-1.png" alt=""></a>
									</li>
									<li>
										<a href="#"><img src="assets/img/icon/v-logo-2.png" alt=""></a>
									</li>
									<li>
										<a href="#"><img src="assets/img/icon/v-logo-3.png" alt=""></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--footer area end-->

		</h>
		<!-- modal area start-->
		<div class="modal fade" id="modal_box" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="icon-x"></i></span>
                </button>
					<div class="modal_body">
						<div class="container">
							<div class="row">
								<div class="col-lg-5 col-md-5 col-sm-12">
									<div class="modal_tab">
										<div class="tab-content product-details-large">
											<div class="tab-pane fade show active" id="tab1" role="tabpanel">
												<div class="modal_tab_img">
													<a href="#"><img src="assets/img/product/productbig1.jpg" alt=""></a>
												</div>
											</div>
											<div class="tab-pane fade" id="tab2" role="tabpanel">
												<div class="modal_tab_img">
													<a href="#"><img src="assets/img/product/productbig2.jpg" alt=""></a>
												</div>
											</div>
											<div class="tab-pane fade" id="tab3" role="tabpanel">
												<div class="modal_tab_img">
													<a href="#"><img src="assets/img/product/productbig3.jpg" alt=""></a>
												</div>
											</div>
											<div class="tab-pane fade" id="tab4" role="tabpanel">
												<div class="modal_tab_img">
													<a href="#"><img src="assets/img/product/productbig4.jpg" alt=""></a>
												</div>
											</div>
										</div>
										<div class="modal_tab_button">
											<ul class="nav product_navactive owl-carousel" role="tablist">
												<li>
													<a class="nav-link active" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false"><img src="assets/img/product/product1.jpg" alt=""></a>
												</li>
												<li>
													<a class="nav-link" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false"><img src="assets/img/product/product6.jpg" alt=""></a>
												</li>
												<li>
													<a class="nav-link button_three" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false"><img src="assets/img/product/product2.jpg" alt=""></a>
												</li>
												<li>
													<a class="nav-link" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false"><img src="assets/img/product/product7.jpg" alt=""></a>
												</li>

											</ul>
										</div>
									</div>
								</div>
								<div class="col-lg-7 col-md-7 col-sm-12">
									<div class="modal_right">
										<div class="modal_title mb-10">
											<h2>Donec Ac Tempus</h2>
										</div>
										<div class="modal_price mb-10">
											<span class="new_price">$64.99</span>
											<span class="old_price">$78.99</span>
										</div>
										<div class="modal_description mb-15">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui nemo ipsum numquam, reiciendis maiores quidem aperiam, rerum vel recusandae </p>
										</div>
										<div class="variants_selects">
											<div class="variants_size">
												<h2>size</h2>
												<select class="select_option">
													<option selected value="1">s</option>
													<option value="1">m</option>
													<option value="1">l</option>
													<option value="1">xl</option>
													<option value="1">xxl</option>
												</select>
											</div>
											<div class="variants_color">
												<h2>color</h2>
												<select class="select_option">
													<option selected value="1">purple</option>
													<option value="1">violet</option>
													<option value="1">black</option>
													<option value="1">pink</option>
													<option value="1">orange</option>
												</select>
											</div>
											<div class="modal_add_to_cart">
												<form action="#">
													<input min="1" max="100" step="2" value="1" type="number">
													<button type="submit">add to cart</button>
												</form>
											</div>
										</div>
										<div class="modal_social">
											<h2>Share this product</h2>
											<ul>
												<li class="facebook">
													<a href="#"><i class="fa fa-facebook"></i></a>
												</li>
												<li class="twitter">
													<a href="#"><i class="fa fa-twitter"></i></a>
												</li>
												<li class="pinterest">
													<a href="#"><i class="fa fa-pinterest"></i></a>
												</li>
												<li class="google-plus">
													<a href="#"><i class="fa fa-google-plus"></i></a>
												</li>
												<li class="linkedin">
													<a href="#"><i class="fa fa-linkedin"></i></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- modal area end-->

		<!-- JS
============================================ -->
		<!--jquery min js-->
		<script src="${basepath}/front/assets/js/vendor/jquery-3.4.1.min.js"></script>
		<!--popper min js-->
		<script src="${basepath}/front/assets/js/popper.js"></script>
		<!--bootstrap min js-->
		<script src="${basepath}/front/assets/js/bootstrap.min.js"></script>
		<!--owl carousel min js-->
		<script src="${basepath}/front/assets/js/owl.carousel.min.js"></script>
		<!--slick min js-->
		<script src="${basepath}/front/assets/js/slick.min.js"></script>
		<!--magnific popup min js-->
		<script src="${basepath}/front/assets/js/jquery.magnific-popup.min.js"></script>
		<!--counterup min js-->
		<script src="${basepath}/front/assets/js/jquery.counterup.min.js"></script>
		<!--jquery countdown min js-->
		<script src="${basepath}/front/assets/js/jquery.countdown.js"></script>
		<!--jquery ui min js-->
		<script src="${basepath}/front/assets/js/jquery.ui.js"></script>
		<!--jquery elevatezoom min js-->
		<script src="${basepath}/front/assets/js/jquery.elevatezoom.js"></script>
		<!--isotope packaged min js-->
		<script src="${basepath}/front/assets/js/isotope.pkgd.min.js"></script>
		<!--slinky menu js-->
		<script src="${basepath}/front/assets/js/slinky.menu.js"></script>
		<!-- Plugins JS -->
		<script src="${basepath}/front/assets/js/plugins.js"></script>

		<!-- Main JS -->
		<script src="${basepath}/front/assets/js/main.js"></script>
		<!--layUI的js-->
		<script src="${basepath}/front/layui/layui.js" charset="utf-8"></script>
		<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
		<script>
			layui.use('element', function() {
				var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

				//监听导航点击
				element.on('nav(demo)', function(elem) {
					//console.log(elem)
					layer.msg(elem.text());
				});
			});
		</script>
	</body>

</html>