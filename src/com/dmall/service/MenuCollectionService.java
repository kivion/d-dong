package com.dmall.service;

import java.util.List;

import com.dmall.dao.MenueCollectionDao;
import com.dmall.entity.Menue;
import com.dmall.entity.MenueCollection;
import com.dmall.pagedata.PageData;

public class MenuCollectionService {
	MenueCollectionDao mcd = new MenueCollectionDao();
	/**
	 *	分页+模糊查询菜谱收藏
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<MenueCollection> getMenuCollection(String keyword,int pageNo,int pageSize){
		
		PageData<MenueCollection> pd = mcd.getMenuCollection(keyword, pageNo, pageSize);		
		return pd;
	}
	/**
	 * 模糊查询
	 * @param keywords 关键字
	 * @return
	 */
    public List<Menue> getMenuCollectionlist(String cid,String keywords){
		return mcd.getMenuByKeywordscid(cid, keywords);
	}
	/**
	 *	添加菜谱收藏
	 * @param Menue
	 * @return Menu 对象
	 */
	public boolean addMenueCollection(MenueCollection menucollection) {
		boolean flag = mcd.addMenueCollection(menucollection);
		return flag;
	}
	/**
	 *	按菜谱id删除菜谱收藏
	 * @param m_id
	 * @return flag  true操作成功，false操作失败
	 */
	public boolean delMenuCollectionById(int m_id) {
		boolean flag = mcd.delMenuCollectionById(m_id);
		return flag;
	}
}
