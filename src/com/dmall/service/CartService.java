package com.dmall.service;

import java.util.List;

import com.dmall.dao.CartDao;
import com.dmall.entity.Cart;
import com.dmall.pagedata.PageData;

/**
 * 购物车Service
 * 
 * @author yong
 *
 */
public class CartService {

	// 调用dao
	CartDao cartdao = new CartDao();

	/**
	 * 查询业务
	 * 
	 * @param keywords 商品名称关键字
	 * @return list集合
	 */
	public List<Cart> getCart(String keywords) {
		return cartdao.getCartByName(keywords);
	}

	/**
	 * 增加操作
	 * 
	 * @param cart 购物车对象
	 * @return true 成功 false 失败
	 */
	public boolean addCart(Cart cart) {
		boolean flag = false;
		// 业务逻辑判断
		// 如果购物车存在该商品，直接更新即可
		//根据
		Cart result = cartdao.getCartByCidAndPid(cart.getC_id(), cart.getP_id());
		if (result == null) {
			// 不存在，直接添加操作
			flag = cartdao.addCart(cart);
		}else {
			cart.setCart_sn(result.getCart_sn());
			flag = cartdao.updCart(cart);
		}
		return flag;
	}

	/**
	 * 删除操作
	 * 
	 * @param cart_id 购物车编号
	 * @return true 成功 false 失败
	 */
	public boolean delCart(int cart_id) {
		return cartdao.delCartById(cart_id);
	}

	/**
	 * 分页业务层
	 * 
	 * @param keywords
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public PageData<Cart> getCartByPage(String keywords, int pageNo, int pageSize) {
		// 判断页码和页面记录数
		if (pageNo < 1) {
			pageNo = 1;
		}
		if (pageSize < 0) {
			pageNo = 10;
		}

		return cartdao.getCartByPage(keywords, pageNo, pageSize);
	}
}
