package com.dmall.service;

import java.util.List;
import com.dmall.dao.AddressDao;
import com.dmall.entity.Address;
import com.dmall.pagedata.PageData;
/**
 * 用户地址service
 * @author yong
 *
 */
public class AddressService {

	//调用dao
	AddressDao ad = new AddressDao();
	
	/**
	 * 查询操作
	 * @param keywords 地址关键字
	 * @return List集合
	 */
	public List<Address> getAddress(String keywords){
		return ad.getAddressByName(keywords);	 
	}
	
	/**
	 * 增加操作
	 * @param address 
	 * @return
	 */
	public boolean addAddress(Address address) {
		return ad.addAddress(address);
	}
	
	/**
	 *	删除操作
	 * @param a_sn 地址流水号
	 * @return
	 */
	public boolean delAddress(int a_sn) {
		return ad.delAddressById(a_sn);
	}
	
	/**
	 * 修改操作
	 * @param address
	 * @return
	 */
	public boolean updateAddress(Address address) {
		return ad.updAddressById(address);
	}
	
	/**
	 * Address 业务层
	 * @param keywords 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页码数
	 * @return 
	 */
	public PageData<Address> getAddressByPage(String keywords,int pageNo,int pageSize){
		//判断页码和页面记录数
		if (pageNo < 1) {
			pageNo=1;
		}
		if (pageSize < 0) {
			pageNo=10;
		}
		
		return ad.getAddressByPage(keywords, pageNo, pageSize);
	}
}
