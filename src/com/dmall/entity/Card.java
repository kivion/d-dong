package com.dmall.entity;

public class Card {
	private String cardname;
	private String cardid;
	private String cardaddress;
	private String bank;
	public String getCardname() {
		return cardname;
	}
	public void setCardname(String cardname) {
		this.cardname = cardname;
	}
	public String getCardid() {
		return cardid;
	}
	public void setCardid(String cardid) {
		this.cardid = cardid;
	}
	public String getCardaddress() {
		return cardaddress;
	}
	public void setCardaddress(String cardaddress) {
		this.cardaddress = cardaddress;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public Card(String cardname, String cardid, String cardaddress, String bank) {
		super();
		this.cardname = cardname;
		this.cardid = cardid;
		this.cardaddress = cardaddress;
		this.bank = bank;
	}
	public Card() {
		super();
	}
	@Override 
	public String toString() {
		return "Card [cardname=" + cardname + ", cardid=" + cardid + ", cardaddress=" + cardaddress + ", bank=" + bank
				+ "]";
	}
	

	
}
