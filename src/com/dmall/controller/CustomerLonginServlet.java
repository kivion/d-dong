package com.dmall.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dmall.entity.Customer;
import com.dmall.entity.Menue;
import com.dmall.entity.MenueCollection;
import com.dmall.service.CustomerService;
import com.dmall.service.MenuCollectionService;

/**
 * Servlet implementation class SerConBlog
 */
@WebServlet("/Cuspanda.do")
public class CustomerLonginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CustomerService cs = new CustomerService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerLonginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if("login".equals(op)) {
			doLogin(request,response);
		}else if("cusadd".equals(op)) {
			doCusAdd(request,response);
		}else {
			//doCusForget(request,response);
		}
		 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

/**
 * 登入
 * @param request
 * @param response
 * @throws ServletException
 * @throws IOException
 */
	protected void doLogin(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cusname=request.getParameter("cusname");
		String cuspwd=request.getParameter("cuspwd");
		String rem=request.getParameter("rem");
		Customer customer=cs.login(cusname, cuspwd);
		if(customer==null) {
			response.sendRedirect("./front/CusLogin.jsp");
		}else {
			HttpSession session=request.getSession();
			session.setAttribute("customer", customer);
		
			if(rem!=null) {
				Cookie cookie1=new Cookie("cusname",customer.getC_account());
			    Cookie cookie2=new Cookie("cuspwd",customer.getC_pwd());
		
			cookie1.setMaxAge(60*60*24*7);
			cookie2.setMaxAge(60*60*24*7);
			response.addCookie(cookie1);
			response.addCookie(cookie2);
			}
		
			request.getRequestDispatcher("./front/index.jsp").forward(request, response);
		}
	}
	/**
	 * 注册
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
		protected void doCusAdd(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			String custell="";
			String cuspwd="";
			String cid="c"+(int)(Math.random()*1000000000);
			int stste=1;
			if(request.getParameter("custell")!=null) {
				custell=request.getParameter("custell");
			}
			if(request.getParameter("cuspwd")!=null) {
				cuspwd=request.getParameter("cuspwd");
			}
			Customer customer=new Customer(cid,custell,custell,cuspwd,stste);
			boolean flag=cs.AddCusBytell(customer);
			if(flag) {
				System.out.println("成功");
				request.getRequestDispatcher("./front/CusLogin.jsp").forward(request, response);
			}else {
				System.out.println("失败");
			}
		}
		/**
		 * 找回密码
		 * @param request
		 * @param response
		 * @throws ServletException
		 * @throws IOException
		 */
			protected void doCusForget(HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {
				String oop="";
				String custell="";
				String checkcode="";
				if(request.getParameter("oop")!=null) {
					oop=request.getParameter("oop");
				}
				if(request.getParameter("custell")!=null) {
					custell=request.getParameter("custell");
				}
				if(request.getParameter("checkcode")!=null) {
					checkcode=request.getParameter("checkcode");
				}
				System.out.println("oop"+oop);
				System.out.println("custell"+custell);
				System.out.println("checkcode"+checkcode);
			}
}
