package com.dmall.service;

import java.util.List;

import com.dmall.dao.MenueTypeNextDao;
import com.dmall.entity.MenueTypeNext;
import com.dmall.pagedata.PageData;

public class MenuTypeNextService {
	MenueTypeNextDao mrnd = new MenueTypeNextDao();
	/**
	 *	查询某个一级菜谱分类下的所有二级菜谱分类信息，针对后台
	 * @param keyword 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return PagData分页对象
	 */
	public PageData<MenueTypeNext> getMenuTypeFrst(String keyword,int pageNo,int pageSize){
		PageData pd = mrnd.getMenuTypeFrst(keyword, pageNo, pageSize);
		return pd;
	}
	/**
	 *	查询某个一级菜谱分类下的所有二级菜谱分类信息，针对前台
	 * @param keyword
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<MenueTypeNext> getMenuTypeFrst(){
		List<MenueTypeNext> list = mrnd.getMenuTypeFrst();
		return list;
	}
	
	/**
	 *	增加二级菜谱分类
	 * @param mtf
	 * @return
	 */
	public boolean addMenuTypeNext(MenueTypeNext mtn) {
		boolean flag = mrnd.addMenuTypeNext(mtn);
		return flag;
	}
	/**
	 *	按二级菜谱分类的id删除
	 * @param mtf_id
	 * @return
	 */
	public boolean delMenuTypeNext(String mtn_id) {
		boolean flag = mrnd.delMenuTypeNext(mtn_id);
		return flag;
	}
	/**
	 *	根据二级菜谱分类的id修改分类学信息
	 * @param mtf_id
	 * @param mtf_name
	 * @return
	 */
	public boolean updMenuTypeNext(String mtn_id,String mtn_name) {
		boolean flag = mrnd.updMenuTypeNext(mtn_id, mtn_name);
		return flag;
	}
}
