package com.dmall.dao;

import java.util.List;
import com.dmall.entity.Coupon;
import com.dmall.pagedata.PageData;
import com.dmall.util.DBUtil;
/**
 * 优惠券dao操作
 * @author yong
 *
 */
public class CouponDao extends DBUtil {

	/**
	 * 新增优惠券方法
	 * @param coupon 优惠券对象
	 * @return true 成功 false 失败
	 */
	public boolean addCoupon(Coupon coupon) {
		// 判断coupon是否为空
		if (coupon == null) {
			return false;
		}
		// 定义sql语句
		String sql = "INSERT INTO `coupon` (`co_id`, `co_name`,`co_num`,`co_start`,`co_end`,`co_state`) VALUES (?,?,?,?,?,?)";
		// 调用通用类代码
		boolean flag = exeUpdate(sql, coupon.getCo_id(),coupon.getCo_name(),coupon.getCo_num(),coupon.getCo_start(),coupon.getCo_end(),coupon.getCo_state());
		return flag;
	}
	
	/**
	 * 根据优惠券编号删除优惠券的方法
	 * @param co_id 优惠券编号
	 * @return true 成功 false 失败
	 */
	public boolean delCouponById(int co_id) {
		// 定义sql语句
		String sql = "delete FROM `coupon` WHERE (`co_id`=?)";
		// 调用DBUtil方法
		boolean flag = exeUpdate(sql, co_id);
		return flag;
	}
	
	/**
	 * 优惠券分页+根据优惠券名称模糊查询
	 * @param keywords 检索关键字
	 * @param pageNo 页码
	 * @param pageSize 当前页的条数
	 * @return pd PageData分页对象
	 */
	public PageData<Coupon> getCouponByPage(String keywords, int pageNo, int pageSize) {
		// 声明sql语句
		String sql = "select * from `coupon` where co_name like ? ";
		// 调用DBUtil方法
		PageData<Coupon> pd = exeQueryByPage(sql, Coupon.class, pageNo, pageSize, "%" + keywords + "%");

		return pd;
	}
	
	/**
	 * 根据优惠券名称模糊检索
	 * @param keywords 关键字
	 * @return List集合
	 */
	public List<Coupon> getCouponByName(String keywords) {
		// 声明sql语句
		String sql = "SELECT * from coupon where a_name like ?";
		List<Coupon> list = exeQuery(sql, Coupon.class, "%" + keywords + "%");

		return list;
	}
}
