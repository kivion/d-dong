<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
<head>
<meta charset="UTF-8">
<title>个人资料</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<script src="./lib/layui/layui.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>

</head>
<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="">个人资料</a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i></a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body layui-table-body layui-table-main">
						<table class="layui-table layui-form">
							<thead>
								<tr>
									<th>姓名</th>
									<th>性别</th>
									<th>年龄</th>
									<th>手机</th>
									<th>地址</th>
									<th>邮箱</th>
									<th>身份证</th>
									<th>健康状况</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>张三</td>
									<td>男</td>
									<td>25</td>
									<td>13969781232</td>
									<td>厦门市湖里区万达广场</td>
									<td>1305598855@qq.com</td>
									<td>35024199506028888</td>
									<td>良好</td>
								</tr>
							</tbody>
							<td class="td-manage"><a title="编辑"
								onclick="xadmin.open('编辑','member-edit.html',600,400)"
								href="javascript:;"> <i class="layui-icon">&#xe642;</i>
							</a> <a title="删除" onclick="member_del(this,'要删除的id')"
								href="javascript:;"> <i class="layui-icon">&#xe640;</i>
							</a></td>
							</div>
							</div>
							</div>
							</div>
							</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		var form = layui.form;

		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});

		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});

	});

	/*用户-删除*/
	function member_del(obj, id) {
		layer.confirm('确认要删除吗？', function(index) {
			//发异步删除数据
			$(obj).parents("tr").remove();
			layer.msg('已删除!', {
				icon : 1,
				time : 1000
			});
		});
	}

	function delAll(argument) {
		var ids = [];

		// 获取选中的id 
		$('tbody input').each(function(index, el) {
			if ($(this).prop('checked')) {
				ids.push($(this).val())
			}
		});

		layer.confirm('确认要删除吗？' + ids.toString(), function(index) {
			//捉到所有被选中的，发异步进行删除
			layer.msg('删除成功', {
				icon : 1
			});
			$(".layui-form-checked").not('.header').parents('tr').remove();
		});
	}
</script>
</html>