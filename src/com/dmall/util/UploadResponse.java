package com.dmall.util;

public class UploadResponse {
	private int code = 0;
	private String msg = "";
	private String data;
	public UploadResponse(int code, String msg, String data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	public UploadResponse() {
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "AjaxResponse [code=" + code + ", msg=" + msg + ", data=" + data + "]";
	}
}
