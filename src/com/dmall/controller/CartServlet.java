package com.dmall.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;

import com.dmall.entity.Cart;
import com.dmall.entity.Customer;
import com.dmall.pagedata.PageData;
import com.dmall.service.CartService;
import com.dmall.service.ProductService;
import com.dmall.util.AjaxResponse;
import com.dmall.util.UploadResponse;
import com.google.gson.Gson;

/**
 * Servlet implementation class AjaxAddressServlet
 */
@WebServlet("/cart.do")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	// 调用Service
	private CartService cs = new CartService();
	private ProductService ps = new ProductService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CartServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String op = "";
		System.out.println("add");
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if ("add".equals(op)) {
			// 增加操作
			doAdd(request, response);
		} else {
			// 查询
			doQuery(request, response);
		}

	}

	protected void doAdd(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// MIME,返回类型就是json => application/json
		response.setContentType("application/json;charset=utf-8");
		System.out.println("add1");
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		// 后期加入登录判断
		String cart_id = "ca00001";
//		if (request.getParameter("cart_id") != null) {
//			cart_id = request.getParameter("cart_id");
//		}

		String c_id = "";
//		if (request.getParameter("c_id") != null) {
//			c_id = request.getParameter("c_id");
//		}

		String p_id = "";
		if (request.getParameter("p_id") != null) {
			p_id = request.getParameter("p_id");
		}

		Customer customer = (Customer) request.getSession().getAttribute("customer");
		c_id = customer.getC_id();

//		if (request.getParameter("p_name") != null) {
//			p_name = request.getParameter("p_name");
//		}
		String p_name = ps.getProductByProductId(p_id).getP_name();

//		int cart_sn = Integer.parseInt(request.getParameter("cart_sn"));
		int cart_sn = 11;

		int cart_num = Integer.parseInt(request.getParameter("cart_num"));
		double p_price = ps.getProductByProductId(p_id).getP_price();
//		int p_price = Integer.parseInt(request.getParameter("p_price"));
//		double c_total = Double.parseDouble(request.getParameter("c_total"));
		double c_total = cart_num * p_price;
		// 购物车对象
		Cart cart = new Cart(cart_sn, cart_id, p_id, cart_num, p_name, c_id, c_total);

		System.out.println("cart1: " + cart);

		boolean flag = cs.addCart(cart);

		UploadResponse ur = new UploadResponse();
		ur.setCode(0);
		if (flag) {
			ur.setMsg("操作成功");
		} else {
			ur.setMsg("操作失败");
		}
		Gson gson = new Gson();
		String str = gson.toJson(ur);

		System.out.println("str: " + str);

		out.print(str);

		out.close();

	}

	/**
	 * 查询操作
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doQuery(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		// 接收表单提交的数据，获取name="keywords"的值,
		String keywords = "";
		if (request.getParameter("keywords") != null) {
			keywords = request.getParameter("keywords");
		}

		int pageNo = 1;
		int pageSize = 10;
		if (request.getParameter("page") != null) {
			pageNo = Integer.parseInt(request.getParameter("page"));
		}

		if (request.getParameter("limit") != null) {
			pageSize = Integer.parseInt(request.getParameter("limit"));
		}

		System.out.println("keywords :" + keywords);

		// keywords就是传递的查询的关键字

		PageData<Cart> pd = cs.getCartByPage(keywords, pageNo, pageSize);

		// pd.getTotalCount(), 总记录数

		// pd.getList() 页面的数据
		AjaxResponse<Cart> ar = new AjaxResponse<Cart>(0, "success", pd.getTotalCount(), pd.getList());

		Gson gson = new Gson();
		String str = gson.toJson(ar);

		System.out.println("str: " + str);

		out.print(str);
		

		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
