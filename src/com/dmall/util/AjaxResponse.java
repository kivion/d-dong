/**
* <p>Title: AjaxResponse.java</p>
* <p>Description: </p>
* <p>Copyright: Copyright (c) 2021</p>
* <p>Company: www.chinasofti.com</p>
* @author kevin
* version 1.0
*/
package com.dmall.util;

import java.util.List;

/**
 * 
 * Title: AjaxResponse
 * Description: 自定义分页数据响应，和layui的table结合使用(x-admin)
 * @author kevin
 * @param <T>
 * @date 2021年1月12日
 */
public class AjaxResponse<T> {
	private int code = 0;
	private String msg = "";
	private int count = 0;
	private List<T> data;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	public AjaxResponse(int code, String msg, int count, List<T> data) {
		super();
		this.code = code;
		this.msg = msg;
		this.count = count;
		this.data = data;
	}
	public AjaxResponse(int code, String msg,List<T> data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	public AjaxResponse() {
	}
	@Override
	public String toString() {
		
		return "AjaxResponse [code=" + code + ", msg=" + msg + ", count=" + count + ", data=" + data + "]";
	}
}
